# Les scripts Crans

Ce dépôt contient les scripts à destination des membres actifs.

## Historique

Les anciens scripts sont dans `Archives/` si vous voulez vous inspirer pour
écrire un nouveau script.

Si possible, il est plus judicieux de déployer les scripts utilisés dans les
crons / services par Ansible.

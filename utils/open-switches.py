#!/usr/bin/env python3

import argparse
import json
import subprocess
import time


def command(cmd):
    return subprocess.run(['tmux']+cmd.split(' '), capture_output=True).stdout.decode('utf-8')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="SSH on list of machines")
    parser.add_argument('--config', type=argparse.FileType('r'), help="JSON configuration file", default='switches.json')
    parser.add_argument('-c', '--columns', type=int, help="Number of columns in tmux", default=4)
    parser.add_argument('-w', '--width', type=int, help="Width of big pane", default=80)
    parser.add_argument('-s', '--ssh', type=str, help="Run specific SSH command", default='ssh')
    parser.add_argument('-n', '--name', type=str, help="Name of the tmux window", default='switches')
    parser.add_argument('--sleep', type=float, help="Wait between pane creations", default=0)
    args = parser.parse_args()
    with args.config as file:
        config = json.load(file)
    machines = config
    command(f'new-window -n {args.name} {args.ssh} {machines[0]}')
    machines = machines[1:]
    width = int(command(f'display -p -t {args.name}.0 #{{pane_width}}'))
    command(f'split-window -h -l {width-args.width} -t {args.name}.0 {args.ssh} {machines[0]}')
    c = args.columns
    l = len(machines) // c + 1
    for i in range(1, len(machines)):
        if i < c :
            command(f'split-window -h -t {args.name}.{i} -p {100-100//(c-i+1)} {args.ssh} {machines[i]}')
            if args.sleep:
                time.sleep(args.sleep)
        else:
            x = i - c
            a, b = x % c, x // c
            id = (a + 1) * (b + 2) - 2
            command(f'split-window -v -t {args.name}.{id+1} -p {100-(100//(l-(i//c)+1))} {args.ssh} {machines[i]}')
            if args.sleep:
                time.sleep(args.sleep)

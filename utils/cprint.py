#!/bin/python3
# -*- mode: python; coding: utf-8 -*-

def coul(txt, col=None):
    """
    Retourne la chaine donnée encadrée des séquences qui
    vont bien pour obtenir la couleur souhaitée
    Les couleur sont celles de codecol
    Il est possible de changer la couleur de fond grace aux couleur f_<couleur>
    """
    if not col:
        return txt

    codecol = {'rouge': 31,
               'vert': 32,
               'jaune': 33,
               'bleu': 34,
               'violet': 35,
               'cyan': 36,
               'gris': 30,
               'gras': 50}
    codecol_dialog = {'rouge': 1,
                      'vert':  2,
                      'jaune': 3,
                      'bleu':  4,
                      'violet': 5,
                      'cyan': 6,
                      'gris': 0,
                      'gras': 'b'}
    try:
        if col[:2] == 'f_':
            add = 10
            col = col[2:]
        else:
            add = 0
        txt = "\033[1;%sm%s\033[1;0m" % (codecol[col] + add, txt)
    finally:
        return txt

def cprint(txt, col='blanc'):
    print(coul(txt, col))

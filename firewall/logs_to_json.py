#!/usr/bin/python3

import sys
import json

if __name__ == '__main__':
	while True:
		try:
			line = sys.stdin.readline()
            if not line:
                sys.stdout.flush()
                continue
			line = json.loads(line)
			if 'message' not in line:
				print(line)
				sys.stdout.flush()
				continue
			msg = line['message'].strip()
			params = msg.split(" ")[2:]
			d = {}
			for param in params:
				if '=' in param:
					k,v = param.split("=",1)
					if k == "MAC":
						blobs = v.split(":")
						mac_dst = ":".join(blobs[:6])
						mac_src = ":".join(blobs[6:12])
						mac_type = ":".join(blobs[12:])
						d["MACDST"] = mac_dst
						d["MACSRC"] = mac_src
						d["MACTYPE"] = mac_type
					else :
						d[k] = v
				else:
					d[param] = None
			d = { k.lower(): v.lower() if isinstance(v,str) else v for k,v in d.items() }
			line['message'] = d
			line = json.dumps(line)
			print(line)
			sys.stdout.flush()
		except Exception as e:
			print(e)
			sys.stdout.flush()

#!/bin/bash
#
# Script de sauvegarde régulière des logs firewall vers le nfs
#
# Reste silencieux en cas de succès si la variable d'env $QUIET est non vide

umask 0177

logs_src=/var/log/firewall
logs_dst="/logs/$(hostname)"

if [ ! -d "$logs_dst" ]; then
    echo "Destination des logs inexistante"
    exit 42
fi

# tant qu'il existe un fichier, le sauvegarder (<!>)
while ( ls -tr "$logs_src" | grep -E -q 'logall\.log\.[0-9]+\..z2?' ); do
dernier_fichier=$logs_src/$(ls -tr "$logs_src" | grep -E 'logall\.log\.[0-9]+\..z2?' | head -1)

date=$(bzcat "$dernier_fichier"  | head -1 | awk -F 'T' '{print $1}')
if [ -z "$QUIET" ]; then
	echo install -o root -g root -m 400 "$dernier_fichier" "$logs_dst/logall.log.$date.bz2"
	echo rm "$dernier_fichier"
fi;
install -o root -g root -m 400 "$dernier_fichier" "$logs_dst/logall.log.$date.bz2"
rm "$dernier_fichier"
done
# Fin de boucle </!>
find "$logs_dst" -mtime +365 -delete

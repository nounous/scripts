#!/usr/bin/python3
# -*- mode: python; coding: utf-8 -*-

"""
Script générique pour envoyer des mails en masse au crans
"""

import os
import sys
import argparse
import json
from enum import Enum

if '/usr/local/src/scripts' not in sys.path:
    sys.path.append('/usr/local/src/scripts')
from utils.cprint import cprint
import mail


def mail_sender(template, From, recipients, SEND=False, cc=None, bcc=None):
    """
    ``template`` template du mail à envoyer.
    ``From`` Pour remplir le champ From du mail.
    ``recipients`` Liste des addresses mails recipiendaires.
    ``PREV`` Booléen specifiant s'il faut faire un dry-run avec prévisualisation.
             Default = True.
    ``SEND`` Booléen spécifiant s'il faut effectivement envoyer le mail.
             Default = False.
    ``cc`` Liste des addresses mails en copie.
    ``bcc`` Liste des addresses mails en copie cachée.
    """
    echecs = []
    with mail.ServerConnection() as conn_smtp:
        for line in recipients:
            Prenom, Nom, To, extra = line.split(';')
            extra_params = json.loads(extra)
            print("Envoi du mail à {}".format(To))
            params = {
                'To' : To,
                'From' : From,
                'Prenom' : Prenom,
                'Nom': Nom,
            }
            params.update(extra_params)
            mailtxt = mail.generate(template, params).as_bytes()

            if SEND:
                try:
                    conn_smtp.sendmail(From, (To,), mailtxt)
                    print(" Envoyé !")
                except:
                    print(sys.exc_info()[:2])
                    cprint("Erreur lors de l'envoi à {} ".format(To), "rouge")
                    echecs.append(To)
            else:
                print(mailtxt)
                print(" Simulé !")
    if not SEND:
        cprint("\n\
/!\ Avant d'envoyer réellement ce mail all, as-tu vérifié que:\n\
        - Le texte a été lu et relu ?\n\
        - Il existe une version en anglais ?\n\
        - Les destinataires sont bien les bons ?\n\
        - Il y a bien une signature ?\n",
        'rouge'
        )
    else:
        if echecs:
            print("\nIl y a eu des erreurs pour les addresses suivantes :")
            for echec in echecs:
                print(" - {}\n".format(echec))
            sys.exit(1)

class Filtres(Enum):
    ADHERENT = 'adherent'
    ACCESS = 'access'
    CLUB = 'club'
    EVERYONE = 'everyone'
    def __str__(self):
         return self.value

if __name__=="__main__":
    parser = argparse.ArgumentParser(
        description="Mail all générique. Prend un template en argument.",
    )
    parser.add_argument("-t", "--template", type=str,
        help="Un template de mail. Fournir le chemin du dossier principal du mail",
    )
    parser.add_argument("-s", "--sender", type=str, default="respbats@crans.org",
        help="Spécifier un expéditeur particulier. Par défaut respbats@crans.org",
    )
    parser.add_argument("--doit", action="store_true",
        help="Lance effectivement le mail",
    )
    exclusive = parser.add_mutually_exclusive_group(required=True)
    exclusive.add_argument("-f", "--recipientfile", type=str,
        help="Un fichier contenant un destinataire par ligne au format `Prenom;Nom;Email;Params`, avec params au format json.",
    )
    exclusive.add_argument("--to", type=Filtres, choices=list(Filtres),
        help="Selectionne les destinataires du mails parmi un filtre prédéfini"
    )
    args = parser.parse_args()

    if args.recipientfile:
        tag=args.recipientfile
        with open(args.recipientfile, 'r') as recipientfile:
            recipients = recipientfile.readlines()
    else:
        import django
        RE2O = '/var/www/re2o'
        if not RE2O in sys.path:
            sys.path.append(RE2O)
        try:
            import re2o
        except ImportError:
            print("Nécessite une instance re2o")
            sys.exit(42)
        # Setup l'environnement django
        os.environ.setdefault('DJANGO_SETTINGS_MODULE', 're2o.settings')
        django.setup()

        if args.to == Filtres.ACCESS:
            tag = 'toutes les personnes bénéficiant d\'une connexion valide'
            users = re2o.utils.all_has_access()
        elif args.to == Filtres.ADHERENT:
            tag = 'tous les adhérents'
            users = [ user for user in re2o.utils.all_adherent() if not user.is_class_club ]
        elif args.to == Filtres.CLUB:
            tag = 'tous les clubs'
            users = [ user for user in re2o.utils.all_adherent() if user.is_class_club ]
        elif args.to == Filtres.EVERYONE:
            tag = 'tous les gens ayant déjà été adhérent'
            users = re2o.utils.all_account()
        else:
            print("Spécifier au moins un destinataire")
            sys.exit(2)
        recipients = [ f'{u.name};{u.surname};{u.get_mail};{{"pseudo":"{u.pseudo}"}}'
            for u in users]
        with open('recipients.re2o','w') as file:
            file.write('\n'.join(recipients))
    try:
        if args.doit:
            print(f'Envoi du mail à {tag} ({len(recipients)})')
            input(f'Envoyer ? (Ret pour envoyer, Ctrl + C pour annuler l\'envoi)')
        else:
            print(f'Simulation du mail à {tag} ({len(recipients)})')
            input(f'Simuler ? (Ret pour simuler, Ctrl + C pour annuler la simulation)')
    except KeyboardInterrupt:
        cprint(f'\n{"Envoi" if args.doit else "Simulation"} annulé.', "rouge")
        sys.exit(1)

    mail_sender(args.template, args.sender, recipients, args.doit)

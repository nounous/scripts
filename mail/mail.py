#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import jinja2
import sys
import json
import inspect
import locale
import smtplib
import traceback
from contextlib import contextmanager

from email.header import Header
from email.message import Message
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate, parseaddr, formataddr
try:
    import misaka
    def markdown(text):
        return misaka.html(text, misaka.EXT_TABLES)
except ImportError:
    from markdown import markdown
from locale_util import setlocale

if '/usr/scripts' not in sys.path:
    sys.path.append('/usr/scripts')

default_language = 'fr'
local_path = os.path.dirname(os.path.abspath(__file__))
template_path = os.path.join(local_path, 'template')
html_template = os.path.join(template_path, 'html')
text_template = os.path.join(template_path, 'text')

templateLoader = jinja2.FileSystemLoader( searchpath=["/", template_path]  )
templateEnv = jinja2.Environment( loader=templateLoader )

def format_date(d):
    """ Renvoie une jolie représentation (unicode) d'un datetime"""
    # L'encoding dépend de ce qu'on a choisi plus bas
    lang, encoding = locale.getlocale()
    if not encoding:
        encoding = 'ascii'
    if lang == 'fr_FR':
        return d.strftime('%A %d %B %Y').decode(encoding)
    else:
        return d.strftime('%A, %B %d %Y').decode(encoding)

def given_name(adh):
    """Renvoie le joli nom d'un adhérent"""
    if adh.is_class_club:
        return "Club {}".format(adh.surname)
    return adh.name + " " + adh.surname

templateEnv.filters['date'] = format_date
templateEnv.filters['name'] = given_name


# file extension to rendering function map
markup = {
    '' : lambda x:x,
    '.md' : markdown,
    '.html' : lambda x:x,
}

lang_db = {
    'fr': { 'locale': 'fr_FR.utf8', 'index': 0, 'info': 'Version française ci-dessous' },
    'en': { 'locale': 'en_US.utf8', 'index': 1, 'info': 'English version below' },
}
### For an example:
### print (generate('bienvenue', {'From':'respbats@crans.org', 'To':'admin@genua.fr', 'lang_info':'English version below'}).as_bytes())
### or from a shell : python -c "import mail; print(mail.generate('bienvenue', {'From':'respbats@crans.org', 'To':'admin@genua.fr', 'lang_info':'English version below'}))"

def submessage(payload, type, charset='utf-8'):
    """Renvois un sous message à mettre dans un message multipart"""
    submsg = MIMEText('', type, charset)
    del(submsg['Content-Transfer-Encoding'])
    submsg.set_payload(payload)
    return submsg

def get_langs(mail, part):
    """Récupère le chemin vers le fichier à utiliser, en fonction de la
    langue souhaitée"""
    path = os.path.join(template_path,mail,part)
    ret = []
    for file in os.listdir(path):
        lang = os.path.splitext(file)[0]
        ext = os.path.splitext(file)[1]
        if ext in markup.keys() and lang in lang_db.keys():
            ret.append({'lang': lang, 'ext': ext, 'path': os.path.join(mail,part,file)})
    if len(ret) == 0:
        raise ValueError("Language not found")
    ret.sort(key=lambda x: lang_db[x['lang']]['index'])
    return ret

def gen_local_body(fname, params, lang):
    """Génère le texte localisé d'un body"""
    with setlocale(lang_db[lang]['locale']):
        return templateEnv.get_template(fname).render(params)

def body(mail, langs, params, charset):
    """Génère le texte du mail, en deux langues, avec une extension `mk` donnée"""
    ret = []
    langs = [ lang.copy() for lang in langs ]
    for lang in langs:
        lang.update({'txt':gen_local_body(lang['path'], params, lang['lang'])})
    txts = [ (lang['lang'],lang['txt']) for lang in langs if lang['ext'] != '.html' ]
    if len(txts) > 0:
        txt = templateEnv.get_template(text_template).render(params,txts=txts,lang_db=lang_db)
        ret.append(submessage(txt.encode(charset), 'plain', charset))
    htmls = [ (lang['lang'], markup[lang['ext']](lang['txt'])) for lang in langs if lang['ext'] != '' ]
    if len(htmls) > 0:
        html = templateEnv.get_template(html_template).render(params,htmls=htmls,lang_db=lang_db)
        ret.append(submessage(html.encode(charset), 'html', charset))
    return ret

def generate(mail, params, charset='utf-8'):
    """Génère un message multipart"""
    if 'mailer' not in params.keys():
        # Il y a vraiment des gens qui lisent ce champ ?
        params['mailer'] = "Un MA du crans a personalisé ce message à la main pour toi"

    msg = MIMEMultipart('mixed')
    inline_msg = MIMEMultipart('alternative')
    if os.path.isdir(os.path.join(template_path,mail)):
        for filename in os.listdir(os.path.join(template_path,mail)):
            if filename == 'body' and os.path.isdir(os.path.join(template_path,mail,filename)):
                langs = get_langs(mail, filename)
                for part in body(mail, langs, params, charset):
                    inline_msg.attach(part)
            else:
                if os.path.isfile(os.path.join(template_path,mail,filename)):
                    txt = templateEnv.get_template(os.path.join(mail,filename)).render(params)
                    if filename in ['From', 'To', 'Cc', 'Bcc']:
                        msg[filename] = format_sender(txt, charset)
                    else:
                        msg[filename] = Header(txt.encode(charset), charset)
        msg['Date'] = formatdate(localtime=True)
        msg.attach(inline_msg)

    return msg


def format_sender(sender, header_charset='utf-8'):
    """Check and format sender for header."""
    # Split real name (which is optional) and email address parts
    sender_name, sender_addr = parseaddr(sender)

    # We must always pass Unicode strings to Header, otherwise it will
    # use RFC 2047 encoding even on plain ASCII strings.
    sender_name = str(Header(str(sender_name), header_charset))

    # # Make sure email addresses do not contain non-ASCII characters
    # sender_addr = sender_addr.encode('ascii')

    return formataddr((sender_name, sender_addr))

@contextmanager
def bugreport():
    """Context manager: Si erreur, renvoie un bugreport avec un traceback à
    roots@."""
    try:
        yield
    except Exception as exc:
        From = 'roots@crans.org'
        to = From
        tb = sys.exc_info()[2].tb_next

        data = {
            'from': From,
            'to': to,
            'exc': exc,
            'lineno': tb.tb_frame.f_lineno,
            'filename': os.path.basename(tb.tb_frame.f_code.co_filename),
            'traceback': traceback.format_exc(),
        }
        mail = generate('bugreport', data)
        with ServerConnection() as conn:
            conn.sendmail(From, [to], mail.as_string())
        raise

class ServerConnection(object):
    """Connexion au serveur smtp"""
    _conn = None
    def __enter__(self):
        if os.getenv('DBG_MAIL') != 'print':
            self._conn = smtplib.SMTP('smtp.adm.crans.org')
        return self

    def check_sender(self, mail):
        """Vérifie l'expéditeur, pour éviter certaines erreurs"""
        if mail.split('@')[0] in ['ca', 'nounou']:
            raise Exception(u"Merci d'utiliser une autre adresse mail d'expédition, celle-ci est une ML publique sur laquelle des bounces seraient malvenus.")

    def sendmail(self, From, to, mail):
        """Envoie un mail"""
        self.check_sender(From)
        if os.getenv('DBG_MAIL', False):
            deb = os.getenv('DBG_MAIL')
            if '@' in deb:
                to = [deb]
            else:
                print(mail)
                return
        self._conn.sendmail(From, to, mail)

    def send_template(self, tpl_name, data):
        """Envoie un mail à partir d'un template.
        `data` est un dictionnaire contenant entre
        """
        From = data.get('from', '')
        adh = data.get('adh', data.get('proprio', ''))
        to = data.get('to', None) or (adh.get_mail() if adh else None)
        if to is None:
            print("Pas de mail valide pour %r. Skipping..." % (adh, ))
            return
        # TODO: get lang toussa
        body = generate(tpl_name, data).as_string()
        self.sendmail(From, to, body)


    def __exit__(self, type, value, traceback):
        if os.getenv('DBG_MAIL') != 'print':
            self._conn.quit()

# TODO: intégrer ceci dans le ServerConnection
def postconf(i):
    "Fixe la fréquence d'envoi maximale par client (en msg/min)"
    os.system("/usr/sbin/postconf -e smtpd_client_message_rate_limit=%s" % i)
    os.system("/etc/init.d/postfix reload")

#    opt = commands.getoutput("/usr/sbin/postconf smtpd_client_message_rate_limit")

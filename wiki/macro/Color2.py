"""
    MoinMoin - Color2 Macro

    @copyright: 2006 by Clif Kussmaul <clif@kussmaul.org>
                2008 by Clif Kussmaul, Dave Hein (MoinMoin:DaveHein)
                2011 by Clif Kussmaul, Dave Hein (MoinMoin:DaveHein), Gregor Mirai
    @license:   GNU GPL, see COPYING for details

    Usage: <<Color2(text,col=color,bcol=bgcolor,font=_font_)>>
           <<Color2(text,bcol=bgcolor)>>
           <<Color2(text,color)>>

    History:
    - 2017.05.17: [Moin 1.9.9] mitigate CSS injection attacks; by Dave Hein
                  Issue reported by Paul Wise; fix reviewed by Gregor Mirai
                  and Paul Wise.
    - 2011.02.23: [Moin 1.9] updated for Moin 1.9 by Gregor Mirai,
                  code simplified due to new parameter parsing and lots of other improvements in Moin code.
                  MiniPage functionality for the text parameter has been preserved.
    - 2008.01.25: [Moin 1.6] updated for Moin 1.6 by Dave Hein,
                  no functional changes.
    - 2006: [Moin 1.5] written by Clif Kussmaul
    - originally based on Color Macro
      Copyright (c) 2002 by Markus Gritsch <gritsch@iue.tuwien.ac.at>
"""

from MoinMoin import wikiutil
from MoinMoin.parser.text_moin_wiki import Parser as WikiParser

"""
    Parameters that macro accepts are:
        - text (the text to be colored)
        - col  (optional text color)
        - bcol (optional background text color)
        - font (optional font)

    Examples:

    <<Color2(Hello World!,col=red,bcol=blue,font=18px courier)>>
    <<Color2(Hello World!,bcol=blue)>>
    <<Color2(Hello World!,orange)>>
"""

# used to cache the string lists and regular expressions that are
# used by the _color2_is_valid_*() methods to validate CSS property values.
#
_color2_validations = {}

def _color2_initialize_validations(macro):
    """Initialize _color2_validations with lists and regular expressions.

    The Color2 macro uses the lists and regular expressions in the
    _color2_validations dictionary to validate CSS color and font property
    values in order to mitigate CSS injection leading to XSS attacks
    and defacement attacks.

    Args:
        none

    """

    import re

    # From https://www.w3.org/TR/css3-color/#colorunits
    #
    _color2_validations['css_basic_color_keywords'] = set([x.lower() for x in [
        "black", "silver", "gray", "white", "maroon", "red", "purple",
        "fuchsia", "green", "lime", "olive", "yellow", "navy", "blue",
        "teal", "aqua"]])
    _color2_validations['css_special_color_keywords'] = set([x.lower() for x in [
        "transparent", "currentColor" ]])
    _color2_validations['css_extended_color_keywords'] = set([x.lower() for x in [
        "aliceblue", "antiquewhite", "aqua", "aquamarine", "azure", "beige",
        "bisque", "black", "blanchedalmond", "blue", "blueviolet", "brown",
        "burlywood", "cadetblue", "chartreuse", "chocolate", "coral",
        "cornflowerblue", "cornsilk", "crimson", "cyan", "darkblue",
        "darkcyan", "darkgoldenrod", "darkgray", "darkgreen", "darkgrey",
        "darkkhaki", "darkmagenta", "darkolivegreen", "darkorange",
        "darkorchid", "darkred", "darksalmon", "darkseagreen",
        "darkslateblue", "darkslategray", "darkslategrey", "darkturquoise",
        "darkviolet", "deeppink", "deepskyblue", "dimgray", "dimgrey",
        "dodgerblue", "firebrick", "floralwhite", "forestgreen", "fuchsia",
        "gainsboro", "ghostwhite", "gold", "goldenrod", "gray", "green",
        "greenyellow", "grey", "honeydew", "hotpink", "indianred", "indigo",
        "ivory", "khaki", "lavender", "lavenderblush", "lawngreen",
        "lemonchiffon", "lightblue", "lightcoral", "lightcyan",
        "lightgoldenrodyellow", "lightgray", "lightgreen", "lightgrey",
        "lightpink", "lightsalmon", "lightseagreen", "lightskyblue",
        "lightslategray", "lightslategrey", "lightsteelblue", "lightyellow",
        "lime", "limegreen", "linen", "magenta", "maroon", "mediumaquamarine",
        "mediumblue", "mediumorchid", "mediumpurple", "mediumseagreen",
        "mediumslateblue", "mediumspringgreen", "mediumturquoise",
        "mediumvioletred", "midnightblue", "mintcream", "mistyrose",
        "moccasin", "navajowhite", "navy", "oldlace", "olive", "olivedrab",
        "orange", "orangered", "orchid", "palegoldenrod", "palegreen",
        "paleturquoise", "palevioletred", "papayawhip", "peachpuff", "peru",
        "pink", "plum", "powderblue", "purple", "red", "rosybrown",
        "royalblue", "saddlebrown", "salmon", "sandybrown", "seagreen",
        "seashell", "sienna", "silver", "skyblue", "slateblue", "slategray",
        "slategrey", "snow", "springgreen", "steelblue", "tan", "teal",
        "thistle", "tomato", "turquoise", "violet", "wheat", "white",
        "whitesmoke", "yellow", "yellowgreen"]])
    _color2_validations['css_system_color_keywords'] = set([x.lower() for x in [
        "ActiveBorder", "ActiveCaption", "AppWorkspace", "Background",
        "ButtonFace", "ButtonHighlight", "ButtonShadow", "ButtonText",
        "CaptionText", "GrayText", "Highlight", "HighlightText",
        "InactiveBorder", "InactiveCaption", "InactiveCaptionText",
        "InfoBackground", "InfoText", "Menu", "MenuText", "Scrollbar",
        "ThreeDDarkShadow", "ThreeDFace", "ThreeDHighlight",
        "ThreeDLightShadow", "ThreeDShadow", "Window", "WindowFrame",
        "WindowText"]])

    _color2_validations['re_rgb_3'] = re.compile("^#[0-9a-fA-F]{3}$")

    _color2_validations['re_rgb_6'] = re.compile("^#[0-9a-fA-F]{6}$")

    res_int_or_pct = "\\s*-?[0-9]{1,3}(?:(?:\\.[0-9]*)?%)?"
    res_rgb = "^rgb\\(%(a0)s\\s*,%(a0)s\\s*,%(a0)s\\s*\\)$" \
        % { 'a0': res_int_or_pct }
    _color2_validations['re_rgb'] = re.compile(res_rgb)

    res_float = "\\s*(?:0?.)?[0-9]+"
    res_rgba = "^rgba\\(%(a0)s\\s*,%(a0)s\\s*,%(a0)s\\s*,%(a1)s\\s*\\)$" \
        % { 'a0': res_int_or_pct, 'a1': res_float }
    _color2_validations['re_rgba'] = re.compile(res_rgba)

    res_int = "\\s*[0-9]{1,3}"
    res_hue = "\\s*[0-9]{1,3}(?:.[0-9]*)?"
    res_pct = "\\s*[0-9]{1,3}(?:.[0-9]*)?%"
    res_hsl = "^hsl\\(%(a0)s\\s*,%(a1)s\\s*,%(a1)s\\s*\\)$" \
        % { 'a0': res_hue, 'a1': res_pct }
    _color2_validations['re_hsl'] = re.compile(res_hsl)

    res_hsla = "^hsla\\(%(a0)s\\s*,%(a1)s\\s*,%(a1)s\\s*,%(a2)s\\s*\\)$" \
        % { 'a0': res_hue, 'a1': res_pct, 'a2': res_float }
    _color2_validations['re_hsla'] = re.compile(res_hsla)

    # From https://www.w3.org/TR/2013/CR-css-fonts-3-20131003/#propdef-font
    #
    _color2_validations['css_system_fonts'] = set([x.lower() for x in [
        "caption", "icon", "menu", "message-box", "small-caption",
        "status-bar"]])
    _color2_validations['css_font_style'] = set([x.lower() for x in [
        "normal", "italic", "oblique" ]])
    _color2_validations['css_font_variant_css21'] = set([x.lower() for x in [
        "normal", "small-caps" ]])
    _color2_validations['css_font_weight'] = set([x.lower() for x in [
        "normal", "bold", "bolder", "lighter", "100", "200", "300", "400",
        "500", "600", "700", "800", "900"]])
    _color2_validations['css_font_stretch'] = set([x.lower() for x in [
        "normal", "ultra-condensed", "extra-condensed", "condensed",
        "semi-condensed", "semi-expanded", "expanded", "extra-expanded",
        "ultra-expanded"]])
    _color2_validations['css_font_absolute_size'] = set([x.lower() for x in [
        "xx-small", "x-small", "small", "medium", "large", "x-large",
        "xx-large" ]])
    _color2_validations['css_font_relative_size'] = set([x.lower() for x in [
        "larger", "smaller" ]])
    _color2_validations['css_line_height'] = set([x.lower() for x in [
        "normal", "inherit" ]])
    _color2_validations['re_length'] = re.compile(
        "^[0-9]+(?:.[0-9]*)?[a-zA-Z]{0,4}$")
    _color2_validations['re_length2'] = re.compile(
        "^[0-9]*(?:.[0-9]+)[a-zA-Z]{0,4}$")
    _color2_validations['re_length_pct'] = re.compile("^[0-9]+%$")
    # Expect that font family names do not start with number or decimal point;
    # required to distinguish lengths from names
    #
    _color2_validations['re_font_family'] = re.compile("^[^0-9.;\\\"',-][^;\\\"',]*,?$")
    _color2_validations['re_font_family_quoted'] = re.compile(
        "^\\\"[^;\\\"']+\\\",?$")
    _color2_validations['re_font_family_squoted'] = re.compile(
        "^'[^;\\\"']+',?$")

    return

def _color2_is_valid_color(macro, col=None):
    """Whether 'col' is a valid CSS color property value.

    The Color2 macro checks the 'col' and 'bcol' arguments to ensure
    a valid color value is specified. This is done to mitigate CSS
    injection leading to XSS attacks and defacement attacks.

    Args:
        macro: the macro argument passed to Color2.
        col: the CSS property value to be validated.

    Returns:
        True if the 'col' argument is a valid CSS color property value;
        otherwise, False.

    """

    import re

    # Initialize the validations dictionary if it is empty
    #
    if 0 == len(_color2_validations):
        _color2_initialize_validations(macro);

    # Validate
    #
    if col is None:
        return False

    t_col = col.lower()
    if t_col in _color2_validations['css_basic_color_keywords']:
        return True
    if t_col in _color2_validations['css_special_color_keywords']:
        return True
    if t_col in _color2_validations['css_extended_color_keywords']:
        return True
    if t_col in _color2_validations['css_system_color_keywords']:
        return True
    if _color2_validations['re_rgb_3'].match(col) is not None:
        return True
    if _color2_validations['re_rgb_6'].match(col) is not None:
        return True
    if _color2_validations['re_rgb'].match(col) is not None:
        return True
    if _color2_validations['re_rgba'].match(col) is not None:
        return True
    if _color2_validations['re_hsl'].match(col) is not None:
        return True
    if _color2_validations['re_hsla'].match(col) is not None:
        return True

    # Did not match any expected color specification string
    #
    return False

def _color2_is_valid_font(macro, font=None):
    """Whether 'font' is a valid CSS font property value.

    The Color2 macro checks the 'font' argument to ensure a valid
    CSS font property value is specified. This is done to mitigate CSS
    injection leading to XSS attacks and defacement attacks.

    This validation is approximate. It should filter out injection attacks
    and wildly improper CSS, but doesn't validate font family names and
    token ordering or length units (i.e., it is not a strict validation).

    Args:
        macro: the macro argument passed to Color2.
        font: the CSS property value to be validated.

    Returns:
        True if the 'font' argument is an approximately valid CSS font
        property value; otherwise, False.

    """

    import re

    # Initialize the validations dictionary if it is empty
    #
    if 0 == len(_color2_validations):
        _color2_initialize_validations(macro);

    # Validate
    #

    if font is None:
        return False

    t_font = font.lower()
    if t_font in _color2_validations['css_system_fonts']:
        return True

    # split property value into tokens, recombining
    # quoted string tokens
    #
    parts_naive = font.split()
    parts = []
    part_accum = ""
    pfx = ""
    for part in parts_naive:
        if 0 == len(part_accum) and not part.startswith(("\"", "'")):
            parts.append(part)
            continue
        if 0 == len(part_accum):
            pfx = part[0]
            if part.endswith((pfx, pfx + ",")):
                parts.append(part)
                pfx = ""
            else:
                part_accum += part
            continue
        if not part.endswith((pfx, pfx + ",")):
            part_accum += part
            continue
        part_accum += part
        parts.append(part_accum)
        part_accum = ""
        pfx = ""
    if 0 != len(part_accum):
        if not part_accum.endswith(pfx):
            # Unmatched quote
            #
            return False;
        parts.append(part_accum)
        part_accum = ""
        pfx = ""

    # Validate each of the tokens
    #
    for part in parts:
        t_part = part.lower()
        if t_part in _color2_validations['css_font_style']:
            continue
        if t_part in _color2_validations['css_font_variant_css21']:
            continue
        if t_part in _color2_validations['css_font_weight']:
            continue
        if t_part in _color2_validations['css_font_stretch']:
            continue
        if t_part in _color2_validations['css_font_absolute_size']:
            continue
        if t_part in _color2_validations['css_font_relative_size']:
            continue
        if not part.startswith("\"") and "/" in part:
            subparts = part.split('/')
            if 2 != len(subparts):
                return False
            if subparts[0] not in _color2_validations['css_font_absolute_size'] \
            and subparts[0] not in _color2_validations['css_font_relative_size'] \
            and _color2_validations['re_length'].match(subparts[0]) is None \
            and _color2_validations['re_length2'].match(subparts[0]) is None \
            and _color2_validations['re_length_pct'].match(subparts[0]) is None:
                return False
            if _color2_validations['re_length'].match(subparts[1]) is None \
            and _color2_validations['re_length2'].match(subparts[1]) is None \
            and _color2_validations['re_length_pct'].match(subparts[1]) is None:
                return False
            continue
        if _color2_validations['re_length'].match(part) is not None:
            continue;
        if _color2_validations['re_length2'].match(part) is not None:
            continue;
        if _color2_validations['re_length_pct'].match(part) is not None:
            continue;
        if _color2_validations['re_font_family'].match(part) is not None:
            continue;
        if _color2_validations['re_font_family_quoted'].match(part) is not None:
            continue;
        if _color2_validations['re_font_family_squoted'].match(part) is not None:
            continue;

        # Did not match any CSS 'font' token
        #
        return False

    # All tokens match expectations for CSS font property
    #
    return True

def macro_Color2(macro, text=None, col=None, bcol=None, font=None):
    """Wrap text in a span element with inline color and font styles.

    Note that CSS requires that a shorthand font property value include
    both a font-size value and a font-family value. The other font style
    properties are optional.

    Security considerations: text argument value is escaped to prevent
    various injection attacks. Each style argument is validated to avoid
    CSS injection attackes; if the style argument fails validation then
    it is ignored (not included in the result string).

    Args:
        text: the text string to be styled.
        col: the CSS color property value to be applied. Optional.
        bcol: the CSS background-color property value to be applied. Optional.
        font: the CSS shorthand font property value. Optional.

    Returns:
        The text string wrapped in a span element with a style attribute.

    """
    f = macro.formatter

    if not text:
        return f.strong(1) + \
               f.text('Color2 examples : ') + \
               f.text('<<Color2(Hello World!,red,blue,18px courier)>>, ') + \
               f.text('<<Color2(Hello World!,col=red,bcol=blue,font="18px courier")>>, ') + \
               f.text('<<Color2(Hello World!,#8844AA)>>') + \
               f.strong(0) + f.linebreak(0)

    # Escape CSS values
    #
    if col is not None:
        col = col.strip()
        if not _color2_is_valid_color(macro, col):
            col = None
    if bcol is not None:
        bcol = bcol.strip()
        if not _color2_is_valid_color(macro, bcol):
            bcol = None
    if font is not None:
        font = font.strip()
        if not _color2_is_valid_font(macro, font):
            font = None

    # Assemble CSS style string
    #
    style = ''
    if col:
        style += 'color: %s; '            % col
    if bcol:
        style += 'background-color: %s; ' % bcol
    if font:
        style += 'font: %s; '             % font

    # Escape HTML stuff.
    text = wikiutil.escape(text)
    text = wikiutil.renderText(macro.request, WikiParser, text)
    text = text.strip()

    # Get out
    #
    return f.rawHTML('<span style="%s">' % style) + text + f.rawHTML('</span>')

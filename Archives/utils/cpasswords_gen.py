#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-

from __future__ import print_function, unicode_literals

from lc_ldap import shortcuts
import pprint

ROLES_FILE = '/var/lib/cpasswords/roles.py'
KEYS_FILE = '/var/lib/cpasswords/keys.py'


conn = shortcuts.lc_ldap_readonly()

# Pour ne faire pas de conf à rallonge, cette liste ne contient pas
# tous les droits crans (par exemple multimachine sont exclus)
TOUS_DROITS = ["nounou", "apprenti", "bureau", "tresorier", "rtc", "president", "cableur"]

# Cette liste ne contient que les EXTRA (on rajoute les canoniques juste après)
ROLES_OF_DROITS = {
    'nounou': [
        'apprenti', 'apprenti-w',
        'cableur', 'cableur-w',
    ],
    'rtc': [
        'tresorier', 'tresorier-w',
        'cableur', 'cableur-w',
    ],
    'president': ['tresorier', 'tresorier-w'],
}
for droits in TOUS_DROITS:
    if droits not in ROLES_OF_DROITS:
        ROLES_OF_DROITS[droits] = []
    ROLES_OF_DROITS[droits] += [droits, droits+'-w']

def format_fpr(fpr):
    return fpr.replace(' ','')

roles = dict()
keys = dict()

fa = '(|%s)' % ''.join(u'(droits=%s)' % x for x in TOUS_DROITS)

#filterstr = '(&(!(droits=ancien))%s)' % fa
filterstr = fa

for member in conn.search(filterstr):
    # Member again ?
    login = member['uid'][0].value

    # On remplit la clé
    if member['gpgFingerprint']:
        fpr = format_fpr(member['gpgFingerprint'][0].value)
    else:
        continue
        #fpr = None
    # Now le mail associé
    if member['gpgMail']:
        mail = member['gpgMail'][0].value
    else:
        mail = member['mail'][0].value
    keys[login] = (mail, fpr)

    # Tous les droits pour login (sans doublon)
    their_roles = set()
    for droit in member['droits']:
        their_roles.update(ROLES_OF_DROITS.get(droit.value.lower(), []))

    # On remplit roles
    for role in their_roles:
        if role not in roles:
            roles[role] = []
        roles[role].append(login)

pp = pprint.PrettyPrinter(indent=4)
with open(KEYS_FILE, 'w') as f:
    f.write('value = %s' % pp.pformat(keys))

with open(ROLES_FILE, 'w') as f:
    f.write('value = %s' % pp.pformat(roles))

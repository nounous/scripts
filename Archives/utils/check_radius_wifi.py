#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
    Script vérifiant la disponibilité d'un serveur RADIUS pour l'authentification
    en wifi.
    eapol_test doit etre installe dans /usr/bin.
    Un couple nom de machine/password doit etre present dans le secrets.py, ainsi que le radius_eap_key.
"""

import os
from gestion import secrets_new as secrets

from argparse import ArgumentParser
from subprocess import check_output

CONF = """network={
        ssid="example"
        key_mgmt=WPA-EAP
        eap=TTLS
        identity="%s"
        anonymous_identity="anonymous"
        password="%s"
        phase2="autheap=MSCHAPv2"
}"""

CONF_FILE = "eap_auth_test.conf"
# Définition des arguments que mange le script
parser = ArgumentParser(description=__doc__)
parser.add_argument('-H', '--ip-host', type=unicode, required=True, help="Adresse IP du serveur RADIUS")
parser.add_argument('-u', '--user-name', type=unicode, required=True, help="Nom d'utilisateur pour l'authentification (User-Name)")
parser.add_argument('-m', '--mac', type=unicode, required=True, help="Mac de la machine")

if __name__ == '__main__':
    args = parser.parse_args()
    radius_eap_key = secrets.get('radius_eap_key')

    with open(CONF_FILE, "w") as f:
        f.write(CONF %(args.user_name, secrets.get('wifi_pass')[args.user_name]))
    try:
        res = check_output(["eapol_test", "-c", CONF_FILE, "-a", args.ip_host, "-s", radius_eap_key, "-M", args.mac])
    except:
        os.remove(CONF_FILE)
        exit(1)
    os.remove(CONF_FILE)
    exit(0)

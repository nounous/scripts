#!/bin/bash

# Pour que www-data puisse créer les fichiers à imprimer pour un user donné
# affiche le dossier cible en sortie

user="$1"
path="/home/${user:0:1}/impressions/$user"

if getent passwd $user > /dev/null; then
    mkdir -p "$path"
    chown -R "$user:www-data" "$path"
    chmod -R 770 "$path"
    echo "$path"
else
    echo "L'utilisateur $user n'existe pas." >&2
    exit 1
fi

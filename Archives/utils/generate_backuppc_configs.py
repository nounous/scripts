#!/usr/bin/python
# -*- mode: python; coding: utf-8 -*-
#
# Script de génération des configs backuppc sur zephir/omnomnom
# Doit être exécuté par root.
# Plein de trucs sont hardcodés, c'est chiant, il faudrait encore
# optimiser tout ça
#
# Boucle sur les divers serveurs pour générer leurs configs dans des fichiers perl
# Fait la même chose pour les homes, en fractionnant ceux-ci suivant la config
# donnée par backups.py
#
# Auteur : Pierre-Elliott Bécue <becue@crans.org>
# Licence : GPLv3
"""Script de génération des fichiers de configuration des
sauvegardes serveurs et homes"""

import os
import pwd
import grp
import json

ROOTPATH = "/etc/backuppc/"
CFGPATH = os.path.join(ROOTPATH, "config")

ROOT = pwd.getpwnam('root').pw_uid
WWWDATA = grp.getgrnam("www-data").gr_gid

def gen_backup_servers():
    """Génère la config de backup des serveurs"""
    if os.path.exists(
            os.path.join(
                CFGPATH,
                "servers_backup",
            )):

        hosts = json.load(open(os.path.join(CFGPATH, "hosts")))
        usual_dirs = json.load(open(os.path.join(CFGPATH, "hosts_classical_dirs")))
        to_exclude = json.load(open(os.path.join(CFGPATH, "hosts_excludes")))
        dirs_to_backup = json.load(open(os.path.join(CFGPATH, "hosts_dirs")))
        pre_user_cmd = json.load(open(os.path.join(CFGPATH, "hosts_preusercmd")))
        rsync_periods = json.load(open(os.path.join(CFGPATH, "hosts_periods")))

        for rsync_host in hosts:
            backup_folders = dirs_to_backup.get(rsync_host, {})
            backup_folders.update(usual_dirs)
            for excluded in to_exclude.get(rsync_host, []):
                backup_folders.pop(excluded)

            # Write

            with open(
                os.path.join(ROOTPATH, "%s.pl" % (rsync_host,)),
                "w",
                ) as fichier:

                fichier.write(
                    """$Conf{RsyncShareName} = ['%(shares)s'];
$Conf{BackupFilesExclude} = {
    %(excludes)s
};

$Conf{IncrPeriod} = %(incr)s;
$Conf{FullPeriod} = %(full)s;""" % {
    'shares' : "', '".join(backup_folders.keys()),
    'excludes' : "\n    ".join(
        [
            "'%s' => ['lost+found']," % nom
            for nom in backup_folders.keys()
        ]),
    'incr': rsync_periods[rsync_host][0],
    'full': rsync_periods[rsync_host][1],
})

                if rsync_host in pre_user_cmd:
                    fichier.write(
                        """\n$Conf{DumpPreUserCmd} = '%(dp)s';""" % {
                            'dp': pre_user_cmd[rsync_host],
                        }
                    )

            # End write

            os.chmod(
                os.path.join(ROOTPATH, "%s.pl" % (rsync_host,)),
                0644,
            )
            os.chown(
                os.path.join(ROOTPATH, "%s.pl" % (rsync_host,)),
                ROOT,
                WWWDATA,
            )

def gen_backup_homes():
    """Génère la config de backup des homes"""
    if os.path.exists(
            os.path.join(
                ROOTPATH,
                "config",
                "homes_backup"
            )):

        home_hosts = json.load(open(os.path.join(CFGPATH, "homes")))

        for home_dir in home_hosts:

            # Write

            with open(
                os.path.join(
                    ROOTPATH,
                    "home-%s.pl" % (home_dir,),
                ), "w") as fichier:
                fichier.write(
                    (u"""# Ces fichiers servent à backuper les homes en séparé.
$Conf{ClientNameAlias} = 'localhost';
$Conf{XferMethod} = 'tar';
$Conf{TarShareName} = ['/home/%(share)s'];
$Conf{TarClientCmd} = '/usr/bin/env LC_ALL=C sudo /usr/local/bin/backuppc_tar -v -f - -C $shareName --totals';

$Conf{BackupFilesExclude} = {
    '/home/%(share)s' => ['lost+found'],
};

# remove extra shell escapes ($fileList+ etc.) that are
# needed for remote backups but may break local ones
$Conf{TarFullArgs} = '$fileList';
$Conf{TarIncrArgs} = '--newer=$incrDate $fileList';""" % {'share' : home_dir,}).encode("utf-8")
                )

            # End write

            os.chmod(
                os.path.join(ROOTPATH, "home-%s.pl" % (home_dir,)),
                0644,
            )
            os.chown(
                os.path.join(ROOTPATH, "home-%s.pl" % (home_dir,)),
                ROOT,
                WWWDATA,
            )

if __name__ == "__main__":
    gen_backup_servers()
    gen_backup_homes()


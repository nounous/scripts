#!/usr/bin/python
# -*- coding: utf-8 -*-

"""API pour envoyer facilement un mail en python
   Author: Vincent Le Gallic <legallic@crans.org>

   """

import sys
if not '/usr/scripts' in sys.path:
    sys.path.append('/usr/scripts')
#: Pour fabriquer un mail en MIME
from email.mime.text import MIMEText
#: Pour encoder le mail en utf-8
from email import Charset
from email.Utils import formatdate
Charset.add_charset('utf-8', Charset.QP, Charset.QP, 'utf-8')

import mail as mail_module

def create_mail(emetteur, destinataires, objet, message, cc=[], more_headers={}):
    """Fabrique un mail"""
    if not isinstance(destinataires, list):
        destinataires = [destinataires]
    mail = MIMEText(message, "plain", "utf-8")
    mail["From"] = emetteur
    mail["To"] = u", ".join(destinataires)
    mail["Subject"] = objet
    mail["Date"] = formatdate(localtime=True)
    more_headers.setdefault("X-Mailer", "/usr/scripts/utils/sendmail.py")
    for k in more_headers.keys():
        mail[k] = more_headers[k]
    if cc != []:
        mail["Cc"] = u", ".join(cc)
    return mail

def actually_sendmail(emetteur, destinataires, mail, debug=False):
    """Envoie un mail en dialoguant avec le serveur SMTP"""
    if debug:
        print mail.as_string()
    else:
        # TODO méthode lazy de connexion au smtp + recyclage d'une ancienne
        # connexion ??
        with mail_module.ServerConnection() as s:
            s.sendmail(emetteur, destinataires, mail.as_string())

def sendmail(emetteur, destinataires, objet, message, cc=[], more_headers={}, debug=False):
    """Fabrique le mail et l'envoie"""
    mail = create_mail(emetteur, destinataires, objet, message, cc, more_headers)
    actually_sendmail(emetteur, destinataires, mail, debug)

class Message(object):
    """Message pré-formaté à envoyer à un adhérent ou une ML"""
    def __init__(self, emetteur, destinataires, objet, corps, cc=[], more_headers={}):
        self.emetteur = emetteur
        self.destinataires = destinataires
        self.objet = objet
        self.cc = cc
        self.more_headers = more_headers
        self.corps = corps % self.__dict__

    def mail(self):
        """Fabrique le mail"""
        return create_mail(self.emetteur, self.destinataires, self.objet,
                           self.corps, self.cc, self.more_headers)

    def send(self):
        """Envoie le mail"""
        return actually_sendmail(self.emetteur, self.destinataires, self.mail())

if __name__ == "__main__":
    print "Exemple d'utilisation :"
    print ">>> import sendmail"
    print '>>> sendmail.sendmail(u"passoire@crans.org", [u"nobody@crans.org", u"root@crans.org"], u"[Test] Envoi de mail", u"Ceci est un message envoyé par un script.\\n-- \\nsendmail.py")'

#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-
# Sccript qui permet  à une personne dotée des droits ancien de retrouver les droits nounou
# Executé avec un sudo
# detraz@crans.org
# GPLv2


from lc_ldap import shortcuts
import gestion.secrets_new as secrets
import os, commands, sys, pwd
import gestion.mail as mail_module
import gestion.config as config

def upgrade(user, ldap, answer, check=True):
    """Procède à l'upgrade des droits"""
    ldap_conn = ldap
    ldap_user = ldap_conn.search(u'uid=%s' % ldap_conn.current_login, mode='rw')
    ldap_user = ldap_user[0]
    if check and not bool('Ancien' in ldap_conn.droits):
        mail_notif("l'utilisateur n'a pas les droits ancien !", user, answer)
        print("Vous n'avez pas les droits anciens")
        return
    if check and bool('Nounou' in ldap_conn.droits):
        mail_notif("tocard qui a déjà les droits nounou ", user, answer)
        print("Vous êtes déjà nounou !")
        return
    # C'est respbats qui fait la modif
    ldap_conn.current_login = 'respbats'
    ldap_conn.droits = [u'Nounou']
    with ldap_user as luser:
        ldap_user['droits'].append(u'Nounou')
        ldap_user.history_gen()
        ldap_user.save()
    mail_notif("", user, answer, success=True)
    print("Droits nounou restaurés")
    return

def question():
    """Vérifie que le compte n'a pas été piraté avant l'upgrade"""
    questions = secrets.get('question_checkcrans')
    for question in questions:
        print(question)
        answer = raw_input()
        if answer.lower() != questions[question]:
            raise EnvironmentError
    return

def commentaire():
    """Demande une raison de vouloir retrouver des droits root"""
    answer = None
    while answer is None:
        try:
            print("Pourquoi voulez-vous retrouver vos droits ?")
            answer = raw_input()
        except EOFError:
            pass
    return answer

def mail_notif(reason, user, answer, success=False):
    # Génération d'un mail
    From = 'roots@crans.org'
    To = 'root@crans.org'
    if success:
        mail = """From: Root <%s>
To: %s
Subject: Restauration des droits nounou par %s

L'utilisateur %s a restauré avec succès ses droits nounous.

Voici la raison donnée : %s
""" % (From, To, user.encode(config.out_encoding), user.encode(config.out_encoding), answer)
    else:
        mail = """From: Root <%s>
To: %s
Subject: Tentative de restauration des droits nounou par %s !

L'utilisateur %s a tenté d'utiliser le script de restauration des droits nounou.
La tentative n'a pas abouti : %s

Voici la raison donnée : %s
""" % (From, To, user.encode(config.out_encoding), user.encode(config.out_encoding), reason, answer)

    # Envoi mail
    with mail_module.ServerConnection() as conn:
        conn.sendmail(From, To, mail)

if __name__=="__main__":
    if socket.gethostname!="zamok":
       sys.exit("Doit être utilisé sur Zamok")
    ldap_conn = shortcuts.lc_ldap_admin()
    user = ldap_conn.current_login
    answer = commentaire()
    if user:
        try:
            result = question()
        except (EnvironmentError,EOFError):
            mail_notif("echec aux questions secretes", user, answer)
            print("Echec aux questions secrètes, l'incident a été reporté")
            sys.exit(1)
        except Exception as e:
            mail_notif("Exceptipon non rattrapé : %s" % e, user, answer)
            print("Une erreur est survenu")
            sys.exit(2)
        upgrade(user, ldap_conn, answer)
    else:
        mail_notif("évaluation de l'user impossible, abandon", user, answer)
        print("Evaluation de l'user impossible, abandon")

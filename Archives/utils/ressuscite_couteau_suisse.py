#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

""" Ne regardez pas ça, c'est affreux.
    
    Bon, si vous insistez, c'est parce que ressuscite est cassé,
    ça permet de dumper un fichier du cimetière et de l'output en raw.

    Ensuite, bah shelldap…"""

import cPickle
import re
import sys
sys.path.append("/usr/scripts/gestion/")
import ldap_crans


def load(file=None):
    """Charge le contenu du fichier."""
    if file is None:
        file = sys.argv[1]
    a = cPickle.load(open(file))
    return a

def dump(obj):
    """Raw-affiche l'objet. En ajoutant une ligne pour le dn."""
    raw= [obj.dn]
    for (field, content) in obj._data.iteritems():
        for value in content:
            raw.append("%s: %s" % (field, value))
    return "\n".join(raw) + "\n"

if __name__ == "__main__":
    print "\n" + dump(load())
    print "Y'a plus qu'à paste ça dans LDAP."
    print "Attention à virer le dn avec ??? et l'objectClass fournis par shelldap"

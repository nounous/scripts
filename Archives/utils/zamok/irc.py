#!/usr/bin/python3
# -*- mode: python; coding: utf-8 -*-
#
# irc.py -- Script pour le choix du client IRC.
#
# Copyright (C) 2016 Cr@ns <roots@crans.org>
# Author: Pierre-Elliott Bécue <becue@crans.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
# * Neither the name of the Cr@ns nor the names of its contributors may
#   be used to endorse or promote products derived from this software
#   without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT
# HOLDER> BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""Outil permettant de choisir son client IRC, voire de sauvegarder la
configuration."""

#pylint: disable=line-too-long

import os
import argparse
import sys

import dialog

import locale
locale.setlocale(locale.LC_ALL, '')

CONFIG_DIR = os.path.join(
   os.path.expanduser("~"),
   ".config",
   "zamok",
)

TIMEOUT = 600

WEECHAT = 'weechat'
IRSSI = 'irssi'

CLIENTS_PATH = {
    WEECHAT: '/usr/bin/weechat',
    IRSSI: '/usr/bin/irssi',
}

def to_client(client_name):
    """Fonction de choix du client, qui propose de sauvegarder"""

    def save(dialog_interface, do_it=False):
        """Fonction de sauvegarde du choix"""

        def _save():
            """Fait effectivement la sauvegarde"""
            if not os.path.exists(CONFIG_DIR):
                os.makedirs(CONFIG_DIR)

            with open(
                os.path.join(
                    CONFIG_DIR,
                    "irc",
                ),
                "w") as conf_file:
                conf_file.write(client_name)

        # Sauvegarde automatique ou non ?
        if do_it:
            _save()
        elif not dialog_interface.yesno(
            (
                "Voulez-vous sauvegarder le choix du client %s "
                "pour votre prochain démarrage de ce programme ? "
                "Si oui, elle sera dans le fichier %s."
            ) % (
                client_name,
                os.path.join(
                    CONFIG_DIR,
                    "irc",
                )
            ),
            width=60,
            height=10,
            title="Sauvegarde du choix",
            timeout=TIMEOUT,
            backtitle="IRC O_O"):
            _save()

        return client_name
    return save

CLIENTS = {
    WEECHAT: {
        'txt': 'Weechat (le plus utilisé par les membres actifs)',
        'help': 'Le client weechat possède une liste étendue de plugins, et est activement développé',
        'callback': to_client(WEECHAT),
    },
    IRSSI: {
        'txt': 'Irssi (moins utilisé, vous devrez vous documenter)',
        'help': 'Le client irssi fut le client le plus utilisé au Crans avant l\'arrivée de weechat',
        'callback': to_client(IRSSI),
    },
}

def choose_client():
    """Choix effectif du client à lancer"""

    dialog_interface = dialog.Dialog()
    _prompt = "Quel client IRC voulez-vous démarrer ?"

    _order = [
        WEECHAT,
        IRSSI,
    ]

    # Menu principal
    (code, tag) = dialog_interface.menu(
        _prompt,
        width=0,
        height=0,
        menu_height=0,
        item_help=1,
        default_item=WEECHAT,
        title="Choix client IRC",
        scrollbar=True,
        timeout=TIMEOUT,
        cancel_label="Quitter",
        backtitle="IRC O_O",
        choices=[(key, CLIENTS[key]['txt'], CLIENTS[key]['help']) for key in _order]
    )

    if code not in [dialog.Dialog.OK]:
        return None

    # On charge la fonction de callback
    _callback = CLIENTS[tag]['callback']

    # S'il y a eu une couille, elle peut valoir None
    if _callback is not None:
        return _callback(dialog_interface)

def check_config():
    """Lit le fichier de config s'il existe, et retourne son contenu si pertinent"""

    if os.path.exists(os.path.join(
        CONFIG_DIR,
        "irc")):

        _client = open(os.path.join(
            CONFIG_DIR,
            "irc"),
            'r').read().strip().replace("\n", "")

        if _client in CLIENTS:
            return _client
    return None

def launch_client(client_name):
    """Lance effectivement le client"""

    _cmd = CLIENTS_PATH[client_name]

    os.execv(_cmd, (_cmd,))

if __name__ == '__main__':

    PARSER = argparse.ArgumentParser(description="Choix de client IRC", add_help=False)
    PARSER.add_argument("-c", "--client", help="Choix direct du client", action="store", type=str)
    PARSER.add_argument("-h", "--help", help="Affiche cette aide et quitte.", action="store_true")
    PARSER.add_argument("-n", "--noconfig", help="Ignore le fichier de config", action="store_true")
    PARSER.add_argument("-s", "--save", help="Sauvegarde du choix du client", action="store", type=str)

    ARGS = PARSER.parse_args()

    if ARGS.help:
        PARSER.print_help()
        sys.exit(0)

    if ARGS.client and ARGS.client.lower() in CLIENTS:
        _CLIENT = ARGS.client.lower()
        if ARGS.save:
            _ = to_client(_CLIENT)(None, True)
    elif not ARGS.noconfig:
        _CLIENT = check_config()

    if _CLIENT is None:
        _CLIENT = choose_client()

    if _CLIENT is None:
        sys.exit(0)

    launch_client(_CLIENT)

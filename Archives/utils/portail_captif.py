# -*- mode: python; coding: utf-8 -*-

"""
Portail captif servant les pages de déconnexion depuis le wiki
en fonction des blacklistes de la machine qui vient s'y connecter
"""

from __future__ import print_function

import subprocess

import ldap
from netaddr import IPNetwork

import lc_ldap.shortcuts
from gestion.config import config
from gestion.gen_confs.ipset import IPSET_PATH

DECO = {
    'non_inscrit': 'ERR_CUSTOM_NON_INSCRIT',
    'inscrit': 'ERR_CUSTOM_INSCRIT',

    'confirmation': 'ERR_RECONNECT_CONFIRM',

    'age': 'ERR_CUSTOM_AGE',
    'ago': 'ERR_CUSTOM_AGO',

    'chambre_invalide': 'ERR_CUSTOM_BL_CHAMBRE',
    'mail_invalide': 'ERR_CUSTOM_BL_NO_MAIL',
    'mail_invalide_inscription': 'ERR_DISABLED_CUSTOM_BL_NO_MAIL',
    'warez': 'ERR_CUSTOM_BL_WAREZ',
    'bloq': 'ERR_CUSTOM_BLOQ',

    'paiement': 'ERR_CUSTOM_READHESION',

    # en cas de sursis, la page donne une url pour continuer à naviguer
    # sur Internet
    'paiement_sursis': 'ERR_CUSTOM_READHESION',
    'proxy_local': 'ERR_CUSTOM_PROXY_LOCAL',

    'virus': 'ERR_CUSTOM_BL_VIRUS',
    'ipv6_ra': 'ERR_CUSTOM_RA',
    'autodisc_virus': 'ERR_CUSTOM_BL_AUTO_VIRUS',
}

BLACKLIST_KEYS = [
    'non_inscrit',
    'age', 'ago',
    'virus', 'autodisc_virus', 'ipv6_ra',
    'chambre_invalide', 'mail_invalide',
    'bloq',
    'proxy_local', 'paiement',
    'inscrit',
]

use_ldap = lc_ldap.shortcuts.with_ldap_conn(constructor=lc_ldap.shortcuts.lc_ldap_readonly)

def get_deco_url(bl):
    """Renvoie l'URL de déconnexion associée à une blackliste donnée"""
    return "/crans-deco/%s" % DECO[bl]

def mac_from_ip(ip_addr):
    """Renvoie l'adresse MAC associée à un IP en cherchant
    dans les tables ARP"""
    if ip_addr == "10.51.0.10":
        return u"02:72:6f:75:74:07"

    cmd = '/usr/sbin/arp -na %s' % ip_addr
    proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    output, _ = proc.communicate()
    if output is not None:
        mac_addr = output.split()[3]
        return mac_addr.strip()
    else:
        return None

def enable_access(ip_addr):
    """Lance la commande idoine pour autoriser l'adresse IP"""
    cmd = [
        'sudo', '-n', IPSET_PATH, 'add', 'CONFIRMATION', ip_addr, "timeout", str(3600*6),
    ]
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate()
    if out or err:
        print("ipset_allow:\n%s\n---\n%s" % (out, err))
        return False
    else:
        return True

@use_ldap
def query(query_str, ldap_conn):
    """Effectue une recherche dans l'annuaire LDAP"""
    try:
        return ldap_conn.search(query_str)
    except ldap.SERVER_DOWN:
        print("LDAP server unreachable")
        return []

def get_machine_by_mac(ip_addr):
    """Renvoie la machine associée à l'adresse IP donnée en utilisant
    son adresse MAC"""
    return query(u'macAddress=%s' % mac_from_ip(ip_addr))

def get_machine_by_ip(ip_addr):
    """Renvoie la machine associée à l'adresse IP donnée depuis
    l'annuaire LDAP"""
    return query(u'ipHostNumber=%s' % ip_addr)

def get_page(ip_addr, by_mac=False, accueil=False):
    """Renvoie la page de déconnexion correspondant à la blackliste
    de la machine concernée"""
    if by_mac:
        machines = get_machine_by_mac(ip_addr)
    else:
        machines = get_machine_by_ip(ip_addr)

    if not machines:
        return get_deco_url('non_inscrit')

    adherent = machines[0].proprio()

    blacklist = [bl.value['type'] for bl in adherent.blacklist_actif()]
    for machine in machines:
        blacklist.extend([bl.value['type'] for bl in machine.blacklist_actif()])
    for bl in BLACKLIST_KEYS:
        if bl in blacklist:
            return get_deco_url(bl)

    return get_deco_url('inscrit') if accueil else None
    #return get_deco_url('ago')   # En cas d'ag

def addr_in_net(ip_addr, net):
    """Indique si l'adresse IP donnée est dans le réseau demandé"""
    return any(ip_addr in IPNetwork(subnet) for subnet in config.NETs[net])

def app(environ, start_response):
    """Fonction exportée par l'interface WSGI"""
    ip_addr = environ.get('HTTP_K_REAL_IP', environ.get('REMOTE_ADDR', None))
    request_uri = environ.get('REQUEST_URI', '')
    retcode = "200 OK"

    if ip_addr is None:
        print("No IP found in WSGI request")
        return ""

    if request_uri.startswith('/enable'):
        enable_access(ip_addr)
        url = get_deco_url('confirmation')
    elif addr_in_net(ip_addr, 'accueil'):
        url = get_page(ip_addr, by_mac=True, accueil=True)
    elif addr_in_net(ip_addr, 'isolement'):
        url = get_page(ip_addr, by_mac=True) or get_deco_url('autodisc_virus')
    elif addr_in_net(ip_addr, 'all') or addr_in_net(ip_addr, 'personnel-ens'):
        url = get_page(ip_addr) or "/crans-proxy/"
    else:
        url = get_deco_url('non_inscrit')

    start_response(
        retcode,
        [
            ("Content-Type", "text/html"),
            ("Cache-Control", "no-cache, must-revalidate"),
            ("Expires", "Thu, 01 Jan 1970 00:00:00 GMT"),
            ("X-Accel-Redirect", url),
            ("Content-Length", "0")
        ]
    )

    return ""

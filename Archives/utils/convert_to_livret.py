#!/usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

"""
Ce script modifie l'ordre des pages à l'aide de la libraire de dialogue à
l'imprimante.
"""

import sys
from impression import impression_hp
from affich_tools import cprint

if __name__ == "__main__":

    if len(sys.argv) != 2:
        cprint(
           "Merci d'indiquer un unique fichier à convertir au format livret",
           "red"
        )
        sys.exit(1)
    printing  = impression_hp.impression(sys.argv[1])
    printing._pdfbook()
    cprint("Pour imprimer, utiliser la commande :\n"+
" lp -d MFPM880 -o number-up=2\
  -o sides=two-sided-short-edge\
  -o media=a4 \
  -o HPStaplerOptions=FoldStitch\
  -o Collate=True\
  -o InputSlot=Tray4\
  -n <number_of_copies> %s " % ("pdfjammed_"+sys.argv[1],),
  'vert'
)

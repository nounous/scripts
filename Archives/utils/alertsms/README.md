Crans SMS
=========

Présentation
------------

Le service de SMS comporte deux composants distincts:

*   `utils.alertsms`    :   Module exportant les fonctions permettant d'envoyer
                            les messages dans une file sur un serveur AMQP

*   `utils.alertsms.cranssmsd`  :   Daemon récupérant les messages de la file
                                    pour les envoyer sous forme de SMS

Configuration
-------------

La configuration se trouve dans `utils.alertsms.config`. On y trouve les
paramètres pour la connexion au serveur AMQP, ainsi que les éléments de
configuration du daemon gammu-smsd.

Paquets requis
--------------

*   Pour envoyer des messages dans la file : `python-pika`

*   Pour le fonctionnement du daemon : `python-pika`, `python-gammu`,
`python-daemon`, `gammu`, `gammu-smsd`

En pratique
-----------

*   Lancer le service sur le serveur connecté au module d'envoi des SMS
    s'il ne tourne pas : `sudo service cranssmsd start`

*   Envoyer des messages :
    `from utils import alertsms

     alertsms.send("<message>",["<destinataire1>", "<destinataire2>", ...])`

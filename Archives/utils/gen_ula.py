#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, unicode_literals

import os
import sys
import argparse

parser = argparse.ArgumentParser("Génère des préfixes IPv6 ULA")
parser.add_argument('--number', type=int, nargs='?', help="Combien doit-on générer de préfixes ?", default=1)
parser.add_argument('--size', type=int, nargs='?', help="Taille des préfixes à générer", default=48)

args = parser.parse_args(sys.argv[1:])

if args.size < 0 or args.size > 64:
    print("La taille du préfixe doit être comprise entre 0 et 64 bits")
    sys.exit(1)

for i in range(args.number):
    data = os.urandom((args.size // 8) - 1)
    prefix = "fd"

    for num, byte in enumerate(data):
        if num % 2:
            prefix += "%02x" % ord(byte)
        else:
            prefix += "%02x:" % ord(byte)

    print(prefix + ":/%d" % args.size)

#!/usr/scripts/python.sh
# coding: utf-8
"""
À lancer sur odlyd ou sable
Compte le nombre d'adhérent apparaissant dans /var/log/firewall/logall.log.1.bz2
"""
import os
import sys
import bz2

import lc_ldap.shortcuts

if __name__ == "__main__":
    conn = lc_ldap.shortcuts.lc_ldap_admin()

    # On récupère tous les adhérents (inclus les clubs).
    # On utilise conn.allAdherents() ça la fonction peuple automatiquement les machines
    # des adhérent, on effectue ainsi une seule requête ldap au lieu de une par adhérent
    sys.stdout.write("Récupérations des adhérents...")
    sys.stdout.flush()
    adhs = conn.allAdherents()
    print("OK")

    # on associe chaque IP au DN d'un adhérent
    IPS_ADH = {}
    for adh in adhs:
        # déjà peuplé par allAdherents(), n'effectue pas de requête ldap
        for m in adh.machines():
            for ip in m['ipHostNumber']:
                IPS_ADH[str(ip)] = adh.dn

    # libère la mémoire
    del adhs
    del conn

    # pour chaque IP apparessant dans /var/log/firewall/logall.log.1.bz2, on aoute le dn
    # correspondant à l'ensemble aid
    aid = set()
    i = 0
    dl = 0
    size = os.path.getsize('/var/log/firewall/logall.log.1.bz2')
    bz = bz2.BZ2Decompressor()
    print("Parsing /var/log/firewall/logall.log.1.bz2")
    last_line = ""
    with open('/var/log/firewall/logall.log.1.bz2', 'r') as f:
        data = f.read(8196)
        while data:
            dl += len(data)
            # on rafraichi de temps en temps la bare de progression (env. tous les 800Ko)
            if i % 100 == 0:
                done = int(round(50 * dl / size))
                sys.stdout.write("\r[%s%s]" % ('=' * done, ' ' * (50 - done)))
                sys.stdout.flush()
            i += 1
            # ajoute des donné au décompresseur qui retourne des donné décompressé quand cela est
            # possible, et sinon retourne une chaine vide
            data = bz.decompress(data)
            if data:
                lines = data.split('\n')
                # la première ligne est la suite de la dernière ligne précédente
                lines[0] = last_line + lines[0]
                # la dernière ligne n'est pas forcement complète, on la garde pour le prochain tour
                #  de boucle
                last_line = lines[-1]
                for line in lines[:-1]:
                    line = line.strip().split()
                    if len(line) > 7 and line[7].startswith('SRC='):
                        ip = line[7][4:]
                    try:
                        aid.add(IPS_ADH[ip])
                    # si le paquet est entrant, l'ip sera inconnu est génèrera une erreur KeyError
                    except KeyError:
                        pass
            data = f.read(8196)
        # on traite la dernière ligne du fichier
        line = last_line.strip().split()
        if len(line) > 7 and line[7].startswith('SRC='):
            ip = line[7][4:]
            try:
                aid.add(IPS_ADH[ip])
            except KeyError:
                pass
        sys.stdout.write("\r%s\r" % (" " * 55))
        sys.stdout.flush()
    print("Au moins %s adhérent ont utilisé la connexion" % len(aid))

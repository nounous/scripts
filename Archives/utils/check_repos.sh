#!/bin/bash
# Petit script qui surveille les dépôts git de /usr/scripts
# En cas de dépôt non pullé, ou si des commits n'ont pas
# été envoyés, une alerte est lancée.

# Pour être sûr que tout marche, on passe en LANG=C
export LANG=C
export LC_ALL=C

# Paths des dépôts à surveiller
GIT_REPOS="/usr/scripts /usr/scripts/lc_ldap"

# Intervalle entre deux fetchs
PERIOD=5

check_repo () {
  echo "Vérification de $1"
  cd $1
  ( git status | grep "Your branch is ahead" ) && {
    echo "...et dépôt pas à jour"
    exit 42
  }
}

fetch_updates () {
  cd $1
  umask 002
  if [ ! -f ".git/FETCH_HEAD" ]; then
      echo "Initial fetching of the repo"
      git fetch
  fi
  if test ! "`find .git/FETCH_HEAD -mmin +$PERIOD`"; then
    return
  fi
  echo "fetching $1"
  if [ $hasmodules -eq 0 ]; then
      echo "Fetching recursively"
      git fetch --recurse-submodules origin > /dev/null || exit 12
  else
      git fetch origin > /dev/null || exit 12
  fi
}

try_ff () {
  cd $1
  if git status | grep -E "^(# )?Your branch is behind.*can be fast-forwarded.$" -q; then
    echo "Fast forward..."
    if [ $hasmodules -eq 0 ]; then
        git pull --ff-only --recurse-submodules origin master || exit 12
    else
        git pull --ff-only || exit 12
    fi
  else
    echo "Nothing to fast forward"
  fi
}


has_submodules () {
    cd $1
    if [ -f .gitmodules ]; then
        subdirs="$(cat .gitmodules | grep path | cut -d= -f2-)"
        empty=1
        for subdir in $subdirs; do
            cd $subdir
            if [ `ls -lA | wc -l` -eq 1 ]; then
                empty=0
            fi;
        done
        cd $1
        if [ $empty -eq 0 ]; then
            echo "Initializing submodule"
            git submodule init
            git submodule update
        fi
        return 0
    fi
    return 1
}

is_shared_repository () {
    cd $1
    if ! git config --get core.sharedrepository | grep "group" -q ; then
        git config core.sharedrepository group
        if [ $? -eq 0 ]; then
            echo "Repository set readable and writable to group"
        else
            echo "Cannot change repository configuration"
            echo "Please human.. act..."
        fi
    else
        echo "Already ok"
    fi
}

# Et on check que les repos sont ok
for dir in $GIT_REPOS; do
    has_submodules $dir
    hasmodules=$?
    is_shared_repository $dir
    fetch_updates $dir
    check_repo $dir
    try_ff $dir
done


exit 0

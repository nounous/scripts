#!/bin/bash
# Author: Pierre-Elliott Bécue <becue@crans.org>
# Script qui est appelé par backuppc lorsqu'il tente
# de backuper thot

PATH="/sbin:/bin:/usr/sbin:/usr/bin"
destination="/pg_backups"
mail="Roots <roots@crans.org>"
liste_backups=$(sudo -u postgres /usr/bin/psql -q -t -c 'SELECT datname FROM pg_database WHERE NOT datistemplate;')
probs=0

gogogadgetobackups (){
    # Expansion voulue ici.
    for dbname in ${liste_backups}; do
        file_dest="${destination}/${dbname}.sql.bz2"
        if sudo -u postgres pg_dump "${dbname}" | bzip2 -z > "${file_dest}.tmp"; then
            mv -f "${file_dest}.tmp" "${file_dest}"
        else
            rm -f "${file_dest}.tmp"
            retour="${retour}\nArchivage de la base ${dbname} échoué le $(date)...";
            probs=1
        fi;
    done
}

if [ ! -e "${destination}" ]; then
    mkdir -p "${destination}"
fi

if [ ! -d "${destination}" ]; then
    retour="${destination} n'est pas un dossier, pas de backups possibles."
    probs=1
else
    gogogadgetobackups
fi

if [[ $probs -eq 1 ]]; then
(cat << EOF
From: backuppc@crans.org
To: ${mail}
Subject: Backups postgresql foirés
Content-Type: text/plain; charset=UTF-8;

${retour}
EOF
) | sendmail -t
fi;

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
sys.path.append('/usr/scripts/gestion')
sys.path.append('/usr/scripts/impression')
from commands import getstatusoutput

class ConversationError(Exception): pass

class snmp:
    def __init__(self, host, version="1", community="public"):
        self.cmd = 'snmpget -O v -v%s -c %s %s ' % (version, community, host)

    def get(self, mib):
        s, r = getstatusoutput(self.cmd + mib)
        if s:
            r = r.replace('snmpget: ', '')
            raise ConversationError(r, self.cmd)
        return r.split(' ', 1)[1]

comm = snmp(host="imprimante.adm.crans.org",version="1",community="public")

from impression_canon import SNMP_CAPA_B, SNMP_CAPA_C, SNMP_CAPA_M, \
                             SNMP_CAPA_Y, SNMP_TON_B, SNMP_TON_C, \
                             SNMP_TON_M, SNMP_TON_Y,SNMP_BAC1, SNMP_BAC2, \
                             SNMP_BAC3, SNMP_BAC4, SNMP_COUNT_A4, \
                             SNMP_COUNT_A3, SNMP_COUNT_A4c, \
                             SNMP_COUNT_A3c, SNMP_COUNT_TOT

dico_courbes_gen = {
    'ton-p' : { 'titre' : u"Etat des toner (pourcentage restant)",
                'vlabel': u"Pourcentage restant",
                'lower-limit' : 0,
                'upper-limit' : 100,
                'warning' : '5:',
                'critical' : '1:',
                'dico' : { 'ton_b' : (SNMP_TON_B, "Toner noir", "000000"),
                           'ton_c' : (SNMP_TON_C, "Toner cyan", "00ffff"),
                           'ton_m' : (SNMP_TON_M, "Toner magenta", "ff00ff"),
                           'ton_y' : (SNMP_TON_Y, "Toner jaune", "ffff00")
                           },
                'max' :  { 'ton_b' : SNMP_CAPA_B,
                           'ton_c' : SNMP_CAPA_C,
                           'ton_m' : SNMP_CAPA_M,
                           'ton_y' : SNMP_CAPA_Y }
                },
    'papier' : { 'titre'   : 'Stock papier',
                 'vlabel' : 'Feuilles restantes',
                 'lower-limit' : 0,
                 'warning' : '100:',
                 'critical' : '1:',
                 'upper-limit' : 550,
                 'dico' : { 'Bac1' : (SNMP_BAC1, "Feuilles A4 (Bac 1)", '3333ff'),
                            'Bac2' : (SNMP_BAC2, "Feuilles A4 (Bac 2)", '4444cc'),
                            'Bac3' : (SNMP_BAC3, "Feuilles A4R (Bac 3)", "33ff33"),
                            'Bac4' : (SNMP_BAC4, "Feuilles A3 (Bac 4)", "ff3333") }
                 },
    'compteur' : { 'titre' : 'Pages imprimees',
                   'vlabel' : 'Pages',
                   'dico' : { 'nb_A4' : (SNMP_COUNT_A4, "Feuilles A4 n/b", '000000'),
                              'nb_A3' : (SNMP_COUNT_A3, "Feuilles A3 n/b", '333333'),
                              'nb_A4c': (SNMP_COUNT_A4c, "Feuilles A4 coul", '00ffff'),
                              'nb_A3c': (SNMP_COUNT_A3c, "Feuilles A3 coul", 'ff00ff'),
                              'total' : (SNMP_COUNT_TOT, "Total", 'ff0000') }
                   }
    }

#Initialisation du dictionnaire des courbes
arg = sys.argv[0].split('_')[1]
dico_courbes = dico_courbes_gen[arg]['dico']
titre = dico_courbes_gen[arg]['titre']
vlabel = dico_courbes_gen[arg]['vlabel']
lower_limit = dico_courbes_gen[arg].get('lower-limit', None)
upper_limit = dico_courbes_gen[arg].get('upper-limit', None)
max_courbes = dico_courbes_gen[arg].get('max', [])
warning = dico_courbes_gen[arg].get("warning")
critical = dico_courbes_gen[arg].get("critical")

try :
    arg = sys.argv[1]
except :
    arg = ''
#Ecrit sur la sortie standard la configuration
if arg == "config" :
    print 'host_name canon'
    print 'graph_category consommables'
    if upper_limit:
        print "graph_args --lower-limit %i --upper-limit %i --rigid" % (lower_limit,upper_limit)
    else:
        print "graph_args --lower-limit 0"
    print 'graph_title %s' % titre
    print "graph_vlabel %s" % vlabel
    keys = dico_courbes.keys()
    keys.sort()
    for fieldname in keys:
        print "%s.label %s" % (fieldname,dico_courbes[fieldname][1])
        if warning:
            print "%s.warning %s" % (fieldname, warning)
        if critical:
            print "%s.critical %s" % (fieldname, critical)
        if len(dico_courbes[fieldname]) == 3 :
            print "%s.colour %s" % (fieldname,dico_courbes[fieldname][2])
        if len(dico_courbes[fieldname]) == 4 :
            print "%s.draw %s" % (fieldname,dico_courbes[fieldname][3])
else:
#Ecrit les valeurs actuelles sur la sortie standard
    for fieldname in dico_courbes.keys():

        try:
            if max_courbes :
                max_val = int(comm.get(max_courbes[fieldname]))
                value = 100*int(comm.get(dico_courbes[fieldname][0]))/max_val
            else :
                value = int(comm.get(dico_courbes[fieldname][0]))
        except Exception,err:
            value = 0
            print err
        if value < 0 : value = 0

        print "%s.value %s" % (fieldname,value)


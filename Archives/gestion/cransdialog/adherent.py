#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

"""
Copyright (C) Valentin Samir
Licence : GPLv3

"""

from __future__ import unicode_literals

import sys
import time
import datetime
from dateutil.relativedelta import relativedelta
import subprocess
import pytz

import config.cotisation

import lc_ldap.objets as objets
import lc_ldap.attributs as attributs
from lc_ldap.attributs import UniquenessError
from lc_ldap import crans_utils

import proprio
from CPS import TailCall, tailcaller, Continue

class Dialog(proprio.Dialog):
    """
    Classe Dialog spécifique aux adhérents pour gest_crans_lc
    """
    def modif_adherent_blacklist(self, adherent, cont):
        """Raccourci vers edit_blacklist spécifique aux adherent"""
        return self.edit_blacklist(
            obj=adherent,
            title="Éditions des blacklist de %s %s" % (
                adherent['prenom'][0],
                adherent['nom'][0]
            ),
            update_obj='adherent',
            cont=cont
        )

    def modif_adherent(self, cont, adherent=None, proprio=None, tag=None):
        """Menu d'édition d'un adhérent"""
        if proprio:
            adherent = proprio
        if adherent is None:
            adherent = self.select(
                ["adherent"],
                "Recherche d'un adhérent pour modification",
                cont=cont
            )
        a = attributs
        menu_droits = {
            'Administratif' : [a.cableur, a.nounou],
            'Personnel':[a.cableur, a.nounou, a.soi],
            'Études':[a.nounou, a.soi, a.cableur],
            'Chambre':[a.cableur, a.nounou],
            'Compte':[a.cableur, a.nounou],
            'GPGFingerprint' : [a.nounou, a.soi],
            'Remarques' : [a.cableur, a.nounou],
            'Droits':[a.nounou, a.bureau],
            'Blackliste':[a.bureau, a.nounou],
            'Vente':[a.cableur, a.nounou],
            'Ticket':[a.cableur, a.nounou],
            'Supprimer':[a.nounou, a.bureau],
        }
        menu = {
            'Administratif' : {
                'text' : "Adhésion, chartes",
                "callback" : self.adherent_administratif
            },
            'Personnel' : {
                'text' : "Nom, prénom, téléphone, et mail de contact",
                "adherent" : "proprio",
                'callback' : self.proprio_personnel
            },
            'Études'       : {
                'text' : "Étude en cours",
                "callback" : self.adherent_etudes
            },
            'Chambre'      : {
                'text' : 'Déménagement',
                "adherent" : "proprio",
                "callback" : self.proprio_chambre
            },
            'Compte'       : {
                'text' : "Gestion du compte crans",
                "adherent" : "proprio",
                "callback" : TailCall(
                    self.proprio_compte,
                    update_obj='adherent'
                ),
                'help' : "Création/Suppression/Activation/Désactivation du compte, "
                         "gestion des alias mails crans du compte",
            },
            'GPGFingerprint' : {
                'text' : 'Ajouter ou supprimer une empreinte GPG',
                'attribut' : attributs.gpgFingerprint
            },
            'Remarques' : {
                'text' : 'Ajouter ou supprimer une remarque à cet adhérent',
                'attribut' : attributs.info
            },
            'Droits' : {
                'text' : "Modifier les droits alloués à cet adhérent",
                "callback" : self.adherent_droits
            },
            'Blackliste' : {
                'text' : 'Modifier les blacklist de cet adhérent',
                'callback' : self.modif_adherent_blacklist
            },
            'Vente' : {
                'text' : "Chargement solde crans, vente de cable ou adaptateur ethernet ou autre",
                "adherent" : "proprio",
                "callback" : self.proprio_vente
            },
            'Ticket' : {
                'text' : "Imprime un ticket complet avec toutes les machines de l'utilisateur",
                "adherent" : "proprio",
                "callback" : self.proprio_ticket
            },
            'Supprimer' : {
                'text' : "Supprimer l'adhérent de la base de donnée",
                'callback' : TailCall(
                    self.delete_adherent,
                    del_cont=cont(proprio=None)
                )
            },
        }
        menu_order = ['Administratif', 'Personnel', 'Études', 'Chambre', 'Compte']
        menu_compte_crans = ['Droits']
        menu_end = ['GPGFingerprint', 'Remarques', 'Blackliste', 'Vente', 'Ticket', 'Supprimer']

        if "cransAccount" in adherent['objectClass']:
            menu_order.extend(menu_compte_crans)
        menu_order.extend(menu_end)

        def box(default_item=None):
            choices = []
            for key in menu_order:
                if self.has_right(menu_droits[key], adherent):
                    choices.append((key, menu[key]['text'], menu[key].get('help', "")))
            return self.dialog.menu(
                "Que souhaitez vous modifier ?",
                width=0,
                height=0,
                menu_height=0,
                timeout=self.timeout,
                item_help=1,
                default_item=unicode(default_item),
                title="Modification de %s %s" % (adherent['prenom'][0], adherent["nom"][0]),
                scrollbar=True,
                cancel_label="Retour",
                backtitle=self._connected_as(),
                choices=choices
            )

        def todo(tag, menu, adherent, cont_ret):
            if not tag in menu_order:
                raise Continue(cont_ret)
            else:
                if 'callback' in menu[tag]:
                    raise Continue(
                        TailCall(
                            menu[tag]['callback'],
                            cont=cont_ret,
                            **{
                                menu[tag].get('adherent', 'adherent') : adherent
                            }
                        )
                    )
                elif 'attribut' in menu[tag]:
                    raise Continue(
                        TailCall(
                            self.modif_adherent_attributs,
                            adherent=adherent,
                            cont=cont_ret,
                            attr=menu[tag]['attribut'].ldap_name
                        )
                    )
                else:
                    raise EnvironmentError(
                        "Il n'y a ni champ 'attribut' ni 'callback' pour le tag %s" % tag
                    )

        (code, tag) = self.handle_dialog(cont, box, tag)
        cont_ret = TailCall(self.modif_adherent, cont=cont, adherent=adherent, tag=tag)

        return self.handle_dialog_result(
            code=code,
            output=tag,
            cancel_cont=cont(proprio=adherent),
            error_cont=cont_ret,
            codes_todo=[([self.dialog.OK], todo, [tag, menu, adherent, cont_ret])]
        )

    def modif_adherent_attributs(self, adherent, attr, cont):
        """Juste un raccourci vers edit_attributs spécifique aux adherents"""
        return self.edit_attributs(
            obj=adherent,
            update_obj='adherent',
            attr=attr,
            title="Modification de %s %s" % (adherent['prenom'][0], adherent['nom'][0]),
            cont=cont
        )

    def adherent_administratif(self, cont, adherent, default_item=None):
        """Menu de gestion du compte crans d'un proprio"""

        a = attributs
        menu_droits = {
            "Adhésion": [a.cableur, a.nounou],
            'Connexion': [a.cableur, a.nounou],
            "Charte MA" : [a.nounou, a.bureau],
        }
        menu = {
            "Adhésion" : {
                "text" : "Pour toute réadhésion *sans* connexion.",
                "help" : "",
                "callback" : self.adherent_adhesion
            },
            'Connexion' : {
                'text' : "Mise à jour de l'accès Internet (effectue la réadhésion si besoin)",
                "help" : "",
                'callback' : self.adherent_connexion
            },
            "Charte MA" : {
                "text" : "Signature de la charte des membres actifs",
                "help" : '',
                "callback":self.adherent_charte
            },
        }
        menu_order = ["Adhésion", 'Connexion']
        menu_order.append("Charte MA")
        def box(default_item=None):
            return self.dialog.menu(
                "Quelle action administrative effectuer",
                width=0,
                height=0,
                timeout=self.timeout,
                item_help=1,
                default_item=unicode(default_item),
                title="Gestion administrative de %s %s" % (adherent.get('prenom', [''])[0], adherent["nom"][0]),
                scrollbar=True,
                cancel_label="Retour",
                backtitle=self._connected_as(),
                choices=[
                    (
                        k,
                        menu[k]['text'],
                        menu[k]['help']
                    ) for k in menu_order if self.has_right(menu_droits[k], adherent)
                ]
            )

        def todo(tag, menu, adherent, self_cont):
            if not tag in menu_order:
                raise Continue(self_cont)
            elif 'callback' in menu[tag]:
                raise Continue(
                    TailCall(
                        menu[tag]['callback'],
                        cont=self_cont,
                        adherent=adherent
                    )
                )
            else:
                raise EnvironmentError(
                    "Il n'y a pas de champs 'callback' pour le tag %s" % tag
                )

        (code, tag) = self.handle_dialog(cont, box, default_item)
        self_cont = TailCall(
            self.adherent_administratif,
            adherent=adherent,
            cont=cont,
            default_item=tag
        )
        return self.handle_dialog_result(
            code=code,
            output=tag,
            cancel_cont=cont,
            error_cont=self_cont,
            codes_todo=[([self.dialog.OK], todo, [tag, menu, adherent, self_cont])]
        )

    def adherent_adhesion_connexion_crediter(self, facture, adherent):
        adhesion = False
        connexion = False
        if [a for a in facture["article"] if a["code"] == "ADH"]:
            adhesion = True
        if [a for a in facture["article"] if a["code"].startswith("CAI")]:
            connexion = True
        # Appeler créditer va créditer ou débiter le solde, sauver le proprio et créer la facture
        if facture.mode == 'rw':
            facture.crediter()
        else:
            with self.conn.search(dn=facture.dn, scope=0, mode='rw')[0] as facture:
                facture.crediter()
        with self.conn.search(dn=adherent.dn, scope=0, mode='rw')[0] as adherent:
            if facture["finAdhesion"]:
                adherent["finAdhesion"].append(facture["finAdhesion"][0])
            if facture["debutAdhesion"]:
                adherent["debutAdhesion"].append(facture["debutAdhesion"][0])
            if facture["debutConnexion"]:
                adherent["debutConnexion"].append(facture["debutConnexion"][0])
            if facture["finConnexion"]:
                adherent["finConnexion"].append(facture["finConnexion"][0])
            adherent.validate_changes()
            adherent.history_gen()
            adherent.save()
        try:
            if adhesion and not connexion:
                self.dialog.msgbox(
                    text=u"Adhésion effectué avec succès",
                    title=u"Adhésion terminé",
                    width=0,
                    height=0,
                    timeout=self.timeout
                )
            elif not adhesion and connexion:
                self.dialog.msgbox(
                    text=u"Connexion prolongée avec succès",
                    title=u"Connexion prolongée",
                    width=0,
                    height=0,
                    timeout=self.timeout
                )
            elif adhesion and connexion:
                self.dialog.msgbox(
                    text=u"Adhésion effectué et connexion prolongée avec succès",
                    title=u"Connexion & Adhésion",
                    width=0,
                    height=0,
                    timeout=self.timeout
                )
        except KeyboardInterrupt:
            pass
        if facture['modePaiement'][0] == "solde":
            try:
                self.dialog.msgbox(
                    text=u"Le solde de l'adhérent à bien été débité",
                    title="Solde débité",
                    width=0,
                    height=0,
                    timeout=self.timeout
                )
            except KeyboardInterrupt:
                pass
        return adherent

    def adherent_adhesion(self, cont, adherent, cancel_cont=None, tag_paiment=None,
                          comment_paiement=None, crediter=True, facture=None):
        """
        Gestion de l'adhésion à l'association d'un adhérent
        Si cancel_cont est à None, cont est utilisé en cas d'annulation
        tag_paiment : un mode de paiement
        comment_paiement : un commentaire pour la facture
        crediter : Doit-on ou non créditer tout de suite la facture
        facture : Doit-t-on éditer une facture existante
        """

        # Boite si on ne peux pas réahdérer
        def box_already(end):
            self.dialog.msgbox(
                "Actuellement adhérent jusqu'au %s.\n"
                "Merci de revenir lorsqu'il restera moins de %s jours avant la fin." % (
                    end,
                    config.cotisation.delai_readh_jour
                ),
                width=0,
                height=0,
                timeout=self.timeout,
            )

        # Boite de confirmation à l'ahésion
        def box_adherer(end=None):
            now = crans_utils.localized_datetime()

            if end < now:
                new_end = now + relativedelta(years=1)
            else:
                new_end = end + relativedelta(years=1)

            if end != crans_utils.localized_datetime():
                adherer = self.confirm(
                    text="Adhésion jusqu'au %s. Réadhérer ?" % new_end,
                    title="Adhésion de %s %s" % (adherent.get("prenom", [''])[0], adherent["nom"][0])
                )
            else:
                adherer = self.confirm(
                    text="Adhésion pour un an, continuer ?",
                    title="Adhésion de %s %s" % (adherent.get("prenom", [''])[0], adherent["nom"][0])
                )
            return adherer

        # Suppression d'une facture si elle est généré mais non validé
        def delete_facture(facture, cont):
            if facture:
                with self.conn.search(dn=facture.dn, scope=0, mode='rw')[0] as facture:
                    facture.delete()
            raise Continue(cont)

        # Génération de la facture pour adhésion
        def paiement(tag_paiement, adherent, finadhesion, comment, facture, cancel_cont, cont):
            now = crans_utils.localized_datetime()
            if hasattr(finadhesion, 'value'):
                finadhesion = finadhesion.value
            new_finadhesion = max(finadhesion, now) + relativedelta(years=1)
            new_debutadhesion = now
            if facture:
                facture = self.conn.search(dn=facture.dn, scope=0, mode='rw')[0]
                to_create = False
            else:
                facture = self.conn.newFacture(adherent.dn, {})
                to_create = True
            with facture as facture:
                facture['modePaiement'] = tag_paiement
                facture['info'] = comment
                facture['article'].append(config.cotisation.dico_adh)
                facture["finAdhesion"] = new_finadhesion
                facture["debutAdhesion"] = new_debutadhesion
                # On peut retarder le credit pour ajouter des contribution pour la connexion internet à la facture
                if crediter:
                    if self.confirm_item(
                        item=facture,
                        text=u"Le paiement de %sEUR a-t-il bien été reçu (mode : %s) ?\n" % (facture.total(), tag_paiement),
                        title=u"Validation du paiement",
                        timeout=self.timeout):
                        # Appeler créditer va créditer ou débiter le solde, sauver le proprio et créer la facture
                        adherent = self.adherent_adhesion_connexion_crediter(facture, adherent)
                    else:
                        if not self.confirm(
                            text=u"Le paiement n'a pas été reçue.\n Annuler ?",
                            title="Annulation de l'adhésion",
                            defaultno=True):
                            raise Continue(cancel_cont)
                else:
                    if to_create:
                        facture.create()
                    else:
                        facture.validate_changes()
                        facture.history_gen()
                        facture.save()
                    raise Continue(cont(facture=facture))
            raise Continue(cont(adherent=adherent))


        now = crans_utils.localized_datetime()
        try:
            finadhesion = adherent.fin_adhesion().value
        except AttributeError:
            finadhesion = now
        # Si fin de l'adhésion trop loin dans le futur, rien a faire
        if finadhesion and (finadhesion - now).days > config.cotisation.delai_readh_jour:
            self.handle_dialog(cancel_cont if cancel_cont else cont, box_already, finadhesion)
            raise Continue(cancel_cont if cancel_cont else cont)

        # Sinon, si on accepte l'adhésion
        elif tag_paiment or self.handle_dialog(cont, box_adherer, finadhesion):
            self_cont = TailCall(
                self.adherent_adhesion,
                cont=cont,
                adherent=adherent,
                cancel_cont=cancel_cont,
                tag_paiment=tag_paiment,
                comment_paiement=comment_paiement,
                crediter=crediter,
                facture=facture
            )
            # On choisi un mode de paiement
            if not tag_paiment or not comment_paiement:
                return self.proprio_choose_paiement(
                    proprio=adherent,
                    cont=self_cont,
                    cancel_cont=TailCall(
                        delete_facture,
                        facture,
                        cancel_cont if cancel_cont else cont
                    )
                )
            else:
                lcont = self_cont.copy()
                lcont(comment_paiement=None)
                return self.handle_dialog_result(
                    code=self.dialog.OK,
                    output=[],
                    cancel_cont=lcont,
                    error_cont=lcont,
                    codes_todo=[
                        (
                            [self.dialog.OK],
                            paiement,
                            [
                                tag_paiment,
                                adherent,
                                finadhesion,
                                comment_paiement,
                                facture,
                                lcont,
                                cont
                            ]
                        )
                    ]
                )
        else:
            return self.handle_dialog_result(
                code=self.dialog.OK,
                output=[],
                cancel_cont=None,
                error_cont=cancel_cont if cancel_cont else cont,
                codes_todo=[
                    (
                        [self.dialog.OK],
                        delete_facture,
                        [facture, cancel_cont if cancel_cont else cont]
                    )
                ]
            )

    def adherent_connexion(self, cont, adherent, cancel_cont=None, facture=None, mois=None,
                           default_item=None, tag_paiment=None, comment_paiement=None):
        """
        Prolonger la connexion d'un adhérent
        Si cancel_cont est à None, cont sera utilisé
        facture : doit-on éditer une facture existante
        mois : de combien de mois prolonger la connexion
        default_item : le nombre de mois selectionné par defaut
        tag_paiment : le mode de paiement a utiliser si on crée une facture
        comment_paiement : un commentaire à mettre si on crée une facture
        """
        menu = {
            "An": {
                'text' : "Prolonger d'un an (pour %s€)" % config.cotisation.plafond_contribution,
                'callback' : TailCall(
                    self.adherent_connexion,
                    cont,
                    adherent,
                    cancel_cont,
                    facture,
                    12,
                    default_item,
                    tag_paiment,
                    comment_paiement
                )
            },
            "NC": {
                'text' : "Pas de connexion",
                'callback' : TailCall(
                    self.adherent_connexion,
                    cont,
                    adherent,
                    cancel_cont,
                    facture,
                    12,
                    default_item,
                    tag_paiment,
                    comment_paiement
                )
            },
        }
        menu_order = ["An"]
        for i in range(1, config.cotisation.duree_conn_plafond):
            if config.cotisation.contribution * i < config.cotisation.plafond_contribution:
                menu["%s mois" % i] = {
                    'text' : "Prolonger de %s mois (pour %s€)" % (
                        i,
                        config.cotisation.contribution * i
                    ),
                    'callback' : TailCall(
                        self.adherent_connexion,
                        cont,
                        adherent,
                        cancel_cont,
                        facture,
                        i,
                        default_item,
                        tag_paiment,
                        comment_paiement
                    )
                }
                menu_order.append("%s mois" % i)

        if facture:
            menu_order.append("NC")

        # Une boite pour choisir un nombre de mois pour prolonger la connexion
        def box(finconnexion, default_item=None):
            t_end = finconnexion
            return self.dialog.menu(
                "Connexion jusqu'au %s" % t_end if finconnexion != datetime.datetime.fromtimestamp(0, tz=pytz.utc) else "N'a jamais été connecté",
                width=0,
                height=0,
                menu_height=0,
                timeout=self.timeout,
                item_help=0,
                default_item=unicode(default_item),
                title="Connexion de %s %s" % (adherent['prenom'][0], adherent["nom"][0]),
                scrollbar=True,
                cancel_label="Retour",
                backtitle=self._connected_as(),
                choices=[(k, menu[k]['text']) for k in menu_order]
            )

        # Génération et crédit de la facture
        def todo(adherent, mois, finadhesion, finconnexion, cancel_cont,
                 cont, facture=None, tag_paiment=None, comment=None):
            # TODO : utiliser la methode lc_ldap adhesion_connexion()
            now = crans_utils.localized_datetime()
            if isinstance(finconnexion, attributs.generalizedTimeFormat):
                finconnexion = finconnexion.value
            new_debutconnexion = max(now, finconnexion)
            new_finconnexion = new_debutconnexion + relativedelta(months=mois)

            if new_debutconnexion > finadhesion.value:
                raise ValueError(
                    "Le début de la connexion (%s) est après la fin de l'adhésion (%s)." % (
                        new_debutconnexion,
                        finadhesion.value
                    )
                )
            if (new_finconnexion - finadhesion.value).days > 0:
                t_end_adh = finadhesion.value
                t_end_conn = new_finconnexion
                if not self.confirm(
                    "La fin de la connexion de l'adhérent (%s) tombera "
                    "après la fin de son adhésion (%s).\n"
                    "S'il veut en profiter, il lui faudra "
                    "éventuellement réadhérer. Continuer ?" % (t_end_conn, t_end_adh),
                    title="Prolongement de connexion"):
                    raise Continue(cancel_cont)
            # On édite une facture existante
            if facture:
                with self.conn.search(dn=facture.dn, scope=0, mode='rw')[0] as facture:
                    if mois:
                        facture["finConnexion"] = new_finconnexion
                        facture["debutConnexion"] = new_debutconnexion
                        facture["article"].append(config.cotisation.dico_cotis(mois))
                    if self.confirm_item(
                        item=facture,
                        text=u"Le paiement de %sEUR a-t-il bien été reçu (mode : %s) ?\n" % (
                            facture.total(),
                            facture['modePaiement'][0]
                        ),
                        title=u"Validation du paiement",
                        timeout=self.timeout):
                        # Appeler créditer va créditer ou débiter le solde, sauver le proprio et crée
                        adherent = self.adherent_adhesion_connexion_crediter(facture, adherent)
                    else:
                        if not self.confirm(
                            text=u"Le paiement n'a pas été reçue.\n Annuler ?",
                            title="Annulation de l'adhésion",
                            defaultno=True):
                            raise Continue(cancel_cont)
                        else:
                            facture.delete()

            else:
                if tag_paiment is None or comment is None:
                    raise ValueError(
                        "Il faut définir une méthode de paiement avec un commentaire"
                    )
                if not mois:
                    raise ValueError(
                        "Il faut prolonger la connexion d'un nombre de mois strictement positif"
                    )
                with self.conn.newFacture(adherent.dn, {}) as facture:
                    facture['modePaiement'] = tag_paiment
                    facture['article'].append(config.cotisation.dico_cotis(mois))
                    facture['info'] = comment
                    facture["finConnexion"] = new_finconnexion
                    facture["debutConnexion"] = new_debutconnexion
                    if self.confirm_item(
                        item=facture,
                        text=u"Le paiement de %sEUR a-t-il bien été reçu (mode : %s) ?\n" % (
                            facture.total(),
                            tag_paiment
                        ),
                        title=u"Validation du paiement",
                        timeout=self.timeout):
                        # Appeler créditer va créditer ou débiter le solde, sauver le proprio et crée
                        adherent = self.adherent_adhesion_connexion_crediter(facture, adherent)
                    else:
                        if not self.confirm(
                            text=u"Le paiement n'a pas été reçue.\n Annuler ?",
                            title="Annulation de l'adhésion",
                            defaultno=True):
                            raise Continue(cancel_cont)
            raise Continue(cont)

        def todo_mois(tag, self_cont):
            if tag == 'An':
                mois = 12
            elif tag == 'NC':
                mois = 0
            else:
                mois = int(tag.split(' ', 1)[0])
            raise Continue(self_cont(mois=mois, default_item=tag))

        self_cont = TailCall(
            self.adherent_connexion,
            cont=cont,
            adherent=adherent,
            cancel_cont=cancel_cont,
            facture=facture,
            mois=mois,
            default_item=default_item,
            tag_paiment=tag_paiment,
            comment_paiement=comment_paiement
        )

        finadhesion = adherent.fin_adhesion()
        # Si on édite une facture, on prolonge la date de finadhesion
        if facture and facture["finAdhesion"]:
            finadhesion = max(finadhesion, facture["finAdhesion"][0])
        finconnexion = adherent.fin_connexion()

        # Si l'adhésion fini avant la nouvelle fin de connexion, on réadhère si on est dans le delais, sinon warning simple sur la réadhésion
        now = crans_utils.localized_datetime()
        if (finadhesion <= now or
            finadhesion <= finconnexion):
            if finadhesion:
                # Si l'adhésion est déjà fini
                if finadhesion <= now:
                    if finadhesion == datetime.datetime.fromtimestamp(0, tz=pytz.utc):
                        self.dialog.msgbox(
                            text=u"L'adhérent n'a jamais adhéré à l'association, "
                                  "on va d'abord le faire adhérer (10€)",
                            title="Adhésion nécessaire",
                            width=0,
                            height=0,
                            timeout=self.timeout
                        )
                    else:
                        self.dialog.msgbox(
                            text=u"L'adhésion a expiré le %s, il va falloir "
                                  "réadhérer d'abord (10€)" % finadhesion,
                            title="Réadhésion nécessaire",
                            width=0,
                            height=0,
                            timeout=self.timeout
                        )
                # Sinon si elle fini avant la fin de la connexion courante
                elif finadhesion < finconnexion:
                    t_end_conn = finconnexion
                    self.dialog.msgbox(
                        text=u"L'adhésion de termine le %s, avant la fin de la connexion "
                              "le %s, il va falloir réadhérer d'abord (10€)" % (finadhesion, t_end_conn),
                        title="Réadhésion nécessaire",
                        width=0,
                        height=0,
                        timeout=self.timeout
                    )
                # Échouera si on essaie de prolonger la connexion au dela de l'adhésion et que l'adhésion est encore valable plus de quinze jours
            return self.adherent_adhesion(
                cont=self_cont,
                cancel_cont=cont,
                adherent=adherent,
                crediter=False
            )

        # Si on édite une facture, elle vient actuellement forcement de adherent_adhesion
        if facture and cancel_cont is None:
            cancel_cont = TailCall(
                self.adherent_adhesion,
                cont=self_cont,
                adherent=adherent,
                cancel_cont=cont,
                tag_paiment=facture['modePaiement'][0] if facture['modePaiement'] else None,
                comment_paiement=None,
                crediter=False,
                facture=facture
            )
            self_cont(cancel_cont=cancel_cont)

        # On choisi le nombre de mois pour prolonger la connexion
        if mois is None:
            (code, tag) = self.handle_dialog(cont, box, finconnexion, default_item)
            return self.handle_dialog_result(
                code=code,
                output=[],
                cancel_cont=cancel_cont if cancel_cont else cont,
                error_cont=self_cont,
                codes_todo=[([self.dialog.OK], todo_mois, [tag, self_cont])]
            )
        # Si on connait le moyen de paiement (il peut être a l'intérieure de la facture existante)
        elif tag_paiment or facture:
            lcont = self_cont.copy()
            if facture:
                lcont(mois=None)
            else:
                lcont(tag_paiment=None)
            return self.handle_dialog_result(
                    code=self.dialog.OK,
                    output=[],
                    cancel_cont=lcont,
                    error_cont=lcont,
                    codes_todo=[
                        (
                            [self.dialog.OK],
                            todo,
                            [
                                adherent,
                                mois,
                                finadhesion,
                                finconnexion,
                                lcont,
                                cont,
                                facture,
                                tag_paiment,
                                comment_paiement
                            ]
                        )
                    ]
                )
        # Sinon, il faut choisir une méthode de paiement
        else:
            lcont = self_cont.copy()
            lcont(mois=None)
            return self.proprio_choose_paiement(
                proprio=adherent,
                cont=self_cont,
                cancel_cont=lcont
            )
        return cont

    def adherent_charte(self, cont, adherent):
        a = attributs
        attribs = [a.charteMA]
        return self.edit_boolean_attributs(
            obj=adherent,
            attribs=attribs,
            title="Signature de la charte membre actif de %s %s" % (
                adherent['prenom'][0],
                adherent['nom'][0]
            ),
            update_obj='adherent',
            cont=cont
        )

    def create_adherent(self, cont):
        """Crée un adhérent et potentiellement son compte crans avec lui"""
        def mycont(proprio=None, **kwargs):
            if proprio:
                # Une fois l'adhérent créé, on vois s'il adhére/prend la connexion internet
                #adh_cont = TailCall(self.modif_adherent, cont=cont, adherent=adherent)
                conn_cont = TailCall(
                    self.adherent_connexion,
                    cont=cont(proprio=proprio),
                    adherent=proprio
                )
                etude_cont = TailCall(
                    self.adherent_etudes,
                    cont=conn_cont,
                    adherent=proprio
                )
                etude_cont(cancel_cont=etude_cont)
                # Comme on crée une facture, pas de retour possible
                conn_cont(cancel_cont=conn_cont)
                raise Continue(etude_cont)
            else:
                raise Continue(cont)
        return self.proprio_personnel(cont=TailCall(mycont))

    def delete_adherent(self, cont, del_cont, adherent=None):
        """Permet la suppression d'un adhérent de la base ldap"""
        if adherent is None:
            adherent = self.select(
                ["adherent"],
                "Recherche d'un adhérent pour supression",
                cont=cont
            )

        def todo(adherent):
            if self.confirm_item(
                item=adherent,
                title="Voulez vous vraiement supprimer l'adhérent ?",
                defaultno=True):
                with self.conn.search(dn=adherent.dn, scope=0, mode='rw')[0] as adherent:
                    adherent.delete()
                self.dialog.msgbox(
                    "L'adherent a bien été supprimé",
                    timeout=self.timeout,
                    title="Suppression d'un adherent"
                )
                raise Continue(del_cont(proprio=None))
            else:
                raise Continue(cont)

        return self.handle_dialog_result(
            code=self.dialog.OK,
            output="",
            cancel_cont=cont(adherent=adherent),
            error_cont=cont(adherent=adherent),
            codes_todo=[([self.dialog.OK], todo, [adherent])]
        )

    def adherent_droits(self, adherent, cont, choices_values=None):
        """Gestion des droits d'un adhérent"""
        def box():
            return self.dialog.checklist(
                text="",
                height=0,
                width=0,
                list_height=0,
                choices=choices_values if choices_values else [
                    (
                        droit,
                        "",
                        1 if droit in adherent["droits"] else 0
                    ) for droit in attributs.TOUS_DROITS
                ],
                title="Droits de %s %s" % (adherent['prenom'][0], adherent['nom'][0]),
                timeout=self.timeout
            )
        def todo(droits, adherent, self_cont, cont):
            # Les vérifications de sécurité sont faites dans lc_ldap
            with self.conn.search(dn=adherent.dn, scope=0, mode='rw')[0] as adherent:
                adherent['droits'] = [unicode(d) for d in droits]
                adherent.validate_changes()
                adherent.history_gen()
                adherent.save()
                if adherent["uid"] and adherent["uid"][0] == self.conn.current_login:
                    self.check_ldap()
            raise Continue(cont(adherent=adherent))

        (code, droits) = self.handle_dialog(cont, box)
        self_cont = TailCall(
            self.adherent_droits,
            adherent=adherent,
            cont=cont,
            choices_values=[
                (
                    d,
                    "",
                    1 if d in droits else 0
                ) for d in attributs.TOUS_DROITS
            ]
        )
        return self.handle_dialog_result(
            code=code,
            output=droits,
            cancel_cont=cont,
            error_cont=self_cont,
            codes_todo=[([self.dialog.OK], todo, [droits, adherent, self_cont, cont])]
        )


    def adherent_etudes(self, adherent, cont, cancel_cont=None, default_item=None, etablissement=None):
        """Gestion des études de l'adhérent. etudes est un triplet (établissement, année, section)"""
        choices_etablissement = [
          ('Autre', ''),
          ('ENS', 'École Normale Supérieure'),
          ('IUT Cachan', ''),
          ('Maximilien Sorre', ''),
          ('Gustave Eiffel', ''),
          ('EFREI', ''),
          ('ESTP', ''),
          ('P1', 'Université Panthéon Sorbonne'),
          ('P2', 'Université Panthéon Assas'),
          ('P3', 'Université de la Sorbonne Nouvelle'),
          ('P4', 'Université Paris Sorbonne'),
          ('P5', 'Université René Descartes'),
          ('P6', 'Université Pierre et Marie Curie'),
          ('P7', 'Université Paris Diderot'),
          ('P8', 'Université Vincennes Saint Denis'),
          ('P9', 'Université Paris Dauphine'),
          ('P10', 'Université de Nanterre'),
          ('P11', 'Université de Paris Sud (Orsay)'),
          ('P12', 'Université Val de Marne (Créteil)'),
          ('P13', 'Université Paris Nord'),
          ('IUFM', ''),
          ('Personnel ENS', "appartement ENS ou personnel CROUS"),
        ]
        LMD = ['P1', 'P2', 'P3', 'P4', 'P5', 'P6', 'P7', 'P8', 'P9', 'P10', 'P11', 'P12', 'P13']
        LM = ['EFREI']
        LYCEE = ['Maximilien Sorre', 'Gustave Eiffel']
        ANNEE = ['IUT Cachan', 'ESTP']


        def box_etablissement(default_item):
            if (etablissement == 'Autre' or
                (etablissement is not None and etablissement not in [i[0] for i in choices_etablissement]) or
                (default_item is not None and default_item not in [i[0] for i in choices_etablissement])):
                return self.dialog.inputbox(
                    text="Choisissez l'établissement :",
                    title="Études de %s %s" % (adherent['prenom'][0], adherent["nom"][0]),
                    timeout=self.timeout,
                    init=unicode(default_item) if default_item else ""
                )
            else:
                return self.dialog.menu(
                    "Choisissez l'établissement :",
                    width=0,
                    height=0,
                    menu_height=0,
                    timeout=self.timeout,
                    item_help=0,
                    default_item=default_item if default_item else 'ENS',
                    title="Études de %s %s" % (adherent['prenom'][0], adherent["nom"][0]),
                    scrollbar=True,
                    cancel_label="Retour",
                    backtitle=self._connected_as(),
                    choices=choices_etablissement
                )

        self_cont = TailCall(
            self.adherent_etudes,
            adherent=adherent,
            cont=cont,
            cancel_cont=cancel_cont,
            default_item=default_item,
            etablissement=etablissement
        )
        if etablissement is None or etablissement == 'Autre':
            if not default_item and adherent["etudes"]:
                default_item = unicode(adherent["etudes"][0])
            lcont = cancel_cont if cancel_cont else cont
            (code, etablissement) = self.handle_dialog(cancel_cont, box_etablissement, default_item)
            output = etablissement
            self_cont(default_item=etablissement)
        else:
            output = ""
            lcont = TailCall(
                self.adherent_etudes,
                adherent=adherent,
                cont=cont,
                cancel_cont=cancel_cont,
                default_item=section,
                etablissement=etablissement
            )

        def todo(etablissement, adherent, self_cont, cont):
            if etablissement is None or etablissement == 'Autre':
                raise Continue(self_cont(default_item=None, etablissement=etablissement))
            else:
                if not adherent["etudes"] or adherent["etudes"][0] != etablissement:
                    with self.conn.search(dn=adherent.dn, scope=0, mode='rw')[0] as adherent:
                        adherent["etudes"] = unicode(etablissement)
                        adherent.validate_changes()
                        adherent.history_gen()
                        adherent.save()
                raise Continue(cont(adherent=adherent))
        return self.handle_dialog_result(
            code=code,
            output=output,
            cancel_cont=lcont,
            error_cont=self_cont,
            codes_todo=[([self.dialog.OK], todo, [etablissement, adherent, self_cont, cont])]
        )

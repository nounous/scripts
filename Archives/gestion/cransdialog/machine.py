#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

"""
Copyright (C) Valentin Samir
Licence : GPLv3

"""

from __future__ import unicode_literals

import sys

import lc_ldap.objets as objets
import lc_ldap.attributs as attributs
import subprocess

from gestion import config

import certificat
import blacklist
from CPS import TailCall, tailcaller, Continue

class Dialog(certificat.Dialog, blacklist.Dialog):
    def machine_information(self, cont, machine=None, objectClass=None,
                            proprio=None, realm=None, fields_values=None):
        """
        Permet de modifier une machine si elle est fournit par le paramètre machine
        sinon, crée une machine à partir de proprio, objectClass et realm.
        Si on ne fait qu'éditer une machine, proprio, objectClass et realm sont ignoré
        D'une machinère générale, il faudrait mettre ici tous les attributs single value
        et les multivalué que l'on peut simplement représenter de façon textuelle avec
        un séparateur.
        Pour le moment il y a :
            * host
            * macAddress
            * ipHostNumber
            * port(TCP|UDP)(in|out)
        """
        a = attributs
        # Quel sont les attributs ldap dont on veut afficher et la taille du champs d'édition correspondant
        to_display = [(a.host, 50),
                      (a.macAddress, 17),
                      (a.ipHostNumber, 15)]

        to_display_port = [(a.portTCPout, 50),
                           (a.portTCPin, 50),
                           (a.portUDPout, 50),
                           (a.portUDPin, 50)]

        to_display_borne = [(a.canal, 10),
                            (a.puissance, 10),
                            (a.positionBorne, 50),
                            (a.prise, 10), (a.untagvlan, 10)]

        to_display_machine_crans = [(a.prise, 10), (a.untagvlan, 10)]

        to_display_switchs = [(a.nombrePrises, 10)]

        # Quel séparateur on utilise pour les champs multivalué
        separateur = ' '

        # Quelle est la taille du plus grand champ utilisé
        max_size = max(map(lambda x: len(x[0].legend), to_display)) + 3

        def box():
            if machine:
                attrs = dict((k,[unicode(a) for a in at]) for k,at in machine.items())
            else:
                attrs = {}

            fields = [
                (
                    "%s :" % a.legend,
                    num + 1,
                    1,
                    separateur.join(attrs.get(a.ldap_name, [a.default] if a.default else [])),
                    num + 1,
                    max_size,
                    l + 1,
                    l
                ) for num, (a,l) in enumerate(to_display)
            ]

            return self.dialog.form(
                "",
                fields_values if fields_values else fields,
                height=0,
                width=0,
                form_height=0,
                timeout=self.timeout,
                title="Paramètres machine",
                backtitle="Gestion des machines du Crans"
            )

        def check_host(host, objectClass, realm):
            # Si c'est une machine wifi, host doit finir par wifi.crans.org
            if objectClass == "machineWifi":
                hostend = ".wifi.crans.org"
            # Si c'est une machine fixe natée, host doit finir par fil.crans.org
            elif objectClass == "machineFixe" and realm == 'fil-new-adherents':
                hostend = ".fil.crans.org"
            # Si c'est une machine fixe (vlan 23), host doit finir par adh.crans.org
            elif objectClass == "machineFixe" and realm == 'fil-pub':
                hostend = ".adh.crans.org"
            # Les machines Crans/bornes sont gérées de façon plus souple.
            elif (realm in ['bornes', 'serveurs', 'switches', 'dmz', 'fil-pub', 'wifi-new-serveurs', 'fil-new-serveurs'] or
                  objectClass in ['machineCrans', 'borneWifi', 'switchCrans']):
                if realm == 'serveurs' or realm == 'dmz':
                    hostend = ".crans.org"
                elif realm == 'adm':
                    hostend = ".adm.crans.org"
                elif realm == 'wifi-new-serveurs':
                    hostend = ".wifi.crans.org"
                elif realm == 'fil-new-serveurs':
                    hostend = ".fil.crans.org"
                elif realm == "switches":
                    hostend = ".switches.crans.org"
                elif realm == 'bornes':
                    hostend = ".borne.crans.org"
                elif realm == "accueil":
                    hostend = ".accueil.crans.org"
                elif realm == "isolement":
                    hostend = ".isolement.crans.org"
                elif realm == "evenementiel":
                    hostend = ".event.crans.org"
                elif realm == 'fil-pub':
                    hostend = '.adh.crans.org'
                if not '.' in host:
                    host = host + hostend
                return host
            # Sinon, libre à chacun d'ajouter d'autres objectClass ou de filtrer
            # plus finement fonction des droits de self.conn.droits
            else:
                raise ValueError(
                    "La machine est de type %r, qui n'est pas pris en charge." % (objectClass,)
                )

            if not host.endswith(hostend) and not '.' in host:
                host = host + hostend
            elif host.endswith(hostend) and '.' in host[:-len(hostend)]:
                raise ValueError(
                    "Nom d'hôte invalide, devrait finir par %s et être sans point dans la première partie" % hostend
                )
            elif not host.endswith(hostend) and '.' in host:
                raise ValueError(
                    "Nom d'hôte invalide, devrait finir par %s et être sans point dans la première partie" % hostend
                )

            return host

        def modif_machine(machine, attrs):
            with self.conn.search(dn=machine.dn, scope=0, mode='rw')[0] as machine:
                for (key, values) in attrs.items():
                    if values!=u'<automatique>' or key != 'ipHostNumber':
                        machine[key]=values
                machine.validate_changes()
                machine.history_gen()
                machine.save()
            self.dialog.msgbox(
                "Modification prise en compte avec succès",
                title="Édition des attributs de la machine %s" % machine['host'][0],
                width=70, timeout=self.timeout
            )
            return machine

        def create_machine(proprio, realm, attrs, update_obj='proprio'):
            # Dans ce cas, on a besoin d'un proprio et d'un realm pour déterminer le rid
            if proprio is None or realm is None:
                raise EnvironmentError(
                    "On essaye de créer une machine mais proprio ou realm vaut None"
                )
            ldif = {
                'macAddress': ['%s' % attrs['macAddress']],
                'host': ['%s' % attrs['host']]
            }
            with self.conn.newMachine(proprio.dn, realm, ldif) as machine:
                for (key, values) in attrs.items():
                    if values!=u'<automatique>' or key != u'ipHostNumber':
                        machine[key]=values
                if attributs.ipsec in machine.attribs:
                    machine[attributs.ipsec.ldap_name]=attributs.ipsec.default
                machine.validate_changes()
                if self.confirm_item(machine, "Voulez-vous vraiment créer cette machine ?"):
                    machine.create()
                    self.display_item(machine, "La machine a bien été créée", ipsec=True)
                    if realm == 'wifi-new-adherents':
                        if self.dialog.yesno(
                            "Imprimer un ticket pour la machine ?",
                            timeout=self.timeout,
                            title="Impression de ticket",
                            width=50) == self.dialog.OK:
                            subprocess.call(
                                [
                                    '/usr/scripts/cransticket/dump_creds.py',
                                    '--forced',
                                    'mid=%s' % machine['mid'][0],
                                ]
                            )
                            self.display_item(
                                machine,
                                "Impression du ticket ...",
                                ipsec=True
                            )
                    return machine
                else:
                    raise Continue(cont(**{update_obj:proprio}))

        def todo(to_display, tags, objectClass, machine, proprio, realm, separateur, cont):
            attrs = {}
            if machine and not realm:
                host=machine["host"][0]
                if host.endswith(".fil.crans.org"):
                    realm="fil-new-adherents"
                elif host.endswith(".adh.crans.org"):
                    realm="fil-pub"
            # On traite les valeurs reçues
            for ((a,l),values) in zip(to_display, tags):
                values = unicode(values)
                # Si le champs n'est pas single value, on utilise separateur pour découper
                # et on ne garde que les valeurs non vides
                if not a.singlevalue:
                    values = [v for v in values.split(separateur) if v]
                # Pour host, on fait quelques vérification de syntaxe
                if a.ldap_name == 'host':
                    attrs[a.ldap_name] = check_host(values, objectClass, realm)
                else:
                    attrs[a.ldap_name]=values
            # Soit on édite une machine existante
            if machine:
                machine = modif_machine(machine, attrs)
            # Soit on crée une nouvelle machine
            else:
                machine = create_machine(proprio, realm, attrs)
                proprio.machines(refresh=True)
            raise Continue(cont(machine=machine))

        if machine:
            objectClass = machine["objectClass"][0]
            host = machine["host"][0]

        if self.has_right(a.nounou, proprio):
            to_display += to_display_port

        # Les bornes wifi et machines Crans ont un to_display différent
        if objectClass == 'borneWifi':
            to_display += to_display_borne
        if objectClass == 'switchCrans':
            to_display += to_display_switchs
        if objectClass == 'machineCrans':
            to_display += to_display_machine_crans

        (code, tags) = self.handle_dialog(cont, box)

        # On prépare les fiels à afficher à l'utilisateur si une erreur a lieu
        # pendant le traitement des donnée (on n'éfface pas ce qui a déjà été entré
        # c'est au cableur de corriger ou d'annuler
        fields_values = [
            (
                "%s :" % a.legend,
                num + 1,
                1,
                values,
                num + 1,
                max_size,
                l + 1,
                l
            ) for num, ((a, l), values) in enumerate(zip(to_display, tags))
        ]
        retry_cont = TailCall(
            self.machine_information,
            machine=machine,
            cont=cont,
            objectClass=objectClass,
            proprio=proprio,
            realm=realm,
            fields_values=fields_values
        )

        return self.handle_dialog_result(
            code=code,
            output=tags,
            cancel_cont=cont,
            error_cont=retry_cont,
            codes_todo=[
                (
                    [self.dialog.OK],
                    todo,
                    [
                        to_display,
                        tags,
                        objectClass,
                        machine,
                        proprio,
                        realm,
                        separateur,
                        cont
                    ]
                )
            ]
        )

    def modif_machine_blacklist(self, machine, cont):
        """Raccourci vers edit_blacklist spécifique aux machines"""
        return self.edit_blacklist(
            obj=machine,
            title=u"Édition des blacklist de la machine %s" % machine['host'][0],
            update_obj='machine',
            cont=cont
        )

    def modif_machine_attributs(self, machine, attr, cont):
        """Juste un raccourci vers edit_attributs spécifique aux machines"""
        return self.edit_attributs(
            obj=machine,
            update_obj='machine',
            attr=attr,
            title=u"Modification de la machine %s" % machine['host'][0],
            cont=cont
        )

    def modif_machine_boolean(self, machine, cont):
        """Juste un raccourci vers edit_boolean_attributs spécifique aux machines"""
        a = attributs
        attribs = [a.dnsIpv6]
        if machine['objectClass'][0] in ['borneWifi', 'machineCrans', 'switchCrans']:
            attribs += [a.statut]
        if machine['objectClass'][0] in ['borneWifi']:
            attribs += [a.hotspot, a.pontwifi]
        return self.edit_boolean_attributs(
            obj=machine,
            attribs=attribs,
            title="Édition des attributs booléens de la machine %s" % machine['host'][0],
            update_obj='machine',
            cont=cont
        )

    def modif_machine(self, cont, machine=None, tag=None):
        """
        Permet d'éditer une machine. Si fournie en paramètre on éditer en place,
        sinon, on en cherche une dans la base ldap
        """
        if machine is None:
            machine = self.select(
                [
                    "machineFixe",
                    "machineWifi",
                    "machineCrans",
                    "borneWifi",
                    "switchCrans"
                ],
                "Recherche d'une machine pour modification",
                cont=cont
            )
        a = attributs
        menu_droits = {
          'Information' : [a.parent, a.cableur, a.nounou],
          'Autre': [a.parent, a.cableur, a.nounou],
          'Blackliste':[a.nounou],
          'Certificat': [a.parent, a.nounou],
          'Alias' : [a.parent, a.cableur, a.nounou],
          'Remarques' : [a.cableur, a.nounou],
          'SshKey' : [a.parent, a.nounou],
          'Supprimer' : [a.parent, a.cableur, a.nounou],
        }
        menu = {
            'Information' : {
                'text' : u"Modifier le nom de machine, l'IP, adresse MAC",
                "callback":self.machine_information
            },
            'Autre' : {
                'text' : u"Modifier les attribut booléen comme actif/inactif ou dnsIpv6",
                "callback" : self.modif_machine_boolean
            },
            'Blackliste' : {
                'text' : u'Modifier les blacklist de la machine',
                'callback' : self.modif_machine_blacklist
            },
            'Certificat' : {
                'text' : u'Modifier les certificats de la machine',
                'callback' : self.modif_machine_certificat
            },
            'Alias' : {
                'text' : u'Créer ou supprimer un alias de la machine',
                'attribut' : attributs.hostAlias
            },
            'Remarques' : {
                'text' : u'Ajouter ou supprimer une remarque de la machine',
                'attribut' : attributs.info
            },
            'SshKey' : {
                'text' : u'Ajouter ou supprimer une clef ssh pour la machine',
                'attribut':attributs.sshFingerprint
            },
            'Supprimer' : {
                'text' : u'Supprimer la machine',
                'callback' : self.delete_machine
            },
        }
        menu_order = [
            'Information',
            'Blackliste',
            'Certificat',
            'Alias',
            'SshKey',
            'Autre',
            'Remarques',
            'Supprimer'
        ]
        def box(default_item=None):
            return self.dialog.menu(
                "Que souhaitez-vous modifier ?",
                width=0,
                height=0,
                menu_height=0,
                timeout=self.timeout,
                item_help=0,
                default_item=unicode(default_item),
                title="Modification de %s" % machine['host'][0],
                scrollbar=True,
                cancel_label="Retour",
                backtitle=self._connected_as(),
                choices=[
                    (
                        key,
                        menu[key]['text']
                    ) for key in menu_order if self.has_right(menu_droits[key], machine)
                ]
            )

        def todo(tag, menu, machine, cont_ret):
            if not tag in menu_order:
                raise Continue(cont_ret)
            else:
                if 'callback' in menu[tag]:
                    raise Continue(
                        TailCall(
                            menu[tag]['callback'],
                            machine=machine,
                            cont=cont_ret
                        )
                    )
                elif 'attribut' in menu[tag]:
                    raise Continue(
                        TailCall(
                            self.modif_machine_attributs,
                            machine=machine,
                            cont=cont_ret,
                            attr=menu[tag]['attribut'].ldap_name
                        )
                    )
                else:
                    raise EnvironmentError(
                        "Il n'y a ni champ 'attribut' ni 'callback' pour le tag %s" % tag
                    )

        (code, tag) = self.handle_dialog(cont, box, tag)
        cont_ret = TailCall(
            self.modif_machine,
            cont=cont,
            machine=machine,
            tag=tag
        )

        return self.handle_dialog_result(
            code=code,
            output=tag,
            cancel_cont=cont(machine=machine),
            error_cont=cont_ret,
            codes_todo=[([self.dialog.OK], todo, [tag, menu, machine, cont_ret])]
        )

    def create_machine_proprio(self, cont, proprio, tag=None):
        """Permet d'ajouter une machine à un proprio (adherent, club ou AssociationCrans)"""
        a = attributs
        menu_droits = {
            'Fixe-new' : [a.soi, a.cableur, a.nounou],
            'Wifi-new' : [a.soi, a.cableur, a.nounou],
            'Fil-pub' : [a.nounou],
        }
        menu = {
            'Fixe-new' : {
                'text' : "Machine filaire natée",
                'objectClass' : 'machineFixe',
                'realm' : 'fil-new-adherents'
            },
            'Wifi-new' : {
                'text' : 'Machine sans fil natée',
                'objectClass' : 'machineWifi',
                'realm':'wifi-new-adherents'
            },
            'Fil-pub' : {
                'text': 'Ajouter une machine sur le vlan ip pub filaires adhérents (23), (185.230.78.0/24)',
                'objectClass':'machineFixe',
                'realm':'fil-pub'
            },
        }
        menu_order = ['Wifi-new', 'Fil-pub', 'Fixe-new']

        # On vérifie qu'un non MA a au plus max_machines_fixes machines fixes
        if (not bool(proprio.get('droits', False)) and
            isinstance(proprio, objets.adherent)):
            # On compte le nombre de machines fixes de l'adhérent.
            if len(
                filter(
                    lambda x: isinstance(x, objets.machineFixe),
                    proprio.machines()
                )
            ) >= config.max_machines_fixes:
                menu_order.remove('Fixe-new')

        if isinstance(proprio, objets.AssociationCrans):
            menu_droits = {
                'Fil-pub' : [a.nounou],
                'Fixe' : [a.nounou],
                'Wifi' : [a.nounou],
                'Fil-new-serveur' : [a.nounou],
                'Wifi-new-serveur' : [a.nounou],
                'Dmz' : [a.nounou],
                'Adm' : [a.nounou],
                'Accueil' : [a.nounou],
                'Isolement' : [a.nounou],
                'Evenementiel' : [a.nounou],
                'Switch' : [a.nounou],
            }
            menu = {
                'Fil-pub' : {
                    'text': 'Ajouter une machine sur le vlan ip pub filaires adhérents (23), (185.230.78.0/24)',
                    'objectClass':'machineCrans',
                    'realm':'fil-pub'
                 },
                'Fixe' : {
                    'text' : "Ajouter un serveur sur le vlan adherent (138.231.136.0/24)",
                    'objectClass' : 'machineCrans',
                    'realm':'serveurs'
                },
                'Wifi' : {
                    'text' : 'Ajouter une borne WiFi sur le vlan wifi (3) 10.231.148.0/24',
                    'objectClass' : 'borneWifi',
                    'realm':'bornes'
                },
                'Wifi-new-serveur' : {
                    'text': 'Ajouter un serveur sur le vlan wifi-new (22), bloc réservé (10.53.0.0/24)',
                    'objectClass':'machineCrans',
                    'realm':'wifi-new-serveurs'
                },
                'Fil-new-serveur' : {
                    'text': 'Ajouter un serveur sur le vlan wifi-new (21), bloc réservé (10.54.0.0/24)',
                    'objectClass':'machineCrans',
                    'realm':'fil-new-serveurs'
                },
                'Dmz' : {
                    'text' : "Ajouter un serveur sur la DMZ (vlan 24) (185.230.79.0/24)",
                    "objectClass" : "machineCrans",
                    'realm' : 'dmz'
                },
                'Accueil' : {
                    'text' : "Ajouter un serveur sur accueil (vlan 7) (10.51.0.0/24)",
                    "objectClass" : "machineCrans",
                    'realm' : 'accueil'
                },
                'Isolement' : {
                    'text' : "Ajouter un serveur sur isolement (vlan 9) (10.52.0.0/24)",
                    "objectClass" : "machineCrans",
                    'realm' : 'isolement'
                },
                'Evenementiel' : {
                    'text' : "Ajouter un serveur sur event (vlan 10) (10.231.137.0/24)",
                    "objectClass" : "machineCrans",
                    'realm' : 'evenementiel'
                },
                'Adm' : {
                    'text' : "Ajouter un serveur sur le vlan adm, "
                             "ou sans controle sur le nom d'hote et l'ip",
                    "objectClass" : "machineCrans",
                    'realm' : 'adm'
                },
                'Switch' : {
                    'text' : "Ajouter un switch sur le VLAN 4 (Switches) (10.231.100.0/24)",
                    "objectClass" : "switchCrans",
                    'realm' : 'switches'
                },
            }
            menu_order = ['Fil-pub', 'Wifi', 'Fixe', 'Adm', 'Switch', 'Dmz', 'Fil-new-serveur', 'Wifi-new-serveur', 'Accueil', 'Isolement', 'Evenementiel']
        def box(default_item=None):
            return self.dialog.menu(
                "Type de Machine ?",
                width=0,
                height=0,
                menu_height=0,
                item_help=0,
                timeout=self.timeout,
                default_item=unicode(default_item),
                title="Création de machines",
                scrollbar=True,
                cancel_label="Retour",
                backtitle=self._connected_as(),
                choices=[
                    (
                        key,
                        menu[key]['text']
                    ) for key in menu_order if self.has_right(menu_droits[key], proprio)
                ]
            )

        def todo(tag, menu, proprio, self_cont, cont):
            if not tag in menu_order:
                raise Continue(self_cont)
            else:
                return self.machine_information(
                    cont=cont,
                    machine=None,
                    objectClass=menu[tag]['objectClass'],
                    proprio=proprio,
                    realm=menu[tag]['realm']
                )

        (code, tag) = self.handle_dialog(cont, box, tag)
        cont = cont(proprio=None) if isinstance(proprio, objets.AssociationCrans) else cont(proprio=proprio)
        self_cont = TailCall(
            self.create_machine_proprio,
            cont=cont,
            proprio=proprio,
            tag=tag
        )

        return self.handle_dialog_result(
            code=code,
            output=tag,
            cancel_cont=cont,
            error_cont=self_cont,
            codes_todo=[([self.dialog.OK], todo, [tag, menu, proprio, self_cont, cont])]
        )

    def create_machine_adherent(self, cont, adherent=None):
        """
        Permet d'ajouter une machine à un adhérent.
        On affiche un menu pour choisir le type de machine (juste filaire et wifi pour le moment)
        """
        if adherent is None:
            adherent = self.select(
                ["adherent"],
                "Recherche d'un adhérent pour lui ajouter une machine",
                cont=cont
            )
        return self.create_machine_proprio(cont=cont, proprio=adherent)

    def create_machine_club(self, cont, club=None):
        """
        Permet d'ajouter une machine à un club.
        On affiche un menu pour choisir le type de machine (juste filaire et wifi pour le moment)
        """
        if club is None:
            club = self.select(
                ["club"],
                "Recherche d'un club pour lui ajouter une machine",
                cont=cont
            )
        return self.create_machine_proprio(cont=cont, proprio=club)

    def create_machine_crans(self, cont):
        """Permet l'ajout d'une machine à l'association"""
        associationCrans = self.conn.search(dn="ou=data,dc=crans,dc=org", scope=0)[0]
        return self.create_machine_proprio(cont=cont, proprio=associationCrans)

    def delete_machine(self, cont, machine=None):
        """Permet la suppression d'une machine de la base ldap"""
        if machine is None:
            machine = self.select(
                [
                    "machineFixe",
                    "machineWifi",
                    "machineCrans",
                    "borneWifi",
                    "switchCrans"
                ],
                "Recherche d'une machine pour supression",
                cont=cont
            )

        def todo(machine):
            if self.confirm_item(
                item=machine,
                title="Voulez-vous vraiment supprimer la machine ?",
                defaultno=True):
                with self.conn.search(dn=machine.dn, scope=0, mode='rw')[0] as machine:
                    machine.delete()
                self.dialog.msgbox(
                    "La machine a bien été supprimée",
                    timeout=self.timeout,
                    title="Suppression d'une machine"
                )
                raise Continue(cont(machine=None))
            else:
                raise Continue(cont)

        return self.handle_dialog_result(
            code=self.dialog.OK,
            output="",
            cancel_cont=cont,
            error_cont=cont,
            codes_todo=[([self.dialog.OK], todo, [machine])]
        )

#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-
"""
    Génération de la configuration d'un switch.

    procédure de configuration initiale :
        * mot de passe admin (password manager user-name <username>)
        * activation du ssh (crypto key generate ssh)
        * copie fichier de conf
    pour les reconfiguration copier le fichier de conf
    dans /cfg/startup-config

    Dans tous les cas FAIRE LE SNMP A LA MAIN (script hptools)
"""

from __future__ import print_function, unicode_literals

import os
import sys
import datetime
import itertools
import argparse
from collections import OrderedDict

import netaddr
import jinja2

import gestion.config as config
import gestion.secrets_new as secrets
import gestion.annuaires_pg as annuaire
import gestion.config.encoding as enc
from gestion.config import hp_switchs
import lc_ldap.objets as ldap_classes
from lc_ldap import crans_utils
from lc_ldap.shortcuts import lc_ldap_admin as make_ldap_conn

MIB_PRISE_VLAN = 'SNMPv2-SMI::enterprises.11.2.14.11.5.1.7.1.15.3.1.1'
MIB_PRISE_MAC = 'SNMPv2-SMI::enterprises.11.2.14.11.5.1.9.4.2'

# Blocs verticaux pour ascii art des prises d'un switch
START_BLOCK = [u' ', u'┌', u'│', u'│', u'├', u'│', u'│', u'└', u' ']
MIDDLE_BLOCK = [u' ', u'┬', u'│', u'│', u'┼', u'│', u'│', u'┴', u' ']
END_BLOCK = [u' ', u'┐', u'│', u'│', u'┤', u'│', u'│', u'┘', u' ']

ldap = make_ldap_conn()

## Paramètres de configuration

# VLANs disponibles
AVAILABLE_VLANS = OrderedDict([
    ('adherent', {
        'id' : config.vlans['adherent'],
        'dhcp_snooping' : True,
        'igmp_snooping' : True,
        'mld_snooping' : True,
        'network' : {
            'IPv4' : netaddr.IPNetwork(config.NETs['serveurs'][0]),
            'IPv6' : netaddr.IPNetwork(config.prefix['serveurs'][0]),
        },
    }),
    ('adm', {
        'id' : config.vlans['adm'],
        'dhcp_snooping' : False,
        'network' : {
            'IPv4' : netaddr.IPNetwork(config.NETs['adm'][0]),
            'IPv6' : netaddr.IPNetwork(config.prefix['adm'][0]),
        },
    }),
    ('bornes', {
        'id' : config.vlans['bornes'],
        'dhcp_snooping' : True,
        'network' : {
            'IPv4' : netaddr.IPNetwork(config.NETs['bornes'][0]),
            'IPv6' : netaddr.IPNetwork(config.prefix['bornes'][0]),
        },
    }),
    ('switches', {
        'id' : config.vlans['switches'],
        'dhcp_snooping' : False,
        'network' : {
            'IPv4' : netaddr.IPNetwork(config.NETs['switches'][0]),
            'IPv6' : netaddr.IPNetwork(config.prefix['switches'][0]),
        },
    }),
    ('accueil', {
        'id' : config.vlans['accueil'],
        'dhcp_snooping' : True,
        'network' : {
            'IPv4' : netaddr.IPNetwork(config.NETs['accueil'][0]),
            'IPv6' : None,
        },
    }),
    ('isolement', {
        'id' : config.vlans['isolement'],
        'dhcp_snooping' : True,
        'network' : {
            'IPv4' : netaddr.IPNetwork(config.NETs['isolement'][0]),
            'IPv6' : None,
        },
    }),
    ('event', {
        'id' : config.vlans['event'],
        'dhcp_snooping' : False,
        'network' : {
            'IPv4' : netaddr.IPNetwork(config.NETs['evenementiel'][0]),
            'IPv6' : netaddr.IPNetwork(config.prefix['evenementiel'][0]),
        },
    }),
    ('fil-new', {
        'id' : config.vlans['fil-new'],
        'dhcp_snooping' : True,
        'igmp_snooping' : True,
        'mld_snooping' : True,
        'network' : {
            'IPv4' : netaddr.IPNetwork(config.NETs['fil-new'][0]),
            'IPv6' : netaddr.IPNetwork(config.prefix['fil-new'][0]),
        },
    }),
    ('wifi-new', {
        'id' : config.vlans['wifi-new'],
        'dhcp_snooping' : True,
        'network' : {
            'IPv4' : netaddr.IPNetwork(config.NETs['wifi-new'][0]),
            'IPv6' : netaddr.IPNetwork(config.prefix['wifi-new'][0]),
        },
    }),
    ('fil-pub', {
        'id' : config.vlans['fil-pub'],
        'dhcp_snooping' : True,
        'igmp_snooping' : True,
        'mld_snooping' : True,
        'network' : {
            'IPv4' : netaddr.IPNetwork(config.NETs['fil-pub'][0]),
            'IPv6' : netaddr.IPNetwork(config.prefix['fil-pub'][0]),
        },
    }),
    ('freebox', {
        'id' : config.vlans['freebox'],
        'dhcp_snooping' : False,
        'network' : {
            'IPv4' : None,
            'IPv6' : None,
        },
    }),
    ('re2o-adm', {
        'id' : config.vlans['re2o-adm'],
        'dhcp_snooping' : False,
        'network' : {
            'IPv4' : netaddr.IPNetwork(config.NETs['re2o-adm'][0]),
            'IPv6' : netaddr.IPNetwork(config.prefix['re2o-adm'][0]),
        },
    }),
     ('re2o-fil-nat', {
        'id' : config.vlans['re2o-fil-nat'],
        'dhcp_snooping' : True,
        'igmp_snooping' : True,
        'mld_snooping' : True,
        'network' : {
            'IPv4' : netaddr.IPNetwork(config.NETs['re2o-fil-nat'][0]),
            'IPv6' : netaddr.IPNetwork(config.prefix['re2o-fil-nat'][0]),
        },
    }),
    ('re2o-wifi-nat', {
        'id' : config.vlans['re2o-wifi-nat'],
        'dhcp_snooping' : True,
        'network' : {
            'IPv4' : netaddr.IPNetwork(config.NETs['re2o-wifi-nat'][0]),
            'IPv6' : netaddr.IPNetwork(config.prefix['re2o-wifi-nat'][0]),
        },
    }),
    ('re2o-dmz', {
        'id' : config.vlans['re2o-dmz'],
        'dhcp_snooping' : False,
        'network' : {
            'IPv4' : netaddr.IPNetwork(config.NETs['re2o-dmz'][0]),
            'IPv6' : netaddr.IPNetwork(config.prefix['re2o-dmz'][0]),
        },
    }),
])

ENABLED_VLANS = [k for k in AVAILABLE_VLANS]

# États possibles d'un port pour l'appartenance à un VLAN
V_TAGGED = "tagged"
V_UNTAGGED = "untagged"
V_FORBID = "forbid"
V_NO = "no"

# États possibles pour la priorité d'un port PoE
POE_CRITICAL = "critical"
POE_HIGH = "high"
POE_LOW = "low"
POE_DISABLED = ""

# Nombre maximum de VLANs
MAX_VLAN_NUMBER = 64

# Nombre maximal de MACs par prise
MAX_MAC_NUMBER = {
    'adherent' : 2,
    'club' : 30,
    'public' : 10,
}

# RIDs des serveurs RADIUS
RADIUS_SERVERS = [
    'radius.switches.crans.org',
    'radius-failover.switches.crans.org',
]

# RIDs des serveurs DHCP
DHCP_SERVER_RID = [34, 160]

# RIDs des serveurs NTP
NTP_SERVERS = ['fy.switches.crans.org']

# RIDs des serveurs de log
LOG_SERVERS = ['thot.switches.crans.org']

################################################################################

def get_servers(server_list):
    """Renvoie les informations sur les serveurs demandés depuis la base LDAP"""
    return ldap.search("(|%s)" % "".join("(host=%s)" % h for h in server_list))

def ipv4(server):
    """Renvoie l'IPv4 d'un serveur donné sous sa forme LDAPObject"""
    return server['ipHostNumber'][0].value

def ipv6(server):
    """Renvoie l'IPv6 d'un serveur donné sous sa forme LDAPObject"""
    return server['ip6HostNumber'][0].value

class Port(object):
    """Un port de switch"""
    num = None

    # : uplink: None ou str
    uplink = None

    # : Liste de serveurs
    servers = None

    # : Liste de bornes
    bornes = None

    # : Liste de noms de chambres
    chambres = None

    # : Liste des macs vues
    seen_macs = None

    # : Liste des vlans vus
    seen_vlans = None

    # : Port (Q)SFP(+) ?
    sfp = False

    # : Port PoE ?
    poe = False

    # Radius activé
    radius_enabled = True

    # : PoE Activé + Priorité
    poe_enabled = POE_DISABLED

    #Ce port est public (kfet, med...)
    public = False

    def __init__(self, num, sfp=False, poe=False):
        self.num = num
        self.servers = list()
        self.bornes = list()
        self.chambres = list()
        self.seen_macs = list()
        self.seen_vlans = list()
        self.sfp = sfp
        self.poe = poe

    def __str__(self):
        if self.uplink:
            return self.uplink
        else:
            labels = []
            if self.servers:
                labels.append(
                    'Srv_%s' % ','.join(s['host'][0].value.split('.', 1)[0] for s in self.servers)
                )
            if self.chambres:
                labels.append('Ch_%s' % ','.join(self.chambres))
            if self.bornes:
                labels.append(
                    'Wifi_%s' % ','.join(b['host'][0].value.split('.', 1)[0] for b in self.bornes)
                )
            return ",".join(labels) or "Inconnu"

    def __int__(self):
        return self.num

    @property
    def brief(self):
        """Description brève de la prise"""
        if self.uplink:
            return unicode(self.uplink).replace(u'uplink->', u'')
        else:
            labels = self.servers + self.bornes
            labels = [s['host'][0].value.split('.', 1)[0] for s in labels]
            labels += [unicode(c) for c in self.chambres]
            return u",".join(labels)

    @property
    def flowcontrol(self):
        """Est-ce que le flowcontrol est activé sur ce port ?"""
        if self.uplink or self.servers:
            return 'no flow-control'
        else:
            return 'flow-control'

    @property
    def trusted(self):
        """Est-ce une prise que l'on maîtrise ?"""
        return self.uplink or self.servers

    @property
    def radius_auth(self):
        """Doit-on faire de l'auth radius ?"""
        return not self.uplink and not self.servers and not self.bornes and self.radius_enabled

    @property
    def adherents(self):
        """Adhérents sur la prise"""
        filtre = u'(|%s)' % (''.join('(chbre=%s)' % c for c in self.chambres))
        return ldap.search(filtre)

    @property
    def num_mac(self):
        """Renvoie le nombre de macs autorisées.
        Ne devrait pas être appelée si c'est une prise d'uplink ou de bornes
        """
        assert not self.bornes and not self.uplink
        # Si la pièce en question héberge au moins un club, on ajoute un certain
        # nombre de machines supplémentaires *PAR CLUB*, car on suppose que les
        # besoins d'un club sont plus grands que ceux d'un adhérent classique
        if self.public:
            return MAX_MAC_NUMBER['public']
        else:
            return (
                 MAX_MAC_NUMBER['adherent']
                 + MAX_MAC_NUMBER['club']*len([chbre for chbre in self.chambres if "cl" in chbre])
            )

    @property
    def enable_poe(self):
        """Indique si le PoE doit être actif sur ce port"""
        return self.poe and (self.poe_enabled or self.bornes)

    @property
    def arp_protect_trust(self):
        """Indique si on active la protection arp.
        Désactivé sur nos prises ainsi que sur le wifi (because roaming) et celles des clubs"""
        return self.bornes or self.trusted or any("cl" in chbre for chbre in self.chambres)

    @property
    def poe_level(self):
        """Indique la priorité accordée à ce port PoE"""
        return self.poe_enabled

    def vlan_member(self, vlan):
        """Renvoie V_TAGGED, V_UNTAGGED ou V_NO
        suivant le ``vlan`` (str) demandé"""
        # Si la prise est particulière et que un vlan untag est spécifié, on l'utilise (imprimante et autres)
        if any(unicode(AVAILABLE_VLANS[vlan]['id']) == server.get('untagvlan', [None])[0] for server in self.servers):
            return V_UNTAGGED
        elif any(unicode(AVAILABLE_VLANS[vlan]['id']) == borne.get('untagvlan', [None])[0] for borne in self.bornes):
            return V_UNTAGGED
        # Sinon, tous les VLANs sont taggués pour nos machines et pour les switches
        elif self.servers or self.uplink:
            if self.chambres and vlan == 'adherent':
                return V_UNTAGGED
            else:
                return V_TAGGED
        # Les VLANs de management ne doivent pas sortir sur des prises inconnues
        elif vlan in ['adm', 'switches']:
            return V_NO
        # Dans le cas d'une borne, on ne veut qu'une sélection de VLANs
        elif self.bornes and vlan in ['bornes', 'accueil', 'bornes', 'event', 'wifi-new']:
            return V_TAGGED
        # Ici, soit on a un VLAN non désiré pour une borne, soit une simple chambre d'adhérent
        # Dans le premier cas, on coupe le VLAN, dans le second on laisse RADIUS travailler.
        else:
            return V_NO

class PortList(list):
    """Liste de ports"""
    def __str__(self):
        """ transforme une liste de prises en une chaine pour le switch
            exemple : 1, 3, 4, 5, 6, 7, 9, 10, 11, 12 => 1,3-7,9-12
        """
        liste = list(int(x) for x in self)
        liste.sort()

        sortie = []
        groupe = [-99999, -99999]
        for x in itertools.chain(liste, [99999]):
            if x > groupe[1]+1: # Nouveau groupe !
                if groupe[0] == groupe[1]:
                    sortie.append('%d' % groupe[1])
                else:
                    sortie.append('%d-%d' % tuple(groupe))
                groupe[0] = x
            groupe[1] = x
        return ','.join(sortie[1:])

    def filter_vlan(self, vlan):
        """Prend un ``vlan`` et renvoie deux PortList, la première
        avec les ports taggués, la seconde pour les ports untaggués"""
        tagged = PortList()
        untagged = PortList()
        for port in self:
            assign = port.vlan_member(vlan)
            if assign == V_TAGGED:
                tagged.append(port)
            elif assign == V_UNTAGGED:
                untagged.append(port)
        return (tagged, untagged)

def get_port_dict(switch, features=None):
    """Renvoie le dictionnaire prise->objet Port"""
    # Build ports !
    ports = {}
    features = features or {'sfp' : [], 'poe' : []}
    for num in xrange(1, 1+int(switch['nombrePrises'][0].value)):
        ports[num] = Port(num, sfp=num in features['sfp'], poe=num in features['poe'])

    bat, sw_num = get_bat_num(unicode(switch['host'][0]))
    # Remplit les machines ayant une prise spécifiée
    # (normalement uniquement les serveurs et les bornes)
    for machine in ldap.search(u'prise=%s%i*' % (bat, sw_num)):
        port = ports[int(machine['prise'][0].value[2:])]
        classe = machine['objectClass'][0].value
        if classe == 'machineCrans':
            port.servers.append(machine)
        elif classe == 'borneWifi':
            port.bornes.append(machine)

    # On remplit les chambres
    annuaire_no_radius = annuaire.disabled_radius()
    annuaire_public = annuaire.lieux_public()
    annuaire_poe_on = dict(annuaire.poe_enabled())
    for prise, chbres in annuaire.reverse(bat).iteritems():
        # TODO rajouter un arg à reverse
        if int(prise[0]) != int(sw_num):
            continue
        port = ports[int(prise[1:])]
        # (below) beware: type(num) == str (ex: 302g)
        port.chambres += [bat.upper() + num for num in chbres]
        # On remplit si radius a été désactivé
        port.radius_enabled = not any(x in annuaire_no_radius for x in port.chambres)
        # On regarde dans la base pour voir si le PoE est autorisé
        port.poe_enabled = annuaire_poe_on.get(bat.upper() + prise, POE_DISABLED)
        # On rajoute si le port est un port publique
        port.public = any(x in annuaire_public for x in port.chambres)

    # Remplit les uplinks
    for num_prise, label in annuaire.uplink_prises[bat].iteritems():
        if num_prise/100 != int(sw_num):
            continue
        port = ports[num_prise % 100]
        port.uplink = label

    return ports


def fill_port_infos(hostname, port_dict):
    """Rajoute des infos sur les ports d'un switch"""

    from gestion.hptools import snmp
    conn = snmp(hostname, version='1', community='public')

    prise_vlan = conn.walk(MIB_PRISE_VLAN, bin_comp=True)
    for res in prise_vlan:
        res = res.split('.')
        port = int(res[-2])
        vlan = int(res[-3])
        port_dict[port].seen_vlans.append(vlan)

    prise_mac = conn.walk(MIB_PRISE_MAC, bin_comp=True)
    for mib in prise_mac:
        port = int(mib.split('.')[-8])
        mib = mib.split('.')
        mac = ':'.join('%02x' % int(mib[i]) for i in xrange(-7, -1))
        port_dict[port].seen_macs.append(mac)

def check_conf_ldap(hostname):
    """Vérifie la conf du switch, la base ldap et les macs/prises associées"""
    bat, sw_num = get_bat_num(hostname)
    switch = ldap.search(
        u'(&(host=%s.switches.crans.org)(objectClass=switchCrans))' % hostname
    )[0]

    port_dict = get_port_dict(switch)
    fill_port_infos(hostname, port_dict)

    for port in port_dict.itervalues():
        print("* Checking port %d (%s)" % (port.num, port))
        # Nombres de vlans
        pr_nb_vlans = len(port.seen_vlans)
        th_nb_vlans = sum(port.vlan_member(vname) != V_NO for vname in ENABLED_VLANS)
        if not th_nb_vlans and port.radius_auth:
            th_nb_vlans = 1
        if port.bornes and port.vlan_member('adherent') == V_NO:
            th_nb_vlans += 1
        if th_nb_vlans != pr_nb_vlans:
            print("  Wrong vlan count (%d instead of %d)" % (pr_nb_vlans, th_nb_vlans))
            print(port.seen_vlans)
            print(list(vname for vname in ENABLED_VLANS if port.vlan_member(vname) != V_NO))

        # Les macs
        if port.uplink:
            if len(port.seen_macs) < 20:
                print("  Uplink but few macs (%d)" % len(port.seen_macs))
        elif port.radius_auth: # On vérifie que c'est la bonne chambre
            th_prises_set = set()
            for mac in set(port.seen_macs):
                res = ldap.search(u'macAddress=%s' % mac)
                if not res:
                    continue
                chbre = unicode(res[0].proprio().get('chbre', ['EXT'])[0])
                th_prise = chbre[0].lower()
                try:
                    th_prise += annuaire.chbre_prises(chbre[0], chbre[1:])
                except annuaire.ChbreNotFound:
                    # La chambre est inconnue -> drop
                    continue
                th_prises_set.add(th_prise)

            pr_prise = bat.lower() + '%d%02d' % (sw_num, port.num)
            if th_prises_set and pr_prise not in th_prises_set:
                print("   Aucune machine de chbre. Candidats: %r" % th_prises_set)
            # Check machine sur prise (si pas uplink)
            machines = []
            for mac in set(port.seen_macs):
                res = ldap.search(u'macAddress=%s' % mac)
                if not res:
                    print("  Unknown mac %s" % mac)
                    continue
                machines += res
            for machine in machines:
                owner = machine.proprio()
                if isinstance(owner, ldap_classes.AssociationCrans):
                    the = unicode(machine.get('prise', ['N/A'])[0])
                    the = the.lower()
                    pra = bat.lower() + '%d%02d' % (sw_num, port.num)
                    if the != pra:
                        print(
                            "   Machine %s sur mauvaise prise (%s,%s)" % (machine, the, pra)
                        )
                        fix_prise(machine, pra)

                elif isinstance(machine, ldap_classes.machineWifi):
                    if not port.bornes:
                        print("   Machine %s sur prise sans borne ?" % machine)

def fix_prise(machine, prise):
    """Répare la base en remplaçant la prise de la machine par ce qui est
    conseillé en paramètre"""
    opt = "yN"
    old_prise = unicode(machine.get('prise', ['N/A'])[0])
    print(
        "Remplacer prise de %s par %s (ancienne valeur: %s) ?" % (machine, prise, old_prise),
        "[%s]" % opt
    )
    while True:
        char = raw_input()
        if char in opt.lower():
            break
        print("[%s]" % opt)
    if char == 'y':
        if 'w' not in machine.mode:
            machine = ldap.search(u'%s' % machine.dn.split(',')[0], mode='rw')[0]
        with machine:
            machine['prise'] = unicode(prise)
            machine.history_gen()
            machine.save()
            print("Done !")

def format_prises_group(data, first, last):
    """Affiche sous forme d'un groupe de prise, en ascii-art, les noms des
    prises. Entre first et last"""
    first = (first-1)/2*2+1
    last = (-last/2)*-2

    def align5(txt, right=False):
        """Aligne le texte en limitant à 5 char"""
        if len(txt) > 5:
            return txt[0:4] + u'*'
        if right:
            return (5-len(txt))*u' ' + txt
        return txt + (5-len(txt))*u' '

    def align2x5(txt):
        """Aligne le texte sur deux lignes de 5 char"""
        return (align5(txt[0:5]), align5(txt[5:]))

    lines = list(START_BLOCK)
    def fill_prise(prise, i_info, i_prise):
        """Remplis le contenu d'une prise, sur deux lignes"""
        if prise in data:
            txt = data[prise].brief
        else:
            txt = u""
        (txt1, txt2) = align2x5(txt)
        lines[i_info] += txt1
        lines[i_info+1] += txt2
        lines[i_prise] += align5(u"%d" % prise, right=(i_prise != 0))

    sep = MIDDLE_BLOCK
    for prise in xrange(first, last, 2):
        for li in [1, 4, 7]:
            lines[li] += u'─'*5
        fill_prise(prise, 2, 0)
        fill_prise(prise+1, -4, -1)
        if prise == last -1:
            sep = END_BLOCK
        for line_i in xrange(0, 9):
            lines[line_i] += sep[line_i]

    return lines

def pretty_print(hostname):
    """Affiche joliement le plan de connexion d'un switch"""
    switch = ldap.search(
        u'(&(host=%s.switches.crans.org)(objectClass=switchCrans))' % hostname
    )[0]

    port_dict = get_port_dict(switch)
    total = max(port_dict.keys())

    first = 1
    while first < total:
        for line in format_prises_group(port_dict, first, min(first+60, total)):
            print(line.encode(enc.out_encoding))
        first += 61

def get_bat_num(hostname):
    """Renvoie un tuple (bat, num) où bat est la lettre du bâtiment et
    num l'entier numéro du switch"""
    return (hostname[3].lower(), int(hostname[5]))

def get_switch_features(switch, model=None):
    """Renvoie le modèle et les fonctionnalités supportées par
    le switch donné. 'model' permet de forcer le modèle du switch si
    besoin est"""
    if 'info' in switch.keys() and not model:
        header = [com for com in switch['info'] if com.value.startswith(';')] or ['']
        model = header[0].split(' ', 2)[1][:-1]

    if not model or model not in hp_switchs.HP_PROCURVE_MAP:
        sys.exit(u'Impossible de déterminer le modèle du switch %s (%s)' % (switch, model))
    else:
        hp_switchs.HP_PROCURVE_MAP[model].setdefault('modules', [])
        hp_switchs.HP_PROCURVE_MAP[model].setdefault('sfp', [])
        hp_switchs.HP_PROCURVE_MAP[model].setdefault('poe', [])
        return (model, hp_switchs.HP_PROCURVE_MAP[model])

def conf_switch(hostname, model=None, ieee8021X=False):
    """Affiche la configuration d'un switch"""
    bat, sw_num = get_bat_num(hostname)

    switch = ldap.search(
        u'(&(host=%s.switches.crans.org)(objectClass=switchCrans))' % hostname
    )[0]
    model, features = get_switch_features(switch, model=model)
    ports_list = PortList(get_port_dict(switch, features=features).itervalues())

    tpl_env = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))
    tpl_env.filters.update({
        'ipv4' : ipv4,
        'ipv6' : ipv6,
        'ip4_of_rid' : crans_utils.ip4_of_rid,
        'ip6_of_mac' : crans_utils.ip6_of_mac,
    })

    context = {
        'switch': switch,
        'model' : model,
        'firmware' : features["firmware"],
        'modules' : features["modules"],
        'features' : features["features"],
        'ports' : ports_list,
        'bat': bat.upper(),
        'hostname': 'bat%s-%d' % (bat, sw_num),
        'date_gen': datetime.datetime.now(),
        'vlans' : AVAILABLE_VLANS,
        'max_vlans' : MAX_VLAN_NUMBER,
        'radius_key': secrets.get('radius_key'),
        'radius_servers' : get_servers(RADIUS_SERVERS),
        'ntp_servers' : get_servers(NTP_SERVERS),
        'log_servers' : get_servers(LOG_SERVERS),
        'dhcp_server_rid': DHCP_SERVER_RID,
        'ieee8021X' : ieee8021X,
        'trusted' : PortList(p for p in ports_list if p.trusted),
        'non_trusted' : PortList(p for p in ports_list if not p.trusted),
    }

    context.update(**{f: getattr(hp_switchs, f) for f in hp_switchs.ALL_FEATURES})

    # On remplit les objets vlans (nom, id, tagged, untagged etc)
    for vname in AVAILABLE_VLANS.keys():
        for assign, p in itertools.groupby(ports_list, lambda p: p.vlan_member(vname)):
            context['vlans'][vname].setdefault(assign, PortList())
            context['vlans'][vname][assign].extend(p)
        if switch['ipHostNumber'][0].value in (context['vlans'][vname]['network']['IPv4'] or []):
            context['vlans'][vname]['ip'] = switch['ipHostNumber'][0].value
        if switch['ip6HostNumber'][0].value in (context['vlans'][vname]['network']['IPv6'] or []):
            context['vlans'][vname]['ipv6'] = switch['ip6HostNumber'][0].value

    # On render :
    return tpl_env.get_template('switch_conf.tpl').render(**context).encode('utf-8')

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Génération de la conf d'un switch.")
    parser.add_argument('hostname', help="Nom du switch à regénérer (ex: batg-4)")
    parser.add_argument(
        'model', type=unicode, nargs='?', default=None,
        help="Force le modèle du switch pour générer la configuration"
    )
    parser.add_argument(
        '-c', '--check', action='store_true', default=False,
        help="Vérifie la conf par rapport aux macs et vlans "
        "effectivement présents sur le switch"
    )
    parser.add_argument(
        '--pretty', action='store_true', default=False,
        help="Affiche un tableau ascii du plan de connexion du switch"
    )
    parser.add_argument(
        '--ieee8021X', action='store_true', default=False,
        help="Générer une configuration supportant IEEE 802.1X"
    )

    options = parser.parse_args(sys.argv[1:])

    if options.check:
        check_conf_ldap(options.hostname)
    elif options.pretty:
        pretty_print(options.hostname)
    else:
        print(conf_switch(options.hostname, model=options.model, ieee8021X=options.ieee8021X))

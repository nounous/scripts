#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-
import utils
import base

from utils import pretty_print, OK, anim
from base import dev, hostname

class firewall(base.firewall_routeur):
    """Pare-feu du routeur principal de l'association"""
    def __init__(self):
        super(self.__class__, self).__init__()

        self.reloadable.update({
            'log_all' : self.log_all,
            'admin_vlans' : self.admin_vlans,
            'clamp_mss' : self.clamp_mss,
            'ingress_filtering' : self.ingress_filtering,
            'ssh_on_https' : self.ssh_on_https,
            'connexion_filnew' : self.connexion_filnew,
            'connexion_wifinew' : self.connexion_wifinew,
            'connexion_ens' : self.connexion_ens,
            'blacklist_soft' : self.blacklist_soft,
            'reseaux_non_routable' : self.reseaux_non_routable,
            'filtrage_ports' : self.filtrage_ports,
            'limitation_debit' : self.limitation_debit,
            'limit_ssh_connexion' : self.limit_ssh_connexion,
            'limit_connexion' : self.limit_connexion,
            'challenge_letsencrypt': self.challenge_letsencrypt,
        })

        self.use_ipset.extend([self.blacklist_soft, self.reseaux_non_routable])
        self.use_tc.extend([self.limitation_debit])

        self.ipset['reseaux_non_routable'] = {
            'deny' : base.Ipset("RESEAUX-NON-ROUTABLE-DENY", "hash:net"),
            'allow' :  base.Ipset("RESEAUX-NON-ROUTABLE-ALLOW", "hash:net"),
        }

        self.ipset['blacklist'].update({
            'soft' : base.Ipset("BLACKLIST-SOFT", "hash:ip"),
        })

        # Portail captif/blacklist soft: ipset des gens ayant cliqué pour continuer à naviguer
        self.ipset['confirmation'] = base.Ipset("CONFIRMATION", "hash:ip", "timeout 3600")

        # Ouvertures de ports temporaires
        self.ipset['ip_port_tmp'] = base.Ipset("IP-PORT-TMP", "hash:ip,port", "timeout 3600")

    def blacklist_maj(self, ips):
        """Mise à jour des blacklistes"""
        self.blacklist_hard_maj(ips)
        self.blacklist_soft_maj(ips)

    def blacklists(self, table=None, fill_ipset=False, apply=False):
        self.blacklist_hard(table=table, fill_ipset=fill_ipset, apply=apply)
        self.blacklist_soft(table=table, fill_ipset=fill_ipset, apply=apply)

    def raw_table(self):
        """Génère les règles pour la table ``raw`` et remplis les chaines de la table"""
        table = 'raw'

        chain = 'PREROUTING'
        self.add(table, chain, '-d 225.0.0.50 -j DROP')
        return


    def mangle_table(self):
        table = 'mangle'
        super(self.__class__, self).mangle_table()

        chain = 'PREROUTING'
        self.add(table, chain, '-p tcp -j CONNMARK --restore-mark')

        chain = 'POSTROUTING'
        # Sable ne supporte pas le logall
        if not base.config.firewall.routeurs_du_crans['routeur_secondary']==hostname:
            self.add(table, chain, '-j %s' % self.log_all(table))
        self.add(table, chain, '-j %s' % self.clamp_mss(table))
        self.add(table,chain, '-j %s' % self.limitation_debit(table, run_tc=True))
        return

    def filter_table(self):
        table = 'filter'
        super(self.__class__, self).filter_table()

        mac_ip_chain = self.test_mac_ip()
        blacklist_hard_chain = self.blacklist_hard()
        blacklist_soft_chain = self.blacklist_soft(table, fill_ipset=True)

        chain = 'INPUT'
        self.flush(table, chain)
        self.add(table, chain, '-i lo -j ACCEPT')
        self.add(table, chain, '-p icmp -j ACCEPT')
        self.add(table, chain, '-m state --state RELATED,ESTABLISHED -j ACCEPT')
        self.add(table, chain, '-j %s' % blacklist_soft_chain)
        self.add(table, chain, '-i %s -j %s' % (dev['out'], self.limit_ssh_connexion(table)))
        self.add(table, chain, '-i %s -j %s' % (dev['zayo'], self.limit_ssh_connexion(table)))
        for net in base.config.NETs['all'] + base.config.NETs['adm']:
            self.add(table, chain, '-s %s -j %s' % (net, mac_ip_chain))
        self.add(table, chain, '-j %s' % blacklist_hard_chain)

        chain = 'FORWARD'
        self.flush(table, chain)
        self.add(table, chain, '-i lo -j ACCEPT')
        self.add(table, chain, '-j %s' % self.reseaux_non_routable(table, fill_ipset=True))
        self.add(table, chain, '-p icmp -j ACCEPT')
        self.add(table, chain, '-j %s' % self.admin_vlans(table))
        self.add(table, chain, '-j %s' % blacklist_soft_chain)
        self.add(table, chain, '-i %s -j %s' % (dev['out'], blacklist_hard_chain))
        self.add(table, chain, '-o %s -j %s' % (dev['out'], blacklist_hard_chain))
        self.add(table, chain, '-i %s -j %s' % (dev['zayo'], blacklist_hard_chain))
        self.add(table, chain, '-o %s -j %s' % (dev['zayo'], blacklist_hard_chain))
        self.add(table, chain, '-m state --state RELATED,ESTABLISHED -j ACCEPT')
        self.add(table, chain, '-o %s -j %s' % (dev['zayo'], self.connexion_ens(table)))
        for net in base.config.NETs['all'] + base.config.NETs['adm']:
            self.add(table, chain, '-s %s -j %s' % (net, mac_ip_chain))
        self.add(table, chain, '-j %s' % self.ingress_filtering(table))
        self.add(table, chain, '-i %s -j %s' % (dev['out'], self.limit_ssh_connexion(table, ttl=30, counter_name="SSH2")))
        self.add(table, chain, '-i %s -j %s' % (dev['zayo'], self.limit_ssh_connexion(table, ttl=30, counter_name="SSH2")))
        self.add(table, chain, '-o %s -j %s' % (dev['out'], self.limit_connexion(table, ip_track='srcip')))
        self.add(table, chain, '-o %s -j %s' % (dev['zayo'], self.limit_connexion(table, ip_track='srcip')))
        self.add(table, chain, '-i %s -d %s -j %s' % ( dev['out'], ','.join(base.config.NETs['adherents']), self.limit_connexion(table, ip_track='dstip')))
        self.add(table, chain, '-i %s -j %s' % (dev['out'], self.filtrage_ports(table)))
        self.add(table, chain, '-o %s -j %s' % (dev['out'], self.filtrage_ports(table)))
        self.add(table, chain, '-i %s -j %s' % (dev['zayo'], self.filtrage_ports(table)))
        self.add(table, chain, '-o %s -j %s' % (dev['zayo'], self.filtrage_ports(table)))
        return

    def nat_table(self):
        table = 'nat'
        super(self.__class__, self).nat_table()

        chain = 'PREROUTING'
        self.add(table, chain, '-j %s' % self.ssh_on_https(table))
        self.add(table, chain, '-j %s' % self.challenge_letsencrypt(table))
        self.add(table, chain, '-j %s' % self.blacklist_soft(table))
        self.add(table, chain, '-j %s' % self.blacklist_hard(table))

        chain = 'POSTROUTING'
        self.add(table, chain, '-j %s' % self.connexion_wifinew(table))
        self.add(table, chain, '-j %s' % self.connexion_filnew(table))
        self.add(table, chain, '-j %s' % self.connexion_ens(table))
        return

    def limit_ssh_connexion(self, table=None, apply=False, ttl=120, counter_name="SSH"):
        chain = 'LIMIT-%s-CONNEXION' % (counter_name,)

        if table == 'filter':
            pretty_print(table, chain)
            self.add(table, chain, '-p tcp --dport ssh -m state --state NEW -m recent --name %s --set' % counter_name)
            self.add(table, chain, '-p tcp --dport ssh -m state --state NEW -m recent --name %s --update --seconds %s --hitcount 10 --rttl -j DROP' % (counter_name, ttl))
            print OK

        if apply:
            self.apply(table, chain)
        return chain

    def limit_connexion(self, table=None, apply=False, ip_track='srcip'):
        chain = 'LIMIT-CONNEXION-%s' % ip_track.upper()

        if table == 'filter':
            pretty_print(table, chain)
            self.add(table, chain, '-m hashlimit -p udp --hashlimit-name LIMIT_UDP_%s_CONNEXION --hashlimit-mode %s --hashlimit-upto 400/sec --hashlimit-burst 800 -j RETURN' % (ip_track.upper(), ip_track))
            self.add(table, chain,'-m hashlimit -p udp --hashlimit-name LIMIT_UDP_%s_CONNEXION_LOG --hashlimit-mode %s --hashlimit-upto 5/hour -j LOG --log-prefix "CONNEXION_LIMIT_UDP_%s "' % (ip_track.upper(), ip_track, ip_track.upper()))
            self.add(table, chain, '-p udp -j REJECT')
            self.add(table, chain, '-m hashlimit -p tcp -m state --state NEW --hashlimit-name LIMIT_TCP_%s_CONNEXION --hashlimit-mode %s --hashlimit-upto 2000/min --hashlimit-burst 4000 -j RETURN' % (ip_track.upper(), ip_track))
            self.add(table, chain,'-m hashlimit -p tcp -m state --state NEW --hashlimit-name LIMIT_TCP_%s_CONNEXION_LOG --hashlimit-mode %s --hashlimit-upto 5/hour -j LOG --log-prefix "CONNEXION_LIMIT_TCP_%s "' % (ip_track.upper(), ip_track, ip_track.upper()))
            self.add(table, chain, '-p tcp -m state --state NEW -j REJECT')
            self.add(table, chain, '-m hashlimit --hashlimit-name LIMIT_OTHER_%s_CONNEXION --hashlimit-mode %s --hashlimit-upto 400/sec --hashlimit-burst 800 -j RETURN' % (ip_track.upper(), ip_track))
            self.add(table, chain,'-m hashlimit --hashlimit-name LIMIT_OTHER_%s_CONNEXION_LOG --hashlimit-mode %s --hashlimit-upto 5/hour -j LOG --log-prefix "CONNEXION_LIMIT "' % (ip_track.upper(), ip_track))
            self.add(table, chain, '-j REJECT')
            print OK

        if apply:
            self.apply(table, chain)
        return chain

    def test_mac_ip(self, table=None, fill_ipset=False, apply=False):
        chain = super(self.__class__, self).test_mac_ip()

        if table == 'filter':
            for key in ['out']:
                self.add(table, chain, '-i %s -j RETURN' % dev[key])

        return super(self.__class__, self).test_mac_ip(table, fill_ipset, apply)


    def log_all(self, table=None, apply=False):
        chain = 'LOG_ALL'

        if table == 'mangle':
            pretty_print(table, chain)
            for device in dev.values():
                self.add(table, chain, '-o %s -m state --state NEW -j LOG --log-prefix "LOG_ALL "' % device)
            print OK

        if apply:
            self.apply(table, chain)
        return chain

    def admin_vlans(self, table=None, apply=False):
        chain = 'VLAN-ADMINS'

        if table == 'filter':
            pretty_print(table, chain)
            for net in base.config.NETs['adm'] + base.config.NETs['switches'] + base.config.NETs['bornes']:
                self.add(table, chain, '-d %s -j REJECT' % net)
            print OK

        if apply:
            self.apply(table, chain)
        return chain

    def qos(self, table=None, apply=False):
        return

    def clamp_mss(self, table=None, apply=False):
        """Force la MSS (Max Segment Size) TCP à rentrer dans la MTU (Max Transfert Unit)"""
        chain = 'CLAMP-MSS'
        if table == 'mangle':
            pretty_print(table, chain)

            # Problèmes de mss sur ratp.fr (1455 au lieu de 1456)
            # Cf mail nounou du 23/06/2016
            for ip in ['195.200.228.10', '195.200.228.170']: # ratp.fr
                self.add(table, chain, '-p tcp --dst %s --tcp-flags SYN,RST SYN -j TCPMSS --set-mss 1455' % ip)

            self.add(table, chain, '-p tcp --tcp-flags SYN,RST SYN -j TCPMSS --clamp-mss-to-pmtu')
            print OK

        if apply:
            self.apply(table, chain)
        return chain

    def ingress_filtering(self, table=None, apply=False):
        """Pour ne pas router les paquêtes n'appartenant pas à notre plage ip voulant sortir de notre réseau
        et empêcher certain type de spoof (cf http://travaux.ovh.net/?do=details&id=5183)"""
        chain = 'INGRESS_FILTERING'
        if table == 'filter':
            pretty_print(table, chain)

            for net in base.config.NETs['all'] + base.config.NETs['wifi-new'] + base.config.NETs['fil-new']:
                self.add(table, chain, '-o %s -s %s -j RETURN' % (dev['out'], net))
            self.add(table, chain, '-o %s -j LOG --log-prefix BAD_ROUTE' % dev['out'])
            self.add(table, chain, '-o %s -j DROP' % dev['out'])
            for net_d in base.config.NETs['all']:
                for net_s in base.config.NETs['all']:
                    self.add(table, chain,'-i %s ! -s %s -d %s -j RETURN' % (dev['out'], net_s, net_d))
            self.add(table, chain,'-i %s -j LOG --log-prefix BAD_SRC' % dev['out'])
            self.add(table, chain,'-i %s -j DROP' % dev['out'])
            print OK

        if apply:
            self.apply(table, chain)
        return chain

    def ssh_on_https(self, table=None, apply=False):
        """Pour faire fonctionner ssh2.crans.org"""
        chain = 'SSH2'

        if table == 'nat':
            pretty_print(table, chain)
            self.add(table, chain, '-p tcp -d 138.231.136.2 --dport 22 -j DNAT --to-destination 138.231.136.1:22')    # redirection du ssh vers zamok
            self.add(table, chain, '-p tcp -d 138.231.136.2 --dport 80 -j DNAT --to-destination 138.231.136.1:81')    # redirection du ssh vers zamok a travers httptunnel
            self.add(table, chain, '-p tcp -d 138.231.136.2 --dport 443 -j DNAT --to-destination 138.231.136.1:22')    # redirection du ssh vers zamok (pour passer dans un proxy, avec corkscrew)
            print OK

        if apply:
            self.apply(table, chain)
        return chain

    def challenge_letsencrypt(self, table=None, apply=False):
        """PNAT des request de challenge letsencrypt vers bakdaur ou frontdaur"""
        chain = "CHALLENGE-LESENCRYPT"

        if table == 'nat':
            pretty_print(table, chain)

            bakdaur = self.conn.search(u"host=bakdaur.crans.org")[0]
            frontdaur = self.conn.search(u"host=frontdaur.crans.org")[0]

            self.add(table, chain, '-m condition ! --condition challenge-letsencrypt -j RETURN')
            for net in (base.config.NETs['serveurs']):
                self.add(table, chain, '-p tcp -d %s --dport 80 -m condition --condition challenge-letsencrypt-bakdaur -j DNAT --to-destination %s:81' % (net, bakdaur['ipHostNumber'][0]))
                self.add(table, chain, '-p tcp -d %s --dport 80 -m condition ! --condition challenge-letsencrypt-bakdaur -j DNAT --to-destination %s:81' % (net, frontdaur['ipHostNumber'][0]))
            print OK

        if apply:
            self.apply(table, chain)
        return chain

    def connexion_filnew(self, table=None, apply=False):
        """PNAT le vlan 21 Fil New par connexion Zayo
        Le nat est effectué sur une plage de 2000 ports.
        Ainsi chaque ip publique nat 26 ip privées (62000 - 10000)/2, car les 10000 premiers ports sont réservés
        Avec 10 ip publique on nat donc 256 ip privées. On pourrait en nater encore plus si on utilisait tous les
        ports, mais on en a pas l'utilité pour l'instant"""
        chain = 'CONNEXION-FIL-NEW'

        if table == 'nat':
            pretty_print(table, chain)
            fil_nat_prive_ip_plage = base.config.firewall.nat_prive_ip_plage['fil']
            for nat_ip_range in range(1, 26):
                range_name = 'nat' + fil_nat_prive_ip_plage.split('.')[1] + '_' + str("%02d" % nat_ip_range )
                self.add(table, chain, '-s ' + '.'.join(fil_nat_prive_ip_plage.split('.')[:2]) + '.' + str(nat_ip_range) + '.0/24 -j ' + range_name)
            for nat_ip_range in range(1, 26):
                range_name = 'nat' + fil_nat_prive_ip_plage.split('.')[1] + '_' + str("%02d" % nat_ip_range)
                for nat_ip_subrange in range(16):
                    subrange_name = range_name + '_' + str(hex(nat_ip_subrange)[2:])
                    self.add(table, range_name, '-s ' + '.'.join(fil_nat_prive_ip_plage.split('.')[:2]) + '.' + str(nat_ip_range) + '.' + str(nat_ip_subrange*16) + '/28 -j ' + subrange_name)
                for nat_private_ip in range(256):
                    ip_src = '.'.join(fil_nat_prive_ip_plage.split('.')[:2]) + '.' + str(nat_ip_range) + '.' + str(nat_private_ip) + '/32'

                    port_low = 10000 + 2000*(nat_private_ip%26)
                    port_high = port_low + 1999

                    subrange_name = range_name + '_' + str(hex(nat_private_ip/16)[2:])

                    # On nat
                    for interface in base.config.firewall.nat_pub_ip_plage['fil']:
                        ip_nat = '.'.join(base.config.firewall.nat_pub_ip_plage['fil'][interface].split('.')[:3]) + '.' + str(10*(nat_ip_range - 1) + nat_private_ip/26)
                        self.add(table, subrange_name, '-s %s -o %s -p tcp -j SNAT --to-source %s' % (ip_src, dev[interface], ip_nat + ':' + str(port_low) + '-' + str(port_high)))
                        self.add(table, subrange_name, '-s %s -o %s -p udp -j SNAT --to-source %s' % (ip_src, dev[interface], ip_nat + ':' + str(port_low) + '-' + str(port_high)))

            # On nat tout ce qui match dans les règles et qui n'est pas du tcp/udp derrière la première ip publique unused (25*10) + 1
            # Ne pas oublier de loguer ce qui sort de cette ip
            for interface in base.config.firewall.nat_pub_ip_plage['fil']:
                self.add(table, chain, '-s ' + fil_nat_prive_ip_plage + ' -o %s -j SNAT --to-source ' % (dev[interface],)  + '.'.join(base.config.firewall.nat_pub_ip_plage['fil'][interface].split('.')[:3]) + '.250')
            print OK

        if apply:
            self.apply(table, chain)
        return chain



    def connexion_wifinew(self, table=None, apply=False):
        """PNAT le vlan 22 WiFi New par connexion Zayo
        Le nat est effectué sur une plage de 2000 ports.
        Ainsi chaque ip publique nat 26 ip privées (62000 - 10000)/2, car les 10000 premiers ports sont réservés
        Avec 10 ip publique on nat donc 256 ip privées. On pourrait en nater encore plus si on utilisait tous les
        ports, mais on en a pas l'utilité pour l'instant"""
        chain = 'CONNEXION-WIFI-NEW'

        if table == 'nat':
            pretty_print(table, chain)
            wifi_nat_prive_ip_plage = base.config.firewall.nat_prive_ip_plage['wifi']
            for nat_ip_range in range(1, 26):
                range_name = 'nat' + wifi_nat_prive_ip_plage.split('.')[1] + '_' + str("%02d" % nat_ip_range )
                self.add(table, chain, '-s ' + '.'.join(wifi_nat_prive_ip_plage.split('.')[:2]) + '.' + str(nat_ip_range) + '.0/24 -j ' + range_name)
            for nat_ip_range in range(1, 26):
                range_name = 'nat' + wifi_nat_prive_ip_plage.split('.')[1] + '_' + str("%02d" % nat_ip_range)
                for nat_ip_subrange in range(16):
                    subrange_name = range_name + '_' + str(hex(nat_ip_subrange)[2:])
                    self.add(table, range_name, '-s ' + '.'.join(wifi_nat_prive_ip_plage.split('.')[:2]) + '.' + str(nat_ip_range) + '.' + str(nat_ip_subrange*16) + '/28 -j ' + subrange_name)
                for nat_private_ip in range(256):
                    ip_src = '.'.join(wifi_nat_prive_ip_plage.split('.')[:2]) + '.' + str(nat_ip_range) + '.' + str(nat_private_ip) + '/32'

                    port_low = 10000 + 2000*(nat_private_ip%26)
                    port_high = port_low + 1999

                    subrange_name = range_name + '_' + str(hex(nat_private_ip/16)[2:])

                    # On nat
                    for interface in base.config.firewall.nat_pub_ip_plage['wifi']:
                        ip_nat = '.'.join(base.config.firewall.nat_pub_ip_plage['wifi'][interface].split('.')[:3]) + '.' + str(10*(nat_ip_range - 1) + nat_private_ip/26)
                        self.add(table, subrange_name, '-s %s -o %s -p tcp -j SNAT --to-source %s' % (ip_src, dev[interface], ip_nat + ':' + str(port_low) + '-' + str(port_high)))
                        self.add(table, subrange_name, '-s %s -o %s -p udp -j SNAT --to-source %s' % (ip_src, dev[interface], ip_nat + ':' + str(port_low) + '-' + str(port_high)))

            # On nat tout ce qui match dans les règles et qui n'est pas du tcp/udp derrière la première ip publique unused (25*10) + 1
            # Ne pas oublier de loguer ce qui sort de cette ip
            for interface in base.config.firewall.nat_pub_ip_plage['wifi']:
                self.add(table, chain, '-s ' + wifi_nat_prive_ip_plage + ' -o %s -j SNAT --to-source ' % (dev[interface],)  + '.'.join(base.config.firewall.nat_pub_ip_plage['wifi'][interface].split('.')[:3]) + '.250')
            print OK

        if apply:
            self.apply(table, chain)
        return chain


    def connexion_ens(self, table=None, apply=False):
        """ Connexion de secours pour l'ENS"""
        chain = 'CONNEXION-ENS-BACKUP'

        if table == 'filter':
            pretty_print(table, chain)
            for net in base.config.NETs['all']:
                self.add(table, chain, ' -s ' + net + ' -j REJECT')
            self.add(table, chain, ' -s ' + base.config.plage_ens + ' -j ACCEPT')

        if table == "nat":
            pretty_print(table, chain)
            self.add(table, chain, ' -s ' + base.config.plage_ens + ' -o %s -j SNAT --to-source ' % (dev['zayo'],)  + '.'.join(base.config.firewall.nat_pub_ip_plage['wifi']['zayo'].split('.')[:3]) + '.251')
            print OK

        if apply:
            self.apply(table, chain)
        return chain


    def blacklist_soft_maj(self, ip_list):
        self.blacklist_soft(fill_ipset=True)

    def blacklist_soft(self, table=None, fill_ipset=False, apply=False):
        """Redirige les gens blacklisté vers le portail captif"""
        chain = 'BLACKLIST_SOFT'

        if fill_ipset:
            # On récupère la liste de toutes les ips blacklistés soft
            bl_soft_ips = self.blacklisted_ips(base.config.blacklist_sanctions_soft)
            anim('\tRestoration de l\'ipset %s' % self.ipset['blacklist']['soft'])
            self.ipset['blacklist']['soft'].restore(bl_soft_ips)
            print OK

        if table == 'filter':
            pretty_print(table, chain)
            self.add(table, chain, '-p tcp --dport 80 -m set --match-set %s src -j ACCEPT' % self.ipset['blacklist']['soft'] )
            self.add(table, chain, '-p tcp --sport 80 -m set --match-set %s dst -j ACCEPT' % self.ipset['blacklist']['soft'] )
            self.add(table, chain, '-p tcp -d 10.231.136.4 --dport 3128 -m set --match-set %s src -j ACCEPT' % self.ipset['blacklist']['soft'] )
            self.add(table, chain, '-p tcp -s 10.231.136.4 --sport 3128 -m set --match-set %s dst -j ACCEPT' % self.ipset['blacklist']['soft'] )
            print OK

        if table == 'nat':
            pretty_print(table, chain)
            for net in base.config.NETs['all']:
                self.add(table, chain, '-d %s -j RETURN' % net)
            self.add(table, chain, '-p tcp --dport 80 -m set --match-set %s src -j RETURN' % self.ipset['confirmation'] ) # Les gens qui ont cliqué -> fine !
            self.add(table, chain, '-p tcp --dport 80 -m set --match-set %s src -j DNAT --to-destination 10.231.136.4:3128' % self.ipset['blacklist']['soft'] )
            print OK

        if apply:
            self.apply(table, chain)
        return chain

    def blacklist_hard(self, table=None, fill_ipset=False, apply=False):
        """Bloque tout, sauf le 80 pour afficher le portail captif"""
        chain = 'BLACKLIST_HARD'

        if fill_ipset:
            # On récupère la liste de toutes les ips blacklistés hard
            bl_hard_ips = self.blacklisted_ips(base.config.blacklist_sanctions)
            anim('\tRestoration de l\'ipset %s' % self.ipset['blacklist']['hard'])
            self.ipset['blacklist']['hard'].restore(bl_hard_ips)
            print OK

        if table == 'filter':
            pretty_print(table, chain)
            # Same as blacklist_soft: autorise le port 80 et 3128 vers soi-même
            self.add(table, chain, '-p tcp --dport 80 -m set --match-set %s src -j ACCEPT' % self.ipset['blacklist']['hard'] )
            self.add(table, chain, '-p tcp --sport 80 -m set --match-set %s dst -j ACCEPT' % self.ipset['blacklist']['hard'] )
            self.add(table, chain, '-p tcp -d 10.231.136.4 --dport 3128 -m set --match-set %s src -j ACCEPT' % self.ipset['blacklist']['hard'] )
            self.add(table, chain, '-p tcp -s 10.231.136.4 --sport 3128 -m set --match-set %s dst -j ACCEPT' % self.ipset['blacklist']['hard'] )
            # Mais on continue en refusant le reste
            self.add(table, chain, '-m set --match-set %s src -j REJECT' % self.ipset['blacklist']['hard'] )
            self.add(table, chain, '-m set --match-set %s dst -j REJECT' % self.ipset['blacklist']['hard'] )
            print OK

        if table == 'nat':
            pretty_print(table, chain)
            for net in base.config.NETs['all']:
                self.add(table, chain, '-d %s -j RETURN' % net)
            self.add(table, chain, '-p tcp --dport 80 -m set --match-set %s src -j RETURN' % self.ipset['confirmation'] ) # Les gens qui ont cliqué -> fine !
            self.add(table, chain, '-p tcp --dport 80 -m set --match-set %s src -j DNAT --to-destination 10.231.136.4:3128' % self.ipset['blacklist']['hard'] )
            print OK

        if apply:
            self.apply(table, chain)
        return chain

    def reseaux_non_routable(self, table=None, fill_ipset=False, apply=False):
        """Bloque les réseaux non routables autres que ceux utilisés par le crans"""
        chain = 'RESEAUX_NON_ROUTABLES'

        if fill_ipset:
            anim('\tRestoration de l\'ipset reseaux_non_routable')
            allowed = [ net for nets in base.config.NETs.values() for net in nets if utils.NetInNets(net, base.config.firewall.reseaux_non_routables) ]
            self.ipset['reseaux_non_routable']['allow'].restore(allowed)
            self.ipset['reseaux_non_routable']['deny'].restore(base.config.firewall.reseaux_non_routables)
            print OK

        if table == 'filter':
            pretty_print(table, chain)
            self.add(table, chain, '-m set --match-set %s src -j RETURN' %  self.ipset['reseaux_non_routable']['allow'])
            self.add(table, chain, '-m set --match-set %s dst -j RETURN' %  self.ipset['reseaux_non_routable']['allow'])
            self.add(table, chain, '-m set --match-set %s src -j DROP' %  self.ipset['reseaux_non_routable']['deny'])
            self.add(table, chain, '-m set --match-set %s dst -j DROP' %  self.ipset['reseaux_non_routable']['deny'])
            print OK

        if apply:
            self.apply(table, chain)
        return chain

    def filtrage_ports_maj(self, ip_lists):
        self.filtrage_ports('filter', apply=True)

    def filtrage_ports(self, table=None, apply=False):
        """Ouvre les ports vers et depuis les machines du réseau crans"""
        chain = 'FILTRAGE-PORTS'

        def format_port(port):
            port = str(port)
            if port.endswith(':'):
                port = '%s65535' % port
            if port.startswith(':'):
                port = '0%s' % port
            return port

        def add_ports(ip, machine, proto, sens):
            self.add(
                table,
                chain,
                '-p %s -%s %s -m multiport --dports %s -j RETURN' % (
                        proto,
                        (sens=='out' and 's') or (sens == 'in' and 'd'),
                        ip,
                        ','.join( format_port(port) for port in machine['port%s%s' % (proto.upper(), sens)])
                )
            )

        if table == 'filter':
            pretty_print(table, chain)
            self.add(table, chain, '-m set --match-set %s dst,dst -j ACCEPT' % self.ipset['ip_port_tmp'] )
            for net in base.config.NETs['serveurs'] + base.config.NETs['dmz']:
                for proto in base.config.firewall.srv_ports_default.keys():
                    if base.config.firewall.srv_ports_default[proto]['output']:
                        self.add(table, chain, '-p %s -s %s -m multiport --dports %s -j RETURN' % (proto, net, ','.join( format_port(port) for port in base.config.firewall.ports_default[proto]['output'])))
                    if base.config.firewall.srv_ports_default[proto]['input']:
                        self.add(table, chain, '-p %s -d %s -m multiport --dports %s -j RETURN' % (proto, net, ','.join( format_port(port) for port in base.config.firewall.ports_default[proto]['input'])))
            for net in base.config.NETs['adherents'] + base.config.NETs['fil-new'] + base.config.NETs['fil-pub'] + base.config.NETs['fil-new-pub'] + base.config.NETs['wifi-new-pub'] + base.config.NETs['wifi-new']:
                for proto in base.config.firewall.ports_default.keys():
                    if base.config.firewall.ports_default[proto]['output']:
                        self.add(table, chain, '-p %s -s %s -m multiport --dports %s -j RETURN' % (proto, net, ','.join( format_port(port) for port in base.config.firewall.ports_default[proto]['output'])))
                    if base.config.firewall.ports_default[proto]['input']:
                        self.add(table, chain, '-p %s -d %s -m multiport --dports %s -j RETURN' % (proto, net, ','.join( format_port(port) for port in base.config.firewall.ports_default[proto]['input'])))

            for machine in self.machines():
                for ip in machine['ipHostNumber']:
                    if 'portTCPout' in machine:
                        add_ports(ip, machine, 'tcp', 'out')
                    if 'portUDPout' in machine:
                        add_ports(ip, machine, 'udp', 'out')
                    if 'portTCPin' in machine:
                        add_ports(ip, machine, 'tcp', 'in')
                    if 'portUDPin' in machine:
                        add_ports(ip, machine, 'udp', 'in')

            self.add(table, chain, '-j REJECT')
            print OK

        if apply:
            self.apply(table, chain)
        return chain

    def limitation_debit(self, table=None, run_tc=False, apply=False):
        """Limite le débit de la connexion selon l'agréement avec l'ENS"""
        chain = 'LIMITATION-DEBIT'

        debit_max = base.config.firewall.debit_max
        uplink_speed = '1024mbit'


        if table == 'mangle':
            pretty_print(table, chain)
            # Pas de QoS vers/depuis la zone ENS
            self.add(table, chain, '-d 138.231.0.0/16 -s 138.231.0.0/16 -j RETURN')

            # Idem pour le ftp
            self.add(table, chain, '-d ftp.crans.org -j RETURN')
            self.add(table, chain, '-s ftp.crans.org -j RETURN')

            self.add(table, chain, '-d tv.crans.org -j RETURN')
            self.add(table, chain, '-s tv.crans.org -j RETURN')

            # Idem vers soyouz pour le test de la connection de secours
            self.add(table, chain, '-d soyouz.crans.org -j RETURN')
            self.add(table, chain, '-s soyouz.crans.org -j RETURN')

            # Classification par defaut pour tous les paquets
            for net in base.config.NETs['all']:
                self.add(table, chain, '-o %s -s %s -j CLASSIFY --set-class 1:10' % (dev['out'], net))
                self.add(table, chain, '-o %s -d %s -j CLASSIFY --set-class 1:10' % (dev['fil'], net))

            print OK

        if run_tc:
            anim('\tApplication des commandes tc')
            for int_key in ['out', 'fil']:
                try:
                    utils.tc('qdisc del dev %s root' % dev[int_key])
                except utils.TcError:
                    pass
                utils.tc('qdisc add dev %s root handle 1: htb r2q 1' % dev[int_key])
                utils.tc("class add dev %s parent 1: classid 1:1 "
                   "htb rate %s ceil %s" % (dev[int_key], uplink_speed, uplink_speed))
                utils.tc("class add dev %s parent 1:1 classid 1:2 "
                   "htb rate %smbit ceil %smbit" % (dev[int_key], debit_max[int_key], debit_max[int_key]))

                # Classe par defaut
                utils.tc('class add dev %s parent 1:2 classid 1:10 '
                       'htb rate %smbit ceil %smbit prio 1' % (dev[int_key], debit_max[int_key], debit_max[int_key]))
                utils.tc('qdisc add dev %s parent 1:10 '
                       'handle 10: sfq perturb 10' % dev[int_key])

                # Classe par pour la voip
                utils.tc('class add dev %s parent 1:2 classid 1:12 '
                       'htb rate %smbit ceil %smbit prio 0' % (dev[int_key], debit_max[int_key], debit_max[int_key]))
                utils.tc('qdisc add dev %s parent 1:12 '
                       'handle 12: sfq perturb 10' % dev[int_key])

            print OK

        if apply:
            self.apply(table, chain)
        return chain



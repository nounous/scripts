; {{ model }}A Configuration Editor; Created on release #{{ firmware }}

hostname "{{ hostname }}"
; Generated on {{ date_gen }} by switchs2.py
{% for module in modules %}
module {{ loop.index }} type {{ model }}A
{% endfor %}
;--- Snmp ---
snmp-server contact "root@crans.org"
snmp-server location "Batiment {{ bat }}"
;A faire à la main
snmpv3 enable
snmpv3 restricted-access
;snmpv3 user "initial"
snmpv3 user "crans"
snmpv3 group ManagerPriv user "crans" sec-model ver3
snmp-server community "public" Operator
;--- Heure/date ---
time timezone 60
time daylight-time-rule Western-Europe
{%- for server in ntp_servers %}
{%- if SNTP_NEW_SYNTAX in features %}
sntp server priority {{ loop.index }} {{ server|ipv4 }} 4
{%- if IPv6_MGMT in features %}
sntp server priority {{ loop.index + 1 }} {{ server|ipv6 }} 4
{%- endif %}
{%- else %}
sntp server {{ server|ipv4 }}
{%- endif %}
{%- endfor %}
timesync sntp
sntp unicast
;--- Misc ---
console inactivity-timer 30
;--- Logs ---
{%- for server in log_servers %}
logging {{ server|ipv4 }}
{%- if IPv6_MGMT in features and IPv6_LOGGING in features %}
logging {{ server|ipv6 }}
{%- endif %}
{%- endfor %}
;--- IP du switch ---
no ip default-gateway
max-vlans {{ max_vlans }}
{%- if OOBM in features %}
oobm
    no ip address
    exit
{%- endif %}
{%- for name, vlan in vlans.iteritems() %}
vlan {{ vlan["id"] }}
   name "{{ name|capitalize }}"
   {%- if vlan["tagged"] %}
   tagged {{ vlan["tagged"] }}
   {%- endif %}
   {%- if vlan["untagged"] %}
   untagged {{ vlan["untagged"] }}
   {%- endif %}
   {%- if vlan['ip'] %}
   ip address {{ vlan['ip'] }} {{ vlan['network']['IPv4'].netmask }}
   {%- else %}
   no ip address
   {%- endif %}
   {%- if vlan['ipv6'] and IPv6_MGMT in features %}
   ipv6 address {{ vlan['network']['IPv6'] }} eui-64
   {%- elif IPv6_MGMT in features %}
   no ipv6 enable
   {%- endif %}
   {%- if vlan['igmp_snooping'] and IGMP_SNOOPING in features %}
   ip igmp
   no ip igmp querier
   {%- endif %}
   {%- if vlan['mld_snooping'] and MLD_SNOOPING in features %}
   ipv6 mld enable
   no ipv6 mld querier
   {%- endif %}
   {%- for l in vlan['extra'] %}
   {{ l|indent(3, false) }}
   {%- endfor %}
exit
{%- endfor %}
;--- Accès d'administration ---
no telnet-server
no web-management
aaa authentication ssh login public-key none
aaa authentication ssh enable public-key none
ip ssh
ip ssh filetransfer
ip authorized-managers {{ vlans['switches']['network']['IPv4'].ip }} {{ vlans['switches']['network']['IPv4'].netmask }}
{%- if IPv6_MGMT in features %}
ipv6 authorized-managers {{ vlans['switches']['network']['IPv6'].ip }} {{ vlans['switches']['network']['IPv6'].netmask }}
{%- endif %}
;--- Protection contre les boucles ---
loop-protect disable-timer 30
loop-protect transmit-interval 3
loop-protect {{ non_trusted }}
;--- Serveurs radius ---
radius-server dead-time 2
{%- for server in radius_servers %}
radius-server host {{ server|ipv4 }} key {{ radius_key }}
{%- endfor %}
{%- if RADIUS_DAE in features %}
{%- for server in radius_servers %}
radius-server host {{ server|ipv4 }} dyn-authorization
{%- endfor %}
radius-server dyn-autz-port 3799
{%- endif %}
;--- Filtrage mac ---
aaa port-access mac-based addr-format multi-colon
;--- Bricoles ---
no cdp run
{%- if STACKING_DISABLED in features %}
no stack
{%- endif %}
{%- if DHCP_SNOOPING in features %}
;--- DHCP Snooping ---
dhcp-snooping vlan {{ vlans.values()|selectattr("dhcp_snooping")|join(" ", attribute="id") }}
{%- for rid in dhcp_server_rid %}
{%- for vlan, vconfig in vlans.items() if vconfig['dhcp_snooping'] %}
dhcp-snooping authorized-server {{ vconfig['network']['IPv4'].ip + rid }}
{%- endfor %}
{%- endfor %}
dhcp-snooping
{%- endif %}
{%- if ARP_PROTECT in features %}
;--- ARP Protect ---
arp-protect
arp-protect vlan {{ vlans.values()|selectattr("dhcp_snooping")|join(" ", attribute="id") }}
arp-protect validate src-mac dest-mac
{%- endif %}
{%- if DHCPv6_SNOOPING in features %}
;--- DHCPv6 Snooping ---
dhcpv6-snooping vlan {{ vlans.values()|selectattr("dhcp_snooping")|join(" ", attribute="id") }}
{%- for mac in dhcpv6_server_mac %}
{%- for vlan, vconfig in vlans.items() if vconfig['dhcp_snooping'] and vconfig['network']['IPv6'] %}
dhcpv6-snooping authorized-server {{ mac|ip6_of_mac(vconfig['network']['IPv6']) }}
{%- endfor %}
{%- endfor %}
dhcpv6-snooping
{%- endif %}
{%- if RA_GUARD in features %}
;--- RA guards ---
ipv6 ra-guard ports {{ non_trusted }}
no ipv6 ra-guard ports {{ trusted }}
{% endif %}
;--- Config des prises ---
{%- for port in ports %}
{%- if port.radius_auth %}
{%- if ieee8021X %}
aaa port-access authenticator {{ port.num }}
aaa port-access authenticator {{ port.num }} client-limit {{ port.num_mac }}
aaa port-access authenticator {{ port.num }} logoff-period 3600
{%- endif %}
aaa port-access mac-based {{ port.num }}
aaa port-access mac-based {{ port.num }} addr-limit {{ port.num_mac }}
aaa port-access mac-based {{ port.num }} logoff-period 3600
aaa port-access mac-based {{ port.num }} unauth-vid 1
{%- endif %}
interface {{ port.num }}
   enable
   name "{{ port }}"
   {{ port.flowcontrol }}
   {%- if port.trusted %}
   {%- if DHCP_SNOOPING in features %}
   dhcp-snooping trust
   {%- endif %}
   {%- endif %}
   {%- if ARP_PROTECT in features and port.arp_protect_trust %}
   arp-protect trust
   {%- endif %}
   {%- if port.trusted and DHCPv6_SNOOPING in features %}
   dhcpv6-snooping trust
   {%- endif %}
   {%- if port.enable_poe and POE in features %}
   power-over-ethernet {{ port.poe_level }}
   poe-allocate-by class
   {%- elif POE in features %}
   no power-over-ethernet
   {%- endif %}
   no lacp
exit
{%- endfor %}
{%- if ieee8021X %}
;--- Configuration IEEE 802.1X ---
aaa authentication port-access eap-radius
aaa port-access authenticator active
{%- endif %}
;--- Configuration comptabilisation RADIUS ---
aaa accounting network start-stop radius
aaa accounting session-id unique
aaa accounting update periodic 240
{%- if FILTER_MDNS in features %}
;--- Filtre de protocole ---
filter multicast 01005e0000fb drop all
filter multicast 3333000000fb drop all
{%- endif %}

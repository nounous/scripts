#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

import pika
import argparse

import gestion.config.trigger as trigger_config
import gestion.secrets_new as secrets

def delete_queues(args):
    """Supprime les queues passées en argument"""
    trigger_password = secrets.get('rabbitmq_trigger_password')
    credentials = pika.PlainCredentials(trigger_config.user, trigger_password)

    connector = pika.adapters.blocking_connection.BlockingConnection(
        pika.ConnectionParameters(
            host=trigger_config.master,
            port=trigger_config.port,
            credentials=credentials,
            ssl=trigger_config.ssl
        )
    )

    channel = connector.channel()

    for queue_name in args.noms:
        try:
            channel.queue_delete(queue_name)
            print "Queue %s supprimée." % (queue_name,)
        except pika.exceptions.ChannelClosed as excep:
            print repr(excep)
            channel = connector.channel()

    try:
        channel.close()
    except pika.exceptions.ChannelClosed as excep:
        pass

    try:
        connector.close()
    except Exception as excep:
        pass

if __name__ == '__main__':
    PARSER = argparse.ArgumentParser(description="Détruit une queue", add_help=False)
    PARSER.add_argument('-h', '--help', help="Affiche ce message et quitte.", action="store_true")
    PARSER.add_argument('noms', type=str, nargs="+", help="Le nom des queues à détruire.")

    ARGS = PARSER.parse_args()

    delete_queues(ARGS)

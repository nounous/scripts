#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

""" Variables de configuration pour le firewall """

import datetime
from .feries import is_ferie

#: Interfaces réseaux des machines ayant un pare-feu particulier
dev = {
   'odlyd': {
        'out' : 'eth1',
        'zayo' : 'ens1f0.26',
        'borne' : 'eth0.3',
        'fil' : 'eth0.1',
        'app' : 'eth0.21',
        'wifi-new' : 'eth0.22',
        'adm' : 'eth0.2',
    },
    'sable': {
        'out' : 'eth1',
        'zayo' : 'eth1.26',
        'borne' : 'eth0.3',
        'fil' : 'eth0.1',
        'app' : 'eth0.21',
        'wifi-new' : 'eth0.22',
        'adm' : 'eth0.2',
    },
    'zamokv5': {
        'fil' : 'eth0.1',
        'adm' : 'eth0.2',
    },
    'ipv6-zayo': {
         'wifi' : 'ens19',
         'fil' : 'ens18',
         'app' : 'ens20',
         'wifi-new' : 'ens21',
         'adm' : 'ens23',
    },
    'zamok': {
        'fil' : 'crans',
        'adm' : 'crans.2'
    },
    'routeur': {
        'fil' : 'eth0',
        'adm' : 'eth1',
        'accueil' : 'eth2',
        'isolement' : 'eth3',
        'app' : 'eth4'
    },
}


### Firewall, binding des interfaces

firewall_config = {
    'sable' : {
        'role' : ['routeur4'],
        'interfaces' : {
            'sortie' : ['eth1.26', 'eth1'],
            'non-routables' : ['eth0.2', 'eth0.3', 'eth0.4', 'eth0.7', 'eth0.9'],
            'routables' : ['eth0.1', 'eth0.21', 'eth0.22', 'eth1.23', 'eth1.24'],
        },
        'dev' : {
            'out' : 'eth1',
            'zayo' : 'eth1.26',
            'fil' : 'eth0.1',
        },
    },
#    'odlyd' : {
#        'role' : ['routeur4'],
#        'interfaces' : {
#            'sortie' : ['ens1f0.26', 'eth1'],
#            'non-routables' : ['eth0.2', 'eth0.3'],
#            'routables' : ['eth0.1', 'eth0.21', 'eth0.22', 'ens1f0.23', 'ens1f0.24'],
#        },
#        'dev' : {
#            'out' : 'eth1',
#            'zayo' : 'ens1f0.26',
#            'fil' : 'eth0.1',
#        },
#    },
    'ipv6-zayo' : {
        'role' : ['routeur6'],
        'interfaces' : {
            'sortie' : ['ens22'],
            'non-routables' : ['ens19', 'ens23'],
            'routables' : ['ens2', 'ens1', 'ens22', 'ens21', 'ens20', 'ens18'],
        },
    },
    'zamok' : {
        'role' : ['users'],
        'interfaces' : {
            'non-routables' : ['eth0.2'],
            'routables' : ['eth0.1'],
        },
    },
    'redisdead' : {
        'role' : [],
        'interfaces' : {
            'non-routables' : ['eth1'],
            'routables' : ['eth0'],
        },
    },
    'silice' : {
        'role' : [],
        'interfaces' : {
            'non-routables' : ['eth1'],
            'routables' : ['eth0'],
        },
    },
    'routeur' : {
        'role' : ['portail'],
        'interfaces' : {
            'sortie' : ['ens18'],
            'non-routables' : ['ens19'],
            'routables' : ['ens20', 'ens21'],
        },
    },
    'radius' : {
        'role' : ['radius'],
        'interfaces' : {
            'sortie' : ['eth3'],
        },
    },
    'eap' : {
        'role' : ['radius'],
        'interfaces' : {
            'sortie' : ['eth3'],
        },
    }
}

### Serveurs / ports d'internet autorisés à parler aux radius
radius_server = [
{   'ipaddr' : '62.210.81.204',
    'ip6addr' : '2001:bc8:273e::',
    'protocol' : 'udp',
    'port' : ['1812', '1813', '1814']},
{   'ipaddr' : '185.230.78.47',
    'ip6addr' : '2a0c:700:0:23:67:e5ff:fee9:5',
    'protocol' : 'udp',
    'port' : ['1812', '1813', '1814']}]


### Role des routeurs
routeurs_du_crans = {
    'routeur_main' : 'odlyd',
    'routeur_secondary' : 'sable',
}

portail = {
        'accueil' : '10.51.0.10',
        'isolement' : '10.52.0.10',
}

# Empiriquement, 95 correspond à un débit de 100Mbit/s
# sur des outils tels que munin
now=datetime.datetime.now()
if now.hour >= 6 and now.hour < 19 and now.weekday() < 5 and not is_ferie():
    #: Débit maximal autorisé
    debit_max = { 'total' : 250,
                  'out' : 250,
                  'wifi' : 100,
                  'fil' : 150 }
    # mbits per second en connexion de jour
    #: Est-ce qu'on est en connexion de jour ou de nuit/week-end ?
    debit_jour = True
else:
    #: Débit maximal autorisé
    debit_max = { 'total' : 600,
                  'out' : 600,
                  'wifi' : 150,
                  'fil' : 450 }
    # mbits per second en conn de nuit et du week-end
    #: Est-ce qu'on est en connexion de jour ou de nuit/week-end ?
    debit_jour = False

#: Liste des réseaux non routables
reseaux_non_routables = [ '10.0.0.0/8', '172.16.0.0/12','198.18.0.0/15',
        '169.254.0.0/16', '192.168.0.0/16', '224.0.0.0/4', '100.64.0.0/10',
        '0.0.0.0/8','127.0.0.0/8','192.0.2.0/24','198.51.100.0/24','203.0.113.0/24',
        '192.0.0.0/29', '240.0.0.0/4', '255.255.255.255/32',
        ]

#### Ouverture des ports

#: Ports ouverts par défaut pour les adhérents dans le pare-feu
ports_default = {
    'tcp' : {
        'input' :  [ '22' ],
        'output' :  [ ':24', '26:134', '136', '140:444', '446:']
    },
    'udp' : {
        'input' :  [],
        'output' :  [ ':136','140:']
    }
}

ports_realm = {
    '4' : ['fil-new', 'wifi-new', 'adherents', 'fil-pub', 'fil-new-pub', 'wifi-new-pub'],
    '6' : ['wifi-new', 'fil-new', 'fil-pub'],
}
ports_realm_srv = {
    '4' : ['dmz', 'serveurs'],
    '6' : ['dmz', 'fil'],
}

srv_ports_default = {
    'tcp' : {
        'input' : [ '22' ],
        'output' : []
    },

    'udp' : {
        'input' : [],
        'output' : []
    }
}


######### NAT ################

# On indique ici OBLIGATOIREMENT des /24 d'ip pub
# qui seront utilisés pour nater le trafic en postrouting
# suivant l'interface de sortie
nat_pub_ip_plage = {
    'wifi' : {
        'zayo' : '185.230.76.0/24',
        'fil' : '185.230.76.0/24',
        'out' : '138.231.144.0/24'
    },
    'fil' : {
        'zayo' : '185.230.77.0/24',
        'fil' : '185.230.77.0/24',
        'out' : '138.231.145.0/24'
    }
}

### Indiquer ici OBLIGATOIREMENT un /16 privé
nat_prive_ip_plage = {
    'wifi' : '10.53.0.0/16',
    'fil' : '10.54.0.0/16'
}

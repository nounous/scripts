#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

""" Variables de configuration pour la gestion du DNS """

import os

# import des variables génériques
import __init__ as config

#: ariane et ariane2 pour la zone parente
parents = [
    '138.231.176.4',
    '138.231.176.54',
]
#: DNS master
master = '10.231.136.118'
#: DNS soyouz
soyouz = '91.121.179.40'
#: DNS slaves (ovh et titanic /aka/ freebox)
slaves = [
    '10.231.136.14', # titanic aka freebox
    '10.231.136.108', # soyouz aka ovh
]
#: DNS master de la zone tv
master_tv = master
#: DNS slaves de la zone tv
slaves_tv = slaves

#: Zone tv
zone_tv = 'tv.crans.org'

#: DNS en connexion de secours
secours_relay = '10.231.136.14';

#: Serveurs autoritaires pour les zones crans, le master doit être le premier
DNSs = [
    'silice.crans.org',
    'freebox.crans.org',
    'soyouz.crans.org',
]

MASTERs = ['silice']
DNSSEC_BACKEND = {
   #'sable': 'opendnssec',
    'silice': 'bind',
}

# noms (relatif) sur lequels on veux mettre les enregistrements MX
# utiliser @ pour l'apex des zone dns (crans.org, crans.fr, etc...)
MXs_NAMES = ['@', 'lists']

MXs = {
    'redisdead.crans.org': {
        'prio': 10,
    },
    'freebox.crans.org': {
        'prio': 25,
    },
    'soyouz.crans.org': {
        'prio': 15,
    },
}

#: Résolution DNS directe, liste de toutes les zones crans hors reverse
zones_direct = [
    'crans.org',
    'crans.ens-cachan.fr',
    'wifi.crans.org',
    'clubs.ens-cachan.fr',
    'adm.crans.org',
    'crans.eu',
    'crans.fr',
    'wifi.crans.eu',
    'tv.crans.org',
    'switches.crans.org',
    'adh.crans.org',
    'fil.crans.org',
    'borne.crans.org',
]
#: Les zones apparaissant dans des objets lc_ldap
zones_ldap = [
    'crans.org',
    'crans.ens-cachan.fr',
    'wifi.crans.org',
    'clubs.ens-cachan.fr',
    'adm.crans.org',
    'tv.crans.org',
    'switches.crans.org',
    'fil.crans.org',
    'adh.crans.org',
    'borne.crans.org',
]
#: Zones signée par opendnssec sur le serveur master
zones_dnssec = [
    'crans.org',
    'wifi.crans.org',
    'adm.crans.org',
    'tv.crans.org',
    'crans.eu',
    'crans.fr',
    'switches.crans.org',
    'adh.crans.org',
    'fil.crans.org',
    'borne.crans.org',
    '7.0.c.0.a.2.ip6.arpa', # subnet
    '1.0.0.0.0.0.0.0.0.0.7.0.c.0.a.2.ip6.arpa', # fil
    '2.0.0.0.0.0.0.0.0.0.7.0.c.0.a.2.ip6.arpa', # adm
    '1.2.0.0.0.0.0.0.0.0.7.0.c.0.a.2.ip6.arpa', # fil-new
    '2.2.0.0.0.0.0.0.0.0.7.0.c.0.a.2.ip6.arpa', # wifi-new
    '3.2.0.0.0.0.0.0.0.0.7.0.c.0.a.2.ip6.arpa', # fil-pub
    '4.2.0.0.0.0.0.0.0.0.7.0.c.0.a.2.ip6.arpa', # dmz
]
#: Zones alias : copie les valeur des enregistrement pour la racine de la zone et utilise un enregistemenr DNAME pour les sous domaines
zone_alias = {
    'crans.org' : [
        'crans.eu',
        'crans.fr',
    ],
}

#: Résolution inverse v4
zones_reverse = config.NETs["all"] + config.NETs["adm"] + config.NETs["fil-new"] + config.NETs['wifi-new'] + config.NETs['dmz'] + config.NETs['fil-pub'] + config.NETs['switches'] + config.NETs['multicast'] + config.NETs['bornes']
#: Résolution inverse v6
zones_reverse_v6 = config.prefix['fil'] + config.prefix['fil-new'] + config.prefix['wifi-new'] + config.prefix['fil-pub'] + config.prefix['dmz'] + config.prefix['adm']# à modifier aussi dans bind.py

#: Serveurs DNS récursifs :
recursiv = {
    'fil' : [
        '138.231.136.160',
        '138.231.136.152',
    ],
    'borne' : [
        '10.231.148.160',
        '10.231.136.152',
    ],
    'evenementiel' : [
        '138.231.136.160',
        '138.231.136.152',
    ],
    'adm' : [
        '10.231.136.160',
        '10.231.136.152',
    ],
    'dmz' : [
        '185.230.79.160',
        '185.230.79.152',
    ],
    'fil-pub' : [
        '185.230.78.160',
        '185.230.78.152',
    ],
    'accueil' : [
        '10.51.0.10',
    ],
    'isolement' : [
        '10.52.0.10',
    ],
    'fil-new' : [
        '10.54.0.160',
        '10.54.0.152',
    ],
    'wifi-new' : [
        '10.53.0.160',
        '10.53.0.152',
    ],
}

#: Domaines correspondant à des mails crans
mail_crans = [
    'crans.org',
    'crans.fr',
    'crans.eu',
    'crans.ens-cachan.fr',
]

#: Les ip/net des vlans limité vue par les récursifs
menteur_clients = [
    "138.231.136.210",
    "138.231.136.10",
] + config.prefix['evenementiel']

# Chemins de fichiers/dossiers utiles.
DNS_DIR = '/etc/bind/generated/'
DNS_KEY = '/etc/bind/keys/'
DNSSEC_DIR = '/etc/bind/signed/'
# Fichier de définition des zones pour le maître
DNS_CONF = os.path.join(DNS_DIR, 'zones_crans')

# Fichier de définition des zones pour les esclaves géré par BCfg2
DNS_CONF_BCFG2 = "/var/lib/bcfg2/Cfg/etc/bind/generated/zones_crans/zones_crans"

DNS_IPV6 = True
REVERSE_IPV6 = True

#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

""" Définition de variables de configuration et de comportement des scripts Cr@ns """

import time
import datetime

# Valeur par défaut pour les champs d'études
etudes_defaults = [
    u"Établissement inconnu",
    u"Année inconnue",
    u"Domaine d'études inconnu"
]

gtfepoch = "19700101000000Z"


# Gestion des câblages
# Selon la date, on met :
# -periode_transitoire : on accepte ceux qui ont payé l'année dernière
def __annee():
    """Retourne l'année civile"""
    return time.localtime()[0]


# Prochaine période transitoire de l'année version generalizedTimeFormat
def gtf_debut_periode_transitoire():
    """Retourne le gtf de début de période transitoire"""
    return "%s0801000000+0200" % (__annee(),)


def gtf_fin_periode_transitoire():
    """Retourne le gtf de fin de période transitoire"""
    return "%s0930235959+0200" % (__annee(),)


# Version timestampées timezone-naïves
def debut_periode_transitoire():
    """Produit un timestamp de début de période transitoire"""
    return time.mktime(
        time.strptime(
            "%s/08/01 00:00:00" % (__annee(),),
            "%Y/%m/%d %H:%M:%S",
        )
    )


def fin_periode_transitoire():
    """Produit un timestamp de fin de période transitoire"""
    return time.mktime(
        time.strptime(
            "%s/09/30 23:59:59" % (__annee(),),
            "%Y/%m/%d %H:%M:%S",
        )
    )


# On est en période transitoire si on est dans le bon intervale
def periode_transitoire():
    """Calcule si on est en période transitoire"""
    return (
        debut_periode_transitoire()
        <= time.time()
        <= fin_periode_transitoire()
    )


def ann_scol():
    """Calcule l'année scolaire courante"""
    if time.time() <= debut_periode_transitoire():
        return __annee() - 1
    return __annee()

# Gel des câbleurs pas à jour de cotisation
# Les droits ne sont pas retirés mais il n'y a plus de sudo
bl_vieux_cableurs = False

###### Gestion du nombre maximum de machines filaires/wifi
max_machines_fixes = 2

##Création de comptes
# Gid des comptes créés
gid = 100
club_gid = 600
mailgroup = 8
default_rights = 0755
default_mail_rights = 0700
quota_soft = 10000000
quota_hard = 12000000
fquota_soft = 0
fquota_hard = 0
# Shell
login_shell = '/bin/zsh'
club_login_shell = '/usr/bin/rssh'
# Longueur maximale d'un login
maxlen_login = 25

shells_possibles = [
    u'/bin/csh',
    u'/bin/sh', # tout caca
    u'/bin/dash', # un bash light
    u'/usr/bin/rc',
    u'/usr/bin/fish',
    u'/usr/bin/ksh', # symlink vers zsh
    u'/bin/ksh', # symlink vers zsh
    u'/usr/bin/tcsh', # TENEX C Shell (csh++)
    u'/bin/tcsh', # TENEX C Shell (csh++)
    u'/bin/bash', # the Bourne-Again SHell
    u'/bin/zsh', # the Z shell
    u'/usr/bin/zsh', # the Z shell
    u'/usr/bin/screen',
    u'/bin/rbash', # Bash restreint
    u'/usr/bin/rssh', # restricted secure shell allowing only scp and/or sftp
    u'/usr/local/bin/badPassSh', # demande de changer de mot de passe
    u'/usr/bin/passwd', # idem
    u'/usr/local/bin/disconnect_shell', # déconnexion crans
    u'/usr/scripts/surveillance/disconnect_shell', # idem
    u'/usr/sbin/nologin', # This account is currently not available.
    u'/bin/false', # vraiment méchant
    u'/usr/bin/es', # n'exsite plus
    u'/usr/bin/esh', # n'existe plus
    u'', # le shell vide pour pouvoir les punir
]

shells_gest_crans_order = [
    "zsh",
    "bash",
    "tcsh",
    "screen",
    "rbash",
    "rssh",
    "badPassSh",
    "disconnect_shell"
]

shells_gest_crans = {
    "zsh" : {
        "path" : u"/bin/zsh",
        "desc" : "Le Z SHell, shell par défaut sur zamok"
    },
    "bash" : {
        "path" : u"/bin/bash",
        "desc" : "Le Bourne-Again SHell, shell par défaut de la plupart des Linux"
    },
    "tcsh" : {
        "path" : u"/bin/tcsh",
        "desc" : "C SHell ++"
    },
    "screen" : {
        "path" : u'/usr/bin/screen',
        "desc" : "Un gestionnaire de fenêtre dans un terminal"
    },
    "rbash" : {
        "path" : u"/bin/rbash",
        "desc" : "Un bash très restreint, voir man rbash"
    },
    "rssh" : {
        "path" : u"/usr/bin/rssh",
        "desc" : "Shell ne permettant que les transferts de fichiers via scp ou sftp"
    },
    "badPassSh" : {
        "path" : u"/usr/local/bin/badPassSh",
        "desc" : "Demande de changer de mot de passe à la connexion"
    },
    "disconnect_shell" : {
        "path" : u"/usr/local/bin/disconnect_shell",
        "desc" : "Shell pour les suspensions de compte avec message explicatif"
    },
}

# Utilisateurs à ajouter statiquement aux groupes
static_users_groups = {
    'adm': [
        'logcheck',
        'arpwatch',
    ],
}

# Quels droits donnent l'appartenance à quel groupe Unix ?
# Le dico est sous la forme groupe: [droits]
droits_groupes = {
    'adm' : [
        u'Nounou',
    ],
    'ancien' : [
        u'Ancien',
    ],
    'respbats' : [
        u'Imprimeur',
        u'Cableur',
        u'Nounou',
        u'Bureau',
    ],
    'apprentis' : [
        u'Apprenti',
    ],
    'moderateurs' : [
        u'Moderateur',
    ],
    'disconnect' : [
        u'Bureau',
    ],
    'imprimeurs' : [
        u'Imprimeur',
        u'Nounou',
        u'Tresorier',
    ],
    'bureau' : [
        u'Bureau',
    ],
    'webadm' : [
        u'Webmaster',
    ],
    'mailing' : {
	u'Nounou',
	u'Bureau',
	u'Moderateur',
    }
}

####### Les modes de paiement accepté par le crans

modePaiement = [
    'liquide',
    'paypal',
    'solde',
    'cheque',
    'carte',
    'comnpay',
    'arbitraire',
    'note',
]

####### Les ML
# Le + devant un nom de ML indique une synchronisation
# ML <-> fonction partielle : il n'y a pas d'effacement automatique
# des abonnés si le droit est retiré
droits_mailing_listes = {
    'paiements' : [
        u'Nounou',
        u'Bureau',
        u'Tresorier',
    ],
    'roots' : [
        u'Nounou',
        u'Apprenti',
    ],
    'mailman' : [
        u'Nounou',
    ],
    '+nounou' : [
        u'Nounou',
        u'Apprenti',
    ],
    'respbats' : [
        u'Cableur',
        u'Nounou',
        u'Bureau',
    ],
    'moderateurs' : [
        u'Moderateur',
        u'Bureau',
    ],
    'disconnect' : [
        u'Nounou',
        u'Bureau',
    ],
    'impression' : [
        u'Imprimeur',
    ],
    'bureau' : [
        u'Bureau',
    ],
    'tresorier' : [
        u'Tresorier',
    ],
    'ripe' : [
        u'Nounou',
        u'Bureau',
        u'Apprenti',
    ],
    '+apprentis' : [
        u'Apprenti',
    ],
    '+ca' : [
        u'Bureau',
        u'Apprenti',
        u'Nounou',
        u'Cableur',
        u'Ancien',
    ],
    '+federez' : [
        u'Bureau',
        u'Apprenti',
        u'Nounou',
    ],
    '+install-party' : [
        u'Bureau',
        u'Apprenti',
        u'Nounou',
    ],

    # Correspondance partielle nécessaire... Des adresses non-crans sont inscrites à ces ML.
    '+dsi-crans' : [
        u'Nounou',
        u'Bureau',
    ],
    '+crous-crans' : [
        u'Nounou',
        u'Bureau',
    ],
    '+saclay' : [
        u'Bureau',
    ],
}

#: Répertoire de stockage des objets détruits
cimetiere = '/home/cimetiere'

#: Adresses mac utiles
# Mac du routeur est la mac du routeur du crans (actuellement odlyd)
# Utilisé par ra2.py, à changer si le routeur est remplacé
mac_komaz = '00:19:bb:31:3b:80'
mac_du_routeur = 'ac:16:2d:7b:ec:e6'
mac_titanic = 'aa:73:65:63:6f:76'

#: Serveur principal de bcfg2
bcfg2_main = "bcfg2.adm.crans.org"

#: Fichier de mapping lun/nom de volume iscsi
ISCSI_MAP_FILE = "/var/lib/iscsi/iscsi_names_%s.py"

#: Algorithmes de hashage pour le champ SSHFP
# format : { algorithm : (IANA_id, ssh_algo) }
# où algorithm est tel qu'il apparait dans les fichiers /etc/ssh/ssh_host_%s_key.pub % algorithm
# IANA_id correspond à l'entier attribué par l'IANA pour l'algorithme dans les champs DNS SSHFP
# ssh_algo correspond a la première chaine de caractères donnant le nom de
# l'algorithme de chiffrement lorsque la clef ssh est dans le format openssh
# (algo key comment)
sshfp_algo = {
    "rsa" : (1, "ssh-rsa"),
    "ed25519" : (2, "ssh-ed25519"),
    "ecdsa-256" : (3, "ecdsa-sha2-nistp256"),
    "ecdsa-384" : (3, "ecdsa-sha2-nistp384"),
    "ecdsa-521" : (3, "ecdsa-sha2-nistp521"),
    "ecdsa"     : (3, "ecdsa-sha2-nistp521"),
    }

sshfs_ralgo = {}
for key, value in sshfp_algo.items():
    sshfs_ralgo[value[1]] = (value[0], key)

sshfp_hash = {
    "sha1" : 1,
    "sha256" : 2,
}

sshkey_max_age = int(9.869604401089358 * (365.25 * 24 * 3600))

sshkey_size = {
    'rsa':4096,
    'dsa':1024,
    'ecdsa':521,
}

#: Nombre de jours après le passage en chambre ???? où on supprime les machines
demenagement_delai = 8



#############################
## Paramètres des machines ##
#############################

## >>>>>>>>>>>>>>> La modification des paramètres suivants doit se
## >> ATTENTION >> faire avec précautions, il faut mettre la base à
## >>>>>>>>>>>>>>> jour en parralèle de ces modifs.

# Sous réseaux alloués à chaque type de machine ou bâtiment
# Pour la zone wifi, il faut penser à modifier le /etc/network/interfaces
# de sable, zamok et komaz pour ajouter les zones en plus (et de
# faire en sorte qu'ils prennent effet immédiatement ; c'est important pour
# komaz car c'est la route par défaut mais aussi pour zamok et sable
# à cause de leur firewall et de leur patte wifi.

plage_ens = '138.231.0.0/16'

plage_zayo = '185.230.76.0/22'

# Ip derrière lesquels sont natées les machines crans
# selon leur type et la destination. Si on va vers l'ENS
# on natte derrière une IP dans le range ENS. Sinon on
# natte derrière une ip crans dans le range Zayo.

NAT_WIFI_ENS='138.231.144.0/24'
NAT_WIFI='185.230.76.0/24'

NAT_FIL_ENS='138.231.145.0/24'
NAT_FIL='185.230.77.0/24'

# NETs_primaires contient une bijection entre des types de machines
# et les plages d'ip qui vont bien. NETs_secondaires contient des
# clefs qui cassent la bijectivité, mais qui peuvent servir.
# NETs est l'union des deux
NETs_primaires = {
    'serveurs' : [
        '138.231.136.0/24',
    ],
    'adherents' : [
        '138.231.137.0/24',
        '138.231.138.0/23',
        '138.231.140.0/22',
    ],
    'switches' : [
        '10.231.100.0/24',
    ],
    'bornes' : [
        '10.231.148.0/24',
    ],
    'adm' : [
        '10.231.136.0/24'
    ],
    'accueil' : [
        '10.51.0.0/16'
    ],
    'wifi-new-serveurs' : [
        '10.53.0.0/24'
    ],
    'wifi-new-adherents' : [
        '10.53.1.0/24',
        '10.53.2.0/23',
        '10.53.4.0/22',
        '10.53.8.0/21',
        '10.53.16.0/22',
        '10.53.20.0/24',
    ],
    'wifi-new-federez' : [
        '10.53.21.0/24',
        '10.53.22.0/23',
        '10.53.24.0/23',
    ],
    'fil-new-serveurs' : [
        '10.54.0.0/24'
    ],
    'fil-new-adherents' : [
        '10.54.1.0/24',
        '10.54.2.0/23',
        '10.54.4.0/22',
        '10.54.8.0/21',
        '10.54.16.0/21',
        '10.54.24.0/23',
    ],
    'isolement' : [
        '10.52.0.0/16'
    ],
    'evenementiel' : [
        '10.231.137.0/24'
    ],
    'multicast' : [
        '239.0.0.0/8'
    ],
    'wifi-new-pub' : [
        '185.230.76.0/24',
    ],
    'fil-new-pub' : [
        '185.230.77.0/24',
    ],
    'fil-pub' : [
        '185.230.78.0/24',
    ],
    'dmz' : [
         '185.230.79.0/24',
    ],
    #Plages de tests pour re2o
    're2o-adm' : [
         '10.155.0.0/24',
    ],
    're2o-fil-nat' : [
         '10.154.0.0/16',
    ],
    're2o-wifi-nat' : [
         '10.153.0.0/16',
    ],
    're2o-dmz' : [
         '138.231.145.0/24',
    ],
}

# Attention, dans NETs['all'] il n'y a que les anciennes IP des adhérents.
NETs_secondaires = {
    'all' : [
        '138.231.136.0/21',
        '138.231.144.0/21',
    ],
    'new' : [
        '185.230.76.0/22',
    ],
    'wifi': [
        '138.231.144.0/21',
    ],
    'fil' : [
        '138.231.136.0/21',
    ],
    'wifi-new' : [
        '10.53.0.0/16',
    ],
    'fil-new' : [
        '10.54.0.0/16',
    ],
}

NETs = {}
NETs.update(NETs_primaires)
NETs.update(NETs_secondaires)

NETs_regexp = {
    'all' : r'^1(38\.231\.1(3[6789]|4[0123456789]|5[01])|85\.230\.7[6789])\.\d+$'
}

# Classes de rid
# Merci d'essayer de les faire correspondre avec les réseaux
# ci-dessus...
# De même que pout NETs, primaires c'est pour la bijectivité, et secondaires
# pour les trucs pratiques
# https://wiki.crans.org/CransTechnique/PlanAdressage#Machines
rid_primaires = {
    # Rid pour les serveurs
    'serveurs' : [(0, 255),],
    # Rid pour les machines fixes
    'adherents' : [(256, 2047),],
    # Rid pour les machines adhérents avec ip publiques ( 185.230.78.0/24)
    'fil-pub' : [(2048, 2303),],
    # Rid pour les serveurs avec ip publique ( 185.230.79.0/24)
    'dmz' : [(2304, 2559),],
    # Rid pour les serveurs wifi sur nouvelle plage
    # Ip 10.53.0.0 - 10.53.0.255
    'wifi-new-serveurs' : [(10240, 10495)],
    # Rid pour les machines adhérents wifi sur la nouvelle plage
    # Ip 10.53.1.0 - 10.53.20.255
    'wifi-new-adherents' : [(10496, 15615)],
    # Rid pour les serveurs filaire natés sur nouvelle plage
    # Ip 10.54.0.0 - 10.54.0.255
    'fil-new-serveurs' : [(20480, 20735)],
    #Rid pour les machines adhérents fil sur la nouvelle plage
    # Ip 10.54.1.0 - 10.53.25.255
    'fil-new-adherents' : [(20736, 27135)],
    # Rid pour l'inscription des serveurs sur le vlan accueil
    'accueil' : [(30208, 30463),],
    # Rid pour l'inscription des serveurs sur le vlan isolement
    'isolement' : [(30464, 30719),],
    # Rid pour l'inscription des serveurs sur le vlan event
    'evenementiel' : [(30720, 30975),],
    # Rid pour les bornes
    'bornes' : [(57344, 57599),],
    # Rid pour les switches
    'switches' : [(57600, 57855),],
    # Rid pour machines spéciales
    'special' : [(4096, 6143),],
    # Rid pour les machines du vlan adm
    'adm' : [(51200, 53247),],
    # Un unique rid pour les machines multicast
    'multicast' : [(65535, 65535),],
}

rid_secondaires = {
    # Rid pour les machines filaire ipv4
    'fil' : [(0, 2047),],
    'wifi-new' : [(10240, 15615),],
    'fil-new' : [(20480, 27135),],
}

rid = {}
rid.update(rid_primaires)
rid.update(rid_secondaires)

# rid pour les machines spéciales (classe 'special' ci-dessus)
rid_machines_speciales = {
    # freebox.crans.org
    4096: '82.225.39.54',
    # ovh.crans.org
    4097: '91.121.84.138',
    # soyouz.crans.org
    4098: '91.121.179.40',
    # radius1-federez.crans.org
    4099: '62.210.81.204',
}

ipv6_machines_speciales = {
    # freebox
    4096: '2a01:e35:2e12:7360:a873:65ff:fe63:6f77',
    # ovh
    4097: '2001:41d0:1:898a::1',
    # soyouz
    4098: '2001:41d0:1:f428::1',
    # radius1
    4099: '2001:bc8:273e::',
}

# Les préfixes ipv6 publics
prefix = {
    'subnet' : [
        '2a0c:700:0::/40',
    ],
    'serveurs' : [
        '2a0c:700:0:1::/64',
    ],
   'adherents' : [
        '2a0c:700:0:1::/64',
    ],
    'multicast' : [
        '2a0c:700:0:1::/64',
    ],
    'fil' : [
        '2a0c:700:0:1::/64',
    ],
    'adm' : [
        '2a0c:700:0:2::/64',
    ],
    'switches' :[
        'fd01:240:fe3d:c804::/64',
    ],
    'accueil' : [
        '2a0c:700:0:7::/64',
    ],
    'isolement' : [
        '2a0c:700:0:9::/64',
    ],
    'wifi-new' : [
        '2a0c:700:0:22::/64',
    ],
   'wifi-new-pub' : [
        '2a0c:700:0:22::/64',
    ],
   'wifi-new-federez' : [
        '2a0c:700:0:22::/64',
    ],
    'wifi-new-serveurs' : [
        '2a0c:700:0:22::/64',
    ],
    'wifi-new-adherents': [
        '2a0c:700:0:22::/64',
    ],
    'fil-new' : [
        '2a0c:700:0:21::/64',
    ],
    'fil-new-serveurs' : [
        '2a0c:700:0:21::/64',
    ],
    'fil-new-adherents': [
        '2a0c:700:0:21::/64',
    ],
     'fil-new-pub': [
        '2a0c:700:0:21::/64',
    ],
     'fil-pub' : [
        '2a0c:700:0:23::/64',
    ],
    'dmz' : [
        '2a0c:700:0:24::/64',
    ],
    'he-ipv6' : [
        '2001:470:11:40::/64',
    ],
    'ens22' : [
        '2001:1b48:2:103::bb:2/126',
    ],
    'evenementiel' : [
        '2a0c:700:0:10::/64',
    ],
    'bornes' :[
        'fd01:240:fe3d:3::/64',
    ],
    're2o-adm' : [
        '2a0c:700:0:102::/64',
    ],
    're2o-fil-nat' : [
        '2a0c:700:0:121::/64',
    ],
    're2o-wifi-nat' : [
        '2a0c:700:0:122::/64',
    ],
    're2o-dmz' : [
        '2a0c:700:0:124::/64',
    ],
}

# Domaines dans lesquels les machines sont placées suivant leur type
domains = {
    'machineFixe': 'crans.org',
    'machineCrans': 'crans.org',
    'switchCrans': 'switches.crans.org',
    'machineWifi': 'wifi.crans.org',
    'borneWifi': 'borne.crans.org',
}

# VLans
vlans = {
    # VLan d'administration
    'adm' : 2,
    # VLan pour les bornes
    'bornes' : 3,
    # VLan pour le wifi de l'ens
    'switches' : 4,
    # VLan des gens qui paient
    'adherent' : 1,
    # VLan des inconnus
    'accueil' : 7,
    # Vlan isolement
    'isolement' : 9,
    # VLan fil naté pour les adhérents
    'fil-new': 21,
    # Vlan Wifi naté pour les adhérents
    'wifi-new': 22,
    # Vlan pour les adhérents avec ip publiques
    'fil-pub' : 23,
    # Vlan pour les serveurs Crans, ip crans
    'dmz' : 24,
    # Vlan evenementiel (install-party, etc)
    'event': 10,
    # Vlan zone routeur ens (zrt)
    'zrt': 1132,
    # iSCSI (pour exporter la baie de disque sur vo)
    'iscsi': 42,
    # freebox (pour faire descendre la connexion au 0B)
    'freebox': 8,
    #Test re2o
    're2o-adm' : 102,
    're2o-fil-nat' : 121,
    're2o-wifi-nat' : 122,
    're2o-dmz' : 124,
}

filter_policy = {
    'komaz' : {
        'policy_input' : 'ACCEPT',
        'policy_forward' : 'ACCEPT',
        'policy_output' : 'ACCEPT',
    },
    'zamok' : {
        'policy_input' : 'ACCEPT',
        'policy_forward' : 'DROP',
        'policy_output' : 'ACCEPT',
    },
    'default' : {
        'policy_input' : 'ACCEPT',
        'policy_forward' : 'ACCEPT',
        'policy_output' : 'ACCEPT',
    }
}

# Cf RFC 4890
authorized_icmpv6 = [
    'echo-request',
    'echo-reply',
    'destination-unreachable',
    'packet-too-big',
    'ttl-zero-during-transit',
    'parameter-problem',
]

output_file = {
    4 : '/tmp/ipt_rules',
    6 : '/tmp/ip6t_rules',
}

file_pickle = {
    4 : '/tmp/ipt_pickle',
    6 : '/tmp/ip6t_pickle',
}

################################################################################
#: Items de la blackliste
blacklist_items = {
    u'bloq': u'Blocage total de tous les services',
    u'paiement': u'Paiement manquant cette année',
    u'virus': u'Passage en VLAN isolement',
    u'ipv6_ra': u'Isolement pour RA',
    u'mail_invalide': u'Blocage pour mail invalide',
    u'warez' : u"Présence de contenu violant de droit d'auteur sur zamok",
}

#: Blacklistes entrainant une déconnexion complète
blacklist_sanctions = [
    'warez',
    'virus',
    'bloq',
    'paiement',
]

#: Blacklistes redirigeant le port 80 en http vers le portail captif (avec des
#: explications)
blacklist_sanctions_soft = [
    'ipv6_ra',
    'mail_invalide',
    'virus',
    'warez',
    'bloq',
    'chambre_invalide',
]

##################################################################################

adm_users = [
    'root',
    'identd',
    'daemon',
    'postfix',
    'freerad',
    'amavis',
    'nut',
    'nagios',
    'respbats',
    'list',
    'sqlgrey',
    'ntpd',
    'lp',
    '_apt',
]

open_ports = {
    'tcp' : '22',
}

# Débit max sur le vlan de la connexion gratuite
debit_max_radin = 1000000
debit_max_gratuit = 1000000

###############################
## Vlan accueil et isolement ##
###############################
accueil_route = {
    '138.231.136.12' : {
        'tcp' : [
            '22'
        ],
        'hosts' : [
            'vo.crans.org',
        ],
    },
    '138.231.136.98' : {
        'tcp' : [
            '20',
            '21',
            '80',
            '111',
            '1024:65535',
        ],
        'udp' : [
            '69',
            '1024:65535',
        ],
        'hosts' : [
            'ftp.crans.org',
        ],
    },
    '138.231.136.145' : {
        'tcp' : [
            '80',
            '443',
        ],
        'hosts' : [
            'intranet2.crans.org',
            'intranet.crans.org',
            'cas.crans.org',
            'login.crans.org',
            'auth.crans.org',
            'wifi.crans.org',
            'ftps.crans.org',
            'www.crans.org',
            'wiki.crans.org',
        ],
    },
    '213.154.225.236' : {
        'tcp' : [
            '80',
            '443',
        ],
        'hosts' : [
            'crl.cacert.org',
        ],
    },
    '213.154.225.237' : {
        'tcp' : [
            '80',
            '443',
        ],
        'hosts' : [
            'ocsp.cacert.org',
        ],
    },
     '46.255.53.35' : {
        'tcp' : [
            '80',
            '443',
        ],
        'hosts' : [
            'secure.comnpay.com',
        ],
    },
     '46.255.53.17' : {
        'tcp' : [
            '80',
            '443',
        ],
        'hosts' : [
            'comnpay.com',
        ],
    },
}

dhcp_servers = ['dhcp.adm.crans.org', 'sable.adm.crans.org']

# Le bâtiment virtuel dans lequel on place des chambres qui n'existent pas,
# pour faire des tests.
bats_virtuels = ['v']

# Liste des batiments
liste_bats = ['a', 'b', 'c', 'h', 'i', 'j', 'm', 'o', 'g', 'p', 'k', 'r']

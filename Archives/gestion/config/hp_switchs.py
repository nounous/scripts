# -*- mode: python; coding: utf-8 -*-
"""
Configuration pour les scripts travaillant sur des switches.
"""

## Fonctionnalités des switches
ALL_FEATURES = [
    'POE', 'DHCP_SNOOPING', 'IPv6_MGMT', 'RA_GUARD',
    'MLD_SNOOPING', 'SNTP_NEW_SYNTAX', 'GIGABIT', 'OOBM',
    'FILTER_MDNS', 'RADIUS_DAE', 'DHCPv6_SNOOPING', 'IGMP_SNOOPING',
    'ARP_PROTECT', 'IPv6_LOGGING',
]

# Support du PoE(+)
POE = "PoE"

# Support de l'IGMP Snooping
IGMP_SNOOPING = "IGMP Snooping"

# Support du DHCP Snooping
DHCP_SNOOPING = "DHCP Snooping"

# Support du DHCPv6 Snooping
DHCPv6_SNOOPING = "DHCPv6 Snooping"

#Support ARP protect
ARP_PROTECT = "Arp Protect"

# Support de RA Guard
RA_GUARD = "RA Guard"

# Management en IPv6
IPv6_MGMT = "IPv6 Management"

# Logs over ipv6
IPv6_LOGGING = "Ipv6 logging"

# Support du MLD snooping
MLD_SNOOPING = "MLD Snooping"

# Syntaxe de la configuration SNTP
SNTP_NEW_SYNTAX = "SNTP New Syntax"

# Ports Gigabit uniquement
GIGABIT = "Gigabit ports"

# Out-Of-Bandwidth Management
OOBM = "Out-Of-Bandwidth Management"

# Filtrage des trames mDNS
FILTER_MDNS = "mDNS filtering"

# Extensions dynamiques RADIUS
RADIUS_DAE = "RADIUS Dynamic Authorization Extensions"

## Configuration des différents modèles de switches
## Chaque référence de switch est représenté par un dictionnaire
## répertoriant :
##      - Le modèle du switch (ex. HP 2530)
##      - La version du firmware (ex. YA.16.04.0008)
##      - La liste des fonctionnalités supportées par le switch
##      - Une liste des ports (Q)SFP(+) du switch, le cas échéant
##      - Une liste des modules qui le constituent, le cas échéant.

HP_PROCURVE_MAP = {
    "J4900" : {
        'model' : "HP 2626",
        'firmware' : "H.10.119",
        'features' : [IGMP_SNOOPING, DHCP_SNOOPING],
    },
    "J9145" : {
        'model' : "HP 2910al",
        'firmware' : "W.15.14.0016",
        'features' : [
            IPv6_MGMT, IGMP_SNOOPING, DHCP_SNOOPING, RA_GUARD,
            GIGABIT, SNTP_NEW_SYNTAX, RADIUS_DAE,
            FILTER_MDNS, ARP_PROTECT,
        ],
        'modules' : ["J9145A"],
        'sfp' : range(21, 25),
    },
    "J9623" : {
        'model' : "HP 2620",
        'firmware' : "RA.16.04.0011",
        'features' : [
            IPv6_MGMT, DHCP_SNOOPING, DHCPv6_SNOOPING, RA_GUARD,
            MLD_SNOOPING, IGMP_SNOOPING, SNTP_NEW_SYNTAX, RADIUS_DAE,
            FILTER_MDNS, ARP_PROTECT, IPv6_LOGGING,
        ],
    },
    "J9624" : {
        'model' : "HP 2620",
        'firmware' : "RA.16.04.0011",
        'features' : [
            IPv6_MGMT, POE, DHCP_SNOOPING, DHCPv6_SNOOPING,
            RA_GUARD, MLD_SNOOPING, IGMP_SNOOPING, SNTP_NEW_SYNTAX,
            RADIUS_DAE, FILTER_MDNS, ARP_PROTECT, IPv6_LOGGING,
        ],
        'sfp' : range(27, 29),
        'poe' : range(1, 13),
    },
    "J9626" : {
        'model' : "HP 2620",
        'firmware' : "RA.16.04.0011",
        'features' : [
            IPv6_MGMT, DHCP_SNOOPING, DHCPv6_SNOOPING, RA_GUARD,
            MLD_SNOOPING, IGMP_SNOOPING, SNTP_NEW_SYNTAX, RADIUS_DAE,
            FILTER_MDNS, ARP_PROTECT, IPv6_LOGGING,
        ],
    },
    "J9727" : {
        'model' : "HP 2920",
        'firmware' : "WB.16.05.0004",
        'features' : [
            IPv6_MGMT, POE, DHCP_SNOOPING, DHCPv6_SNOOPING,
            RA_GUARD, MLD_SNOOPING, IGMP_SNOOPING, SNTP_NEW_SYNTAX,
            GIGABIT, OOBM, RADIUS_DAE, FILTER_MDNS, ARP_PROTECT, IPv6_LOGGING,
        ],
        'modules' : ["J9727A"],
        'sfp' : range(21, 25),
        'poe' : range(1, 25),
    },
    "J9772" : {
        'model' : "HP 2530",
        'firmware' : "YA.16.05.0004",
        'features' : [
            IPv6_MGMT, POE, DHCP_SNOOPING, DHCPv6_SNOOPING,
            RA_GUARD, MLD_SNOOPING, IGMP_SNOOPING, SNTP_NEW_SYNTAX,
            GIGABIT, RADIUS_DAE, FILTER_MDNS, ARP_PROTECT, IPv6_LOGGING,
        ],
        'sfp' : range(49, 53),
        'poe' : range(1, 53),
    },
    "J9773" : {
        'model' : "HP 2530",
        'firmware' : "YA.16.05.0004",
        'features' : [
            IPv6_MGMT, POE, DHCP_SNOOPING, DHCPv6_SNOOPING,
            RA_GUARD, MLD_SNOOPING, IGMP_SNOOPING, SNTP_NEW_SYNTAX,
            GIGABIT, RADIUS_DAE, FILTER_MDNS, ARP_PROTECT, IPv6_LOGGING,

        ],
        'sfp' : range(25, 29),
        'poe' : range(1, 25),
    },
    "J9775" : {
        'model' : "HP 2530",
        'firmware' : "YA.16.05.0004",
        'features' : [
            IPv6_MGMT, DHCP_SNOOPING, DHCPv6_SNOOPING, RA_GUARD,
            MLD_SNOOPING, IGMP_SNOOPING, SNTP_NEW_SYNTAX, GIGABIT,
            RADIUS_DAE, FILTER_MDNS, ARP_PROTECT, IPv6_LOGGING,
        ],
        'sfp' : range(49, 53),
    },
    "J9776" : {
        'model' : "HP 2530",
        'firmware' : "YA.16.05.0004",
        'features' : [
            IPv6_MGMT, DHCP_SNOOPING, DHCPv6_SNOOPING, RA_GUARD,
            MLD_SNOOPING, IGMP_SNOOPING, SNTP_NEW_SYNTAX, GIGABIT,
            RADIUS_DAE, FILTER_MDNS, ARP_PROTECT, IPv6_LOGGING,
        ],
        'sfp' : range(25, 29),
    },
    "J9777" : {
        'model' : "HP 2530",
        'firmware' : "YA.16.05.0004",
        'features' : [
            IPv6_MGMT, DHCP_SNOOPING, DHCPv6_SNOOPING, RA_GUARD,
            MLD_SNOOPING, IGMP_SNOOPING, SNTP_NEW_SYNTAX, GIGABIT,
            RADIUS_DAE, FILTER_MDNS, ARP_PROTECT, IPv6_LOGGING,
        ],
        'sfp' : range(9, 11),
    },
    "JL259" : {
        'model' : "HP 2930",
        'firmware' : "WC.16.05.0004",
        'features' : [
            IPv6_MGMT, DHCP_SNOOPING, DHCPv6_SNOOPING, RA_GUARD,
            MLD_SNOOPING, IGMP_SNOOPING, SNTP_NEW_SYNTAX, GIGABIT,
            RADIUS_DAE, FILTER_MDNS, ARP_PROTECT, IPv6_LOGGING,
        ],
        'sfp' : range(25, 28),
        'modules' : ["JL259A"],
    },
    "JL261" : {
        'model' : "HP 2930",
        'firmware' : "WC.16.05.0004",
        'features' : [
            IPv6_MGMT, DHCP_SNOOPING, DHCPv6_SNOOPING, RA_GUARD,
            MLD_SNOOPING, IGMP_SNOOPING, SNTP_NEW_SYNTAX, GIGABIT,
            RADIUS_DAE, FILTER_MDNS, ARP_PROTECT, IPv6_LOGGING,
        ],
        'sfp' : range(25, 28),
        'modules' : ["JL261A"],
    },
}

ALL_MODELS = list({switch["model"] for switch in HP_PROCURVE_MAP.values()})

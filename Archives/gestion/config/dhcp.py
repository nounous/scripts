# Serveurs dhcp autoritaires
authoritative = ["dhcp", "sable"]

# Fichier de leases
dhcplease = '/var/lib/dhcp/dhcpd.leases'

# Zones (couple zone IP, fichier des correspondandes MAC/IP)
reseaux = { '138.231.136.0/21' : '/etc/dhcp3/generated/adherents.liste.test',
            '10.42.0.0/16' : '/etc/dhcp3/generated/gratuit.liste.test',
            '10.2.9.0/24' : '/etc/dhcp3/generated/appartements.liste.test',
            '138.231.144.0/21' : '/etc/dhcp3/generated/wifi.liste.test' }

# Commande de serveur
server_cmd = "service isc-dhcp-server"

# Options pour la commande
start = "start"
stop = "stop"
restart = "restart"
reload = "reload"

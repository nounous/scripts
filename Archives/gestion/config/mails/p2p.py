#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

""" Templates des mails envoyés en cas de p2p. """

#: Envoyé à la ML disconnect@ en cas de déconnexion pour p2p
avertissement = u"""From: %(From)s
To: %(To)s
Subject: =?utf-8?q?D=C3=A9tection?= de p2p sur la machine %(hostname)s
Content-Type: text/plain; charset="utf-8"

La machine %(hostname)s a été déconnectée pendant 24h pour
utilisation du protocole %(protocole)s.
Nombre de paquets : %(nb_paquets)s paquets depuis le %(datedebut)s.
Chambre (si machine fixe) : %(chambre)s

--\u0020
Message créé par deconnexion.py"""

#: Envoyé à l'adhérent en cas de déconnexion pour p2p
deconnexion = u"""From: %(From)s
To: %(To)s
Subject: Avis de =?utf-8?q?D=C3=A9connexion?=
Content-Type: text/plain; charset="utf-8"

Bonjour,
Nous avons détecté que ta machine, %(hostname)s utilisait le
*protocole* %(protocole)s.

Cela signifie :
    - Ou bien que tu utilises le logiciel qui a le même nom
    que ce protocole,
    - Ou bien que tu utilises un logiciel qui utilise le
    protocole en question pour partager des fichiers.

Or l'usage de *protocoles* de type peer to peer est interdit
sur notre réseau, conformément aux documents que tu as acceptés
en adhérant au CR@NS.

Lorsqu'un seul des adhérents du CR@NS utilise des protocoles interdits,
il pénalise l'ensemble des adhérents de l'association.

Tu seras donc déconnecté 24h.

--\u0020
Disconnect Team"""

#: Envoyé à l'adhérent en cas de détection de p2p
avertissement_bt = u"""From: %(From)s
To: %(To)s
Subject: Avertissement d'utilisation du protocole Bittorrent
Content-Type: text/plain; charset="utf-8"

Bonjour,
Nous avons détecté que ta machine, %(hostname)s utilisait le
*protocole* %(protocole)s.

*Ce message t'est envoyé à titre informatif, il ne te sanctionne pas.*

Cela signifie :
    - Ou bien que tu utilises le logiciel qui a le même nom
    que ce protocole,
    - Ou bien que tu utilises un logiciel qui utilise le
    protocole en question pour partager des fichiers.

Or l'usage de *protocoles* de type peer to peer est interdit
sur notre réseau, conformément aux documents que tu as acceptés
en adhérant au CR@NS.

Nous empêchons donc ce trafic Bittorrent de sortir du réseau Cr@ns,
rendant inopérant les logiciels l'utilisant.

Si tu es à l'origine de ce trafic, merci de couper les logiciels en
question. Sinon, il est possible qu'un logiciel malveillant en soit la
cause, nous te conseillons d'installer un anti-virus et un pare-feu afin
d'arrêter la source de ce trafic.

--\u0020
Disconnect Team"""

#: Envoyé à la ML disconnect@ en cas de déconnexion pour p2p plusieurs fois
message_disconnect_multi = u"""From: %(from)s
To: %(to)s
Subject: %(proprio)s a =?utf-8?q?=C3=A9t=C3=A9_d=C3=A9connect=C3=A9?= %(nbdeco)d fois pour p2p en un an !
Content-Type: text/plain; charset="utf-8"

L'adhérent %(proprio)s a été déconnecté %(nbdeco)d fois pour p2p en un an !

Le PS a été généré et se trouve sur zamok :
%(ps)s

--\u0020
Message créé par deconnexion.py"""

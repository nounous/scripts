#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-
#
# Le Crans étant une association, il est important de distinguer
# cotisation annuelle pour adhésion (qui donne un droit de vote
# etc), et la cotisation mensuelle pour l'accès à Internet.
#
# Merci à chacun de s'en rappeler.

# Délai minimal avant de pouvoir réadhérer.
# Ne tient pas compte de la période transitoire, qui est un confort
# pour l'administration.
delai_readh_jour = 32
delai_readh = delai_readh_jour * 86400

duree_adh_an = 1

# Un compte avec une adhésion valide ne peut être détruit que lorsque celle-ci
# est expirée depuis plus que le délai indiqué ici en secondes
# Ici, on choisit 90 jours.
del_post_adh_jours = 90
del_post_adh = del_post_adh_jours * 86400

# Cotisation pour adhérer à l'association. Les services autres que l'accès à
# Internet sont offerts une et une fois pour toute aux personnes qui adhèrent,
# et ce dès leur première fois. (comprendre : le compte Crans et cie ne sont pas
# subordonnés à une réadhésion)
cotisation = 10

# Contribution mensuelle pour l'accès à internet.
contribution = 5

# 8 mois achetés, les quatre derniers offerts, on est aussi cools qu'Orange \o.
plafond_contribution = 40

# Si le compte n'est pas rond, c'est vraiment pas pratique.
duree_conn_plafond = plafond_contribution/contribution
duree_conn_max = max(12, duree_conn_plafond)

# L'adhésion vaut signature d'un contrat à durée d'un an (année bisextile ou pas)
# Le dico n'est donc pas modifiable.
dico_adh = {
    "nombre": 1,
    "code": "ADH",
    "designation": "Adh pour un an",
    "pu": cotisation,
}

dico_adh_club = {
    "nombre": 1,
    "code": "ADHC",
    "designation": "Adh club pour un an",
    "pu": 0,
}

def dico_cotis(nb_mois):
    """Retourne un dictionnaire pour ajout à une facture, contenant
    les bonnes données.
    """

    if nb_mois >= duree_conn_plafond:
        nb_mois = 12
        pu = plafond_contribution
    else:
        pu = contribution*nb_mois

    dico_cotis_tpl = {
        "nombre": 1,
        "code": "CAI%s" % (nb_mois,), # Contribution pour l'Accès à Internet (original, hein ?)
        "designation": "Contribution pour %s mois." % (nb_mois,),
        "pu": pu,
    }
    return dico_cotis_tpl

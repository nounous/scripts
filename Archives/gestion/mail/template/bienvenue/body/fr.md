Bienvenue !
===========

Si tu lis ce mail, c'est que ton inscription à l'association
s'est déroulée correctement.

Le Cr@ns est une association gérée par des étudiants bénévoles pour les
résidents du campus.

Ses membres actifs s'occupent de la maintenance du réseau, des adhésions 
(câblages), ainsi que de plusieurs services mis à la disposition de tous ses 
adhérents :

  * La connexion à Internet en filaire et en wifi [1] ;
  * un wiki, regroupant plein d'informations diverses concernant la vie ici [2] ;
  * les news, fora de discussion abordant divers thèmes [3] ;
  * un serveur IRC [4] ;
  * un service de messagerie : une adresse mail @crans.org disponible à vie, 
    fiable, et avec possibilité d'installer des filtres très précis [5] ;
  * un espace personnel de **10Go** sur le serveur des adhérents [6] ;
  * un service d'impression, 24h/24 7j/7, A3 ou A4, couleur ou noir et blanc, 
    avec ou sans agrafe, à prix coûtant [7] ;
  * la télévision par le réseau [8].
  * et bien d'autres que tu peux retrouver sur <http://intranet.crans.org>

Afin d'assurer le bon fonctionnement de ces services, il est
nécessaire que chaque membre respecte du règlement intérieur [9] accepté lors 
de son inscription. 


La notice d'utilisation des services du CR@NS est disponible à cette adresse :
<https://wiki.crans.org/CransPratique>. Nous conseillons vivement de s'y
reporter aussi bien pour apprendre à utiliser les différents services que pour
résoudre les problèmes éventuellement rencontrés.

Pour être tenu au courant des opérations de maintenance vous pouvez consulter
les média suivants :
 * Facebook : <https://www.facebook.com/CachanReseauNormaleSup>
 * Twitter : <https://twitter.com/TwCrans>
 * Wiki : <https://wiki.crans.org/CransIncidents>
 
 En cas de problèmes, contacter <respbats@crans.org>

Chaque membre intéressé par le fonctionnement de l'association peut contacter 
l'équipe technique à <nounous@crans.org> ou l'équipe administrative à 
<ca@crans.org> et/ou se rendre aux réunions publiques du Cr@ns [10]. 

**Aucune connaissance technique préalable n'est requise pour participer !**

**A noter** : l'accès sans authentification aux news et au wiki est limité à 
un usage interne au campus.

 * Pour accéder depuis l'extérieur au wiki, il faut se créer un compte [11].
 * Pour accéder depuis l'extérieur aux news, il faut s'identifier 
   (Utilisateur  : Vivelapa  /  Mot de passe : ranoia!)


{% include 'bienvenue/body/links' %}

-- 

Les membres actifs.

PS: Il t'est conseillé de conserver ce mail à toutes fins utiles

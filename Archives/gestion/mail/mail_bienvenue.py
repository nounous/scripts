#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
#Gabriel Detraz 2017

from lc_ldap import shortcuts
import argparse
import os
from datetime import datetime, timedelta
ldap = shortcuts.lc_ldap_readonly()

from gestion import mail as mail_module

From = "cableurs@crans.org"

def get_users_list(mode='day'):
    now = datetime.now()
    if mode=='day':
        day_before = now - timedelta(days=1)
        ldap_filter = u'(&(aid=*)(historique=%s/%s/%s*: inscription))' % ("{0:0=2d}".format(day_before.day), "{0:0=2d}".format(day_before.month), day_before.year)
        users = ldap.search(ldap_filter)
    elif mode=='hour':
        hour_before = now - timedelta(hours=1)
        ldap_filter = u'(&(aid=*)(historique=%s/%s/%s %s*: inscription))' % ("{0:0=2d}".format(hour_before.day), "{0:0=2d}".format(hour_before.month), hour_before.year, "{0:0=2d}".format(hour_before.hour))
        users = ldap.search(ldap_filter)
    else:
        raise EnvironmentError
    return users

def send_to_users(users, verbose=False):
    errors = []
    for user in users:
        mail = user.get_mail()
        if '@crans.org' in mail:
            home = user.get('homeDirectory', default=[None])[0]
            if home:
                if not os.path.exists(home.value):
                    errors.append(user)
                    return
        if verbose:
            print(u"Envoie du mail de bienvenue à " + unicode(user))
            print(mail)
        send_bienvenue_mail(mail)
    return users, errors

def send_bienvenue_mail(mail):
    with mail_module.ServerConnection() as conn_smtp:
        mailtxt=mail_module.generate('bienvenue', {'To':mail, 'From': From}).as_string()
        conn_smtp.sendmail(From, (mail,), mailtxt)
    return

def notif_sent_bienvenue(users, errors, mode='all'):
    To = 'respbats@crans.org'
    if not mode=='all' and not errors:
        return
    if not users:
        return
    with mail_module.ServerConnection() as conn_smtp:
        all_users = u' ;\n    '.join([unicode(user.get('aid', [None])[0]) for user in users]) or u"aucun"
        all_errors = u' ;\n     '.join([unicode(user.get('aid', [None])[0]) for user in errors]) or u"aucun"
        mailtxt = mail_module.generate('notifbienvenue', {'users' : all_users, 'errors' : all_errors, 'To':To, 'From': From}).as_string()
        conn_smtp.sendmail(From, (To,), mailtxt)

parser = argparse.ArgumentParser()
parser.add_argument('--hour', action="store_true", help="Traite l'ensemble des users crées dans l'heure précédente")
parser.add_argument('--day', action="store_true", help="Traite l'ensemble des users crées dans la journée précédente")
parser.add_argument('--mail', help="Envoie un mail de bienvenue au mail précisé")
parser.add_argument('--verbose', action="store_true", help="Mode verbeux")
parser.add_argument('--notifall', action="store_true", help="Notifie toutes les actions")
parser.add_argument('--notiferrors', action="store_true", help="Notifie les erreurs")

if __name__ == '__main__':
    args = parser.parse_args()
    if args.verbose:
        verbose=True
    else:
        verbose=False
    if args.mail:
        print("Envoie du mail de bienvenue au mail " + args.mail)
        send_bienvenue_mail(args.mail)
    else:
        if args.day:
            users, errors = send_to_users(get_users_list(mode='day'), verbose=verbose)
        elif args.hour:
            users, errors = send_to_users(get_users_list(mode='hour'), verbose=verbose)
        if args.notifall:
            notif_sent_bienvenue(users, errors, mode='all')
        if args.notiferrors:
            notif_sent_bienvenue(users, errors, mode='errors')


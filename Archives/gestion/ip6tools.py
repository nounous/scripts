# -*- mode: python; coding: utf-8 -*-
#
# IP6TOOLS.PY -- IPv6 manipulation routines
#
# Copyright © 2011 Nicolas Dandrimont <olasd@crans.org>
#
# Authors: Nicolas Dandrimont <olasd@crans.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import netaddr

def mac_to_ipv6(ipv6_prefix, mac_address):
    """Convert a MAC address (EUI48) to an IPv6 (prefix::EUI64)."""
    if mac_address == '<automatique>':
        return ''

    if type(mac_address) in [str, unicode]:
        mac_address = netaddr.EUI(mac_address)
    addr = int(mac_address.bits(netaddr.mac_bare), 2)
    ip6addr = (((addr >> 24) ^ (1 << 17)) << 40) | (0xFFFE << 24) | (addr & 0xFFFFFF)
    n = netaddr.IPNetwork(ipv6_prefix)
    return netaddr.IPAddress(n.first + ip6addr)

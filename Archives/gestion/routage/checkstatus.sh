#!/bin/bash

# Renvoie un si le fichier MASTER est présent

if [ -f /etc/keepalived/MASTER ]; then
    exit 0
else
    exit 1
fi

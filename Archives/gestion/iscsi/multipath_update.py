#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-
#
# Auteur : Pierre-Elliott Bécue <becue@crans.org>
# Licence : BSD

import sys
import os

from gestion.config import ISCSI_MAP_FILE

DEST_FILE = "/etc/multipath.conf"

def update_bindings():
    """Met à jour une carte de bindings à donner à manger à multipath ensuite"""

    map_file = ISCSI_MAP_FILE % ("nols",)

    if not os.access(map_file, os.R_OK):
        print >>sys.stderr, u"Impossible de lire le fichier de mapping(%s)" % map_file
        sys.exit(1)

    globals()['volume_map'] = {}
    execfile(map_file, globals())

    with open(DEST_FILE, 'w') as bindings_file:
        bindings_file.write("""# Fichier généré en mode sagouin par
# /usr/scripts/gestion/iscsi/multipath_update.py
defaults {
        find_multipaths yes
        user_friendly_names yes
        path_grouping_policy multibus
        path_selector "service-time 0"
}

multipaths {
""")

        for lun, data in volume_map.iteritems():
            bindings_file.write("""    multipath {
        wwid %s
        alias mpath-%s
    }
""" % (data['wwn'], data['name'].replace('_', '-')))
        bindings_file.write("""}""")

if __name__ == '__main__':
    update_bindings()

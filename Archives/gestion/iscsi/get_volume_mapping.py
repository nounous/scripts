#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

# slon-get-volume-mapping.py
# --------------------------
# Copyright : (c) 2012, Olivier Iffrig <iffrig@crans.org>
# Copyright : (c) 2008, Jeremie Dimino <dimino@crans.org>
# Licence   : BSD3

u'''Outil pour récupérer le mapping lun/volume depuis la baie de
stockage'''

import slonlib
import nolslib
import re
import sys

coding = "utf-8"

from gestion.config import ISCSI_MAP_FILE
from gestion import affich_tools

def get_mapping(baie_name):
    map_file = ISCSI_MAP_FILE % (baie_name,)
    print u"Connexion à la baie de stockage…".encode(coding)

    if baie_name == "slon":
        baie = slonlib.Slon()
    else:
        baie = nolslib.Nols()

    print u"Récupération des informations…".encode(coding)

    volume_map = baie.volume_map()
    volume_list = baie.volume_list()

    for k, v in volume_map.items():
        if v['name'] in volume_list:
            v['wwn'] = "3%s" % (volume_list[v['name']]['wwn'].lower(),)

    volume_map = volume_map.items()
    volume_map.sort()

    print u"Déconnexion…".encode(coding)

    baie.logout()

    print u"Enregistrement des informations…".encode(coding)

    f = open(map_file, "w")
    f.write((u"""\
# /usr/bin/env python
# -*- mode: python; coding: utf-8 -*-
# Fichier de mapping lun -> nom de volume
#
# Ce fichier est généré par %s %s

volume_map = {
    """ % (sys.argv[0], baie)).encode(coding))

    for lun, data in volume_map:
        f.write('  %d : %r,\n' % (lun, data))

    f.write("}\n")

    f.close()

    print (u"Terminé, mapping enregistré dans %s" % map_file).encode(coding)

    print (u"Le mapping actuel est : ").encode(coding)
    print affich_tools.tableau(
        titre=["lun", "nom", "serial", "wwn"],
        data=[[e[0], e[1]['name'], e[1]['serial'], e[1]['wwn']] for e in volume_map],
        alignement=["g", "c", "c", "c"])

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print """\
Usage : get_volume_mapping.py baie

Récupère le volume mapping sur la baie de disques choisie.
        """
    else:
        get_mapping(sys.argv[1])

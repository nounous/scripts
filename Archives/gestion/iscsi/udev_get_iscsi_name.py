#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

# udev-get-iscsi-name.py
# ----------------------
# Copyright : (c) 2012, Olivier Iffrig <iffrig@crans.org>
# Copyright : (c) 2008, Jeremie Dimino <jeremie@dimino.org>
# Licence   : BSD3

u'''Script appelé par udev (/etc/udev/rules.d/10_crans_iscsi.rules)
pour les liens symboliques pour les périphériques iscsi'''

import sys
import os
from gestion.config import ISCSI_MAP_FILE

def getname(device):
    if not device.isalpha():
        block = "".join([i for i in device if i.isalpha()])
        part = "_part" + device.replace(block, "")
    else:
        block = device
        part = ""
    # Rechreche le nom complet du périphérique dans /sys
    dev = os.readlink("/sys/block/%s/device" % block)

    # L'identifiant est de la forme "../../../0:0:0:42", où 42 (j'ai perdu)
    # est le lun.
    try:
        lun = int(dev.rsplit(":", 1)[1])
    except:
        print >>sys.stderr, u"périphérique invalide"
        sys.exit(1)

    map_file = ISCSI_MAP_FILE % ("nols",)

    if not os.access(map_file, os.R_OK):
        print >>sys.stderr, u"Impossible de lire le fichier de mapping(%s)" % map_file
        sys.exit(1)

    globals()['volume_map'] = {}
    execfile(map_file, globals())

    if lun not in volume_map:
        to_ret = "lun%d%s" % (lun, part)
    else:
        to_ret = volume_map[lun]['name'] + part

    return to_ret

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print >>sys.stderr, u"usage: %s <nom de périphérique>" % sys.argv[0]
        sys.exit(2)
    a = getname(sys.argv[1])
    print a

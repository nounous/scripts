# -*- mode: python; coding: utf-8 -*-

import os
import psycopg2

from functools import wraps

import time
import socket

conn = None
# : échec définitif, on raise une exception direct
def_failed = False
def _need_conn(f):
    """Décorateur à appliquer aux fonctions nécessitant une connexion pgsql"""
    retries = 1
    delay = 5
    @wraps(f)
    def first_connect(*args, **kwargs):
        global conn, def_failed
        if def_failed:
            raise NameError("La connexion à la pase postgresql ne peut être établie.")
        attempts = 0
        while not conn or not attempts:
            host = os.getenv('DBG_ANNUAIRE', 'pgsql.v4.adm.crans.org')

            # Test habituel sur vo:
            if host == '1' or __name__.endswith('annuaires_pg_test'):
                host='localhost'

            # "connecting …"
            try:
                if not conn:
                    if attempts:
                        # Attend un peu avant de reessayer
                        time.sleep(delay)
                    conn = psycopg2.connect(user='crans_ro', database='django',
                                            host=host)
                    if conn.status != psycopg2.extensions.STATUS_IN_TRANSACTION:
                        conn.set_session(autocommit=True)
                return f(*args, **kwargs)
            except psycopg2.OperationalError:
                attempts += 1
                conn = None   # Connexion morte, on recommence à zéro
                if attempts > retries:
                    def_failed = True
                    raise NameError("Trop d'erreur de connection %i tentatives sur %i" % (attempts, retries))
            # Les scripts appelant annuaires_pg n'ont pas à connaître le
            # backend pgsql. On utilise donc une exception plus standard
    return first_connect

# Le v est virtuel.
bat_switchs = ["a", "b", "c", "g", "h", "i", "j", "k", "m", "o", "p", "r", "v"]

class ChbreNotFound(ValueError):
    """Lorsqu'une chambre n'existe pas"""
    pass

@_need_conn
def chbre_prises(batiment, chambre = None):
    """Correspondance chambre -> prise"""
    batiment = batiment.lower()
    if chambre:
        chambre = chambre.lower()
        cur = conn.cursor()
        cur.execute("SELECT prise_crans FROM prises_prise WHERE (batiment, chambre) = (%s, %s)", (batiment, chambre))
        try:
            return "%03d" % cur.fetchone()[0]
        except TypeError:
            raise ChbreNotFound("Chambre inexistante bat %r, chbre %r" % (batiment, chambre))
    else:
        cur = conn.cursor()
        cur.execute("SELECT chambre, prise_crans FROM prises_prise WHERE batiment = %s", batiment)
        ret = {}
        for chambre, prise_crans in cur.fetchall():
            ret[chambre] = "%03d" % prise_crans
        if not ret:
            raise ValueError("Batiment %s inexistant" % batiment)
        return ret

@_need_conn
def chbre_commentaire(batiment, chambre):
    """ Renvoie le commentaire associé à la chambre """
    global conn
    batiment = batiment.lower()
    chambre = chambre.lower()
    cur = conn.cursor()
    cur.execute("SELECT commentaire FROM prises_prise WHERE (batiment, chambre) = (%s,%s)", (batiment, chambre))
    try:
        return cur.fetchone()[0]
    except TypeError:
        raise ValueError("Chambre inexistante bat %r, chbre %r" % (batiment, chambre))

@_need_conn
def lieux_public():
    cur = conn.cursor()
    cur.execute("SELECT batiment,chambre FROM prises_prise WHERE public=True")
    results = cur.fetchall()
    return [batiment.upper()+chambre for batiment, chambre in results]

@_need_conn
def disabled_radius():
    cur = conn.cursor()
    cur.execute("SELECT batiment,chambre FROM prises_prise WHERE has_radius=False")
    results = cur.fetchall()
    return [batiment.upper()+chambre for batiment, chambre in results]

@_need_conn
def poe_enabled():
    cur = conn.cursor()
    cur.execute("SELECT UPPER(batiment)||TO_CHAR(prise_crans, 'FM099'), poe_status FROM prises_prise;")
    return cur.fetchall()

@_need_conn
def reverse(batiment, prise = None):
    """Correspondance prise -> chambre"""
    batiment = batiment.lower()
    if prise:
        cur = conn.cursor()
        cur.execute("SELECT chambre FROM prises_prise WHERE (batiment, prise_crans) = (%s, %s)", (batiment, int(prise)))
        try:
            return [chbre for (chbre,) in cur.fetchall()]
        except TypeError:
            raise ValueError("Prise %s inexistante" % prise)
    else:
        cur = conn.cursor()
        cur.execute("SELECT chambre, prise_crans FROM prises_prise WHERE batiment = %s", batiment)
        ret = {}
        for chambre, prise_crans in cur.fetchall():
            try:
                ret["%03d" % prise_crans].append(chambre)
            except KeyError:
                ret["%03d" % prise_crans] = [chambre]

        if not ret:
            raise ValueError("Batiment %s inexistant" % batiment)
        return ret
@_need_conn
def is_connected(batiment, chambre):
    """Cablage physique effectue ?"""
    batiment = batiment.lower()
    chambre = chambre.lower()
    cur = conn.cursor()
    cur.execute("SELECT cablage_effectue FROM prises_prise WHERE (batiment, chambre) = (%s, %s)", (batiment, chambre))
    return cur.fetchone()[0]

# Prises d'uplink, de machines du crans / Prises d'utilité CRANS
uplink_prises={ 'a' :
{ 49 : 'uplink->bata-2',  50 : 'libre-service',
 149 : 'uplink->bata-2', 150 : 'libre-service',
 424 : 'uplink->bata-2', 423 : 'libre-service',
 321 : 'uplink->bata-2', 322 : 'libre-service',
 201 : 'uplink->bata-0', 202 :  'uplink->bata-1',
 203 : 'uplink->bata-4', 204 :  'uplink->bata-3',
 224 : 'uplink->backbone',
 },
'b' :
{ 49 : 'libre-service',  50 : 'uplink->batb-4',
 149 : 'libre-service', 150 : 'uplink->batb-4',
 249 : 'libre-service', 250 : 'uplink->batb-4',
 251 : 'libre-service', 252 : 'libre-service',
 349 : 'libre-service', 350 : 'uplink->batb-4',
 351 : 'libre-service', 352 : 'libre-service',
 421 : 'uplink->batb-0', 422 :  'uplink->batb-1',
 423 : 'uplink->batb-2', 424 :  'uplink->batb-3',
 412 : 'uplink->batb-7',
 405 : 'uplink->backbone',
 505 : 'libre-service', 506 : 'libre-service',
 509 : 'uplink->backbone',
 517 : 'libre-service', 518 : 'libre-service',
 519 : 'libre-service', 520 : 'libre-service',
 521 : 'libre-service', 522 : 'libre-service',
 523 : 'libre-service', 524 : 'libre-service',
 525 : 'libre-service',
 626 : 'uplink->backbone',
 721 : 'uplink->backbone',
 },
'c' :
{ 49 : 'uplink->batc-3',  50 : 'libre-service',
 147 : 'uplink->batc-3',
 224 : 'uplink->batc-3',
 301 : 'uplink->batc-0', 302 : 'uplink->batc-1',
 304 : 'uplink->batc-4',
 303 : 'uplink->batc-2', 324 : 'uplink->backbone',
 426 : 'uplink->batc-3',
},
'g' :
{
  22 : 'uplink->backbone',
  23 : 'libre-service', 24 : 'uplink->batg-8',

  149 : 'uplink->batg-8',  150 : 'libre-service',

  249 : 'uplink->batg-8', 250 : 'libre-service',

 449 : 'uplink->batg-9',  450 : 'libre-service',

 549 : 'uplink->batg-9',  550 : 'libre-service',

 649 : 'uplink->batg-9',  650 : 'libre-service',

 825 : 'uplink->batg-0',
 827 : 'uplink->batg-1',  826 : 'uplink->batg-2',
 828 : 'libre-service',
 823 : 'uplink->batg-9',

 925 : 'uplink->batg-4',  926 : 'uplink->batg-5',
 927 : 'uplink->batg-6',  904 : 'uplink->batg-8',
 },
'h' :
{ 49 : 'libre-service', 50 : 'uplink->bath-2',
 149 : 'libre-service', 150 : 'uplink->bath-2',
 221 : 'uplink->bath-0',
 222 : 'uplink->bath-3', 223 : 'uplink->backbone',
 224 : 'uplink->bath-1',
 303 : 'uplink->bath-2' },
'i' :
{ 49 : 'uplink->bati-3',  50 : 'libre-service',
 149 : 'uplink->bati-3', 150 : 'libre-service',
 301 : 'uplink->bati-0', 326 : 'uplink->bati-1',
 328 : 'uplink->backbone' },
'j' :
{ 49 : 'uplink->batj-3', 50 : 'libre-service',
 149 : 'uplink->batj-3', 147  : 'libre-service',
 223 : 'uplink->batj-3',  224 : 'libre-service',
 328 : 'uplink->backbone',
 301 : 'uplink->batj-0', 326 : 'uplink->batj-1',
 305 : 'uplink->batj-2', 307 : 'uplink->batj-4',
 421 : 'uplink->batj-3', 422 : 'libre-service',
},
'k' : {
 23 : 'uplink->backbone',
},
'm' :
{
  49 : 'libre-service',  50 : 'uplink->batm-7',
  51 : 'libre-service',  52 : 'libre-service',
 149 : 'libre-service', 150 : 'uplink->batm-7',
 151 : 'libre-service', 152 : 'libre-service',
 249 : 'libre-service', 250 : 'uplink->batm-7',
 251 : 'libre-service', 252 : 'libre-service',
#341 : '? 502b ? Label 5', 345 : 'Label 7',
 349 : 'libre-service', 350 : 'uplink->batm-7',
 351 : 'libre-service', 352 : 'libre-service',
 449 : 'libre-service', 450 : 'uplink->batm-7',
 451 : 'libre-service', 452 : 'libre-service',
 549 : 'libre-service', 550 : 'uplink->batm-7',
 551 : 'libre-service', 552 : 'libre-service',
 650 : 'uplink->batm-7', 651 : 'libre-service',
 652 : 'libre-service',

 747 : 'libre-service', 750 : 'libre-service',
 751 : 'libre-service', 752 : 'libre-service',

 749 : 'uplink->backbone', 720 : 'uplink->batm-0',
 719 : 'uplink->batm-1',   718 : 'uplink->batm-2',
 717 : 'uplink->batm-3',   716 : 'uplink->batm-4',
 715 : 'uplink->batm-5',   714 : 'uplink->batm-6',
 },
 'p' :
{
 49 : 'uplink->batp-4 (R4.1)',
 149: 'uplink->batp-4 (R3.1)',
 249: 'uplink->batp-4 (R2.1)',
 347: 'uplink->batp-4 (R1.2)',
 401: 'uplink->batp-3', 402: 'uplink->batp-2',
 403: 'uplink->batp-1', 404: 'uplink->batp-0',
 405: 'libre-service', 409: 'uplink->bato-1',
},
 'o' :
 {
 121: 'uplink->NRD',
 122: 'uplink->backbone', 123: 'uplink->backbone (unused)',
 124: 'uplink->batp-4',
 } ,
 'r' :
 {
 27 : 'uplink->backbone', 28 : 'libre-service',
 } ,
 'v' :
 {
  23: 'uplink', 24 : 'uplink',
 } ,
'backbone' : #For your consideration
 {
 #A: 12eth+12fibre,  B: 24 eth
 'A1': 'odlyd',     'B1': 'bata',
 'A2': 'zamokv5',   'B2': 'libre-service',
 'A3': 'sable',     'B3': 'sable-zrt',
 'A4': 'komaz',     'B4': 'fy',
 'A5': 'zbee',      'B5': 'switch-ilo',
 'A6': 'thot',      'B6': 'vigile 0B',
 'A7': 'odlyd',      'B7': 'kdell',
 'A8': 'odlyd-zrt', 'B8': 'batb',
 'A9': 'osm3',      'B9': '2b',
 'A10': 'osm3-idrac','B10': 'fz',
 'A11': 'osm4-idrac','B11': 'ft',
                    'B12': 'nols2',
 'A13': 'batm',
 'A14': 'batp',     'B14': 'zamok',
 'A15': 'batc',     'B15': 'charybde',
 'A16': 'bato',
 'A17': 'bati',
 'A18': 'bath',     'B18': 'nols',
 'A19': 'batj',
 'A20': 'batg',     'B20': 'gulp',
 'A21': 'batk',     'B21': 'osm1',
 'A22': 'batr',     'B22': 'osm1-ilo',
                    'B23': 'osm2',
                    'B24': 'osm2-ilo',
 },
}

_SPECIAL_SWITCHES = [
    'backbone.switches.crans.org',
    'minigiga.switches.crans.org',
]
_HIDDEN_SWITCHES = [
    'batv-0.switches.crans.org',
]

def guess_switch_fqdn(switch_name):
    """Retourne le FQDN d'un switch à partir de son nom"""

    try:
        return socket.gethostbyname_ex(switch_name + ".switches.crans.org")[0]
    except socket.gaierror:
        pass

    try:
        return socket.gethostbyname_ex(switch_name)[0]
    except socket.gaierror:
        pass

    raise socket.gaierror

def all_switchs(bat=None, hide=_SPECIAL_SWITCHES + _HIDDEN_SWITCHES):
    """Retourne la liste des switchs pour un batiment.

    Si bat est donné, seulement pour le bâtiment demandé, sinon pour
    tous les bâtiments. bat peut être une liste aussi. Le backbone n'est
    pas pris en compte. La convention est batx-y sauf si y=0 et on a donc
    simplement batx"""

    if bat == None:
        bat = list(bat_switchs)
    if type(bat) not in [ tuple, list ] :
        bat = [bat]
    switchs = []
    for b in bat:
        indexes = set(n/100 for n in uplink_prises[b])
        for i in indexes:
            switch_name = "bat%s-%s" % (b, i)
            try:
                hostname = guess_switch_fqdn(switch_name)
            except socket.gaierror:
                print "Le switch %s ne semble pas exister." % (switch_name,)
                continue
            if hostname not in hide:
                switchs.append(hostname)
    switchs = set(switchs)
    # on ajoute quand-même le backbone et/ou multiprise-v6 si demandé
    switchs |= (set(_SPECIAL_SWITCHES) - set(hide))
    switchs = list(switchs)

    switchs.sort()
    return switchs

# Locaux clubs : lecture dans chbre_prises et ajout des locaux dans les bats non
# manageables
def locaux_clubs():
    """ Retourne le dictionaire des locaux club : {bat: [locaux]} """
    locaux_clubs = {key: key for key in lieux_public()}
    return locaux_clubs

#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-
#
# secrets.py
# ----------
#
# Copyright (C) 2007 Jeremie Dimino <dimino@crans.org>
# Copyright (C) 2014 Daniel STAN <daniel.stan@crans.org>
#
# This file is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This file is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.

"""
Recuperation des secrets depuis /etc/crans/secrets.
"""

import sys
import os
import logging
import logging.handlers
import getpass
import json
import functools
import subprocess
import socket

from gestion.config.services import services

SECRET_PATH = '/etc/crans/secrets'

SECRET_BIN = [
    'sudo', '-n',
    '/usr/scripts/gestion/secrets_new.py',
]

# Initialisation d'un logger pour faire des stats etc
# pour l'instant, on centralise tout sur thot en mode debug
logger = logging.getLogger('secrets_new')
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(name)s: [%(levelname)s] %(message)s')
handler = logging.handlers.SysLogHandler(address = '/dev/log')
try:
    handler.addFormatter(formatter)
except AttributeError:
    handler.formatter = formatter
logger.addHandler(handler)

def in_group(name):
    """Indique si le serveur courant est dans le groupe bcfg2 ``name`` donné"""
    return socket.gethostname() in services.get(name, [])

class SecretNotFound(Exception):
    """Le secret n'a pas été trouvé"""
    pass

class SecretForbidden(Exception):
    """Un secret a été trouvé mais nous n'avons pas le droit de le lire.
    Lancer cette exception signifie que tout chargement ultérieur du secret
    par d'autres moyens est abandonné"""
    pass

# Définitions de fonctions renvoyant un secret, si existant, en utilisant
# **UNE** méthode d'accès
def python_loader(name):
    """Charger depuis le fichier python la variable au ``name`` correspondant"""
    try:
        sys.path.insert(0, SECRET_PATH)
        import secrets as module
        sys.path.pop(0)
        try:
            return getattr(module, name)
        except AttributeError:
            raise SecretNotFound()
    except ImportError:
        raise SecretNotFound()

def single_file_loader(name, secret_path=SECRET_PATH, fatal_io=True):
    """Charger depuis un fichier isolé appelé ``name``,
    Si ``fatal_io`` est à False, une erreur d'accès au fichier (droits)
    sera considérée comme un fichier absent, et le mécanisme pourra
    ainsi continuer avec d'autres loaders.
    """
    path = os.path.join(secret_path, name)
    if not os.path.isfile(path):
        raise SecretNotFound()
    try:
        with open(path, 'r') as source:
            result = source.read().strip()
        return result
    except IOError:
        if fatal_io:
            raise SecretForbidden()
        else:
            raise SecretNotFound()

def json_loader(name, secret_path=SECRET_PATH, fatal_io=True):
    """
    Idem, mais en json
    """
    path = os.path.join(secret_path, name) + '.json'
    if not os.path.isfile(path):
        raise SecretNotFound()
    try:
        with open(path, 'r') as source:
            result = json.load(source)
        return result
    except IOError:
        if fatal_io:
            raise SecretForbidden()
        else:
            raise SecretNotFound()

def sudo_loader(name):
    """Fais une recherche récursive en rappelant ce fichier en tant que script
    sudo."""
    proc = subprocess.Popen(SECRET_BIN + [name],
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    out, err = proc.communicate()
    if proc.returncode == 2:
        last = err.split('\n')[-1]
        if last == 'NotFound':
            raise SecretNotFound()
        elif last == 'Forbidden':
            raise SecretForbidden()
        raise SecretNotFound()
    elif proc.returncode != 0:
        sys.stderr.write(err)
        raise SecretNotFound("Erreur d'appel secrets_new.py")
    else:
        last = out.split('\n')[-1]
        return json.loads(last)

class SequenceLoader(list):
    """meta-loader qui permet de chainer les loaders précédents. Se comporte
    comme une liste, mais a le bon goût d'être callable"""
    def __call__(self, name):
        notfound_error = None
        for loader in self:
            try:
                return loader(name)
            except SecretNotFound as exc:
                notfound_error = notfound_error or exc

        raise notfound_error

class ACLChecker(object):
    """meta-loader qui permet d'encapusler un loader dans des tests."""
    def __init__(self, loader):
        self.loader = loader

    def __call__(self, name):
        if self.check(name):
            return self.loader(name)
        else:
            # Essaie d'y accéder
            self.loader(name)
            # Si pas d'exception, c'est que l'objet existe bien mais qu'on
            # n'avait pas le droit de le voir
            raise SecretForbidden()

    def check(self, name):
        """Teste si ``name`` a le droit d'être lu"""
        user = os.getenv('SUDO_USER')

        # TODO Trigger et sa clé SSH !
        if user == 'root':
            return True

        # Secrets nécéssaires au câblage
        if ((user == 'respbats' and in_group('cablage')) and
            name in [
                'ldap_readonly_auth_dn',
                'ldap_readonly_password',
                'ldap_auth_dn',
                'ldap_password',
                'dhcp_omapi_keyname',
                'dhcp_omapi_keys',
                'rabbitmq_oie',
            ]):
            return True

        # radius a besoin des mdp clients (pour dynamic_clients),
        # du ldap et du dhcp pour inscrire des gens
        if user == 'freerad' and in_group('freeradius') and \
           name.split('_', 1)[0] in ['radius', 'dhcp', 'ldap']:
            return True

        if user == 'www-data' and in_group('intranet2-service'):
            if name.split('_', 1)[0] in [ 'comnpay', 'dhcp', 'digicode', 'intranet',
                'ldap', 'validation', 'django']:
                return True
            if name.split('-', 1)[0] in ['trigger', 'icecast']:
                return True

        if user == 'www-data' and in_group('site-accueil-service'):
            if name.startswith('ldap_readonly_') or\
               name.startswith('accueil'):
                return True

        if user == 'www-data' and (in_group('portail-captif') or in_group('cas')) and \
           name.startswith('ldap_readonly_'):
            return True

        # gammu doit réceptionner les messages avant de les envoyer par SMS
        if user == 'gammu' and in_group('service-sms') and name == "rabbitmq_sms":
            return True

        # nagios a besoin de faire des recherches dans la base LDAP
        if (user == 'nagios' and
            in_group('icinga2-master') and
            name in ['ldap_readonly_auth_dn', 'ldap_readonly_password']):
            return True

        if (user == 'nagios' and in_group('freeradius') and name == 'radius_key'):
            return True

        if (user == 'nagios'
                and (in_group('connection-main')
                    or in_group('connection-secondary')
                    or in_group('connection-ipv6'))
                and name == 'zebra_password'):
            return True

# Rempli loader avec la variable qu'on veut
loader = SequenceLoader([python_loader, single_file_loader, json_loader])

# En env de test, on s'autorise aussi à regarder dans un dossier custom
if os.getenv('DBG_SECRETS'):
    file_loaders = [single_file_loader, json_loader]
    for floader in file_loaders:
        dbg_loader = functools.partial(floader,
            secret_path=os.getenv('DBG_SECRETS'),
            fatal_io=False)
        loader.insert(0, dbg_loader)

if __name__ == '__main__':
    loader = ACLChecker(loader)
else:
    loader.append(sudo_loader)

def get(name):
    """ Récupère un secret. """
    prog = os.path.basename(getattr(sys, 'argv', ['undefined'])[0])
    logger.debug('%s (in %s) asked for %s' % (getpass.getuser(), prog, name))

    try:
        return loader(name)
    except SecretNotFound:
        logger.critical('...and that failed (not found).')
        raise
    except SecretForbidden:
        logger.critical('...and that failed (Forbidden).')
        raise

if __name__ == '__main__':
    try:
        json.dump(get(sys.argv[1]), sys.stdout)
    except SecretNotFound:
        sys.stderr.write("NotFound")
        exit(2)
    except SecretForbidden:
        sys.stderr.write("Forbidden")
        exit(2)

#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-
"""
Effectue la recréditation des impressions qui n'ont pas été effectuées
par l'imprimante, soit ne sont pas sorties correctement, et contiennent
des logs d'erreur.

Le script est fait pour éviter de recréditer plusieurs fois les mêmes
impressions, et croise dans l'historique de l'adhérent les impressions
(débits), et les créditations que ce script a lui même déjà effectuées.

Un mail est envoyé pour signaler les activités de crédits.
L'exécution se fait pas appel du script suivi de l'aid.

Auteurs : Gabriel Détraz <detraz@crans.org>
Pierre-Elliott Bécue <becue@crans.org>
avec contributions importantes de Daniel Stan, Valentin Samir et Lucas Serrano
"""

# Import des modules génériques
import argparse
import sys
import datetime
import re

# Imports des scripts développés au Crans
import lc_ldap.shortcuts
import lc_ldap.filter2 as lfilter
import gestion.config as config
from dialog import Dialog
from gestion import mail

# On ouvre une connexion LDAP à la base LDAP une fois pour toute.
CONN = lc_ldap.shortcuts.lc_ldap_admin()

# Des fonctions...
IMP_DONE_REGEX = re.compile(r".*debit (?P<montant>[^ ]*) Euros \[impression\((?P<jid>.*)\):.*/(?P<login>.*)/(?P<file>[^ ]*) .*\]")
IMP_DEJ_REMB_REGEX = re.compile(r".*credit (?P<montant>[^ ]*) Euros \[Impression ratee, jid=(?P<jid>.*), tache=(?P<fichier>.*)\]")

def do_remb(args):
    """Fonction appelant l'interface curses et listant les remboursements
    possibles.
    """

    rendu = []

    adh = CONN.search(
        lfilter.human_to_ldap(
            args.filtre.decode(config.encoding.in_encoding)
            ),
        mode="w"
        )
    # CONN.search peut retourner une liste vide.
    if not adh:
        return []

    with adh[0] as adh:
        liste_rembs = find_rembs(adh)
        while True:
            (ret, val) = dialog_remb(liste_rembs)
            if ret == Dialog.CANCEL:
                adh.save()
                break
            else:
                selected = liste_rembs[int(val)]
                if confirm(selected):
                    adh.solde(
                        float(selected["montant"]),
                        u"Impression ratée, jid=%(jid)s, tache=%(file)s" %
                        (selected)
                        )
                    _ = liste_rembs.pop(int(val))
                    rendu.append(selected)
                else:
                    continue
    send_mail(rendu, adh)
    return

def find_rembs(adh):
    """Fonction qui calcule les entrées remboursables d'un adhérent.
    """

    now = datetime.datetime.now()
    begin = now - datetime.timedelta(days=config.impression.delta_recredit_jours)
    liste_taches = list()
    liste_rembs = list()

    for ent in adh["historique"]:
        date = ent.get_datetime()
        if date > begin:
            match_entry = IMP_DONE_REGEX.match(unicode(ent))
            if match_entry is not None:
                liste_taches.append(match_entry.groupdict())
                continue
            match_entry = IMP_DEJ_REMB_REGEX.match(unicode(ent))
            if match_entry is not None:
                liste_rembs.append(match_entry.groupdict())
                continue

    # On génère la liste avant le return, pour ne pas la regénérer autant de
    # fois qu'il y a de tâches dans liste_taches
    jid_rembs = [remb["jid"] for remb in liste_rembs]
    return [tache for tache in liste_taches if tache['jid'] not in jid_rembs]

def dialog_remb(liste_rembs):
    """Crée une boîte de dialogue à partir de la liste des remboursements, pour
    en faire un.
    """
    if not liste_rembs:
        return (1, "")

    dialog = Dialog()
    choices = [
        (u"%s" % liste_rembs.index(dico),
         u"Remb impression du fichier %(file)s \
         (montant: %(montant)s, jid: %(jid)s)." % dico)
        for dico in liste_rembs]
    choices = [
                (index.encode('ascii', 'replace'),
                 remb.encode('ascii', 'replace'))
                for  (index, remb) in choices]

    return dialog.menu(
        u"Quelle impression souhaitez-vous rembourser ?",
        width=0, height=0, menu_height=0, title=u"Remboursement",
        choices=choices, cancel_label=u"Quitter",
        backtitle=u"Remboursement d'impressions ratées.".encode("ascii", 'ignore'))

def confirm(impression):
    """Demande avec dialog si on doit confirmer le remboursement.
    """
    dialog = Dialog()
    formatted = "%(file)s (montant: %(montant)s, jid: %(jid)s)" % impression
    confirm = u"Confirmer le remboursement de %s ?" % formatted
    return dialog.yesno(
            confirm.encode("ascii", "ignore")
    ) == dialog.DIALOG_OK

def send_mail(liste_rendus, adh):
    """Si la liste des remboursements est non-vide, envoie un mail pour prévenir
    les imprimeurs.
    """
    if not liste_rendus:
        return

    montant = sum([float(dico["montant"]) for dico in liste_rendus])
    affs = [dico['file'] for dico in liste_rendus]
    login = unicode(adh["uid"][0])

    with mail.ServerConnection() as conn_smtp:
        To = adh.get_mail()
        From = 'impression@crans.org'
        Cc = 'impression@crans.org'
        mailtxt = mail.generate('remboursement_impressions', {
            'To': To,
            'From': From,
            'adhname': login,
            'taches': u', '.join(affs),
            'montant': montant,
            'imprimeur': lc_ldap.shortcuts.current_user,
        }).as_string()

        try:
            conn_smtp.sendmail(From, (To, Cc,), mailtxt)
        except:
            print sys.exc_info()[:2]
            print u"Erreur de l'envoi à %s" % To
            sys.exit(1)

#Un bloc de test
if __name__ == "__main__":
    PARSER = argparse.ArgumentParser(
        description="Script pour recréditer les adhérents.",
    )
    PARSER.add_argument(
        'filtre',
        type=str,
        metavar="FILTRE",
        help="Le filtre LDAP à utiliser"
    )
    PARSER.add_argument(
        '-d', '--do-it',
        help="Sans cette option, le script ne fait rien"
        "(la méthode canonique de remboursement est via l'intranet)",
        action="store_true"
    )

    # Et on parse
    ARGS = PARSER.parse_args()

    if not ARGS.do_it:
        print """Pour pouvoir utiliser ce script, merci de fournir l'option -d
ou --do-it. La méthode usuelle pour recréditer une impression qui a échoué
est de passer par l'interface d'impression de l'Intranet. Allez d'abord
voir là-bas si vous pouvez faire le recrédit, et ensuite utilisez ce
script. Merci :)"""
        sys.exit(1)
    else:
        do_remb(ARGS)


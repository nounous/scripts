#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

"""
Transforme un pdf, en fichier pcl formatté pour l'impression d'une sauce,
c'est-à-dire avec agrafes, recto verso, et première feuille prise dans
le bac d'alimentation manuelle.
"""

import subprocess
import sys

DEFAULT_BAC = 7

# Commande ghostscript de conversion en PCL
params = ['gs', '-dNOPAUSE', '-dBATCH', '-sDEVICE=pxlmono',
    '-sPAPERSIZE=A4', '-dDuplex=true',
    '-dMediaPosition=%d' % DEFAULT_BAC,
    '-sOutputFile=%stdout%',
    '-sstdout=/dev/null',
    '-f', # To be continued there
]

def tweak(source, dest, **opt_):
    """Tweak un fichier pcl (source est un descripteur de fichier en lecture)
    en rajoutant les options PJL nécessaires, et en modifiant le PCL
    pour que les deux premières pages (première feuille) soient prises depuis
    un autre bac"""
    raw = False
    opt = {
        'manual_feed_count': 2,
        'copies': 1,
        'finish': 'STAPLE',
        'stapleoption': 'TWO',
        'hold': 'STORE',
        'jobname': 'sauce_test',
    }
    opt.update(opt_)
    while not raw:
        l = source.readline()
        if l.startswith('@PJL ENTER LANGUAGE'):
            raw = True # Derniere ligne avant les trucs degueux
            dest.write("""@PJL SET JOBNAME = "%(jobname)s"
@PJL SET HOLD = "%(hold)s"
@PJL SET QTY = %(copies)d
@PJL SET FINISH = "%(finish)s"
@PJL SET STAPLEOPTION = "%(stapleoption)s"
""" % opt)
        dest.write(l)

    # Entrée manuelle sur les n premières pages. On remplace n fois
    s_string = '\xf8\x25\xc0' + chr(DEFAULT_BAC)
    r_string = '\xf8\x25\xc0\x01'
    count = opt['manual_feed_count']
    #
    while True:
        x = source.read(102400)
        #print "Read 100ko"
        #sys.stdout.flush()
        if not x:
            return
        while count:
            y = x.replace(s_string, r_string, 1)
            if x != y:
                x = y
                count -= 1
            else:
                break
        dest.write(x)

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print sys.argv[0] + " source.pdf dest.pcl"
        exit(1)

    fsource = sys.argv[1]
    fdest = sys.argv[2]
    params.append(fsource)
    
    proc = subprocess.Popen(params, stderr=sys.stdout,
                                    stdout=subprocess.PIPE,
                                    stdin=sys.stdin)
    with open(fdest, 'w') as dest:
        tweak(proc.stdout, dest)


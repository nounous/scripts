#!/usr/bin/python3

import argparse
import sys

from pysnmp.hlapi import *


class Oid:
	status_messages = '1.3.6.1.2.1.43.16.5.1.2.1'
	status_ink = '1.3.6.1.2.1.43.11.1.1.9.1'
	recent_jobs = '1.3.6.1.4.1.11.2.3.9.4.2.1.1.6.5.1'
	supplies_left = '1.3.6.1.2.1.43.11.1.1.9.1'
	prtInputCurrentLevel = '1.3.6.1.2.1.43.8.2.1.10'


class Printer:

	def __init__(self, host='imprimante.adm.crans.org'):
		self.host = host
		self.session = None

	def walk(self, attribute):
		
		iterator = nextCmd(
			SnmpEngine(),
			CommunityData('public'),
			UdpTransportTarget((self.host, 161)),
			ContextData(),
			ObjectType(ObjectIdentity(*attribute)),
			lexicographicMode=False
		)

		values = []

		try:
			while True:
				errorIndication, errorStatus, errorIndex, varBinds = next(iterator)
				if errorIndication:
					print(errorIndication)
				elif errorStatus:
					print('%s at %s' % (errorStatus.prettyPrint(), errorIndex and varBinds[int(errorIndex) - 1][0] or '?'))
				for varBind in varBinds:
					values.append(varBinds)
		except StopIteration:
			pass

		return values

	def recent_jobs(self):
		jobs = {}
		for job in self.walk((Oid.recent_jobs,)):
			oid = job[0][0].getOid().asTuple()
			value = job[0][1].asOctets()[2:].decode('utf-8', 'ignore')
			jobs[oid[17]] = value
		return jobs

	def supplies(self):
		supplies_list = []
		for supply in self.walk((Oid.supplies_left,)):
			supplies_list.append(supply[0][1]._value)
		supplies = {
			'encre': {
				'noir': supplies_list[0],
				'cyan': supplies_list[1],
				'magenta': supplies_list[2],
				'jaune': supplies_list[3]
			},
			'toner': {
				'noir': supplies_list[4],
				'cyan': supplies_list[5],
				'magenta': supplies_list[6],
				'jaune': supplies_list[7]
			},
			'kit': {
				'transfert': supplies_list[8],
				'alimentation': supplies_list[9],
				'fusion': supplies_list[10]
			}
		}
		return supplies

	def status(self):
		messages = []
		for status in self.walk((Oid.status_messages,)):
			message = status[0][1].asOctets().decode('utf-8', 'ignore')
			if message:
				messages.append(message)
		return messages

	def sheets_left(self):
		sheets = []
		for tray in self.walk((Oid.prtInputCurrentLevel,)):
			sheets.append(tray[0][1]._value)
		sheets = {
			'bac 1': sheets[0],
			'bac 2': sheets[1],
			'bac 3': sheets[2],
			'bac 4': sheets[3]
		}
		return sheets


if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('-j', '--jobs', help='Afficher les tâches récentes.', action='store_true')
	parser.add_argument('-s', '--supplies', help="Afficher la quantité restante de cartouche/toner/kit en %.", action='store_true')
	parser.add_argument('--status', help="Afficher les messages de statut de l'imprimante", action='store_true')
	parser.add_argument('-p', '--paper', help="Afficher le nombre de feuilles de papier dans les bacs de l'imprimante", action='store_true')
	args = parser.parse_args()

	p = Printer()

	if args.jobs:
		recent_jobs = p.recent_jobs()
		for job in sorted(recent_jobs):
			print(job, ':', recent_jobs[job])
	elif args.status:
		for message in p.status():
			print(message)
	elif args.supplies:
		supplies = p.supplies()
		for type in sorted(supplies):
			print(type, ':')
			for color in sorted(supplies[type]):
				print(' -', color, ':', supplies[type][color], '%')
	elif args.paper:
		sheets_left = p.sheets_left()
		for tray in sorted(sheets_left):
			print(tray, ':', sheets_left[tray])

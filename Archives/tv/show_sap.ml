#load "unix.cma";;
#load "bigarray.cma";;
#load "mmap.cma";;
#load "threads.cma";;
#load "lwt.cma";;
#load "lwt_unix.cma";;

open Lwt.Infix

module String = struct
  include String

  let hash = Hashtbl.hash

  let split_on_char sep s =
    let r = ref [] in
    let j = ref (length s) in
    for i = length s - 1 downto 0 do
      if unsafe_get s i = sep then (
        r := sub s (i + 1) (!j - i - 1) :: !r ;
        j := i )
    done ;
    sub s 0 !j :: !r
end

module Stringtbl = Hashtbl.Make (String)

let timeout = 60.

let mcast_grp = "224.2.127.254" (* sap.mcast.net *)

let mcast_port = 9875

let socket = Lwt_unix.socket Unix.PF_INET Unix.SOCK_DGRAM 0

let channel : string Stringtbl.t = Stringtbl.create 20

let buffer = Bytes.create 10240

let read () =
  Lwt_unix.recv socket buffer 0 10240 []
  >>= fun l ->
  if l > 0 then (
    String.split_on_char '\n' (Bytes.to_string buffer)
    |> List.map String.trim
    |> fun l ->
    let s =
      List.fold_left
        (fun x line ->
          match x with
          | Some _ ->
              x
          | None -> (
            match String.split_on_char '=' line with
            | ["s"; value] ->
                Some value
            | _ ->
                None ))
        None l
    in
    let c =
      List.fold_left
        (fun x line ->
          match x with
          | Some _ ->
              x
          | None -> (
            match String.split_on_char '=' line with
            | ["c"; value] -> (
              match String.split_on_char ' ' value with
              | [_; _; ip] -> (
                match String.split_on_char '/' ip with
                | ip :: _ ->
                    Some ip
                | _ ->
                    None )
              | _ ->
                  None )
            | _ ->
                None ))
        None l
    in
    let _a =
      List.fold_left
        (fun x line ->
          match x with
          | Some _ ->
              x
          | None -> (
            match String.split_on_char '=' line with
            | ["a"; value] -> (
              match String.split_on_char ':' value with
              | ["x-plgroup"; grp] ->
                  Some grp
              | _ ->
                  None )
            | _ ->
                None ))
        None l
    in
    let c = match c with Some c -> c | None -> "" in
    ( match s with
    | Some s ->
        if Stringtbl.mem channel s then Stringtbl.replace channel s c
        else Stringtbl.add channel s c
    | None ->
        () ) ;
    Lwt.return_unit )
  else Lwt.return_unit

let rec loop condition f () =
  if condition () then f () >>= loop condition f else Lwt.return_unit

let main =
  Lwt_unix.setsockopt socket Unix.SO_REUSEADDR true ;
  Lwt_unix.setsockopt_float socket Unix.SO_RCVTIMEO timeout ;
  Lwt_unix.bind socket (Unix.ADDR_INET (Unix.inet_addr_any, mcast_port))
  >>= fun () ->
  Lwt.return
    (Lwt_unix.mcast_add_membership socket (Unix.inet_addr_of_string mcast_grp))
  >>= fun () ->
  let starting = Unix.time () in
  loop (fun () -> Unix.time () -. starting < timeout) read ()
  >>= fun () ->
  Stringtbl.iter
    (fun s ip -> Format.fprintf Format.std_formatter "%s: %s@." s ip)
    channel ;
  Lwt.return_unit

let () = Lwt_main.run main

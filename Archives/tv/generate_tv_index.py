#!/usr/bin/env python3
from jinja2 import Environment, FileSystemLoader
import requests
import pathlib

"""
Generate TV index
"""

def request_channels(cards_addr):
    """
    Request channels list from each instance of mumudvb

    Each returned channel contains a "name", "sap_group", "service_id".
    """
    for addr in cards_addr:
        try:
            response = requests.get(f"http://{addr}/channels_list.json")
        except requests.exceptions.ConnectionError:
            continue
        for r in response.json():
            service_id = r["service_id"]
            r["full_addr"] = f"http://{addr}/bysid/{service_id}"
            port = addr.split(':')[-1]
            r["full_addr_noip"] = f"http://tv.adh-nat.crans.org:{port}/bysid/{service_id}"
            yield r


def generate_index(channels):
    """
    Load and compile Jinja2 templates
    """
    searchpath = str(pathlib.Path(__file__).parent.absolute())
    loader = FileSystemLoader(searchpath=searchpath)
    env = Environment(loader=loader, autoescape=True)

    # HTML index
    template_html = env.get_template("index.html.j2")
    with open("/var/www/html/index.html", "w") as f:
        f.write(template_html.render(channels=channels))

    # M3U index
    template_m3u = env.get_template("index.m3u.j2")
    with open("/var/www/html/index.m3u", "w") as f:
        f.write(template_m3u.render(channels=channels))


if __name__ == "__main__":
    channels = list(request_channels([
        f"100.64.0.33:{port}" for port in range(4242, 4252)
    ]))
    generate_index(channels)

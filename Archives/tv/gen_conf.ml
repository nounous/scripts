#!/usr/bin/env ocaml
#load "unix.cma"

type tnt = {
  card:int;
  freq:int;
  channel:int;
  sid_list:int list option;
}

type polarity = Vertical | Horizontal | Left_circular | Right_circular
let string_of_pol = function
  | Vertical -> "v"
  | Horizontal -> "h"
  | Left_circular -> "l"
  | Right_circular -> "r"

type sat = {
  card:int;
  freq:int;
  pol:polarity;
  srate:int;
  sid_list:int list option;
}

let make_tnt d card channel =
  {card; freq = 306000 + 8000 * channel + d * 166; channel; sid_list = None}

(*TNT*)
let r1 = make_tnt 1 0 35
let r2 = make_tnt 1 1 25
let r3 = make_tnt 1 2 22
let r4 = make_tnt 1 3 30
let r5 = make_tnt 1 8 28
let r6 = make_tnt 1 9 32
let r7 = make_tnt 1 10 37

(*SAT*)
let sat1 = {card = 4; freq = 10847; pol = Vertical; srate = 23000; sid_list = None}
let sat2 = {card = 5; freq = 10773; pol = Horizontal; srate = 22000; sid_list = None}
let sat3 = {card = 6; freq = 11344; pol = Vertical; srate = 27500; sid_list = None}
let sat4 = {card = 7; freq = 11126; pol = Vertical; srate = 22000; sid_list = None}

(* Qui host la tv *)
let host = "cochon"

(* Les cartes TNT *)
let transpondeur_tnt = [r1;r2;r3;r4;r5;r6;r7]
(* Les cartes SAT *)
let transpondeur_sat = [sat1;sat2;sat3;sat4]
(* Fichiers de conf des cartes *)
let conf_file = Printf.sprintf "/etc/sat/carte%i.conf"

(* Template conf carte *)
let template = Printf.sprintf "### Fichier généré par /usr/scripts/tv/*
### Ne pas éditer !
#------------ TUNING -------------
#The DVB/ATSC card we want to use
card=%i
#The Transponder frequency
freq=%i
%s
#---------- AUTOCONFIGURATION -----------
#We want the full autoconfiguration (ie we discover the channels and their pids)
autoconfiguration=full
#Do we want to add the radios to the list of channels (default 0) ?
autoconf_radios=1
#Do we want to configure channels marked as scrambled (automatically done if cam_support=1)
#autoconf_scrambled=1
#
#--------- NETWORKING --------------
#What is the \"ip header\"?
#The autoconfigured multicast ip have the form header.card.channelnumber
#The default header is 239.100
#autoconf_ip_header=239.10
#
#Do we want to change the default port (optionnal) ?
#common_port=4422
#
#Do we need to change the default multicast TTL (if you have routers, default value : 2) ?
#multicast_ttl=10
#
#--------- SAP ANNOUNCES --------------
#The sap announces are sent automatically with full autoconfiguration
#Do we want NOT to send the announces ?
#sap=0
#
#What is the default playlist group for the SAP announces (optionnal) ?
sap_default_group=%s
#Who is the organisation wich send the stream (optionnal) ?
sap_organisation=Crans
#The intervall between the SAP announces in second (default 5)
#sap_interval=10
# ---------- PAT REWRITING ----------
#If some of the clients are set top boxes we will probably need to rewrite the PAT pid
#rewrite_pat=1
#
# ---------- SCRAMBLED CHANNELS ----------
#Do we want hadware descrambling ?
#cam_support=1
#
#If you have multiple CAMs you can specify the number of the one wich will be used
#cam_number=2
dont_send_scrambled=1
#
# ---------- MUTLICAST ----------
# multicast_ipv6=1 avoid sending two times each channel (ipv4 + ipv6)
#
# Disable Multicast
multicast_ipv4=0
multicast_ipv6=0
#
# HTTP unicast
unicast=1
ip_http=100.64.0.33
port_http=4242+%%card
%s
"

let template_sid_list = Printf.sprintf "# ---------- SID LIST ----------
# List of sid to stream
autoconf_sid_list=%s"

(* Template conf tuning pour le SAT *)
let template_tuning_sat = Printf.sprintf "#The polarisation (can be h, v, l, r)
pol=%s
#The symbol rate
srate=%i
";;


let gen_conf_tnt (tnt:tnt) = template tnt.card tnt.freq "" "TNT"
    (match tnt.sid_list with
     | None -> ""
     | Some l -> template_sid_list
                   (List.fold_left (fun s sid -> s ^ " " ^ (string_of_int sid)) "" l))
let gen_conf_sat (sat:sat) =
  template sat.card sat.freq
    (template_tuning_sat(string_of_pol sat.pol) sat.srate) "SAT"
    (match sat.sid_list with
     | None -> ""
     | Some l -> template_sid_list
                   (List.fold_left (fun s sid -> s ^ " " ^ (string_of_int sid)) "" l))

let write_conf card conf =
  let oc = open_out (conf_file card) in
  Printf.fprintf oc "%s" conf;
  close_out oc
;;


let () =
  (* Si on est pas sur le bon serveur ça ne marche pas *)
  if not (host = Unix.gethostname ()) then begin
    prerr_endline (Printf.sprintf "Script à lancer sur %s" host);
    exit 1
  end;
  List.iter (fun (tnt:tnt) ->
      write_conf tnt.card (gen_conf_tnt tnt)) transpondeur_tnt;
  List.iter (fun (sat:sat) ->
      write_conf sat.card (gen_conf_sat sat)) transpondeur_sat;

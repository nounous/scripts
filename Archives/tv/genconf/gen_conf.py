#! /usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

""" Script de lancement de mumudvb

Auteur : Frederic Pauget
         Brice Dubost
Licence : GPLv2
"""
import sys, getopt, os

#On importe les cartes
from sat_conf import conf
cartes = conf

# On genre la conf
for carte in cartes :
    carte.start()

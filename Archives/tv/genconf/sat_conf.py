#! /usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

""" Definit les transpondeurs a associer a chacune des cartes """

#NE PAS OUBLIER DE MODIFIER LE cartedesactivees.local et lancer bcfg2 apres
from sat_base_ng import *
from socket import gethostname

host = gethostname()

transpondeurs = { 'cochon' : [ TNT_R1_586000(0),
                               TNT_R2_506000(1),
                               TNT_R3_482000(2),
                               TNT_R4_546000(3),
                               SAT_11344h(4),
                               SAT_10773h(5),
                               SAT_10714h(6),
                               SAT_11222h(7),
                               #TNT_R5_530000(8),
                               TNT_R6_562000(9),
                               TNT_R7_642000(10),
                               TNT_L8_570000(11),
                             ]
                }

conf = transpondeurs.get(host,[])

if __name__ == '__main__' :
    import sys
    if len(sys.argv) == 2 :
        conf = transpondeurs.get(sys.argv[1],[])
    for t in conf :
        print t, "sur la carte", t.card
        for (sap_group, chaine) in t.chaines.values() :
            print '\t%s\t: \t%s' % (sap_group,chaine)

#!/bin/bash
# À lancer par bash dans un shabang de la forme :
#!/bin/bash /usr/scripts/python.sh
#Ceci permet alors de lancer python avec un PYTHONPATH custom (/usr/scripts)
#pour éviter de l'importer dans chaque script

# CPATH: dossier de base de tous les scripts
export CPATH=/usr/scripts

# Quel binaire lancer ?
BIN=python
if [ "$1" = ipython ]; then
  BIN=ipython
  shift 1
fi

# Et quel script ?
SCRIPT=$1
if [ -z "$SCRIPT" ]; then
  SCRIPT=$0
fi

# Si nous ne trouvons pas dans /usr/scripts (le vrai), alors on part à la
# recherche de la racine du dépôt cloné. (Màj $CPATH)
readlink -e $SCRIPT | grep "^/usr/scripts/" -q || {
    f=`readlink -e $SCRIPT`
    p=`dirname "$f"`
    while [ "$p" != "/" ]; do
        if [ -f "$p/CRANS_SCRIPTS_ROOT" ]; then
            CPATH=$p
            break
        fi
        p=`dirname "$p"`
    done;
    export CPATH
    # Placer dans testing.sh les variables d'env supplémentaires pour
    # debugguer des scripts
    test -f $CPATH/testing.sh && . $CPATH/testing.sh
}

# Some additionnal paths, for 3rd party libs
for i in $CPATH/lib/python2.7/site-packages/*.egg; do
    CPATH=$CPATH:$i
done
CPATH=$CPATH:/usr/scripts/lib/python2.7/site-packages/

# Fin:
/usr/bin/env PYTHONPATH=$CPATH $BIN "$@"

#!/bin/sh
# Pas de licence.

SCRIPT=/usr/scripts/surveillance/mac_prises/mac_prise_holder.py
ANALYZER=/usr/scripts/surveillance/mac_prises/mac_prise_analyzer.py

# Lancement du listage des macs en parallèle
python $SCRIPT

# Analyse
python $ANALYZER

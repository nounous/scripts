#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-
#
# list_droits.py --- Récupère la liste des adhérents ayant actuellement
#                    des droits.
"""Récupère la liste des adhérents avec des droits et l'affiche
triée par type de droit."""

from lc_ldap import shortcuts
from config.encoding import out_encoding

def fetch_adhs(ldap):
    """Récupère la liste des adhérents avec des droits et
    les trie par droits actuellement possédés"""
    adhs_avec_droits = ldap.search(u"droits=*")

    adhs_par_droit = {}

    for adh in adhs_avec_droits:
        for droit in adh['droits']:
            adhs_par_droit.setdefault(droit, []).append(adh)

    return adhs_par_droit

def make_output(adhs_par_droit):
    """Génère une sortie à partir de la liste d'adhérents
    triés par droits"""
    d = adhs_par_droit.keys()
    d.sort()

    output = []

    for droit in d:
        adhs = adhs_par_droit[droit]

        noms = []

        txt = '%s\n' % droit
        for adh in adhs :
            noms.append(u'%s %s' % (adh['prenom'][0], adh['nom'][0]))
        noms.sort()
        txt += u'   %s' % '\n   '.join(noms)

        output.append(txt)
    return output

if __name__ == '__main__':
    LDAP = shortcuts.lc_ldap_readonly()
    OUTPUT = make_output(fetch_adhs(LDAP))

    print u'\n- - - - - - = = = = = = # # # # # # # # = = = = = = - - - - - -\n'.join(OUTPUT).encode(out_encoding)

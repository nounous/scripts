#!/bin/bash

# update.sh
# ---------
# Modifié : Pierre-Elliott Bécue <becue@crans.org> (8 août 2012)
# Copyright : (c) 2008, Jeremie Dimino <jeremie@dimino.org>
# Licence   : BSD3

# Ce script fait tout ce qu'il faut après l'ajout d'un volume sur la
# baie de stockage.

HNAME=$(hostname)

exec_cmd() {
    local msg=$1
    shift
    local cmd="$@"
    if tty -s <&1; then
        echo -e "\e[37;1m===== $msg\e[0m"
        echo -e "\e[1m=> commande: $cmd\e[0m"
    else
        echo "===== $msg"
        echo "=> commande: $cmd"
    fi
    if [[ $UID = 0 ]]; then
        $cmd
    else
        sudo $cmd
    fi
}

exec_cmd "Récupération du mapping lun<->nom de volume" \
    /usr/scripts/iscsi/get_volume_mapping.py nols
#   La baie slon est inactive
#    /usr/scripts/iscsi/get_volume_mapping.py slon

exec_cmd "Rechargement des règles de udev" \
    invoke-rc.d udev reload

exec_cmd "Rescan des volumes iscsi" \
    iscsiadm -m session --rescan

exec_cmd "Mises à jours des liens symboliques dans /dev" \
    /usr/scripts/iscsi/udev_update_symlinks.py --clean

exec_cmd "Génération du fichier de configuration pour multipath" \
    /usr/scripts/iscsi/multipath_update.py

exec_cmd "On fait le ménage d'éventuels liens multipath." \
    multipath -r

exec_cmd "Redémarrage de multipathd" \
    service multipathd restart

if [ $HNAME = "zbee" ]; then
    (cat << EOF
# This is an example quotatab file
# 
# This file is used as a translation for device names, so a warnquota message
# makes sense to the user.
#
# syntax is as follows:
#
# colon ':' is used to specify the start of the substituted text
# pipe  '|' is used to specify a line break
#
# device:substituted text
# device:text on line 1|test on line2
#
# For instance if you would like to have warnquota tell the user their
# 'mailspool' is full instead of '/dev/hdb1' is full, use the following
# example.
#
# /dev/hdb1:mailspool

# Attention, sur zbee, les lignes suivantes sont générées par un update.sh

EOF
) > /etc/quotatab

    for i in {a..z}; do
        echo "/dev/$(readlink /dev/iscsi_home_${i}_part1):Partition /home/${i}" >> /etc/quotatab
    done
fi

echo "" >> /etc/quotatab

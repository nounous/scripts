#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

##############################################
# Système d'inscription en ligne à jabber    #
#     14/02/2004 -- Frédéric Pauget          #
# Migration à ejabberd le 9 août 2009 -- adg #
##############################################

import os, cgi, string, time, sys, commands, datetime
form = cgi.FieldStorage()
print "content-type: text/html"
print

file=open('/var/www/jabber/template')
html=file.read()

logfile='/var/log/ejabberd/inscriptions.log'

page ="<H2>Création d'un compte Jabber.</H2>"

try :
    form['traitement'] # Si ne passe pas c'est que la page est donnés sans arguements
    # Récupération des arguments et test
    try :
	user=form['user'].value
	user=string.lower(user)
	for i in user[:] :
	    if not i in string.lowercase+string.digits : raise
	if not user[0] in string.lowercase : raise
	col_user='FFFFFF'
    except :
	col_user='FF0000'
	user=''

    # vérification des mots de passe
    try :
	pass1=form['pass1'].value
        pass2=form['pass2'].value
        if pass1!=pass2 : raise
	password=pass1
        col_pass='FFFFFF'
    except :
	password=''
        col_pass='FF0000'

    # vérification du nom
    try :
	nom=form['nom'].value
	nom=string.capitalize(nom)
	for i in nom[:] :
	    if not i in string.letters+' -' : raise
	if not nom[0] in string.uppercase : raise
	col_nom='FFFFFF'
    except :
	nom=''
	col_nom='FF0000'

    # vérification du prénom
    try :
	prenom=form['prenom'].value
	prenom=string.capitalize(prenom)
	for i in prenom[:] :
	    if not i in string.letters+' -' : raise
	if not prenom[0] in string.uppercase : raise
	col_prenom='FFFFFF'
    except :
	prenom=''
	col_prenom='FF0000'

    # vérification du mail
    try :
	mail=form['mail'].value
	mail = string.lower(mail)
	if not mail[0] in string.lowercase : raise
	if string.find(mail,'@')<1 : raise
	if not (mail[-3] in '.' or mail[-4] in ('.')) : raise
	for l in mail[:]:
	    if not l in (string.lowercase + string.digits + '-_.@') : raise
	col_mail='FFFFFF'
    except :
	mail=''
	col_mail='FF0000'

# Fin des tests
except :
    user ='' ; col_user='FFFFFF'
    nom='' ; col_nom='FFFFFF'
    prenom='' ; col_prenom='FFFFFF'
    mail='' ; col_mail='FFFFFF'
    col_pass='FFFFFF'

if col_user!='FFFFFF' or col_pass!='FFFFFF' or col_mail!='FFFFFF' or col_nom!='FFFFFF' or col_prenom!='FFFFFF' :
    page+="Certaines donnes sont incorrectes :<br><br>"

if user and password and nom and prenom and mail:

    r, msg = commands.getstatusoutput('/usr/sbin/ejabberdctl %s%s%s%s' %
                                      ('register', commands.mkarg(user),
                                       ' jabber.crans.org',
                                       commands.mkarg(password)))
    if msg:
        page += '<p><b>ejabberdctl&nbsp:</b> %s</p>' % msg

    if r == 0:
        r, msg = commands.getstatusoutput('/usr/sbin/ejabberdctl %s%s%s%s' %
                                          ('set_vcard', commands.mkarg(user),
                                           ' jabber.crans.org EMAIL',
                                           commands.mkarg(mail)))
        if r and msg:
            page += '<p><b>ejabberdctl&nbsp:</b> %s</p>' % msg

        r, msg = commands.getstatusoutput('/usr/sbin/ejabberdctl %s%s%s%s' %
                                          ('set_vcard', commands.mkarg(user),
                                           ' jabber.crans.org FN',
                                           commands.mkarg('%s %s' % (prenom,nom))))
        if r and msg:
            page += '<p><b>ejabberdctl&nbsp:</b> %s</p>' % msg

        try:
            log = open(logfile, 'a')
            now = datetime.datetime.now()
            log.write('%(now)s Creation de %(user)s@jabber.crans.org par %(prenom)s %(nom)s <%(mail)s>\n' % locals())
            log.close()
        except Exception, e:
            page+= str(e)
        page += "<b>Enregistrement réussi :</b><br><ul>"
        page += "<li>vous pouvez immédiatement vous connecter au serveur jabber du crans<br>"
        page += "<li>Votre nom d'utilisateur est : %s<br>" % user
        page += "<li>Le serveur est : jabber.crans.org<br>"
        page += "<li>Votre JID est %s@jabber.crans.org<br>" %user
        page += "<li>Le port standard est le 5222<br>"
        page += "<li>Notre serveur accepte le SSL sur le port 5223 ('ancien SSL')<br>"
        page += "</ul><p><b>Bonne utilisation de Jabber :)</b></p>"
        page += "<p>Plus d'informations pour configurer son client sur le <a href=http://wiki.crans.org/VieCrans/UtiliserJabber>wiki</a>."

        print html % page
        sys.exit(0)

    else:
#        page += "<b>Erreur : </b>login déja pris.<br><br>"
	col_user='FF0000'
        user=''

page += "<table align=center border=1 cellpadding=5>"
page += "<form method=post>"
page += "<tr><td align=right>Identifiant souhaité (JID):<td bgcolor=#%s><input type=text name=user size=10 value='%s'>@jabber.crans.org" % (col_user,user)
page += "<tr><td align=right>Mot de passe :<td bgcolor=#%s><input type=password name=pass1 size=15>" % col_pass
page += "<tr><td align=right>Retaper mot de passe :<td bgcolor=#%s><input type=password name=pass2 size=15>"  % col_pass
page += "<tr><td align=right>Nom :<td bgcolor=#%s><input type=text name=nom size=15 value='%s'>" % (col_nom,nom)
page += "<tr><td align=right>Prénom :<td bgcolor=#%s><input type=text name=prenom size=15 value='%s'>" % (col_prenom,prenom)
page += "<tr><td align=right>Adresse mail :<td bgcolor=#%s><input type=text name=mail size=30 value='%s'>" % (col_mail,mail)
page += "<tr><td align=right><td><input type=submit name=traitement value=\"S'inscrire\">"
page += "</table></form>"

print html % page

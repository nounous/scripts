#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

########################################################################
# Fichier : lostPassword.py
# Qu'est-ce que ça fait :   génère automatiquement un nouveau mot de passe
#                           et l'envoie à l'utilisateur
#
# Récupèrera l'adresse mail dans la vcard du compte
#   Le fichier de log
logfile='/var/log/jabber/inscriptions.log'
########################################################################

########################################################################
# Si on a reçu les champs du formulaire
# -> l'utilisateur a envoyé son login et son mail
#       ->  s'ils correspondent bien à un fichier xml
#           on envoie le mail avec le nouveau mot de passe
#       ->  s'il y a une erreur, on réaffiche le formulaire
# Sinon on affiche juste la template du formulaire

import os, cgi, string, sys, fonctions, commands
from datetime import datetime
form = cgi.FieldStorage()
print "content-type: text/html"
print
erreur = ''
try:
    host = os.environ['REMOTE_HOST']
except KeyError:
    host = os.environ['REMOTE_ADDR']
html = open('/var/www/jabber/template').read()
new_password_form = open('./templates/new_password_form.tpl').read()
await_new_password = open('./templates/await_new_password.tpl').read()
mail_template = open('./templates/mail.tpl').read()

try :
    form['traitement'] # Si ne passe pas c'est que la page est donnée sans arguments
    # Récupération des arguments et test
    try :
        user=form['user'].value
        user=string.lower(user)
        for i in user[:] :
            if not i in string.lowercase+string.digits : raise
        if not user[0] in string.lowercase : raise
        col_user='FFFFFF'
    except :
        col_user='FF0000'
        user=''

    try :
        mail = form['mail'].value
        mail = string.lower(mail)
        col_mail = 'FFFFFF'
    except :
        erreur+= "<b>Erreur : </b>adresse mail incorecte.<br><br>"
        mail=''
        col_mail='FF0000'

# Fin des tests
except :    # si ce n'est pas passé : on a pas reçu le formulaire
    user = '' ; col_user='FFFFFF'
    mail = '' ; col_mail='FFFFFF'

if user and mail:
    r, msg = commands.getstatusoutput('/usr/sbin/ejabberdctl %s%s%s' %
                                      ('get_vcard', commands.mkarg(user),
                                       ' jabber.crans.org EMAIL'))

    try:
        # Paranoia : on loggue
        file=open(logfile,'a')
        file.write("%s %s %s %s <%s>\n" %  (datetime.now(), host, user, mail))
        file.close()
    except:
        pass

    if r == 0 and msg == mail:
        fn = commands.getoutput('/usr/sbin/ejabberdctl %s%s%s' %
                                ('get_vcard', commands.mkarg(user),
                                 ' jabber.crans.org FN'))

        fonctions.sendmail(mail_template % locals())
        # ecrire la confirmation
        print html % (await_new_password % locals())
        sys.exit(0)
    else:
        erreur = '<p><b>Erreur&nbsp:</b> Utilisateur inconnu ou adresse mail erronée</p>'
        mail = ''
        user = ''

# on affiche le formulaire
print html % (new_password_form % locals())

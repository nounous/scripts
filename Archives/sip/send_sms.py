#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

"""
Petit script d'envoi de message via sip.
Pour l'instant uniquement pour les personnes connectées
(le message est jeté sinon)
Prend le message sur stdin.

Alternatives intéressantes à étudier :
https://github.com/grengojbo/python-sipsimple
https://github.com/grengojbo/sipclients
"""

import sys
import gestion.secrets_new as secrets
import os
import getpass
import sys
import argparse

parser = argparse.ArgumentParser(description="Envoi d'un message sip")
parser.add_argument('-d', '--dst', default=None,
    help="Destinataire, peut être de la forme sip:13570. (Défaut: soi-même)")

# Attention: on a besoin d'importer le module sip de /usr/scripts
# et non celui des libs python
import lc_ldap.shortcuts
from sip.asterisk import Manager, AsteriskError, Sms, Profile

if __name__ == '__main__':
    args = parser.parse_args(sys.argv[1:])

    login = getpass.getuser()
    if login == 'respbats':
        login = os.getenv('SUDO_USER')
    ldap = lc_ldap.shortcuts.lc_ldap_readonly()
    src = ldap.search(u'uid=%s' % login)[0]

    dst = args.dst
    if not dst:
        dst = src
    elif not dst.startswith('sip:') and '@' in dst and not dst.endswith('@crans.org'):
        dst = 'sip:%s' % dst
    elif not dst.startswith('sip:'):
        dst = dst.replace('@crans.org', '')
        try:
            dst = ldap.search(u"(|(uid=%(dst)s)(mailAlias=%(dst)s@crans.org)(canonicalAlias=%(dst)s@crans.org)(aid=%(dst)s))" % {'dst' : dst})[0]
        except IndexError:
            sys.stderr.write("Pas utilisateur trouvé pour %s\n" % args.dst)
            sys.exit(1)
    sms=Sms("dbname='django' user='crans' host='pgsql.v4.adm.crans.org'", "voip_sms")
    sms.send(dst, sys.stdin.read(), src)

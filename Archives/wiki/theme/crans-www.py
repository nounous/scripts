#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-
"""
    MoinMoin monobook theme.

    Author: Antoine Durand-Gasselin <adg@crans.org>
"""

from MoinMoin.theme import ThemeBase
import crans   # C'est le thème crans que l'import

class Theme(ThemeBase):

    name = "crans-www"

    # liste des liens du menu
    menu_links = [
        # tire             #lien

        (u"Accueil",       u"http://www.crans.org/"),
        (u"Webmail",       u"https://webmail.crans.org/"),
        (u"Accès SSH",     u"http://www.crans.org/VieCrans/OpenSsh"),
        (u"Accès Intranet",u"https://intranet.crans.org/"),
        (u"Pages persos",  u"http://www.crans.org/PagesPerso"),
        ]

    # Chemin des badges
    badgesPath = u'/wiki/additional/badges/'


    # liste des badges
    support_badges = [
        # page,                          # image                       # alt text
        (u'http://www.mozilla-europe.org/fr/firefox/',
                                         u'badges_80x15_firefox.gif',  u'Get firefox'),
        (u'http://www.debian.org/',      u'badges_80x15_debian.png',   u'Debian powered'),
        (u'http://wiki.nginx.org/',      u'badges_80x15_nginx.png',    u'Nginx powered'),
        (u'http://www.python.org/',      u'badges_80x15_python.png',   u'Python powered'),
        (u'http://www.federez.org/',     u'badges_80x15_federez.png',  u'Membre du r&eacute;seau federez'),
        (u'/VieCrans/Donateurs',         u'badges_80x15_donor.png',    u'Généreux donateurs'),
        (u'http://moinmo.in/',           u'badges_80x15_moinmoin.png', u'Moinmoin powered'),
        (u'http://jigsaw.w3.org/css-validator/check?uri=referer&amp;profile=css3&amp;warning=no',
                                         u'valid_css_80x15.png',       u'Valid css3'),
        (u'http://validator.w3.org/check?uri=referer',
                                         u'valid_html401_80x15.gif',   u'Valid HTML 4.01'),
    ]

    icons = crans.Theme.icons
    def img_url(self, img):
        return "%s/crans/img/%s" % (self.cfg.url_prefix_static, img)

    def header(self, d):
        """
        Assemble page header

        @param d: parameter dictionary
        @rtype: string
        @return: page header html
        """
        html = [ self.emit_custom_html(self.cfg.page_header1),
                 u'<div id="mainContent">', self.logo(), ]
        if d['page'].page_name != 'VieCrans':
            html += [u'<h1 id="title">', self.title(d), u'</h1>']
        html += [ self.msg(d), self.startPage() ]

        return u'\n'.join(html)

    editorheader = header

    def footer(self, d, **keywords):
        """ Assemble wiki footer

        @param d: parameter dictionary
        @keyword ...:...
        @rtype: unicode
        @return: page footer html
        """

        page = d['page']

        html = [
            #self.pageinfo(page),
            self.endPage(),
            self.menu(d),
            u'</div>',
            self.username(d),
            self.badges(d)
            ]
        return u'\n'.join(html)

    def menu(self, d):
        """ assemble all the navigation aids for the page
        """
        liens = []
        for titre, lien in self.menu_links:
            liens.append(u'<li><a href="%s">%s</a></li>' % (lien, titre))
        html = [
            u'<div id="menuNavigation">',
            u'<ul>',
            u'\n'.join(liens),
            u'<li>%s</li>' % self.searchform(d),
            u'</ul>',
            u'</div>',
            ]
        return u''.join(html)

    def badges(self,d ):
        badges_html = []
        for page, image, alt_text in self.support_badges:
            badges_html.append(u'<li><a href="%(href)s" title="%(alt)s"><img src="%(path)s%(image)s" alt="%(alt)s"></a></li>' % {'href':page, 'path':self.badgesPath, 'image':image, 'alt':alt_text})
        html = [
            u'<div id="supportBadges">\n  '
            u'<ul>\n    ',
            u'\n    '.join(badges_html),
            u'\n  </ul>\n</div>\n\n',
        ]
        return ''.join(html)


def execute(request):
    """ Generate and return a theme object

    @param request: the request object
    @rtype: MoinTheme
    @return: Theme object
    """
    return Theme(request)

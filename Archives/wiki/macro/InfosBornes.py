#! /usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

# Génération d'un fichier XML indiquant le status des bornes

import sys
import os
import time, urllib2
import xml.dom.minidom

def execute(macro, text):
    f=macro.formatter

    # ouverture du fichier d'info et parsage
    try:
        #os.putenv("http_proxy", "http://proxy.crans.org:3128")
        status=xml.dom.minidom.parseString(urllib2.urlopen("https://wifi.crans.org/status.xml").read())
    except:
        return f.text(u"Impossible d'accéder aux informations des bornes")

    # On récupère l'ensemble des bornes
    bornes = status.childNodes[0]
    code = f.text(u"Mise à jour le "+bornes.getAttribute("date"))
    code += f.linebreak(0)

    code += f.table(True, {'tablealign': 'center'})

    code += f.table_row(True, {'rowbgcolor': '#FFFFA0'})
    for nom_col in (u"Nom", u"Hotspot", u"État", u"Localisation", u"Clients",
                    u"MAC", u"Canal", u"Uptime", u"°E", u"°N"):
        code += f.table_cell(True)
        code += f.strong(True)
        code += f.text(nom_col)
        code += f.strong(False)
        code += f.table_cell(False)
    code += f.table_row(False)


    bornes.childNodes.sort(lambda x, y: cmp(x.getAttribute(u"nom"),
                                            y.getAttribute(u"nom")))

    dico = {u"hotspot": {u"1": f.smiley('(./)'), u"0": f.text(u"")},
            u"up": {u"1": f.smiley(u"(!)"), u"0": f.smiley(u"{X}")}}

    for b in bornes.childNodes:
        code += f.table_row(1)

        code += f.table_cell(1)
        code += f.text(b.getAttribute(u"nom"))
        code += f.table_cell(0)


        for nom_attribut in (u"hotspot", u"up"):
            code += f.table_cell(1)
            code += dico[nom_attribut][b.getAttribute(nom_attribut)]
            code += f.table_cell(0)

        for tag in [u'description', u"clients",u"mac",u"canal"]:
            code += f.table_cell(1)
            if (b.getElementsByTagName(tag)!=[]):
                code += f.text(b.getElementsByTagName(tag)[0].firstChild.data)
            else:
                code += f.text(u"")
            code += f.table_cell(0)

        code += f.table_cell(1)
        if (b.getElementsByTagName(u"uptime")!=[]):
            code += f.text("%.2f" % float(b.getElementsByTagName(u"uptime")[0].firstChild.data)+" j")
        else:
            code += f.text(u"")
        code += f.table_cell(0)

        if (b.getElementsByTagName(u"position")!=[]):
            for nom_attribut in [u'x',u'y']:
                code += f.table_cell(1)
                code += f.text(b.getElementsByTagName(u"position")[0].getAttribute(nom_attribut))
                code += f.table_cell(0)
        else:
            code += f.table_cell(1)
            code += f.text(u"")
            code += f.table_cell(0)
            code += f.table_cell(1)
            code += f.text(u"")
            code += f.table_cell(0)

        code += f.table_row(0)

    code += f.table(0)
    return code

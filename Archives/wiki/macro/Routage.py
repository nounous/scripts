# -*- mode: python; coding: utf-8 -*-

import MonitStatus
import sys
if '/usr/scripts' not in sys.path:
    sys.path.append('/usr/scripts')
from gestion.config import firewall

def Cellule(texte, couleur, f) :
    """
    Retourne le code HTML d'une cellule formattée aver le formatter f
    """
    code  = f.table(1)
    code += f.table_row(1)
    code += f.table_cell(1,{'style':'background-color:%s; color: black;' % couleur })
    code += f.text(texte)
    code += f.table_cell(0)
    code += f.table_row(0)
    code += f.table(0)
    return code

def execute(macro, routeur):
    try:
        map_routeurs = dict(map(reversed, firewall.routeurs_du_crans.items()))
        routeur_role = map_routeurs[routeur]
        if routeur_role==u"routeur_main":
            status = MonitStatus.HostStatus(routeur)['Program']['etat_routage_main']['status']
        elif routeur_role==u"routeur_secondary":
            status = MonitStatus.HostStatus(routeur)['Program']['etat_routage_secondary']['status']
        else:
            raise NotImplementedError
        if status=="Status ok":
            if routeur_role==u"routeur_main":
                return Cellule(u'Routage actif sur %s (routeur main), nous sommes en connexion normale' % routeur,'lime',macro.formatter)
            elif routeur_role==u"routeur_secondary":
                return Cellule(u'Routage inactif sur %s (routeur secondaire)' % routeur,'yellow',macro.formatter)
        else:
            if routeur_role==u"routeur_main":
                return Cellule(u'Routage inactif sur %s (routeur main)' % routeur,'red',macro.formatter)
            elif routeur_role==u"routeur_secondary":
                return Cellule(u'Routage actif sur %s (routeur secondaire), connexion secondaire active' % routeur,'lime',macro.formatter)
    except :
        return Cellule(u'Impossible de déterminer l\'état de la connexion sur %s' % routeur,'yellow',macro.formatter)

# -*- coding: iso-8859-1 -*-
"""
    MoinMoin - YouTube Macro
    Benjamin « esum » « coq » Graillot
    pour le Cr@ns

    Syntaxe :
    <<YouYube(V8tSRJ8e3b0)>>
    <<YouTube(https://youtube.com/watch?v=wKDD1H-Hlpc)>>
    <<YouTube(V8tSRJ8e3b0, loop=true)
    <<YouTube(V8tSRJ8e3b0;wKDD1H-Hlpc)

    Arguments:
    * video: liste de liens ou d'identifiants de video

    Arguments optionnels:
    * loop: boucler la lecture.
    * autoplay: lancer la vidéo à l'ouverture de la page.
    * controls: afficher les contrôles de lecture de la vidéo.
    * width: largeur du lecteur.
    * height: hauteur du lecteur.
    * frameborder: afficher la bordure du lecteur.

"""

def execute(macro, text):
    text = text.split(',')
    videos = text[0].split(';')
    args = []
    kwargs = {}
    for i, video in enumerate(videos):
        video = video.strip()
        if video.startswith('https://') or video.startswith('http://'):
            video = video.split('v=')[1].strip()
        if i == 0:
            args.append(video)
        elif i == 1:
            kwargs['playlist'] = video
        else:
            kwargs['playlist'] += ',' + video
    for arg in text[1:]:
        kw = None
        if '=' in arg:
            arg = arg.split('=')
            kw = arg[0].strip()
            arg = arg[1]
        arg = arg.strip()
        if arg.lower() in {'true', 'false'}:
            arg = int(bool(arg))
        elif arg.isnumeric():
            arg = int(arg)
        if kw is None:
            args.append(arg)
        else:
            kwargs[kw] = arg
    return macro.formatter.rawHTML(html(*args, **kwargs))

def html(video, controls=1, loop=0, autoplay=0, width=640, height=360, frameborder=False, playlist=None):
    html = u'''<iframe width="{}" height="{}" src="https://www.youtube.com/embed/{}?loop={}&autoplay={}&controls={}{}" frameborder="{}"></iframe>'''.format(
        width,
        height,
        video,
        loop,
        autoplay,
        controls,
        '&playlist={}'.format(playlist) if playlist is not None else '',
        int(frameborder))
    return html

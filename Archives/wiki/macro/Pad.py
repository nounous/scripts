# -*- mode: python; coding: utf-8 -*-


def execute(macro, args):
    name = macro.request.page.page_name

    name = name.split('/')[-1]

    url = "http://pad.crans.org/p/" + name
    return macro.formatter.rawHTML("""Lien vers l'<a href="%s">etherpad associ&eacute;</a>""" % url)

# -*- coding: iso-8859-1 -*-
"""
    WikiNinjas Macro (from Moimoin - RandomPage Macro)

    @copyright: 2000 Aurore "Zelda" Moisy-Mabille
    @license: GNU GPL, see COPYING for details.
"""

import random
random.seed()

Dependencies = ["time"]

def macro_WikiNinjas(macro, args):
    """On sort un wiki-lien vers la page d'un des trois WikiNinjas choisi aléatoirement"""
    all_pages = args.split(',')

    # select random page
    page = random.choice(all_pages)

    f = macro.formatter
    # return a single page link
    return (f.pagelink(1, page, generated=1) +
            f.text(page) +
            f.pagelink(0, page))

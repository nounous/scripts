# -*- coding: iso-8859-1 -*-
"""
                                            ..                
                       ....  ............   ........          
                     .     .......   .            ....  ..    
                   .  ... ..   ..   ..    ..   ..... .  ..    
                   .. .. ....@@@.  ..  .       ........  .    
              ..  .  .. ..@.@@..@@.  .@@@@@@@   @@@@@@. ....  
         .@@@@. .@@@@. .@@@@..@@.@@..@@@..@@@..@@@@.... ....  
       @@@@... .@@@.. @@ @@  .@..@@..@@...@@@.  .@@@@@.    .. 
     .@@@..  . @@@.   @@.@@..@@.@@..@@@   @@ .@@@@@@..  ..... 
    ...@@@.... @@@    .@@.......... ........ .....        ..  
   . ..@@@@.. .         .@@@@.   .. .......  . .............  
  .   ..   ....           ..     .. . ... ....                
 .    .       ....   ............. .. ...                     
 ..  ..  ...   ........ ...      ...                          
  ................................                            
                                                              
    MoinMoin - Block with bgpicture

    PURPOSE:
        Un block avec une image en arri�re-plan

    CALLING SEQUENCE:
      {{{
      #!BgPic picture
      blablabla

      tables, images....
      }}}

    Copyright 2009 � Antoine Durand-Gasselin <adg@crans.org>

"""

import os,string,re,StringIO
from MoinMoin import wikiutil
from MoinMoin.action import AttachFile
from MoinMoin.parser.text_moin_wiki import Parser as WikiParser

Dependencies = []

#####################################################################
# Parser :  classe principale, c'est elle qui fait tout
#######
class Parser:
    parsername = "BgPic"
    Dependencies=Dependencies

    def __init__(self, raw, request, **kw):
        self.request = request
        self.pn = request.page.page_name
        self.raw = raw
        self.pic = kw["format_args"].strip()

    def format(self, formatter):

        content_buf = StringIO.StringIO()
        self.request.redirect(content_buf)
        WikiParser(self.raw, self.request).format(formatter)
        content = content_buf.getvalue()
        self.request.redirect()

        del content_buf

        html_fmt = '''
<div style="background: transparent url('%s') center left no-repeat; margin: auto;">
%s
</div>
    '''

        pic_url = AttachFile.getAttachUrl(self.pn, self.pic, self.request)
        pic_url = wikiutil.escape(pic_url)

        html = formatter.rawHTML(html_fmt % (pic_url, content))
        self.request.write(html)




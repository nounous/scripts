#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

# Script de création de compte sur le wiki
# Auteur : Stéphane Glondu
# Licence : GPLv2

import sys, os, httplib, urllib, locale
from getpass import getpass
from smtplib import SMTP
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.MIMEMultipart import MIMEMultipart
from email.Encoders import encode_7or8bit

if '/usr/scripts' not in sys.path:
    sys.path.append("/usr/scripts")
from gestion.affich_tools import coul, prompt, cprint
from gestion.user_tests import getuser

bugmail = ["root@crans.org"]
bugreport = u"""
Nom d'utilisateur : %(name)s
Adresse e-mail : %(email)s
"""

# C'est l'encodage du wiki et des bugreports
encoding = "utf-8"

# Utilisateur qui lance le script
user = getuser()

def send(msg, user=user):
    msg['From'] = '%s@crans.org' % user
    msg['To'] = ','.join(bugmail)
    msg['User-Agent'] = 'creer_compte_wiki.py'
    smtp = SMTP()
    smtp.connect()
    smtp.sendmail('%s@crans.org' % user, bugmail, msg.as_string())
    smtp.quit()

def creer_compte(nom, mdp, email, user=user):
    form = {'action': 'crans_newaccount',
            'name': nom.encode(encoding),
            'password': mdp.encode(encoding),
            'email': email.encode(encoding),
            'create_only': 'Create+Profile'}
    params = urllib.urlencode(form)
    headers = {"Content-type": "application/x-www-form-urlencoded",
               "Accept": "text/plain"}
    conn = httplib.HTTPSConnection("wiki.crans.org")
    conn.request("POST", "/UserPreferences", params, headers)
    response = conn.getresponse()
    data = response.read()
    conn.close()
    if 'created' in data:
        return u"Compte %s créé avec succès !" % nom, False
    elif 'nonunique name' in data:
        return u"Le compte %s existe déjà !" % nom, True
    elif 'nonunique email' in data:
        return u"L'adresse %s est déjà utilisée !" % email, True
    elif 'Password not acceptable: Password too short.' in data:
        return u"Le mot de passe choisi est trop court", True
    elif 'invalid name' in data:
        msg = u"Le nom d'utilisateur %s est invalide !" % nom
        msg += u"""
Le nom d'utilisateur doit être un WikiNom, voir à ce sujet :
    http://wiki.crans.org/NomWiki
Il peut contenir n'importe quel caractère alphanumérique, avec
éventuellement un espace facultatif entre chaque mot. Il peut aussi
contenir des accents, mais assurez-vous que votre terminal est
correctement configuré (en %s).""" % (locale.getdefaultlocale()[1])
        return msg, True
    else:
        html = MIMEBase('text', 'html')
        html.set_payload(data)
        html.add_header('Content-Disposition', 'attachment', filename='response.html')
        txt = MIMEText(bugreport.encode(encoding) % form, 'plain', encoding)
        msg = MIMEMultipart()
        msg['Subject'] = '[Bugreport] creer_compte_wiki.py: creer_compte'
        msg.attach(txt)
        msg.attach(html)
        send(msg, user)
        return (u"Erreur inconnue\nUn rapport de bug a été envoyé. "
                u"Réessayez plus tard."), True

if __name__ == '__main__':
    try:
        nom = prompt("Nom d'utilisateur (utiliser un WikiNom) : ")
        while True:
            mdp = getpass(coul("Mot de passe (ne pas utiliser le même que pour zamok) : ", "gras"))
            if getpass(coul("Confirmation du mot de passe : ", "gras")) == mdp:
                break
            else:
                cprint(u"Les deux mots de passe sont différents, veuillez réessayer...", "jaune")
        email = prompt("Adresse e-mail : ")
        message, erreur = creer_compte(nom, mdp, email)

        if erreur:
            print coul(message, "rouge")
        else:
            print coul(message, "vert")

    except KeyboardInterrupt:
        print "Interruption par l'utilisateur."
        exit = 255

    except SystemExit, c:
        if c.__str__() == '254':
            os.system('reset')
            print "Votre session d'édition à été tuée."
        exit = c

    except:
        import traceback
        msg = MIMEText('\n'.join(traceback.format_exception(sys.exc_type, sys.exc_value, sys.exc_traceback)).encode(encoding), 'plain', encoding)
        msg['Subject'] = '[Bugreport] creer_compte_wiki.py: __main__'
        send(msg)
        print coul(u"Erreur inconnue\n", "rouge") + \
              u"Un rapport de bug a été automatiquement envoyé. Réessayez plus tard."

# ⁻*- mode: python; coding: utf-8 -*-
"""
Backend Python pour FreeRADIUS.

Inspirés du code source du module rlm_python et d'autres exemples trouvés ici :
https://github.com/FreeRADIUS/freeradius-server/blob/v3.0.x/src/modules/rlm_python/
"""

from __future__ import print_function, unicode_literals

import os
import time
import logging
import traceback
import threading

import radiusd
import psycopg2
from psycopg2 import extras
from ldap import SERVER_DOWN, SCOPE_BASE

from lc_ldap import shortcuts, crans_utils, objets
from gestion.config import config
from gestion.annuaires_pg import reverse, lieux_public

###############################################################################
##                              Environnement                                ##
###############################################################################

#: Serveur radius de test
TEST_SERVER = bool(os.getenv('DBG_FREERADIUS', False))

#: Sur certains serveurs, on veut désactiver l'accounting
NO_ACCOUNTING = bool(os.getenv('NO_ACCOUNTING', False))

#: Nombre maximal de tentatives lors de la connexion à un annuaire LDAP
LDAP_CONN_MAX_TRY = 2

## Filtres LDAP
#: Filtre LDAP pour la recherche de machines
LDAP_FILTER = '(&(objectClass=%(objectClass)s)(%(af)s=%(address)s))'

## Paramètres SQL
#: Configuration de la connexion à la base de données SQL
SQL_PARAMS = {
    'user' : 'freerad',
    'dbname' : 'radius',
    'host' : 'pgsql.adm.crans.org' if not TEST_SERVER else 'vo.adm.crans.org',
    'cursor_factory' : extras.DictCursor,
}

#: Temps (en s) pendant lequel les résultats (propriétaire, machine) sont mis en cache
CACHE_TIMEOUT = 5.0

## Tagging dynamique des VLANs
#: Activation du tagging dynamique de VLANs en filaire
DYN_VLAN_WIRED = True

#: Activation du tagging dynamique de VLANs en Wi-Fi
DYN_VLAN_WIRELESS = False

## Change-Of-Authorization/Disconnect Messages (RFC 5176)
#: Activation des CoA/DMs
USE_COA = True

#: Envoi de CoA-Requests plutôt que des Disconnect-Requests en filaire
COA_REQUESTS_WIRED = True

#: Envoi de CoA-Requests plutôt que des Disconnect-Requests en sans fil
COA_REQUESTS_WIRELESS = False

## Blacklistes
#: Blacklistes impliquant un rejet immédiat
BL_REJECT = {'bloq'}

#: Blacklistes impliquant l'isolement de la machine
BL_ISOLEMENT = {'virus', 'autodisc_virus', 'autodisc_p2p', 'ipv6_ra'}

#: Blacklistes impliquant le traitement de la machine comme inconnue
BL_ACCUEIL = {'paiement', 'chambre_invalide'}

# TODO: mettre ça dans config.py en explicitant un peu comment ça marche
# et en trouvant moyen de refresh en fonction de la période de l'année
# (bl soft/hard parefeu ou pas)

## Valeurs par défaut de certains attributs LDAP
#: Adresse MAC de substitution pour les nouvelles machines
MAC_PLACEHOLDER = '<automatique>'

## Types d'adhérents
#: Personnes physiques
OWNER_ADHERENT = objets.adherent

#: Personnes morales
OWNER_CLUB = objets.club

#: Le Crans (Pseudo-Adhérent)
OWNER_CRANS = objets.AssociationCrans

## Types de machines
#: NAS
TYPE_NAS = 'NAS'

#: Machine filaire
TYPE_WIRED = 'Wired'

#: Machine sans fil
TYPE_WIRELESS = 'Wireless'

## VLANs
#: VLAN pour machines filaires des adhérents
VLAN_ADHERENTS = unicode(config.vlans['adherent'])

#: VLAN pour les machines sans fil des adhérents natées
VLAN_WIFI_NEW = unicode(config.vlans['wifi-new'])

#: VLAN pour les machines filaires des adhérents natées
VLAN_FIL_NEW = unicode(config.vlans['fil-new'])

#: VLAN pour les machines des adhérents avec ip pub
VLAN_FIL_PUB = unicode(config.vlans['fil-pub'])

#: VLAN d'accueil
VLAN_ACCUEIL = unicode(config.vlans['accueil'])

#: VLAN d'isolement
VLAN_ISOLEMENT = unicode(config.vlans['isolement'])


## Noms de domaines
#: Nom de domaine associé à chaque type de machine
DOMAINS = {
    TYPE_WIRED : 'crans.org',
    TYPE_WIRELESS : 'wifi.crans.org',
    None : 'nodomain',
}

## Paramètres liés à des durées
#: Période (en secondes) utilisée par le NAS pour notifier le serveur RADIUS
#  des statistiques concernant une session donnée
#  Cette valeur NE DEVRAIT PAS être inférieure à 600 secondes et NE DOIT PAS
#  être inférieure à 60 secondes (RFC 2869)
ACCT_INTERIM_INTERVAL = '14400'  # 4 heures

###############################################################################
##                            Attributs RADIUS                               ##
###############################################################################

## Types de réseaux (NAS-Port-Type)
#: NAS-Port-Type filaires
NAS_PORT_TYPE_WIRED = ['Ethernet']

#: NAS-Port-Type sans fil
NAS_PORT_TYPE_WIRELESS = [
    'Wireless-802.11', 'Wireless-802.16', 'Wireless-802.20', 'Wireless-802.22',
    'Wireless-Other',
]

## Types de paquets de comptabilisation (Acct-Status-Type)
#: Début
ACCT_STATUS_TYPE_START = 'Start'

#: Mise à jour des informations concernant une machine
ACCT_STATUS_TYPE_INTERIM_UPDATE = 'Interim-Update'

#: Fin
ACCT_STATUS_TYPE_STOP = 'Stop'

#: Reboot du NAS, perte de lien avec le client, ...
ACCT_STATUS_TYPE_ACCOUNTING_OFF = 'Accounting-Off'

#: Remise en route du NAS
ACCT_STATUS_TYPE_ACCOUNTING_ON = 'Accounting-On'

################################################################################
##                                  Logging                                   ##
################################################################################

class RadiusdHandler(logging.Handler):
    """Handler de logs pour FreeRADIUS"""

    def emit(self, record):
        """Process un message de log, en convertissant les niveaux"""
        if record.levelno >= logging.ERROR:
            rad_sig = radiusd.L_ERR
        elif record.levelno >= logging.WARNING:
            rad_sig = radiusd.L_WARN
        elif record.levelno >= logging.INFO:
            rad_sig = radiusd.L_INFO
        else:
            rad_sig = radiusd.L_DBG
        logmsg = self.format(record)
        radiusd.radlog(rad_sig, logmsg.encode('utf-8', 'replace'))

logger = logging.getLogger('auth.py')
logger.setLevel(logging.DEBUG if TEST_SERVER else logging.INFO)
formatter = logging.Formatter('%(name)s: [%(levelname)s] %(message)s')
handler = RadiusdHandler()
handler.setFormatter(formatter)
logger.addHandler(handler)

###############################################################################
##                          Fonctions utilitaires                            ##
###############################################################################

def __clean_pairs(pairs, bytestring=False):
    """Prend en entrée une liste de paires de chaînes de caractères et
    renvoie cette liste en garantissant qu'elle ne contienne que des
    bytestrings"""
    if bytestring:
        # On effectue le filtrage des guillemets et espace AVANT
        # la transformation en bytestring, faute de quoi une
        # UnicodeDecodeError est levée car les caractères à chercher
        # sont donnés en Unicode
        return tuple(
            (
                key.strip(' "').encode('utf-8', 'ignore'),
                value.strip(' "').encode('utf-8', 'ignore'),
            ) if not isinstance(value, bytearray) else (
                key.strip(' "').encode('utf-8', 'ignore'),
                bytes(value),
            ) for key, value in pairs
        )
    else:
        return tuple(
            (
                key.decode('utf-8', 'ignore').strip(' "'),
                value.decode('utf-8', 'ignore').strip(' "'),
            )
            for key, value in pairs
        )

def cleaned_request(request):
    """Prend en entrée une liste de paires de chaînes de caractères et renvoie
    un dictionnaire ne contenant que des bytestrings, après avoir retiré les
    guillemets et espaces surnuméraires autour des valeurs renvoyées"""
    if isinstance(request, dict):
        request = request.items()
    else:
        pass

    return dict(__clean_pairs(request))

def cleaned_response(response):
    """Prend en entrée une réponse à un évenement RADIUS et s'assure que les
    chaînes de caractères présentes à l'intérieur ne sont pas des unicodes"""
    if not isinstance(response, (list, tuple)):
        return response
    else:
        retcode = response[0]
        reply = response[1]
        radius_config = response[2]
        return (
            retcode,
            __clean_pairs(reply, bytestring=True),
            __clean_pairs(radius_config, bytestring=True),
        )

def ldap_unpack(ldap_list):
    """Récupère la valeur du premier élément de la liste `ldap_list` renvoyée
    par lc_ldap"""
    try:
        return ldap_list[0].value
    except IndexError:
        return None
    except AttributeError:
        raise

def type_of_machine(request):
    """Renvoie le type de la machine à authentifier"""
    nas_port_type = request.get('NAS-Port-Type', None)
    if nas_port_type in NAS_PORT_TYPE_WIRED:
        return TYPE_WIRED
    elif nas_port_type in NAS_PORT_TYPE_WIRELESS:
        return TYPE_WIRELESS
    else:
        return None

def is_registered(machine):
    """Indique si la machine est déjà enregistrée dans la base ou non"""
    if (machine is None
            or (ldap_unpack(machine['macAddress']) == MAC_PLACEHOLDER)
            or 'rid' not in machine.keys()
            or not machine['rid']):
        return False
    else:
        return True

def dynamic_vlan_enabled_for(m_type):
    """Renvoie True si l'assignation de VLAN est faite de manière dynamique
    pour le type de machine `m_type`"""
    if m_type == TYPE_WIRED and DYN_VLAN_WIRED:
        return True
    elif m_type == TYPE_WIRELESS and DYN_VLAN_WIRELESS:
        return True
    else:
        return False

def prefer_coa(m_type):
    """Renvoie True si le type de machine donné supporte les CoA-Requests, False
    dans le cas contraire"""
    if m_type == TYPE_WIRED and COA_REQUESTS_WIRED:
        return True
    elif m_type == TYPE_WIRELESS and COA_REQUESTS_WIRELESS:
        return True
    else:
        return False

def is_ieee8021X(request):
    """Indique si la requête reçue a été faite en utilisant IEEE 802.1X"""
    return 'EAP-Message' in request.keys()

def ieee8021X_password(machine):
    """Renvoie le mot de passe associé à `machine` utilisé pour
    l'authentification IEEE 802.1X"""
    try:
        return ldap_unpack(machine['ipsec'])
    except KeyError:
        return None

def blacklists_of(owner, machine):
    """Renvoie l'ensemble des types de blacklistes actives sur une machine
    et son propriétaire."""
    return set(bl['type'] for bl in owner.blacklist_actif() + machine.blacklist_actif())

def assign_vlan(machine, owner, m_type):
    """Décide quel VLAN assigner à la machine `machine`"""
    assignment = (VLAN_ACCUEIL, "Unknown machine")
    if machine is None:
        return assignment

    # Assignation forcée de VLAN à des fins de test
    if TEST_SERVER:
        infos = machine.get('info', [])
        vlan_id, reason = None, None
        while len(infos) > 0:
            if ldap_unpack(infos).startswith('force_vlan:'):
                vlan_id, reason = ldap_unpack(infos)[11:].split(',')
                vlan_id = vlan_id.strip()
                assignment = (vlan_id or None, reason)
                infos = []
            else:
                infos = infos[1:]
        if vlan_id is not None:
            return assignment

    rid = ldap_unpack(machine['rid'])
    blacklists = blacklists_of(owner, machine)
    bl_reject = blacklists & BL_REJECT
    bl_isolement = blacklists & BL_ISOLEMENT
    bl_accueil = blacklists & BL_ACCUEIL

    if bl_reject:
        assignment = (None, "blacklisted (%s)" % bl_reject.pop())
    elif bl_isolement:
        assignment = (VLAN_ISOLEMENT, "blacklisted (%s)" % bl_isolement.pop())
    elif bl_accueil:
        assignment = (VLAN_ACCUEIL, "blacklisted (%s)" % bl_accueil.pop())
    elif any(begin <= rid <= end for (begin, end) in config.rid['fil-new']):
        assignment = (VLAN_FIL_NEW, "OK")
    elif any(begin <= rid <= end for (begin, end) in config.rid['wifi-new']):
        assignment = (VLAN_WIFI_NEW, "OK")
    elif any(begin <= rid <= end for (begin, end) in config.rid['fil-pub']):
        assignment = (VLAN_FIL_PUB, "OK")
    elif m_type == TYPE_WIRED:
        assignment = (VLAN_ADHERENTS, "OK")
    else:
        # À ce stade, on a une machine d'un type inconnu, on rejette
        assignment = (None, "Unknown machine type %s" % m_type)

    return assignment

################################################################################
##                                  Décorateurs                               ##
################################################################################

def radius_response(func):
    """Décorateur pour les fonctions d'interfaces avec FreeRADIUS.
    Une telle fonction vérifie les conditions suivantes :
    A)  elle prend un unique argument : un dictionnaire contenant les attributs
        de la requête RADIUS
    B)  elle renvoie soit None, soit un code de retour seul soit un triplet
        dont les composantes sont :
        1) le code de retour (voir radiusd.RLM_MODULE_*)
        2) un tuple de paires (clé, valeur) pour les valeurs de réponse (accès ok
           et autres trucs du genre)
        3) un tuple de paires (clé, valeur) pour les valeurs internes à mettre à
           jour (mot de passe par exemple)"""

    def new_f(self, request, *args, **kwargs):
        # On vérifie dans un premier temps que la connexion à la base LDAP
        # est bien initialisée
        #if (self.ldap is None) or (self.ldap_rw is None):
        #    logger.error("LDAP server is unreachable.")
        #    return radiusd.RLM_MODULE_FAIL

        try:
            request = cleaned_request(request or [])
            logger.debug("Processing RADIUS Request with data %s", request)
            response = func(self, request, *args, **kwargs)
            logger.debug('RADIUS Request processed. Answered: %s', (response,))
            return cleaned_response(response)
        except Exception:
            logger.error(traceback.format_exc())
            logger.error('RADIUS Request data: %s', request)
            return radiusd.RLM_MODULE_FAIL

    return new_f

################################################################################
##                      Gestionnaire d'évènements RADIUS                      ##
################################################################################

class RadiusEventHandler(object):
    """Gestionnaire d'évenements RADIUS"""

    #: Connexion aux annuaires LDAP local et en écriture
    __ldap, __ldap_rw = None, None

    #: Connexion à la base SQL pour la comptabilisation
    __sql_conn, __sql_cursor = None, None

    #: Cache pour les résultats des requêtes LDAP
    __cache = {}

    #: Thread de nettoyage des caches
    __cache_cleaner = None

    #: Variable de contrôle pour les boucles des threads
    __is_running = False

    ## Gestionnaires d'évènements RADIUS

    @radius_response
    def instantiate(self, *_):
        """Lance le module d'authentification"""
        logger.info('[instantiate] Loading Python RADIUS auth backend')
        if TEST_SERVER:
            logger.info('[instantiate] DBG_FREERADIUS is enabled')

        if NO_ACCOUNTING:
            logger.info('[instantiate] Accounting is disabled')

        self.__is_running = True
        self.__cache_cleaner = threading.Thread(target=self.__cleaner)
        logger.info("[instantiate] Starting cleaner thread")
        self.__cache_cleaner.start()
        logger.info("[instantiate] Cleaner thread started")
        return radiusd.RLM_MODULE_OK

    @radius_response
    def authorize(self, request):
        """Récupère les informations de la machine concernée en vue de
        préparer la phase d'authentification"""
        m_type = type_of_machine(request)
        username = request.get('Stripped-User-Name', request.get('User-Name', None))
        mac = request.get('Calling-Station-Id', None)
        if m_type not in [TYPE_WIRED, TYPE_WIRELESS]:
            logger.error('[pre-auth] Invalid machine type %s.', m_type)
            return radiusd.RLM_MODULE_INVALID

        logger.debug('[pre-auth] Supplicant treated as a %s machine', m_type)
        owner, machine, _, _, _ = self.find_match(request, m_type)
        if (owner is None) or (machine is None):
            logger.info('[pre-auth] No match found for %s/%s', mac, username)
            return radiusd.RLM_MODULE_NOTFOUND

        mid = ldap_unpack(machine['mid'])
        if isinstance(owner, OWNER_CRANS):
            logger.error('[pre-auth] Our machine %d tries to authenticate', mid)
            return radiusd.RLM_MODULE_REJECT

        if is_ieee8021X(request):
            # Dans le cas d'une authentification IEEE 802.1X, chaque machine
            # a un mot de passe qui lui est propre
            logger.debug('[pre-auth] Preparing IEEE 802.1X authentication')
            ieee8021X_pwd = ieee8021X_password(machine)
            cleartext_password = (('Cleartext-Password', ieee8021X_pwd),)
            if ieee8021X_pwd is None:
                logger.error(
                    '[pre-auth] Machine %d has no password set. '
                    'Unable to continue with IEEE 802.1X',
                    mid
                )
                return radiusd.RLM_MODULE_REJECT
        else:
            # Dans le cas d'un authentification MAC Auth, le switch utilise
            # l'adresse MAC du client comme mot de passe
            logger.debug('[pre-auth] Preparing CHAP authentication')
            cleartext_password = (('Cleartext-Password', username),)

        return (
            radiusd.RLM_MODULE_UPDATED,
            (),
            cleartext_password,
        )

    @radius_response
    def authenticate(self, request):
        """Authentification de l'utilisateur et de sa machine"""
        m_type = type_of_machine(request)
        _, machine, _, _, _ = self.find_match(request, m_type)
        remote_mac = request.get('Calling-Station-Id', None)
        if machine is None:
            # Il s'agit d'une machine inconnue, on la mettra sur le VLAN accueil
            # en phase de post-authentification
            logger.debug('[auth] Unknown machine %s is trying to connect', remote_mac)
            return radiusd.RLM_MODULE_NOTFOUND

        registered_mac = ldap_unpack(machine['macAddress'])
        if registered_mac == MAC_PLACEHOLDER:
            logger.debug(
                '[auth] Machine %s has no registered MAC address yet.',
                ldap_unpack(machine['mid'])
            )
            return radiusd.RLM_MODULE_NOOP

        if remote_mac is None:
            logger.error('[auth] No remote MAC address provided.')
            return radiusd.RLM_MODULE_INVALID

        if registered_mac.lower() == remote_mac.lower():
            logger.debug('[auth] Registered and remote MAC match (%s)', remote_mac)
            return radiusd.RLM_MODULE_OK
        else:
            logger.error(
                '[auth] Registered and remote MAC mismatch (%s != %s)',
                registered_mac, remote_mac
            )
            return radiusd.RLM_MODULE_REJECT

    @radius_response
    def preacct(self, *_):
        """Pré-traitement des requêtes de comptabilisation"""
        return radiusd.RLM_MODULE_NOOP

    @radius_response
    def accounting(self, request):
        """Traitement des requêtes de comptabilisation"""
        # Certains switches HP semblent ne pas donner de NAS-Port-Type dans les
        # Accounting-Request, on suppose donc en l'absence de cet attribut qu'il
        # s'agit d'une machine filaire
        m_type = type_of_machine(request) or TYPE_WIRED
        acct_status_type = request.get('Acct-Status-Type', None)
        acct_session_id = request.get('Acct-Session-Id', None)
        nas = request.get('NAS-IP-Address', request.get('NAS-IPv6-Address', None))
        mac = request.get('Calling-Station-Id', None)

        if NO_ACCOUNTING:
            # On ne traite pas les requêtes d'accounting
            return radiusd.RLM_MODULE_OK

        if acct_status_type == ACCT_STATUS_TYPE_START:
            # Le NAS indique juste après avoir reçu un Access-Accept le début
            # de la session : on garde une trace du couple MAC/Acct-Session-Id
            logger.debug("[accounting] Recording session ID %s for %s", acct_session_id, mac)
            self.register_session(request)
            return radiusd.RLM_MODULE_OK

        elif acct_status_type == ACCT_STATUS_TYPE_STOP:
            # Le NAS indique que la session est terminée.
            # On peut supprimer les données associées à cette MAC
            logger.debug(
                "[accounting] Removing record for %s (Session ID %s)", mac, acct_session_id
            )
            self.unregister_session(mac=mac)
            return radiusd.RLM_MODULE_OK

        elif acct_status_type in [ACCT_STATUS_TYPE_ACCOUNTING_ON, ACCT_STATUS_TYPE_ACCOUNTING_OFF]:
            # Le NAS a planté/rebooté/perdu le lien avec le client/(autre) et signale
            # que la session est suspendue ou a repris : on supprime toutes
            # les sessions enregistrées liées à ce NAS
            nas_mac = self._mac_of_nas(nas, m_type)
            logger.debug('[accounting] Deleting all sessions managed by NAS %s', nas_mac)
            self.unregister_session(nas=nas_mac)
            return radiusd.RLM_MODULE_OK

        elif not acct_status_type == ACCT_STATUS_TYPE_INTERIM_UPDATE:
            # On accuse récéption des autres paquets, pour que le NAS ne les renvoie pas
            # à intervalles réguilers (RFC 2866)
            logger.debug('[accounting] Received %s packet: ignoring', acct_status_type)
            return radiusd.RLM_MODULE_OK

        elif not USE_COA:
            # La gestion des changements d'autorisation et déconnexions dynamiques
            # est désactivée. On met juste à jour la base de données.
            logger.debug("[accounting] Updating SQL accounting database for MAC %s", mac)
            self.update_session(mac)
            return radiusd.RLM_MODULE_OK

        else:
            # Le NAS nous informe régulièrement sur l'état de la session en cours
            # Ces paquets sont l'occasion de chercher si de nouvelles blacklistes ont
            # été posées, et d'agir en conséquence
            pass

        session_info = self.retrieve_session(mac)
        vlan_id = session_info['vlan'] if session_info else None
        owner, machine, _, _, _ = self.find_match(request, m_type)
        new_vlan_id, why = assign_vlan(machine, owner, m_type)
        if vlan_id is None:
            # Le VLAN n'a pas été renvoyé par le NAS, on a donc
            # dû le recalculer. On met à jour l'enregistrement correspondant
            self.update_session(mac, vlan_id=new_vlan_id)
            return radiusd.RLM_MODULE_OK

        elif unicode(vlan_id) == new_vlan_id:
            # Aucun changement de VLAN n'est nécéssaire
            logger.debug("[accounting] No change for %s", mac)
            logger.debug("[accounting] Updating SQL accounting database")
            self.update_session(mac)
            return radiusd.RLM_MODULE_OK

        elif (new_vlan_id is None
              or not dynamic_vlan_enabled_for(m_type)
              or not prefer_coa(m_type)):
            # Aucun VLAN ne peut être affecté à la machine
            # On demande sa déconnexion
            logger.debug(
                "[accounting] Sending Disconnect-Request for machine %s (%s)", mac, why
            )
            return (
                radiusd.RLM_MODULE_OK,
                (),
                (
                    ("Send-Disconnect-Request", 'Yes'),
                ),
            )

        else:
            # Un changement de VLAN est nécéssaire pour la machine
            # On envoie un CoA avec l'ID du nouveau VLAN
            # XXX: Un changement brutal de VLAN déconnecte les machines,
            #      celles-ci fonctionnant encore avec leur ancienne IP
            logger.debug(
                "[accounting] Sending CoA-Request for machine %s (%s)", mac, why
            )
            return (
                radiusd.RLM_MODULE_OK,
                (),
                (
                    ("Tunnel-Private-Group-ID", new_vlan_id),
                    ("Send-CoA-Request", 'Yes'),
                ),
            )

    @radius_response
    def pre_proxy(self, *_):
        """Pré-traitement des requêtes avant proxification"""
        return radiusd.RLM_MODULE_NOOP

    @radius_response
    def post_proxy(self, *_):
        """Traitement des requêtes après retour de la proxification"""
        return radiusd.RLM_MODULE_NOOP

    @radius_response
    def post_auth(self, request):
        """Traitement de la requête après l'authentification"""
        m_type = type_of_machine(request)
        nas_id = request.get('NAS-IP-Address', request.get('NAS-IPv6-Address', 'Unknown'))
        owner, machine, hosts, plug, room = self.find_match(request, m_type)
        if room and plug:
            mac = request.get('Calling-Station-Id', request.get('Stripped-User-Name', request.get('User-Name', None)))
            logger.info("[post-auth] Mac-address %s auth on room %s/%s" % (mac, room, plug))
        room = ldap_unpack(hosts[0]['chbre']) if hosts else None
        if machine is None:
            # La machine est inconnue : si on fait de l'assignation dynamique,
            # on la place sur le VLAN d'accueil, sinon on la rejette.
            if dynamic_vlan_enabled_for(m_type):
                return (
                    radiusd.RLM_MODULE_OK,
                    (
                        ('Tunnel-Type', 'VLAN'),
                        ('Tunnel-Medium-Type', 'IEEE-802'),
                        ('Tunnel-Private-Group-ID', VLAN_ACCUEIL),
                        ('Class', VLAN_ACCUEIL),
                        ('Acct-Interim-Interval', ACCT_INTERIM_INTERVAL),
                    ),
                    (),
                )
            else:
                return radiusd.RLM_MODULE_REJECT

        # Protection anti-squattage
        # Une machine utilisant une prise autre que celle de son propriétaire
        # doit utiliser celle d'un adhérent à jour de cotisation
        if (any(h.blacklist_actif() and not h.dn == owner.dn for h in hosts)
                and not room in lieux_public()):
            logger.warning(
                "[auth] Anti-squatting protection triggered for room %s and MAC %s. Rejecting.",
                room,
                request.get('Calling-Station-Id', None)
            )
            return (
                radiusd.RLM_MODULE_OK,
                (
                    ('Tunnel-Type', 'VLAN'),
                    ('Tunnel-Medium-Type', 'IEEE-802'),
                    ('Tunnel-Private-Group-ID', VLAN_ACCUEIL),
                    ('Class', VLAN_ACCUEIL),
                    ('Acct-Interim-Interval', ACCT_INTERIM_INTERVAL),
                ),
                (),
            )

        if not is_registered(machine):
            # La machine vient d'être enregistrée, on lui associe la
            # MAC que l'on vient de trouver
            mid = ldap_unpack(machine['mid'])
            mac = request.get('Calling-Station-Id', request.get('User-Name', None))
            logger.info('[post-auth] mid=%s <- %s', mid, mac)
            machine = self.register_new_machine(mac, machine)
        else:
            mid = ldap_unpack(machine['mid'])
            mac = ldap_unpack(machine['macAddress'])

        # On assigne maintenant un réseau à la machine en fonction
        # de ses blacklistes
        logger.debug("[post-auth] Trying to find a VLAN for %s", mac)
        vlan_id, why = assign_vlan(machine, owner, m_type)
        if vlan_id is None:
            # Impossible de savoir où placer l'adhérent, on rejette
            logger.info("[post-auth] %s rejected: %s", mac, why)
            return radiusd.RLM_MODULE_REJECT

        # Les RFC 2865 et 2866 spécifient que l'attribut Class PEUT être
        # renvoyé dans un Access-Accept, auquel cas il DOIT être renvoyé par le NAS
        # dans l'Accounting-Request qui suit. On peut s'en servir pour garder
        # une trace du VLAN affecté à la machine et voir s'il a changé.
        if dynamic_vlan_enabled_for(m_type):
            logger.info(
                "[post-auth] [%s/Dynamic] VLAN %s assigned to %s: %s", nas_id, vlan_id, mac, why
            )
            return (
                radiusd.RLM_MODULE_OK,
                (
                    ('Tunnel-Type', 'VLAN'),
                    ('Tunnel-Medium-Type', 'IEEE-802'),
                    ('Tunnel-Private-Group-ID', vlan_id),
                    ('Class', vlan_id),
                    ('Acct-Interim-Interval', ACCT_INTERIM_INTERVAL),
                ),
                (),
            )
        elif vlan_id not in [VLAN_ADHERENTS, VLAN_WIFI_NEW, VLAN_FIL_NEW, VLAN_FIL_PUB]:
            # Dans le cas de VLANs statique, on ne peut pas placer les adhérents
            # sur des VLANs isolés en cas de blackliste, on est donc contraints
            # de rejeter l'authentification
            logger.info("[post-auth] [%s/Static] Rejected %s: %s", nas_id, mac, why)
            return radiusd.RLM_MODULE_REJECT
        else:
            logger.info("[post-auth] [%s/Static] Accepted %s: %s", nas_id, mac, why)
            return (
                radiusd.RLM_MODULE_OK,
                (
                    ('Class', vlan_id),
                    ('Acct-Interim-Interval', ACCT_INTERIM_INTERVAL),
                ),
                (),
            )

    @radius_response
    def recv_coa(self, *_):
        """Traitement des réponses aux changements d'autorisation"""
        return radiusd.RLM_MODULE_NOOP

    @radius_response
    def send_coa(self, *_):
        """Traitement des requêtes de changement d'autorisation envoyées aux NAS"""
        return radiusd.RLM_MODULE_NOOP

    @radius_response
    def checksimul(self, *_):
        """Traitement de la session associée à la requête en cours"""
        return radiusd.RLM_MODULE_NOOP

    @radius_response
    def detach(self, *_):
        """Décharge le module Python"""
        logger.info('[detach] Unloading Python RADIUS auth backend')
        logger.info('[detach] Terminating cleaner thread')
        self.__is_running = False
        self.__cache_cleaner.join(1.5*CACHE_TIMEOUT)

    # Méthodes internes

    def __connect(self, constructor):
        """Essaie d'établir une connexion avec une base LDAP"""
        count = LDAP_CONN_MAX_TRY
        while count > 0:
            try:
                connection = constructor()
                break
            except SERVER_DOWN:
                count -= 1
        else:
            logger.error('LDAP server did not respond')
            connection = None

        return connection

    @property
    def ldap(self):
        """Renvoie une connexion active à la base LDAP locale"""
        if not self.__ldap:
            logger.debug('Connecting to Local LDAP Server')
            self.__ldap = self.__connect(shortcuts.lc_ldap_local)
            self.__ldap.mode = 'ro'
        return self.__ldap

    @property
    def ldap_rw(self):
        return None
    #    """Renvoie une connexion à la base LDAP maîtresse"""
    #    if not self.__ldap_rw:
    #        logger.debug('Connecting to Master LDAP Server')
    #        self.__ldap_rw = self.__connect(shortcuts.lc_ldap_admin)
    #        self.__ldap_rw.mode = 'rw'
    #    return self.__ldap_rw

    @property
    def sql(self):
        """Renvoie un curseur sur la base de données SQL"""
        if not self.__sql_conn or self.__sql_conn.closed:
            logger.debug("Connecting to SQL database")
            self.__sql_conn = psycopg2.connect(**SQL_PARAMS)
            self.__sql_conn.set_session(autocommit=True)
        if not self.__sql_cursor or self.__sql_cursor.closed:
            self.__sql_cursor = self.__sql_conn.cursor()
        return self.__sql_cursor

    def register_session(self, request):
        """Enregistre une nouvelle session dans la base SQL"""
        p_mac = request.get('Calling-Station-Id', None)
        p_type = type_of_machine(request) or TYPE_WIRED
        p_session_id = request.get('Acct-Session-Id', None)
        p_nas = request.get('NAS-IP-Address', request.get('NAS-IPv6-Address', None))
        p_port = request.get('NAS-Port', None)
        p_ssid = request.get('Called-Station-SSID', None)
        p_bss = request.get('Called-Station-Id', None)
        p_vlan = request.get('Class', '')

        params = {
            'mac' : p_mac,
            'type' : p_type,
            'session_id' : p_session_id,
            'nas' : self._mac_of_nas(p_nas, p_type),
            'port' : p_port,
            'ssid' : p_ssid,
            'bss' : p_bss,
            'vlan' : p_vlan.replace('0x', '').decode('hex').decode('utf-8') or None,
        }

        logger.debug("Registering new session in SQL database with parameters %s", params)
        self.sql.execute(
            "INSERT INTO accounting (mac, type, session_id, nas, port, ssid, bss, vlan) VALUES "
            "(%(mac)s, %(type)s, %(session_id)s, %(nas)s, %(port)s, %(ssid)s, %(bss)s, %(vlan)s) "
            "ON CONFLICT (mac) "
            "DO UPDATE SET (type, session_id, nas, port, ssid, bss, last_update) = "
            "(%(type)s, %(session_id)s, %(nas)s, %(port)s, %(ssid)s, %(bss)s, now());",
            params
        )

    def update_session(self, mac, vlan_id=None):
        """Met à jour les données de la session associée à la MAC donnée.
        Si vlan_id est donné, mets à jour le champ 'vlan' en même temps"""
        logger.debug("Updating session for MAC %s", mac)
        if vlan_id is None:
            self.sql.execute(
                "UPDATE accounting SET last_update = DEFAULT WHERE mac = %s;",
                (mac,)
            )
        else:
            self.sql.execute(
                "UPDATE accounting SET (last_update, vlan) = (DEFAULT, %s) WHERE mac = %s;",
                (vlan_id, mac,)
            )

    def unregister_session(self, mac=None, nas=None):
        """Supprime des sessions enregistrées. Si `mac` est spécifié, supprime la
        session associée à cette MAC. Si `nas` est spécifié, supprime toutes les
        sessions gérées par ce NAS. `nas` prévaut sur `mac`"""
        if nas:
            logger.debug('Deleting all sessions managed by NAS %s in SQL database', nas)
            self.sql.execute("DELETE FROM accounting WHERE nas = %s;", (nas,))
        elif mac:
            logger.debug("Deleting old session for MAC address %s in SQL database", mac)
            self.sql.execute("DELETE FROM accounting WHERE mac = %s;", (mac,))
        else:
            logger.error("Unable to delete session without MAC or NAS address.")

    def retrieve_session(self, mac):
        """Renvoie les données de la session associée à la MAC donnée.
        Si aucune donnée n'est disponible, renvoie None"""
        logger.debug("SQL lookup for MAC %s", mac)
        self.sql.execute("SELECT * FROM accounting WHERE mac = %s;", (mac,))
        return self.sql.fetchone()

    def register_new_machine(self, mac, machine):
        """Enregistre la MAC d'une nouvelle machine"""
        if self.ldap_rw is None:
            logger.error("Unable to reach master LDAP server")
            return None

        machine_rw = self.ldap_rw.search(dn=machine.dn, scope=SCOPE_BASE, mode='rw')[0]
        with machine_rw:
            machine_rw['macAddress'] = mac
            machine_rw.renew_rid()
            machine_rw.validate_changes()
            machine_rw.history_gen()
            machine_rw.save()

        return machine_rw

    def find_match(self, request, m_type):
        """Renvoie un couple (propriétaire, machine) à partir des
        données fournies, en utilisant le cache du module pour
        économiser les requêtes à la base LDAP"""
        mac = request.get('Calling-Station-Id', None)
        result = self.__find_match(request, m_type)
        self.__cache.update({(mac, m_type) : result + (time.time(),)})
        return result

    def _mac_of_nas(self, nas_ip, m_type):
        """Renvoie l'adresse MAC assoiciée à l'IP du NAS donné et du type donné"""
        if nas_ip is None:
            return None
        else:
            params = {
                'objectClass' : 'switchCrans' if m_type == TYPE_WIRED else 'borneWifi',
                'af' : 'ip6HostNumber' if ':' in nas_ip else 'ipHostNumber',
                'address' : crans_utils.escape(nas_ip),
            }
            nas = self.ldap.search(LDAP_FILTER % params)
            return ldap_unpack(nas[0]['macAddress']) if len(nas) > 0 else None

    def _plug_info(self, nas_id, port):
        """Renvoie la chambre associé à un NAS et une prise donnée d'une part
        et l'ensemble des adhérents y résidant d'autre part"""
        building = nas_id[3]
        plug = "%d%02d" % (int(nas_id[5:]), int(port))
        logger.debug('Trying to find a match by room (plug %s)', plug)
        room = reverse(building, plug)
        if room:
            room = room[0]
            logger.debug('Found room %s%s', building.upper(), crans_utils.escape(room))
            return building.lower() + plug, building.upper()+room, self.ldap.search('(&(chbre=%s%s)(!(chbre=EXT)))' % (building, room))
        else:
            logger.debug('No room for plug %s%s', building, plug)
            return None, None, []

    def __find_match(self, request, m_type):
        """Renvoie un triplet (propriétaire, machine, hébergeurs) à partir
        des données fournies.
        Peut renvoyer None pour n'importe laquelle des deux composantes"""
        mac = request.get('Calling-Station-Id', None)
        username = request.get('Stripped-User-Name', request.get('User-Name', None))
        nas_id = request.get('NAS-Identifier', None)
        port = request.get('NAS-Port', None)
        params = {}

        if self.ldap is None:
            logger.error('Unable to reach local LDAP server')
            return (None, None, [], None, None)

        if mac is None or username is None:
            logger.error('No User-Name or Calling-Station-Id attribute')
            return (None, None, [], None, None)

        if m_type == TYPE_WIRED:
            params.update({'objectClass' : 'machineFixe'})
        elif m_type == TYPE_WIRELESS:
            params.update({'objectClass' : 'machineWifi'})
        else:
            # Type de machine non géré
            return (None, None, [], None, None)

        if is_ieee8021X(request):
            # Si la requête a été effectuée en utilisant IEEE 802.1X, on cherche
            # la machine grâce à son User-Name.
            # Dans ce cas, la liste des hébergeurs n'est pas nécéssaire, car
            # il n'y a pas de spoofing a priori
            logger.debug('Trying to find a match by hostname (%s)', username)
            params.update({
                'af' : 'host',
                'address' : "%s.%s" % (crans_utils.escape(username), DOMAINS[m_type]),
            })
            m_list = self.ldap.search(LDAP_FILTER % params)
            hosts = []
            room, plug = None, None

        else:
            # Si la requête n'utilise pas IEEE 802.1X, on cherche la machine via sa MAC.
            # Dans ce cas, s'il s'agit d'une machine filaire, on renvoie la liste
            # des hébergeurs de la chambre en question afin d'effectuer une
            # vérification anti-spoofing
            logger.debug('Trying to find a match by MAC (%s)', mac)
            params.update({'af' : 'macAddress', 'address' : crans_utils.escape(mac)})
            m_list = self.ldap.search(LDAP_FILTER % params)
            plug, room, hosts = self._plug_info(nas_id, port) if m_type == TYPE_WIRED else (None, None, [])

        if not m_list and not is_ieee8021X(request) and m_type == TYPE_WIRED and nas_id and port:
            # La recherche n'a rien donné, IEEE 802.1X n'est pas utilisé
            # mais on est en présence d'une machine filaire : on essaie de voir si
            # le locataire de la chambre a une nouvelle machine
            params.update({'af' : 'macAddress', 'address' : MAC_PLACEHOLDER})
            plug, room, hosts = self._plug_info(nas_id, port)
            owner = hosts[0] if hosts else None
            m_list = self.ldap.search(LDAP_FILTER % params, dn=owner.dn, scope=1) if owner else []

        if len(m_list) > 1:
            logger.warning(
                'Multiple wired machines registered with address %s : Using first match.',
                mac
            )

        if m_list:
            # On a une machine, on peut en déduire le propriétaire présumé
            machine = m_list[0]
            logger.debug('Match found (machine %s)', ldap_unpack(machine['mid']))
            owner = machine.proprio()
            return (owner, machine, hosts, plug, room)
        else:
            logger.debug("No match for %s", mac)
            return (None, None, [], None, None)

    def __cleaner(self):
        """Retire périodiquement les entrées inutiles dans les caches"""
        logger.info("Started cache cleaner thread")
        while self.__is_running:
            now = time.time()
            for (mac, mtype), (_, _, _, timestamp) in self.__cache.items():
                if now - timestamp > CACHE_TIMEOUT:
                    logger.debug("Removing %s from entry cache", mac)
                    del self.__cache[(mac, mtype)]
            time.sleep(CACHE_TIMEOUT)
        logger.info("Stopped cache cleaner thread")


################################################################################
##                  Instantiation d'un gestionnaire d'évènements              ##
################################################################################

radius_events = RadiusEventHandler()

instantiate = radius_events.instantiate
authorize = radius_events.authorize
authenticate = radius_events.authenticate
preacct = radius_events.preacct
accounting = radius_events.accounting
checksimul = radius_events.checksimul
pre_proxy = radius_events.pre_proxy
post_proxy = radius_events.post_proxy
post_auth = radius_events.post_auth
send_coa = radius_events.send_coa
recv_coa = radius_events.recv_coa
detach = radius_events.detach

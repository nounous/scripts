-- Schéma de la table utilisée pour l'accounting RADIUS

CREATE TABLE IF NOT EXISTS accounting (
    -- Adresse MAC du client
    "mac" macaddr PRIMARY KEY,
    -- Type de machine
    "type" varchar NOT NULL,
    -- Identifiant de session unique
    "session_id" varchar UNIQUE NOT NULL,
    -- Date de dernière mise à jour de l'entrée
    "last_update" timestamp with time zone NOT NULL DEFAULT now(),
    -- NAS ayant fourni le service au client
    "nas" macaddr NOT NULL,
    -- Port attribué au client par la NAS
    "port" integer NOT NULL,
    -- SSID auquel a accédé le client, le cas échéant
    "ssid" varchar DEFAULT NULL,
    -- BSS auquel le client a accédé, le cas échéant
    "bss" macaddr,
    -- VLAN sur lequel a été placé le client
    "vlan" integer,

    CHECK ("type" IN ('Wired', 'Wireless')),
    CHECK (("type" = 'Wired' AND "ssid" IS NULL) OR ("type" = 'Wireless' AND "ssid" IS NOT NULL)),
    CHECK ("vlan" IS NULL OR (0 <= "vlan" AND "vlan" <= 4094))
);

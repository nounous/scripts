#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

# Copyright (C) Stéphane Glondu, Alexandre Bos, Michel Blockelet
# Remanié en 2015 par Gabriel Détraz
# Licence : GPLv2

u"""Ce script permet au secrétaire de repérer plus facilement les membres
actifs qui n'ont pas signé la charte du même nom.

Utilisation :
%(prog)s {liste|modif|spam} [--debug <adresse>]

L'unique option est :
  --debug <adresse>     envoyer tous les mails à l'<adresse> indiquée, plutôt
                        qu'aux vrais destinataires
Les commandes sont :
  * liste               énumérer les membres n'ayant pas signé la charte
  * modif               modifier les membres actifs n'ayant pas signé la charte
  * spam                envoie des mails de rappel pour les chartes
"""


import sys, os, re

# Fonctions d'affichage
from gestion.affich_tools import coul, tableau, prompt, cprint

from utils.sendmail import actually_sendmail
from gestion import mail

# Importation de la base de données
from lc_ldap import shortcuts

# Lors des tests, on m'envoie tous les mails !
from socket import gethostname
debug = False

# Conn à la db
ldap = shortcuts.lc_ldap_admin()

if __name__ == '__main__':
    if len(sys.argv) > 3 and sys.argv[-2] == '--debug':
        debug = sys.argv[-1]
        sys.argv.pop()
        sys.argv.pop()

if debug:
    cprint(u'Mode debug, tous les mails seront envoyés à %s.' % debug)


def _controle_interactif_adherents(liste):
    """
    Contrôle interactif des adhérents de la liste.
    Retourne (nb_OK, nb_pas_OK).
    """

    restant = len(liste)
    if restant == 0:
        return 0, 0

    cprint(u'\nContrôle des membre actifs' , 'cyan')
    cprint(u"Pour chaque entrée, il faut taper 'o' ou 'n' (défaut=n).")
    cprint(u"Une autre réponse entraîne l'interruption du processus.")
    cprint(u"Le format est [nb_restant] Nom, Prénom (aid).")
    cprint(u"")

    nb = 0
    for a in liste:
        valeur = a['charteMA']
        if valeur:
            suggestion = 'o'
        else:
            suggestion = 'n'
        ok = prompt(u'[%3d] %s, %s (%s) ?'
                    % (restant, a['nom'][0], a['prenom'][0], a['aid'][0]), suggestion, '').lower()
        restant -= 1
        if ok == 'o':
            nb += 1
            if a['charteMA'] != True :
                modifiable = ldap.search(u'aid=%s' % a['aid'][0], mode='rw')
                try:
                    with modifiable[0] as adh:
                        adh['charteMA']=True
                        adh.history_gen()
                        adh.save()
                        cprint(u'Controle OK')
                except:
                    cprint(u'Adhérent %s locké, réessayer plus tard' % a['nom'][0], 'rouge')
        elif ok != 'n':
            cprint(u'Arrêt du contrôle des membres actifs', 'rouge')
            break

    return nb, len(liste)-nb


def liste_charte_nok():
    """Retourne la liste des membres actifs qui n'ont pas signé la charte."""
    liste_actifs = ldap.search(u'droits=*')
    liste_nok = []
    for adh in liste_actifs:
        if (len([droit for droit in adh['droits']
                if droit not in ['Multimachines',]]) > 0
                and not adh['charteMA']):
            liste_nok.append(adh)
    return liste_nok

def controle_interactif():
    """
    Procédure interactive de contrôle des chartes de membres actifs.
    """
    todo_list = liste_charte_nok()

    # Tri de la liste des adhérents selon nom, prénom
    # Ça peut se faire plus facilement en Python 2.4 avec l'argument key
    todo_list.sort(lambda x, y: cmp((x['nom'][0], x['prenom'][0]), (y['nom'][0], y['prenom'][0])))

    # Zou !
    ok, nok = _controle_interactif_adherents(todo_list)

    cprint(u'\nRécapitulatif des nouveaux contrôles :', 'violet')
    liste = [[u'membres actifs', str(ok), str(nok)]]
    cprint(tableau(liste,
                  titre = [u'Catégorie', u'OK', u'pas OK'],
                  largeur = [15, 10, 10]))

def spammer():
    # On envoie un mail à chacun des membres actifs qui n'ont pas donné le papier
    todo_list = liste_charte_nok()

    if todo_list:
        print "Envoi des mails de rappel pour les chartes des membres actifs"

        for adh in todo_list:
            to = adh['mail'][0]
            print to
            if not debug:
                From = u"respbats@crans.org"
                data=mail.generate('missing_charte_MA', {
                    'To': unicode(to),
                    'From': From,
                })
                actually_sendmail(u'respbats@crans.org', (unicode(to),), data)

def __usage(message=None):
    """ Comment ça marche ? """
    cprint(__doc__ % { 'prog': sys.argv[0] })
    if message:
        cprint(message)
    sys.exit(1)


if __name__ == '__main__' :
    # Utilisation depuis la ligne de commande
    if len(sys.argv) <= 1:
        __usage()
    elif sys.argv[1] == 'liste':
        if len(sys.argv) != 2:
            __usage(u'Mauvaise utilisation de liste')
        print "Liste des membres actifs n'ayant pas signé la charte :"
        for adh in liste_charte_nok():
            print unicode(adh['prenom'][0]) + u" " + unicode(adh['nom'][0])
    elif sys.argv[1] == 'modif':
        if len(sys.argv) != 2:
            __usage(u'Mauvaise utilisation de modif')
        controle_interactif()
    elif sys.argv[1] == 'spam':
        if len(sys.argv) != 2:
            __usage(u'Mauvaise utilisation de spam')
        spammer()
    else:
        __usage(u'Commande inconnue : %s' % sys.argv[1])

    sys.exit(0)


# pydoc n'aime pas l'unicode :-(

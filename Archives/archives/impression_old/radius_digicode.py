#! /usr/bin/env python
# -*- coding: iso-8859-15 -*-

"""
Script de test du code de la porte du local de l'imprimante
"""


""" !!! Ce script n'est pas en production !!!
Le serveur du digicode fonctionne en standalone via le fichier
/usr/scripts/impression/digicode_server.py
"""

import os, sys
from syslog import openlog, syslog

def identification(password):
    # test un code entr� sur la porte :
    #   doit retourner 0 si le code n'est pas bon
    #                  1 pour ouvrir la porte
    
    #codes en statique pour les tests
    #Dans l'avenir tout sera sous forme de fichiers
    #si le fichier existe le code est bon (evite les problemes par rapport au fait de
    # stocker les codes dans un fichier
    try:
        user = file("/var/impression/codes/%s" % password).readline().strip()
    except:
        user = ""
    if password in os.listdir('/var/impression/codes'):
        if password in os.listdir('/var/impression/codes'):
            os.remove("/var/impression/codes/%s" %password)
        return user, True
    else:
        return "", False

if __name__ == '__main__' :
    openlog("digicode")
    # on r�cup�re le code dans les variables d'environement
    try:
        code = os.getenv('USER_PASSWORD', '').replace('"', '')
    except:
        code = ''

    # test de l'authentification
    user, ok = identification(code)
    if ok:
        # c'est bon
        syslog("code %s [%s] correct" % (code, user))
        sys.exit(0)
    else:
        # c'est pas bon
        syslog("code %s incorrect" % code)
        sys.exit(1)

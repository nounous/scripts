#!/usr/bin/python 
#   -*- python -*-
import sys,time,os,pickle,string,popen2,random

FORKLEN = 5
TIMEOUT = 15 # une machine a 15 secondes pour recevoir un popup !

def do_bomb(Machines):
    time.sleep(random.random())
    if len(Machines) < FORKLEN:
        for mach in Machines:
            begin = time.time()
            print "r�-exp�dition SMBpopup � ",mach
            fd = popen2.Popen3("/etc/CRANS/code/SMBMessage %s %s %s" % (from_whom,mach,msgfile))
            while 1:
                time.sleep(1)
                if fd.poll() != -1: break # fini ! (cool) 
                if (time.time() - begin) > TIMEOUT: break
            del fd
    else:
        Half = len(Machines) / 2
        
        pid = os.fork()
        if pid == 0:
            # child
            do_bomb(Machines[:Half])
        else:
            # parent
            do_bomb(Machines[Half:])
            try:
                os.waitpid(pid,0)
            except:
                pass
    # � ce stade, on a �limin� toute r�cursion, tant pile que process...
    # croisons les doigts !!!
                
        
if len(sys.argv) < 4:
    sys.stderr.write('Attend 3 arguments !\n')
    sys.exit(1)

Zone = pickle.load(open(CFG_READ_DIR+"Zone.db","r"))

from_whom = sys.argv[1]
from_where = sys.argv[2]
msgfile = sys.argv[3]+"-red"
message = open(sys.argv[3],"r").readlines()

message.insert(0,("Message de %s (@%s) � ALL : \n-----\n" % (from_whom,from_where)))
open(msgfile,"w").writelines(message)


# pour gagner du temps, on n'envoie le message qu'aux machines qui sont
# (probablement) vivantes. Pour gagner encore plus de temps, on fait
# confiance au cache ARP (15 minutes)

Alive = []
arp = os.popen("arp -a","r")
while 1:
    s = arp.readline()
    if not s: break
    mach = string.split(string.split(s)[0],'.')[0]
    if mach[:5] == 'zamok': continue # �vitons les boucles infininies...
    if Zone.has_key(mach): Alive.append(mach)
print Alive

arp.close()

#maintenant, au boulot...
do_bomb(Alive)
unlink(msgfile)
unlink(sys.argv[3])





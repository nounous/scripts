#! /bin/bash
###############################################################################
# Plus utilis� ! Cf https://wiki.crans.org/CransTechnique/CransPasswords
# cranspasswords : Outil de (d�)chiffrage de mots de passe
###############################################################################
#
# Copyright (C) 2006 Etienne Chov�
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
###############################################################################
# variables g�n�rales

SRV=vert.adm.crans.org
REP=/root
TMP=`if which tempfile &> /dev/null; then tempfile -m 600; else mktemp; fi`
#trap "rm -f ${TMP} ${TMP}.asc" EXIT

##############################################################################
# parsage des arguments

if [[ -z "$1" ]] ; then
  ACTION="--view"
  CHOIX=""
elif echo $1 | grep -q '^--' ; then
  ACTION=$1
  CHOIX=$2
else
  ACTION='--view'
  CHOIX=$1
fi

##############################################################################
# fonctions utiles

function liste () {
  # donne la liste des mot de passe
  echo -e "\033[1;34mListe des mot de passe disponibles :\033[1;0m" >&2
  ssh ${SRV} "ls ${REP} | grep -e '.asc$' | sed 's/\.asc$//g' | sed 's/^/    /g'" 2> /dev/null
}

function choix () {
  # choix du fichier
  if [[ -z "$CHOIX" ]] ; then
    liste
    echo
    echo -e -n "\033[1;34mChoix : \033[1;0m"
    read CHOIX
  else
    echo -e "\033[1;34mChoix :\033[1;0m $CHOIX"
  fi
  echo
}

function dechiffre () {
  # r�cup�re le mot de passe $CHOIX, le d�chiffre et le colle dans $TMP
  # ${TMP} sera vide si les pass n'existe pas
  echo -e "\033[1;34mConnection � ${SRV}\033[1;0m"
  ssh ${SRV} -t "sudo -p 'Mot de passe sudo : ' echo -n" 2> /dev/null
  echo
  echo -e "\033[1;34mR�cup�ration du fichier et d�chifrage\033[1;0m"  
  ssh ${SRV} "sudo cat ${REP}/$CHOIX.asc" | gpg --decrypt > ${TMP}
  echo
}

function chiffre () {
  # chiffre les fichier $TMP et les colle dans le fichier $CHOIX
  echo -e "\033[1;34mChiffrage et envoi sur ${SRV}\033[1;0m"
  if [[ ! -s ${TMP} ]] ; then
    echo -e "\033[1;31mFichier vide, abandon...\033[1;0m"
    return
  fi
  full_rec=(
      051ADBC8  #   maioli@crans.org
      0BF3708E  #   xavier.lagorce@crans.org       
      2127F85A  #   jdimino@dptinfo.ens-cachan.fr  
      41C2B76B  #   samir@crans.org
      49881AD3  #   Stephane.Glondu@crans.org
      5BEC9A2F  #   olivier.iffrig@crans.org       
      66475AAF  #   nicolas.dandrimont@crans.org   
      6E1C820B  #   daniel.stan@crans.org
      7D980513  #   parret-freaud@crans.org
      8E96ACDA  #   adg@crans.org                  
      A8A4F83E  #   bruot@crans.org                
      AF087A52  #   blockelet@crans.org            
      E0DCF376  #   olivier.huber@crans.org        
      F2530FCE  #   pierre.chambart@crans.org      
  )
  count=${#full_rec[@]}
  RECIPIENTS=$(index=0; while [ "$index" -lt "$count" ]; do echo -n "-r "; echo -n 0x${full_rec[$index]}; echo -n " "; ((index++)); done)
  yes | gpg --armor --encrypt ${RECIPIENTS} ${TMP}
  
  # 3603EFD9 alexandre@alexandre-bos.fr     
  # 4EF9E1D1 xavier.pessoles@crans.org
  # 6D1DF0FA pauget@crans.org
  # 81DDB065 gregoire.detrez@crans.org
  # 98E76332 etienne.chove@crans.org
  # BD156CC4 dubost@crans.org
  # C5C4ACC0 vincent.thomas@crans.org       
  # D6ADFD5A carlos@crans.org               
  # F22A794E Vincent.Bernat@crans.org
  # FBFA4781 Nicolas.Stransky@crans.org
  [[ "$#" == "0" ]] && ssh ${SRV} "sudo sh -c \"cat > ${REP}/${CHOIX}.asc\"" < ${TMP}.asc || echo -e "\033[1;31mErreur, abandon...\033[1;0m"
}  

function droits () {
  # modifie les droits du fichier chiffr�
  echo -e "\033[1;34mChangement des droits sur le fichier\033[1;0m"
  ssh ${SRV} "sudo chmod 600 ${REP}/${CHOIX}.asc"
}

function edite () {
  # �dite le fichier temporaire
  $EDITOR ${TMP}
}

function voir () {
  # affiche le contenu du fichier temporaire
  echo -e "\033[1;34mAffichage du fichier\033[1;0m"  
  less ${TMP}
}

function supprime () {
  # supprime un fichier
  echo -e "\033[1;34mSuppression du fichier chiffr� : \033[1;0m${CHOIX}"  
  CONFIRM="Oui, je suis certain."
  echo -n "Tapez \"$CONFIRM\" : "
  read CONFIRM2
  echo
  if [[ "${CONFIRM}" == "${CONFIRM2}" ]] ; then
    echo -e "\033[1;34mConnexion � ${SRV}\033[1;0m"
    ssh ${SRV} -t "sudo -p 'Mot de passe sudo : ' echo -n" 2> /dev/null
    echo
    echo -e "\033[1;34mSuppression du fichier\033[1;0m"
    ssh ${SRV} "sudo rm -f ${REP}/$CHOIX.asc 2> /dev/null"
    echo -e "\033[1;32mFichier supprim�...\033[1;0m"    
  else
    echo -e "\033[1;31mAbandon...\033[1;0m"
  fi
}

##############################################################################

if [[ "$ACTION" == "--re-encrypt-all" ]] ; then
  for CHOIX in `liste 2> /dev/null` ; do
    echo -e "\033[1;33mTraitement de : \033[1;0m${CHOIX}"
    echo
    dechiffre
    chiffre
    droits
    rm -f ${TMP} ${TMP}.asc
    echo
  done
elif [[ "$ACTION" == "--edit" ]] ; then
  choix
  dechiffre
  edite
  chiffre
  droits
elif [[ "$ACTION" == "--remove" ]] ; then
  choix
  supprime
elif [[ "$ACTION" == "--view" ]] ; then
  choix
  dechiffre
  voir
elif [[ "$ACTION" == "--list" ]] ; then
  liste 2> /dev/null | cut -b 5-
else
  echo "Usage : cranspasswords [--re-encrypt-all|--edit|--remove|--view|--list] [FICHIER]"
fi

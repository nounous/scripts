#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

import subprocess

from impression_canon import DECOUVERT_AUTHORISE, DICT_AGRAFAGE, AVAIL_AGRAFES,\
    DICT_PAPIER, FichierInvalide, SoldeInsuffisant, PrintError, SettingsError
import impression_canon

class impression(impression_canon.impression):
    """ Extension de la classe d'impression de l'imprimante canon,
        la seule méthode ayant un comportement différent est _exec_imprime
        qui fait appel à canon_wrapper.py au lieu de lp"""

    def _exec_imprime(self):
        """ Envoie l'impression à l'imprimante avec les parametres actuels """

        opt=[]
        if (self._adh != None):
            self.log.info('Impression(%d) [%s] : %s' % (self._jid, self._adh, self._fichier))
        else:
            self.log.info("Impression(%d) : %s" % (self._jid, self._fichier))

        # Pour spécifier un jobname de la forme jid:adh:nom_du_fichier
        jobname = '%d:%s:%s' % (self._jid, self._adh, self._fichier.split('/')[-1].replace("\"","\\\""))
        opt += ['--jobname', jobname]

        opt += ['--copies', str(self._settings['copies'])]

        opt += ['--page-size', self._settings['papier']]

        opt += ['--colors', 'yes' if self._settings['couleur'] else 'no']

        if self._settings['livret']:
            opt += ['--duplex-type', 'book']
        else:   # Not livret
            if self._settings['recto_verso']:
                opt += ['--duplex-type', ('long' if self._settings['portrait']
                                     else 'short')+'-edge']
            else:
                opt += ['--duplex-type', 'one-sided']
            opt += ['--staple-position', self._settings['agrafage']]
        opt.append(self._fichier)
        self.log.info("Impression(%d) : %s" % (self._jid, " ".join(opt)))
        subprocess.Popen(['/usr/scripts/impression/canon_wrapper.py'] + opt)

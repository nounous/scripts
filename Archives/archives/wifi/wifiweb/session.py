#! /usr/bin/env python
# -*- coding: iso-8859-15 -*-

import os, random
from time import time

class session :
    
    def __init__ (self, sid = None) :
        """
        Si sid est fournit, on regarde si la session est valide ;
        on soul�ve un exeption si il y a un probl�me
        
        Si sid n'est pas fourni un cr�� une nouvelle session
        """
        self.save = True
        
        if sid :
            
            # on v�rifie la validit�
            if not os.access( self._sess_file(sid), os.W_OK ) :
                raise ValueError, 'Session inconnue'
            
            # on exporte le sid
            self.sid = sid
            
            # on lit les donn�es
            self.data = {}
            f = open(self._sess_file(sid))
            for i in f.readlines() :
                if not i.strip() :
                    continue
                key = i.split(' ')[0]
                value = ' '.join(i.split(' ')[1:]).strip()
                self.data[key] = value
            
            if int(self.data['perime']) < int(time()) :
                self.destroy()
                raise ValueError, 'Session p�rim�e'
            
        else :
            
            # on cr�� un nouveau sid
            self.data = {}
            ok = False
            while not ok :
                sid = ''.join( [ random.choice('abcdefghijklmnopqrstuvwxyz0123456789') for i in range(0,30) ])
                # est ce que la session existe ?
                if not os.path.exists(self._sess_file(sid)) :
                    # on cr�� un nouveau fichier avec un timeout de 60
                    f = os.open(self._sess_file(sid), os.O_WRONLY + os.O_CREAT , 0600)
                    f
                    # on valide
                    ok = True
            self.sid = sid
            
            # on initialise les donn�es
            self.data = {'timeout' : '600'}
        
    def _sess_file (self, sid = None) :
        """ Retourne le nom du fichier correspondant � la session """
        if not sid :
            sid = self.sid
        return '/tmp/pysession-%s' % sid
    
    def __del__ (self) :
        """ On enregsitre la session � la destruction de l'instance """
        
        if self.save :
            # epok de peromption
            self.data['perime'] = str(int(time() + int(self.data['timeout'])))

            f = open(self._sess_file(), 'w')
            for k in self.data.keys() :
                f.write( '%s %s\n' % (k,self.data[k]) )
            f.close()
    
    def destroy(self) :
        """ Supprime la session """
        self.save = False
        os.remove( self._sess_file() )

# on supprime toutes les vieilles sessions
for file in os.listdir('/tmp') :
    if file[0:10] == 'pysession-' :
        #print file[10:]
        try :
            s = session(file[10:])
            s.save = False
        except :
            continue

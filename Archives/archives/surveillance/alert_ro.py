#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-
"""
Script qui alerte dès qu'une partition se monte en ro

Gabriel Détraz
Licence GPL2
"""

from __future__ import print_function

import json
import collections
import re
import subprocess
from socket import gethostname

import pika

from lc_ldap import shortcuts
from gestion import secrets_new as secrets

## Quelques objets utiles ...

# Les partitions
Partition = collections.namedtuple('Partition', ['dev', 'mountpoint', 'type', 'flags'])
mount_re = re.compile('(?P<dev>.+) on (?P<mountpoint>.+) type (?P<type>.+) [(](?P<flags>.+)[)]')

# Les données qui viennent du LDAP
ldap = shortcuts.lc_ldap_readonly()
nounous_actif = ldap.search(u'(&(droits=nounou)(droits=cableur))',mode='rw')
list_num = [unicode(noun['tel'][0]) for noun in nounous_actif]

# La liste des partitions problématiques
buggy_fs = []

## ... quelques paramètres ...

# Où qu'on est ?
HOSTNAME = gethostname()

# Contrôle des partitions à surveiller
EXCLUDE_FS_TYPES = ['tmpfs', 'devtmpfs']
EXCLUDE_MOUNTPOINTS = []

# AMQP
QUEUE_NAME = 'SMS'

## ... Et on y va !

# Récupération des partitions
raw_partoches = map(mount_re.match, subprocess.check_output(['mount']).split('\n'))
partoches = [Partition(**raw_p.groupdict()) for raw_p in raw_partoches if raw_p is not None]

# Vérification des options de montage
for part in partoches:
    if part.type in EXCLUDE_FS_TYPES:
        # Ce type de filesystem ne nous intéresse pas
        continue
    if part.mountpoint in EXCLUDE_MOUNTPOINTS:
        # Ce point de montage ne nous intéresse pas
        continue

    if 'ro' in part.flags.split(','):
        # On surveille les partitions remontées en RO
        explanation = "Partition remontée en RO : %(dev)s (sur %(mountpoint)s)" % part._asdict()
        buggy_fs.append(explanation)
        print(explanation)

# Pour les nounous qui le souhaitent, on envoie une alerte par SMS
if buggy_fs:
    CREDS = pika.credentials.PlainCredentials('sms', secrets.get('rabbitmq_sms'), True)

    PARAMS = pika.ConnectionParameters(host='rabbitmq.crans.org',
        port=5671, credentials=CREDS, ssl=True)

    rabbit_c = pika.BlockingConnection(PARAMS)
    ch = rabbit_c.channel()

    ch.queue_declare(QUEUE_NAME)

    for num in list_num:
        SMS = {
            'numero': num,
            'sms': 'Les partitions suivantes sur %s présentent des bogues:\n%s' % (HOSTNAME, '\n'.join(buggy_fs))
        }

        ch.basic_publish(exchange='', routing_key=QUEUE_NAME,
            body=json.dumps(SMS))

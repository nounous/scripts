#! /usr/bin/env python
# -*- mode: python; coding: utf-8 -*-


###########################
# Import des commmandes : #
###########################

import commands
import os
# import pg # Import des commandes de postgres
import sys
sys.path.append('/usr/scripts/gestion')
import iptools
import psycopg2
import re
sys.path.append('/usr/script/surveillance')
import strptime

# Définition de constantes :
############################
reseau = ["138.231.136.0/21", "138.231.148.0/22"]

# Ouverture de la base de données :
###################################
pgsql = psycopg2.connect(host='pgsql.adm.crans.org', database='filtrage', user='crans')
pgsql.set_session(autocommit=True)
curseur = pgsql.cursor()


###########################################
# Récupération des tables de protocoles : #
###########################################

requete = "SELECT nom,id_p2p from protocole_p2p"
curseur.execute(requete)
curseur.fetchall
tableau = curseur.fetchall()
protocole_p2p = {}
for cellule in tableau:
    protocole_p2p[cellule[0]]=cellule[1]

requete = "SELECT nom,id from protocole"
curseur.execute(requete)
curseur.fetchall
tableau = curseur.fetchall()
protocole = {}
for cellule in tableau:
    protocole[cellule[0]]=cellule[1]



##############################################################
# Parser ler log du firewall: /var/log/firewall/filtre.log : #
##############################################################

# Définition des motifs des comparaisons :
##########################################
motif_p2p = re.compile("^(.*) komaz .* IPP2P=([^ ]*).* SRC=([^ ]*).* DST=([^ ]*).* PROTO=([^ ]*).* SPT=([^ ]*).* DPT=([^ ]*).*") 

motif_virus = re.compile("^(.*) komaz .* Virus:([^ ]*).* SRC=([^ ]*).* DST=([^ ]*).* PROTO=([^ ]*).* SPT=([^ ]*).* DPT=([^ ]*).*") 

motif_flood = re.compile("^(.*) komaz .* Flood:([^ ]*).* SRC=([^ ]*).* DST=([^ ]*).* PROTO=([^ ]*).* SPT=([^ ]*).* DPT=([^ ]*).*") 


# On récupère en continu les log du firewall:
#############################################
filtre = os.popen("tail -F /var/log/firewall/filtre.log 2> /dev/null")


# On matche les log du firewall avec les motifs :
#################################################
for log in filtre:
    resultat_p2p = motif_p2p.match(log)
    resultat_virus = motif_virus.match(log)
    resultat_flood = motif_flood.match(log)
    
    if resultat_p2p :
        try:
            ip_src = resultat_p2p.group(3)
            verif = iptools.AddrInNets (ip_src,reseau)
        except ValueError:
            continue #IP malformee
        if verif :
            try:
                date =  resultat_p2p.group(1)
                id_p2p = int(protocole_p2p[resultat_p2p.group(2)])
                ip_src = resultat_p2p.group(3)
                ip_dest = resultat_p2p.group(4)
                proto = int(protocole[resultat_p2p.group(5)]) #C'est à dire id pour la base
                port_src = int(resultat_p2p.group(6))
                port_dest = int(resultat_p2p.group(7))
                date=strptime.syslog2pgsql(date)
            except ValueError, KeyError:
                continue #mal parse

            # On remplit la base :
            ######################
            requete = "INSERT INTO p2p (date,ip_src,ip_dest,id_p2p,id,port_src,port_dest) VALUES ('%s','%s','%s',%d,%d,%d,%d)" % (date,ip_src,ip_dest,id_p2p,proto,port_src,port_dest)
            curseur.execute(requete)

        # On teste si le log contient des virus
        ########################################
    elif resultat_virus :
        try:
            ip_src = resultat_virus.group(3)
            verif = iptools.AddrInNets (ip_src,reseau)
        except ValueError:
            continue
        if verif :
            try:
                date =  resultat_virus.group(1)
                ip_src = resultat_virus.group(3)
                ip_dest = resultat_virus.group(4)
                proto = int(protocole[resultat_virus.group(5)]) #C'est à dire id pour la base
                port_src = int(resultat_virus.group(6))
                port_dest = int(resultat_virus.group(7))
                # On remplit la base :
                ######################
                date=strptime.syslog2pgsql(date)
            except ValueError, KeyError:
                continue
            requete = "INSERT INTO virus (date,ip_src,ip_dest,id,port_src,port_dest) VALUES ('%s','%s','%s',%d,%d,%d)" % (date,ip_src,ip_dest,proto,port_src,port_dest)
            curseur.execute(requete)
            
    elif resultat_flood :
        try:
            ip_src = resultat_flood.group(3)
            verif = iptools.AddrInNets (ip_src,reseau)
        except ValueError:
            continue
        if verif :
            try:
                date =  resultat_flood.group(1)
                ip_src = resultat_flood.group(3)
                ip_dest = resultat_flood.group(4)
                proto = int(protocole[resultat_flood.group(5)]) #C'est à dire id pour la base
                port_src = int(resultat_flood.group(6))
                port_dest = int(resultat_flood.group(7))
                
                # On remplit la base :
                ######################
                date=strptime.syslog2pgsql(date)
            except ValueError, KeyError:
                continue
            requete = "INSERT INTO flood (date,ip_src,ip_dest,id,port_src,port_dest) VALUES ('%s','%s','%s',%d,%d,%d)" % (date,ip_src,ip_dest,proto,port_src,port_dest)
            curseur.execute(requete)

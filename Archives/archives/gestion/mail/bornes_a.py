#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-
# Auteur: Gabriel Detraz

from email.header import Header
from email.MIMEText import MIMEText
from email.Utils import formatdate

from lc_ldap import shortcuts
import gestion.mail as mail_module

SEND = False

con = shortcuts.lc_ldap_readonly()

adh = con.search(u'(chbre=A*)')
print "Va contacter %d adhérents" % len(adh)

Serveur_SMTP = smtplib.SMTP('localhost')
for x in adh: 
    if not x.paiement_ok():
        continue
    tname = unicode(x.get('prenom',["club"])[0]) + " " + unicode(x['nom'][0])
    mail = x.get_mail()
    if mail is None:
        continue
#    print tname
    print mail
    txt = u''' Bonjour ''' + tname + u''', 
Les membres du Crans ont installé 3 nouveaux points d'accès WiFi dans ton
immeuble, au 1er, 3ème et 5ème étage. Ainsi, la couverture WiFi du A est
théoriquement complète.

N'hésite pas à nous contacter pour toute question, remarque ou problème à
l'adresse cableurs@crans.org.

Plus d'informations sont disponibles sur :
 * https://intranet.crans.org/wifimap/  pour une carte de la couverture
 * https://wifi.crans.org/  pour configurer sa machine en WiFi

En te souhaitant une bonne journée,

--
Les membres actifs du Crans'''
    m2 = mail
    MonEmail = MIMEText(txt, _charset='utf8')
    MonEmail['Subject'] = Header('Nouvelles bornes WiFi au A', 'utf8')
    MonEmail['From'] = 'cableurs@lists.crans.org'
    MonEmail['To'] = m2
    MonEmail['Date'] = formatdate(localtime=True)
    if SEND:
        with mail_module.ServerConnection() as conn:
            conn.sendmail('cableurs@lists.crans.org', m2, MonEmail.as_string())
Serveur_SMTP.quit()

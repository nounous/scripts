#!/bin/bash /usr/scripts/python.sh
# -*- mode: python; coding: utf-8 -*-

"""
Script de mise a jour des firmwares de switchs

J.Benoist Leger
"""

from time import sleep
from gestion.annuaires_pg import all_switchs
import pexpect
import getopt
import sys
import re
import os

if sys.argv[1] == '-h' or sys.argv[1]=='--help' :
    print "Usage : %s <ip du serveur tftp> <fichier sur le tftp> regex " % sys.argv[0]
    print "Envoi les commandes données au switchs matchant la regex"
    sys.exit(0)

ip_tftp=sys.argv[1]

file_tftp=sys.argv[2]



# Quels switchs ?
switchs=[]
re=re.compile(sys.argv[3])
for sw in all_switchs() :
    if re.match(sw) :
        switchs.append(sw)

print "IP du serveur TFTP : %s" % ip_tftp
print "Fichier sur le serveur TFTP : %s" % file_tftp
print "Switchs concernes :"
print switchs
print "---------------"

if not switchs :
    print "Aucun switch trouvé"
    print "Note : il faut une _regex_ (!= wilcards au sens du shell)"
    sys.exit(1)

if not ip_tftp :
    print "Pas de serveur tftp donné"
    sys.exit(1)

if not file_tftp :
    print "Pas de fichier donné"
    sys.exit(1)

# On fait les tests uniquement sur batv-3
# switchs=["batv-3.adm.crans.org"]


echecs=[]

for bestiole in switchs :
    host =  bestiole.split('.')[0]
    print "MaJ de %s" % host
    print "Ping de %s" % host

    if os.system("fping -r 1 -t 1000 -q %s 2>/dev/null" % host) != 0 :
        print "Ne repond pas au ping"
        echecs.append(bestiole)
        print "---------------"
        continue

    print "Connexion ssh"

    sshout = ''
    ssh = pexpect.spawn("ssh %s" % host)
    ssh.sendline("")
    ssh.sendline("configure")

    try:
        ssh.expect("%s\(config\)# " % host, timeout=40)
    except pexpect.TIMEOUT:
        print "Timeout de ssh"
        print "-----"
        echecs.append(bestiole)
        continue

    ssh.sendline("no ip ssh filetransfer")
    ssh.sendline("tftp client")
    ssh.sendline("exit")

    try:
        ssh.expect("%s# " % host, timeout=40)
    except pexpect.TIMEOUT:
        print "Timeout de ssh"
        print "-----"
        echecs.append(bestiole)
        continue

    # Maintenant on lui donne l'ordre de telecharger l'os

    print "Envoi de l'OS"
    ssh.sendline("copy tftp flash %s %s primary" % (ip_tftp,file_tftp) )
    sleep(2)
    ssh.send("y")

    # On attend !
    try:
        ssh.expect("%s# " % host, timeout=300)
    except pexpect.TIMEOUT:
        print "Timeout de ssh"
        print "----------------"
        echecs.append(bestiole)
        continue

    # On reboot le switch

    print "Reboot"

    ssh.sendline("boot system flash primary")
    ssh.send("y")
    ssh.send("n")

    # On attend qu'il ne reponde plus au ping
    i=0
    aie=0
    while os.system("fping -r 1 -t 2000 -q %s 2>/dev/null" % host ) == 0 :
        i=i+1
        sleep(1)
        if i==300 :
            print "Le switch n'a tjs pas rebooté"
            echecs.append(bestiole)
            aie=1
            break

    if aie==1 :
        print "---------------"
        continue

    print "Reboot commencé"
    print "Attente de réponse au ping"

    i=0
    aie=0
    while os.system("fping -r 1 -t 2000 -q %s 2>/dev/null" % host ) != 0 :
        i=i+1
        sleep(1)
        if i==300 :
            print "Le switch ne repond tjs pas au ping"
            echecs.append(bestiole)
            aie=1
            break

    if aie==1 :
        print "---------------"
        continue

    print "Le switch répond au ping"

    print "Connexion ssh"

    sshout = ''
    ssh = pexpect.spawn("ssh %s" % host)
    ssh.sendline("")

    try:
        ssh.expect("%s# " % host, timeout=40)
    except pexpect.TIMEOUT:
        print "Timeout de ssh"
        echecs.append(bestiole)
        print "-----"
        continue

    ssh.sendline("show version")

    aie=0

    try:
        ssh.expect("Boot Image:     Primary", timeout=40)
    except pexpect.TIMEOUT:
        print "Boot incorrect"
        echecs.append(bestiole)
        aie=1

    if aie==0 :
        print "Boot correct"

    print "Deconnexion ssh"
    ssh.sendline("logout")
    ssh.send("y")
    ssh.send("n")

    print "--------------"

print "Liste des echecs"
for bestiole in echecs :
    print bestiole


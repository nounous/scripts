#! /usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

import sys
sys.path.append('/usr/scripts/gestion')

from ldap_crans import crans_ldap, decode

db = crans_ldap()
adherents = db.search('solde=*')['adherent']

txts = []

for a in adherents :
    if a.solde() == 0 :
        continue

    # texte pour l'adhérent
    txt =  u''
    txt += u'Nom   : %s\n' % a.Nom()
    txt += u'Solde : %s\n' % a.solde()
    
    txts.append(txt.strip())

print '\n- - - - - - = = = = = = # # # # # # # # = = = = = = - - - - - -\n'.join(txts)

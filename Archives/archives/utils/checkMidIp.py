#!/usr/bin/python
# -*- mode: python; coding: utf-8 -*-
# © Olivier Huber, 2010
# Licence MIT
""" Ce script vérifie la cohérence entre le mid et l'adresse ip de toutes les
machines """

# Depuis la modification de la sémantique du mid et la création du rid,
# ceci n'a plus de raison d'être
# -- 
# 20-100, 08/02/13

import sys

sys.path.append('/usr/scripts/gestion')

from ldap_crans import crans_ldap
import midtools

db = crans_ldap()

machines = db.all_machines()

for machine in machines:
    try:
        mid_calc = midtools.Mid(None, machine.ip()).mid
    except ValueError, e:
        print e
    else:
        if mid_calc != int(machine.id()):
            print "La machine associée à l'ip %s a le mid %s alors qu'elle \
            devrait avoir %d" % (machine.ip(), machine.id(), mid_calc)

exit(0)

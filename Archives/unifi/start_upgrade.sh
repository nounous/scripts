# Permet temporairement aux bornes d'accéder à internet pour récupérer le firmware unifi (https sur le site web)
iptables -t nat -A POSTROUTING -s 10.231.148.0/24 -j SNAT --to-source 138.231.136.254
iptables -I FORWARD 1 -s 10.231.148.0/24 -j ACCEPT
iptables -I FORWARD 1 -d 10.231.148.0/24 -j ACCEPT

"""
Ce script touche à la base MongoDB du contrôleur
afin de recopier la position de la borne dans
le champ SNMP "sysLocation" (1.3.6.1.2.1.1.6).
Cela permet ensuite de tracer des WiFiMap dans Grafana.

Nécessite pymongo
"""

from pymongo import MongoClient


def geohash(latitude, longitude, precision=12):
    """
    Encode a position given in float arguments latitude, longitude to
    a geohash which will have the character count precision.

    From Geohash under GPL license, Leonard Norrgard
    """
    __base32 = '0123456789bcdefghjkmnpqrstuvwxyz'
    lat_interval, lon_interval = (-90.0, 90.0), (-180.0, 180.0)
    geohash = []
    bits = [16, 8, 4, 2, 1]
    bit = 0
    ch = 0
    even = True
    while len(geohash) < precision:
        if even:
            mid = (lon_interval[0] + lon_interval[1]) / 2
            if longitude > mid:
                ch |= bits[bit]
                lon_interval = (mid, lon_interval[1])
            else:
                lon_interval = (lon_interval[0], mid)
        else:
            mid = (lat_interval[0] + lat_interval[1]) / 2
            if latitude > mid:
                ch |= bits[bit]
                lat_interval = (mid, lat_interval[1])
            else:
                lat_interval = (lat_interval[0], mid)
        even = not even
        if bit < 4:
            bit += 1
        else:
            geohash += __base32[ch]
            bit = 0
            ch = 0
    return ''.join(geohash)


def get_device_collection():
    """
    Connexion sur la base MongoDB du contrôleur Unifi
    et récupère les données des périphériques
    """
    client = MongoClient("mongodb://localhost:27117")
    return client.ace.device


collection = get_device_collection()
for device in collection.find():
    # Get device location
    x, y = device.get('x'), device.get('y')

    if not (x and y):
        print("Oh crap, un périphérique n'est pas placé sur la carte")
    else:
        # Compute geohash and replace
        snmp_location = geohash(x, y)
        if snmp_location != device['snmp_location']:
            print("{} ({}) updated with geohash {}, was {}".format(
                device['name'],
                device['_id'],
                snmp_location,
                device['snmp_location'],
            ))
            collection.update_one(
                {'_id': device['_id']},
                {'$set': {'snmp_location': snmp_location}},
            )


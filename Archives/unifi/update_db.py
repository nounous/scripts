import requests
import getpass
from pymongo import MongoClient

def connect_as_self(base_url, username=None):
    """Get a re2o token for the current unix user """
    if username is None:
        username = getpass.getuser()
    r = requests.post(
            base_url + "token-auth",
            data={
                "username": username,
                "password": getpass.getpass("Login to {} as {} with password :".format(base_url, username))
            }
    )
    print(r.status_code)
    if r.status_code != 200:
        print("Wrong login/password")
        exit(1)
    token = r.json().get("token")
    return token

def get_macips(token, re2o_instance):
    base_url = "{}/api/".format(re2o_instance)
    headers = {"Authorization": "Token " + token}
    # On récupère la config du bon switch
    macips = []
    r = requests.get(base_url + "dhcp/hostmacip", headers=headers)
    macips += r.json()["results"]
    while r.json().get("next"):
         r = requests.get(r.json().get("next"), headers=headers)
         macips += r.json()["results"]
    return macips

def update_device(devices, macaddress, name):
    devices.find_one_and_update({"mac": macaddress}, {"$set": {"name": name}})


if __name__ == "__main__":
    client = MongoClient("mongodb://localhost:27117")
    db = client.ace
    devices = db.device
    re2o_user = "paulon"
    base_url = "http://172.16.10.156/api/"
    token = connect_as_self(base_url, re2o_user)
    macips = get_macips(token, base_url)
#    for d in devices.find():
#        mac = d.get("mac")
#        print(d.get("mac"), d.get("name"))
#        name = list(filter(lambda x: x["mac_address"] == mac.upper(), macips))[0]["hostname"]
#        print(name)
#        update_device(devices, mac, name)

# on édite le script pour générer la liste des bornes que le controlleur ne voit pas
    list_bornes = list(filter(lambda x: x.get("ipv4", "").startswith("172.16.34"), macips))
    list_unifi = devices.find()
    unifi_macs = [x.get("mac").upper() for x in list_unifi]
    list_bornes_mortes = list(filter(lambda x: x.get("mac_address") not in unifi_macs, list_bornes))
    print("=============pas dans controlleur")
    for b in map(lambda x: x.get("hostname"), list_bornes_mortes):
        print(b + ".infra.crans.org")
    print("\n=============unifi")
    for d in devices.find():
        print(d.get("name")+".infra.crans.org")

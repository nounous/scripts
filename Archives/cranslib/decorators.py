#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

import functools

def static_var(*couples):
    """Decorator setting static variable
    to a function.

    """
    # Using setattr magic, we set static
    # variable on function. This avoid
    # computing stuff again.
    def decorate(fun):
        functools.wraps(fun)
        for (name, val) in couples:
            setattr(fun, name, val)
        return fun
    return decorate


#
# Ebauche de script d'import de la bdd Crans vers re2o
#
# 2017-2018 Gabriel Detraz

from unidecode import unidecode

from users.models import School, User

from django.db import transaction
from reversion.models import Version
from reversion import revisions as reversion

import sys

sys.path.append('/usr/scripts')

from lc_ldap import shortcuts

ldap = shortcuts.lc_ldap_admin()

adh = ldap.search('aid=*', sizelimit=10000)

from topologie.models import Room


# Import sql de chambre prises :
# COPY (select batiment, chambre, prise_crans, has_radius, public, commentaire from prises_prise) To '/tmp/output2.csv' With CSV;

f = open('/home/chibrac/output2.csv')

data = f.readlines()

for d in data:
    chambre = Room()
    chambre.name = d.split(',')[0].upper()+ d.split(',')[1]
    if d.split(',')[5]!='""\n':
        chambre.details = d.split(',')[5].replace("\n", "")
    chambre.save()

# Import des adhérents
from users.models import Adherent

import pytz
import datetime

for rank, ad in enumerate(adh):
     u = Adherent()
     u.id = ad['aid'][0].value
     if ad.get('uidNumber', []):
         u.uid_number = ad['uidNumber'][0].value
     u.rezo_rez_uid = ad['aid'][0].value
     u.telephone = ad['tel'][0].value
     school = ad.get('etudes', [])
     if school:
         u.school, result  = School.objects.get_or_create(name=school[0].value.encode("utf-8").lower())
     backup_pseudo = ad['nom'][0].value + u'-' + ad['prenom'][0].value
     u.pseudo = ad.get('uid', [unidecode(backup_pseudo.lower())] )[0]
     u.name = ad['prenom'][0].value
     u.surname = ad['nom'][0].value
     shell = ad.get('loginShell', [])
     if shell:
         u.shell, result = ListShell.objects.get_or_create(shell=shell[0].value)
     if ad.get('userPassword', []):
         passwd = ad['userPassword'][0].value
         if '{SSHA}' in passwd or '{SMD5}' in passwd:
             u.password = passwd[:6] + '$' + passwd[6:]
         elif '{crypt}' in passwd and passwd[7] != '$':
             u.password = passwd[:7] + '$' + passwd[7:]
         else:
             u.password = passwd
     if ad['derniereConnexion']:
         u.last_login = datetime.datetime.utcfromtimestamp(ad['derniereConnexion'][0].value).replace(tzinfo=pytz.utc)
     u.save()


clubs = ldap.search('cid=*', sizelimit=10000)

for rank, ad in enumerate(clubs):
     u = Club()
     u.rezo_rez_uid = ad['cid'][0].value + 20000
     u.id = ad['cid'][0].value + 20000
     telephone = ad.get('tel', None)
     if telephone:
         u.telephone = telephone[0].value
     school = ad.get('etudes', [])
     if school:
         u.school, result  = School.objects.get_or_create(name=school[0].value)
     backup_pseudo = ad['nom'][0].value
     u.pseudo = ad.get('uid', [unidecode(backup_pseudo.lower())] )[0]
     u.surname = ad['nom'][0].value
     if ad.get('userPassword', []):
         passwd = ad['userPassword'][0].value
         if '{SSHA}' in passwd:
             u.password = passwd[:6] + '$' + passwd[6:]
     if ad['derniereConnexion']:
         u.last_login = datetime.datetime.utcfromtimestamp(ad['derniereConnexion'][0].value).replace(tzinfo=pytz.utc)
     u.save()

# Import des chambres

for ad in adh:
    chbre = ad['chbre'][0].value.capitalize()
    print ad['nom'][0]
    print chbre
    if chbre.upper()!='EXT' and chbre.upper()!='????':
        user = Adherent.objects.get(rezo_rez_uid=ad['aid'][0].value)
        user.room = Room.objects.get(name=chbre)
        user.save()

from django.db import IntegrityError
count = 0
length = len(clubs)
for club in clubs:
     print count/length
     chbre = club['chbre'][0].value.capitalize()
     if chbre.upper()!='EXT' and chbre.upper()!='????':
         user = Club.objects.get(rezo_rez_uid=club['cid'][0].value + 20000)
         user.room = Room.objects.get(name=chbre)
         try:
             user.save()
         except IntegrityError:
             print(user)



# Import des droits

from users.models import ListRight, get_fresh_gid

adh = ldap.search('droits=*', sizelimit=10000)

count=0.
length = len(adh)
for ad in adh:
    print count/length
    count += 1.0
    if 'droits' in ad:
        user = User.objects.get(rezo_rez_uid=ad['aid'][0].value)
        for droit in ad['droits']:
            try:
                right = ListRight.objects.get(unix_name=droit.value.lower())
            except ListRight.DoesNotExist:
                right= ListRight(unix_name=droit.value.lower(), name=droit.value.lower(), gid=get_fresh_gid())
                right.save()
            user.groups.add(right)
        user.save()

# Import des comptes mails

from users.models import EMailAddress
from django.db import IntegrityError

adh = ldap.search('(&(aid=*)(objectClass=cransAccount))', sizelimit=10000)

c = 0.
l = len(adh)
for ad in adh:
    print c/l
    c += 1
    user = User.objects.get(rezo_rez_uid=ad['aid'][0].value)
    if 'canonicalAlias' in ad:
        try:
            alias, created = EMailAddress.objects.get_or_create(user=user, local_part=ad['canonicalAlias'][0].value.split('@')[0])
        except IntegrityError:
            print(ad)
    for alias in ad['mailAlias']:
        try:
            alias, created = EMailAddress.objects.get_or_create(user=user, local_part=alias.value.split('@')[0])
        except IntegrityError:
            print(alias.value.split('@')[0])
            print(ad)
    user.local_email_enabled = True
    if 'mailExt' in ad:
        user.email = ad['mailExt'][0].value
    user.save()


adh = ldap.search('(&(cid=*)(objectClass=cransAccount))', sizelimit=10000)

c = 0.
l = len(adh)
for ad in adh:
    print c/l
    c += 1
    user = User.objects.get(rezo_rez_uid=ad['cid'][0].value + 20000)
    if 'canonicalAlias' in ad:
        try:
            alias, created = EMailAddress.objects.get_or_create(user=user, local_part=ad['canonicalAlias'][0].value.split('@')[0])
        except IntegrityError:
            print(ad)
    for alias in ad['mailAlias']:
        try:
            alias, created = EMailAddress.objects.get_or_create(user=user, local_part=alias.value.split('@')[0])
        except IntegrityError:
            print(alias.value.split('@')[0])
            print(ad)
    user.local_email_enabled = True
    if 'mailExt' in ad:
        user.email = ad['mailExt'][0].value
    user.save()


adh = ldap.search('(&(aid=*)(!(objectClass=cransAccount)))', sizelimit=10000)
c = 0.
l = len(adh)
for ad in adh:
    print c/l
    c += 1
    if 'mail' in ad:
        user = User.objects.get(rezo_rez_uid=ad['aid'][0].value)
        user.external_mail = ad['mail'][0].value
        user.save()

adh = ldap.search('(&(cid=*)(!(objectClass=cransAccount)))', sizelimit=10000)
c = 0.
l = len(adh)
for ad in adh:
    print c/l
    c += 1
    if 'mail' in ad:
        user = User.objects.get(rezo_rez_uid=ad['aid'][0].value + 20000)
        user.external_mail = ad['mail'][0].value
        user.save()



#Import des members et admin club
clubs = ldap.search('cid=*', sizelimit=10000)

from users.models import Adherent, Club
c = 0.
l = len(clubs)
for club in clubs:
    print c/l
    c += 1
    cl_obj = Club.objects.get(rezo_rez_uid=20000 + int(club['cid'][0].value))
    for resp in club['responsable']:
        cl_obj.administrators.add(Adherent.objects.get(rezo_rez_uid=int(resp.value['aid'][0].value)))
    for member in club['imprimeurClub']:
        cl_obj.members.add(Adherent.objects.get(rezo_rez_uid=int(member.value['aid'][0].value)))
    cl_obj.save()


#11

from cotisations.models import Cotisation
from cotisations.models import Paiement
from cotisations.models import Banque, Facture, Vente
from dateutil import parser
from django.utils.timezone import get_current_timezone, make_aware


factures = ldap.search('fid=*', sizelimit=100000)

c = 0.
l = len(factures)
for compt, fact  in enumerate(factures):
    print c/l
    c += 1
    if not fact.proprio().get('aid', None):
        continue
    f = Facture()
    try:
        f.user = User.objects.get(rezo_rez_uid=fact.proprio()['aid'][0])
    except :
        print("User not found !")
        continue
    f.id = int(fact['fid'][0].value)
    f.paiement, created = Paiement.objects.get_or_create(moyen=fact['modePaiement'][0].value)
    if fact['recuPaiement']:
        f.valid =  True
    else:
        f.valid = False
    f.control = fact.get('controle', [None])[0] == u'TRUE'
    for histo in fact['historique']:
        date_hist = histo.split(',')[0]
        every_else = "".join(histo.split(',')[1:])
        user = every_else.split(":")[0].replace(" ", "")
        user_query = User.objects.filter(pseudo=user)
        comment = "".join(every_else.split(":")[1:])
        with transaction.atomic(), reversion.create_revision():
            f.save()
            if user_query:
                reversion.set_user(user_query.first())
            reversion.set_comment(comment)
            format_date = parser.parse(date_hist, dayfirst=True)
            reversion.set_date_created(make_aware(format_date, get_current_timezone(), is_dst=False))
    if fact['recuPaiement']:
        f.date = fact['recuPaiement'][0].value
        f.save()
    else:
        f.valid = False
        f.date = parser.parse(fact['historique'][0].value.split(',')[0], dayfirst=True)
        f.save()

    for art in fact['article']:
        v = Vente()
        v.facture = f
        v.number = int(art.value['nombre'])
        v.prix = art.value['pu']
        v.name = art.value['designation'].replace(u"à", u"a").replace(u"ç", u"c").replace(u"ê", u"e").replace(u"é", u"e").replace(u"è", u"e").replace(u"€", u"euros")
        if art.value['code'][0:3]=='CAI' and fact['recuPaiement']:
            if int(art.value['code'][3:])!=0:
                v.type_cotisation = 'Connexion'
                v.duration = int(art.value['code'][3:])
                v.create_cotis(date_start=fact['recuPaiement'][0].value)
        if 'ADH' in art.value['code'] and fact['recuPaiement']:
            v.type_cotisation = 'Adhesion'
            v.duration = 12
            v.create_cotis(date_start=fact['recuPaiement'][0].value)
        v.save()

factures = ldap.search('fid=*', sizelimit=100000)
c = 0.
l = len(factures)
for compt, fact  in enumerate(factures):
    print c/l
    c += 1
    if not fact.proprio().get('cid', None):
        continue
    f = Facture()
    try:
        f.user = User.objects.get(rezo_rez_uid=fact.proprio()['cid'][0].value + 20000)
    except:
         continue
    f.id = int(fact['fid'][0].value)
    f.paiement, created = Paiement.objects.get_or_create(moyen=fact['modePaiement'][0].value)
    if fact['recuPaiement']:
        f.valid =  True
    else:
        f.valid = False
    f.control = fact.get('controle', [None])[0] == u'TRUE'
    for histo in fact['historique']:
        date_hist = histo.split(',')[0]
        every_else = "".join(histo.split(',')[1:])
        user = every_else.split(":")[0].replace(" ", "")
        user_query = User.objects.filter(pseudo=user)
        comment = "".join(every_else.split(":")[1:])
        format_date = None
        with transaction.atomic(), reversion.create_revision():
            f.save()
            if user_query:
                reversion.set_user(user_query.first())
            reversion.set_comment(comment)
            format_date = parser.parse(date_hist, dayfirst=True)
            reversion.set_date_created(make_aware(format_date, get_current_timezone(), is_dst=False))
    f.save()
    if fact['recuPaiement']:
        f.date = fact['recuPaiement'][0].value
        f.save()
    else:
        f.valid = False
        f.date = parser.parse(fact['historique'][0].value.split(',')[0], dayfirst=True)
        f.save()

    for art in fact['article']:
        v = Vente()
        v.facture = f
        v.number = int(art.value['nombre'])
        v.prix = art.value['pu']
        v.name = art.value['designation'].replace(u"à", u"a").replace(u"ç", u"c").replace(u"ê", u"e").replace(u"é", u"e").replace(u"è", u"e").replace(u"€", u"euros")
        if 'ADH' in art.value['code'] and fact['recuPaiement']:
            v.type_cotisation = 'All'
            v.duration = 12
            v.create_cotis(date_start=fact['recuPaiement'][0].value)
        v.save()

# import des vlans
from gestion.config.config import vlans
from machines.models import Vlan

for name in vlans:
    vlan = Vlan()
    vlan.name = name
    vlan.vlan_id = vlans[name]
    vlan.save()


from machines.models import Extension, SOA, IpType, Vlan

import netaddr
from gestion.config.config import NETs_primaires, prefix
linker = {
    'serveurs' : "adherent",
    'adherents' : "adherent",
    'wifi-new-serveurs' : "wifi-new",
    'wifi-new-pub' : "fil-pub",
    'wifi-new-federez' : "wifi-new",
    'wifi-new-adherents' : "wifi-new",
    'fil-new-serveurs' : "fil-new",
    'fil-new-adherents' : "fil-new",
    'fil-new-pub' : "fil-pub",
    'multicast' : "fil-new" ,
    'evenementiel' : "event",
}



linker_ext = {
    'adherents' : '.crans.org',
    'fil-new-adherents' : '.fil.crans.org',
    'fil-new-serveurs' :  '.fil.crans.org',
    'serveurs' : '.crans.org',
    'wifi-new-serveurs' : ".wifi.crans.org",
    'wifi-new-federez' : ".wifi.crans.org",
    'wifi-new-adherents' : ".wifi.crans.org",
    'fil-pub' : '.adh.crans.org',
    'bornes' : '.borne.crans.org',
    }

for net in NETs_primaires:
    if net not in ['multicast'] and not(bool(IpType.objects.filter(type=net))):
        type = IpType()
        type.type = net
        ext = linker_ext.get(net, "."+net+".crans.org")
        type.extension = Extension.objects.get_or_create(name=ext, soa=SOA.objects.first())[0]
        ip_list = NETs_primaires[net]
        first_ip = netaddr.IPAddress(netaddr.IPNetwork(ip_list[0]).first +1)
        last_ip = netaddr.IPAddress(netaddr.IPNetwork(ip_list[-1]).last-1)
        type.domaine_ip_start = str(first_ip)
        type.domaine_ip_stop = str(last_ip)
        type.prefix_v6 = prefix[net][0].split( "/")[0]
        vlan = linker.get(net,net)
        type.vlan = Vlan.objects.get(name=vlan)
        if net in ["serveurs", "fil-new-serveurs"] :
            type.need_infra = True
        type.save()


#15

from machines.models import IpType, Machine, Extension, MachineType, Interface, Domain, IpList

for iptype in IpType.objects.all():
    machtype = MachineType()
    machtype.type = iptype.type
    machtype.ip_type = iptype
    machtype.save()

ldap = shortcuts.lc_ldap_admin()
machines = ldap.search(u'mid=*', sizelimit=20000)

from django.db import transaction
from django.db import IntegrityError

c = 0.
l = len(machines)
for num, machine in enumerate(machines):
    if (c % 50) == 0:
        print c/l
    c += 1
    try:
        with transaction.atomic():
            mach = Machine()
            inter = Interface()
            dom = Domain()
            ipv4 = machine.get('ipHostNumber', [None])[0]
            if ipv4:
                ipv4_object = IpList.objects.filter(ipv4=str(ipv4))
                if not ipv4_object:
                    print(ipv4)
                    continue
                inter.ipv4 = ipv4_object.first()
            macaddress = machine.get('macAddress', [None])[0]
            if macaddress and macaddress.value!=u"<automatique>":
                inter.mac_address = str(macaddress.value)
            else:
                continue
            name=machine['host'][0].split('.')[0]
            if not ipv4:
                extension='.'+'.'.join(machine['host'][0].split('.')[1:])
                initial_type = MachineType.objects.filter(ip_type__in=IpType.objects.filter(extension=Extension.objects.filter(name=extension)))
                if initial_type.count() == 1:
                    inter.type=initial_type.first()
                elif not initial_type:
                    print(extension)
                    continue
                # Pour crans.org, on a les serveurs et les machines adhérents et divers autres trucs
                elif initial_type.count() == 2:
                    if machine['objectClass'][0].value==u"machineCrans":
                        initial_type = MachineType.objects.filter(ip_type__in=IpType.objects.filter(need_infra=True).filter(extension=Extension.objects.filter(name=extension)))
                    else:
                        initial_type = MachineType.objects.filter(ip_type__in=IpType.objects.filter(need_infra=False).filter(extension=Extension.objects.filter(name=extension)))
                inter.type = initial_type.first()
            else:
                inter.type = ipv4_object.first().ip_type.machinetype_set.first()  #initial_type.first()
            id =  machine.proprio().get('aid', [None])[0]



            if machine.proprio().__unicode__()==u'Le Crans':
                user = User.objects.get(pseudo='crans')
            elif not id:
                # Cas des clubs
                id = machine.proprio().get('cid', [None])[0]
                user = User.objects.get(rezo_rez_uid=id + 20000)
                if not id:
                    print("erreur !")
                    continue
            else:
                user = User.objects.get(rezo_rez_uid=id)

            interface_existante = Interface.objects.filter(domain__name=name, machine__user=user)

            if interface_existante:
                mach = interface_existante.first().machine
                print("Regrouppement avec %s" % str(interface_existante))
            else:
                mach.user = user

            new_interface = inter
            new_interface.clean()
            dom.interface_parent = new_interface
            dom.name = name
            dom.clean()
            mach.save()
            new_interface.machine=mach
            new_interface.save()
            dom.interface_parent = new_interface
            dom.save()
        if not ipv4:
            new_interface.unassign_ipv4()
            new_interface.save()
    except IntegrityError:
        print("integrity error !")

#16

machines = ldap.search(u'(&(mid=*)(objectClass=switchCrans))', sizelimit=20000)
from machines.models import IpList, Interface
from topologie.models import Switch, Port, AccessPoint

for m in machines:
    # On importe pas les "faux switches"
    if int(m['nombrePrises'][0]) > 1:
        sw = Switch()
        sw.location = m['host'][0].value.split('.')[0]
        sw.details = ' '.join([info.value for info in m['info'][0:]])
        sw_interface = Interface.objects.filter(ipv4=IpList.objects.get(ipv4=str(m['ipHostNumber'][0].value)))
        if sw_interface:
            sw.machine_ptr_id = sw_interface.first().machine.pk
            sw.number = int(m['nombrePrises'][0])
            sw.__dict__.update(sw_interface.first().machine.__dict__)
            sw.save()
        else:
            print(m)


### import du backbone

back = Switch()
back.machine_ptr_id = Interface.objects.filter(domain__name="backbone").first().machine.pk
back.location = "0B"
back.number = 48
back.__dict__.update(Interface.objects.filter(domain__name="backbone").first().machine.__dict__)
back.save()

# Ajout des ports

for sw in Switch.objects.all():
    for portnumber in range(1,sw.number + 1):
        port = Port()
        port.switch = sw
        port.port = portnumber
        port.save()

#18

# ajout des bornes

machines = ldap.search(u'(&(mid=*)(objectClass=borneWifi))', sizelimit=20000)
for m in machines:
    b = AccessPoint()
    b_interface = Interface.objects.filter(ipv4=IpList.objects.get(ipv4=str(m['ipHostNumber'][0].value)))
    if b_interface:
        b.machine_ptr_id = b_interface.first().machine.pk
        b.__dict__.update(b_interface.first().machine.__dict__)
        b.location = ','.join([str(info) for info in m['info']])
        b.save()
    else:
        print(m)


f = open('/home/chirac/output.csv')

data = f.readlines()

### Chargement des relations ports chambre

from topologie.models import Room

for d in data:
    room = Room.objects.get(name=d.split(',')[0].upper()+ d.split(',')[1])
    interface = Interface.objects.filter(domain__name='bat' + d.split(',')[0].lower() + '-' + str(int(d.split(',')[2])/100))
    if not interface:
        continue
    else:
        port = Port.objects.get(switch__machine_ptr=interface.first().machine, port=int(d.split(',')[2])%100)
        port.room = room
        if d.split(',')[3]=='t':
            if d.split(',')[4]=='t':
                port.radius = 'COMMON'
            else:
                port.radius='STRICT'
        else:
            port.radius = 'NO'
        port.save()

#19

### Chargement des uplinks

from gestion import annuaires_pg

for a, b in annuaires_pg.uplink_prises.items():
    if a!='backbone':
        for c, d in b.items():
            if "bat" in d:
                switch1_name = d.split("->")[1].split()[0]
                switch2_name = 'bat' + a + '-' + str(int(c)/100)
                switch2_port = str(int(c)%100)
                for e, f in annuaires_pg.uplink_prises[switch1_name.split('-')[0][-1]].items():
                    if switch2_name in f and str(int(e)/100)==switch1_name.split('-')[1]:
                        switch1_port = str(int(e)%100)
                        break
                port1 = Port.objects.get(port=int(switch1_port), switch=Switch.objects.get(machine_ptr=Interface.objects.get(domain__name=switch1_name).machine))
                port2 = Port.objects.get(port=int(switch2_port), switch=Switch.objects.get(machine_ptr=Interface.objects.get(domain__name=switch2_name).machine))
                port1.related = port2
                port1.save()

### import des ports de libre service

for a, b in annuaires_pg.uplink_prises.items():
    if a!='backbone':
        for c, d in b.items():
            if "libre" in d:
                port = Port.objects.get(port=int(c)%100, switch=Switch.objects.get(machine_ptr=Interface.objects.get(domain__name='bat'+a+'-'+str(int(c)/100)).machine))
                port.details = "Libre-service"
                port.save()



for a, b in annuaires_pg.uplink_prises.items():
    if a=='backbone':
        for c, d in b.items():
            if "bat" in d:
                if "A" in c:
                    switch1_port = int(c[1:])
                elif "B" in c:
                    switch1_port = int(c[1:]) + 24
                port1 = Port.objects.get(port=int(switch1_port), switch=Switch.objects.get(machine_ptr=Interface.objects.filter(domain__name="backbone").first().machine))
                for e, f in annuaires_pg.uplink_prises.items():
                    if e==d[3]:
                        for g, h in f.items():
                            if "backbone" in h and not "unused" in h:
                                switch2_port = int(g)%100
                                switch2_name = "bat" + e  + "-" + str(int(g)/100)
                                port2 =  Port.objects.get(port=int(switch2_port), switch=Switch.objects.get(machine_ptr=Interface.objects.filter(domain__name=switch2_name).first().machine))
                                port1.related = port2
                                port1.save()
                                port1.make_port_related()
                                port1.save()
                                break
            else:
                if "A" in c:
                    switch1_port = int(c[1:])
                elif "B" in c:
                    switch1_port = int(c[1:]) + 24
                port1 = Port.objects.get(port=int(switch1_port), switch=Switch.objects.get(machine_ptr=Interface.objects.filter(domain__name="backbone").first().machine))
                port1.machine_interface=Interface.objects.filter(domain__name=d).first()
                #, domain__extension__in=Extension.objects.filter(name='.adm.crans.org'))
                port1.save()

#20

### Import des bornes et serveurs

from machines.models import Vlan
from topologie.models import PortProfile

machines = ldap.search(u'prise=*')
for machine in machines:
    if not "back" in machine['prise'][0].value.lower():
        interface = Interface.objects.filter(domain__name=machine['host'][0].value.split('.')[0], type__ip_type__extension__name='.'+'.'.join(machine['host'][0].value.split('.')[1:]))
        if not interface or interface.count()!=1:
            print(machine['host'][0].value)
            continue
        number = machine['prise'][0].value[1:]
        port = Port.objects.get(switch=Interface.objects.get(domain__name='bat' + machine['prise'][0].value[0].lower() + '-' + str(int(number)/100)).machine, port=int(number)%100)
        port.machine_interface = interface.first()
        if 'untagvlan' in machine:
            vlan = Vlan.objects.filter(vlan_id=int(machine['untagvlan'][0].value)).first()
            if vlan:
                profil, created = PortProfile.objects.get_or_create(name="force_vlan_" + str(machine['untagvlan'][0].value), vlan_untagged=vlan, radius_type='NO')
                port.custom_profil = profil
        port.save()

# Import des alias machines

from machines.models import Domain, Extension, SOA

machines = ldap.search(u'HostAlias=*')
for machine in machines:
    for alias in machine['hostAlias']:
        extension, created = Extension.objects.get_or_create(name='.'+'.'.join(alias.value.split('.')[1:]), soa = SOA.objects.first())
        cname_on = Domain.objects.filter(name=machine['host'][0].value.split('.')[0], interface_parent__type__ip_type__extension__name='.'+'.'.join(machine['host'][0].value.split('.')[1:]))
        # Or si  l'id correspond au mid
        # cname_on = Domain.objects.filter(interface_parent__id=int(machine['id'][0].value))
        if not cname_on:
            print(alias)
            print(machine)
            continue
        dom = Domain(cname=cname_on.first(), name=alias.split('.')[0], extension=extension)
        dom.save()


### Création des types spécifique
from machines.models import Interface, MachineType, IpType
types = {
    'ilo' : '-ilo',
    'camera' :  "brother",
    }

adm = IpType.objects.get(type='adm')
for ty in types:
    mt = MachineType()
    mt.ip_type = adm
    mt.type = ty
    mt.save()
    for interface in Interface.objects.filter(domain__name__contains=types[ty]).filter(machine__user__pseudo='crans'):
        interface.type = mt
        interface.save()
# onduleurs
mt = MachineType()
mt.ip_type = adm
mt.type = 'onduleur'
mt.save()
for o in ['pulsar', 'quasar']:
    interface = Interface.objects.filter(domain__name=o).filter(machine__user__pseudo='crans').filter(type__ip_type = adm).first()
    interface.type = mt
    interface.save()
# imprimante
mt = MachineType()
mt.ip_type = adm
mt.type = 'imprimante'
mt.save()
interface = Interface.objects.filter(domain__name='imprimante').filter(machine__user__pseudo='crans').filter(type__ip_type = adm).first()
interface.type = mt
interface.save()


serveurs =

### Création des roles
from machines.models import Role, Interface
servs = [
    { 'host' : 'vert', 'iptype' : 'adm', 'roles' : ['ldap-master',],},
    { 'host' : 're2o', 'iptype' : 'adm', 'roles' : ['re2o-server',],},
    { 'host' : 'nem', 'iptype' : 'adm', 'roles' : ['dns-recursive',],},
    { 'host' : 'silice', 'iptype' : 'adm', 'roles' : ['dns-authoritary-master',],},
    { 'host' : 'soyouz', 'iptype' : 'adm', 'roles' : ['dns-authoritary-slave', 'sql-client',],},
    { 'host' : 'titanic', 'iptype' : 'adm', 'roles' : ['dns-authoritary-slave',],},
    { 'host' : 'dhcp', 'iptype' : 'adm', 'roles' : ['dhcp', 'dhcp-backup',],},
    { 'host' : 'sable', 'iptype' : 'adm', 'roles' : ['dhcp',],},
    { 'host' : 'dhcp', 'iptype' : 'pub', 'roles' : ['dhcp-authoritative',],},
    { 'host' : 'sable', 'iptype' : 'pub', 'roles' : ['dhcp-authoritative',],},
        ]

for t in servs:
    interface = Interface.objects.filter(domain__name=t['host']).filter(machine__user__pseudo='crans').filter(type__ip_type__type__iexact=t['iptype']).first()
    for r in t['roles']:
        print interface
        role = Role.objects.get_or_create(role_type=r)[0]
        role.servers.add(interface)

roles = [
    { 'role' : 'sql-client',
        'iptype' : 'adm',
        'serveurs' :  [
                        'o2', 'zamok', 'kenobi', 'owl', 'roundcube',
                        'horde', 'mediadrop', 'titanic', 'redisdead',
                        'owncloud', 'fy', 'cas', 'alice',
                        'thot', 'ytrap-llatsni', 're2o-server', 'eap',
                        'radius',
                        ],
        },
        ]

for r in roles:
    role = Role.objects.get_or_create(role_type=r["role"])[0]
    for t in r["serveurs"]:
        interface = Interface.objects.filter(domain__name=t).filter(machine__user__pseudo='crans').filter(type__ip_type__type__iexact=r['iptype']).first()
        role.servers.add(interface)

#### Import des ssh fingerprint

from machines.models import Interface, SshFp

serveurs = ldap.search('sshfingerprint=*', sizelimit=10000)

for serveur in serveurs:
    if not serveur['ipHostNumber']:
        continue
    interface = Interface.objects.filter(ipv4__ipv4=str(serveur['ipHostNumber'][0].value))
    if not interface:
        print(serveur)
        print("introuvable")
        continue
    interface = interface.first()
    for fingerprint in serveur['sshFingerprint']:
        fpralgo = fingerprint['type']
        # On check si on a pas déjà ajouté la clef
        if not SshFp.objects.filter(pub_key_entry=fingerprint['key'], machine=interface.machine, algo=fpralgo):
            sshfgprint = SshFp(pub_key_entry=fingerprint['key'], machine=interface.machine, algo=fpralgo, comment=fingerprint['comm'])
            sshfgprint.save()



### import des Ns
 from machines.models import Ns, Extension, Role, Interface

     servs = Role.objects.get(role_type='dns-authoritary').servers.all()
     exts = Extension.objects.all()

     for ext in exts:
         for serv in servs:
             ns = Ns()
             ns.ns = serv.domain
             ns.zone = ext
             ns.save()

#23

### import des ouvertures de port

from machines.models import Interface,Extension, OuverturePort, OuverturePortList
machines = ldap.search('(|(portTCPin=*)(portTCPout=*)(portUDPin=*)(portUDPout=*))')
leftovers = []
for m in machines:
         ipv4 = m.get('ipHostNumber', [None])[0]
         if ipv4:
             interface = Interface.objects.get(ipv4__ipv4=str(ipv4.value))
             pti =[]
             for p in m.get('portTCPin', []):
                 pti.append(p.value)
             pto =[]
             for p in m.get('portTCPout', []):
                 pto.append(p.value)
             pui =[]
             for p in m.get('portUDPin', []):
                 pui.append(p.value)
             puo =[]
             for p in m.get('portUDPout', []):
                 puo.append(p.value)

             port_list = 'TI:'+ str(pti) + ',TO:'+ str(pto)+  'UI:'+ str(pui) + ',UO:'+ str(puo)
             new_port_list = OuverturePortList.objects.get_or_create(name=port_list)[0]
             for p in pti:
                 if len(p)==1:
                     port = OuverturePort.objects.get_or_create(begin=p[0],end=p[0],io=u"I",protocole=u"T",port_list=new_port_list)
                 elif len(p)==2:
                     port = OuverturePort.objects.get_or_create(begin=p[0],end=p[1],io=u"I",protocole=u"T",port_list=new_port_list)
             for p in pto:
                 if len(p)==1:
                     port = OuverturePort.objects.get_or_create(begin=p[0],end=p[0],io=u"O",protocole=u"T",port_list=new_port_list)
                 elif len(p)==2:
                     port = OuverturePort.objects.get_or_create(begin=p[0],end=p[1],io=u"O",protocole=u"T",port_list=new_port_list)
             for p in pui:
                 if len(p)==1:
                     port = OuverturePort.objects.get_or_create(begin=p[0],end=p[0],io=u"I",protocole=u"U",port_list=new_port_list)
                 elif len(p)==2:
                     port = OuverturePort.objects.get_or_create(begin=p[0],end=p[1],io=u"I",protocole=u"U",port_list=new_port_list)
             for p in puo:
                 if len(p)==1:
                     port = OuverturePort.objects.get_or_create(begin=p[0],end=p[0],io=u"O",protocole=u"U",port_list=new_port_list)
                 elif len(p)==2:
                     port = OuverturePort.objects.get_or_create(begin=p[0],end=p[1],io=u"O",protocole=u"U",port_list=new_port_list)

             interface.port_lists.clear()
             interface.port_lists.add(new_port_list)
             interface.save()
     	 else:
   	     leftovers.append(m)

from users.models import User, Ban

### Import des blacklistes users
adh = ldap.search('(&(blacklist=*)(aid=*))', sizelimit=10000)
from users.models import Ban
from django.utils.timezone import now, get_current_timezone, make_aware
import datetime
for ad in adh:
    for b in ad['blacklist']:
        ban = Ban()
        ban.user = User.objects.get(rezo_rez_uid=ad['aid'][0].value)
        try:
	        ban.raison = b['comm'].encode() + u' ' + b['type'].encode()
   	except:
 		ban.raison = "archived"
        ban.date_start =  make_aware(datetime.datetime.fromtimestamp(b['debut']), get_current_timezone(), is_dst=False)
        if b['fin']!='-':
            ban.date_end = make_aware(datetime.datetime.fromtimestamp(b['fin']), get_current_timezone(), is_dst=False)
        else:
            ban.date_end = now()
        ban.save()
        ban.date_start =  make_aware(datetime.datetime.fromtimestamp(b['debut']), get_current_timezone(), is_dst=False)
        ban.save()

### import historiques
from dateutil import parser
from django.utils.timezone import get_current_timezone, make_aware
adh = ldap.search('aid=*', sizelimit=10000)
c =0.
l = len(adh)
for rank,ad in enumerate(adh):
    print c/l
    c += 1
    u = Adherent.objects.get(id=ad['aid'][0].value)
    for histo in ad['historique']:
        date_hist = histo.split(',')[0]
        every_else = "".join(histo.split(',')[1:])
        user = every_else.split(":")[0].replace(" ", "")
        user_query = User.objects.filter(pseudo=user)
        comment = "".join(every_else.split(":")[1:])
        with transaction.atomic(), reversion.create_revision():
            u.save()
            if user_query:
                reversion.set_user(user_query.first())
            reversion.set_comment(comment)
            format_date = parser.parse(date_hist, dayfirst=True)
            reversion.set_date_created(make_aware(format_date, get_current_timezone(), is_dst=False))

machines = ldap.search(u'mid=*', sizelimit=20000)
from dateutil import parser
from django.utils.timezone import get_current_timezone, make_aware

c = 0.
l = len(machines)
for num, machine in enumerate(machines):
    if (c % 50) == 0:
        print c/l
    c += 1
    name=machine['host'][0].split('.')[0]
    id =  machine.proprio().get('aid', [None])[0]
    if machine.proprio().__unicode__()==u'Le Crans':
        user = User.objects.get(pseudo='crans')
    elif not id:
        # Cas des clubs
        id = machine.proprio().get('cid', [None])[0]
        user = User.objects.get(rezo_rez_uid=id + 20000)
        if not id:
            print("erreur !")
            continue
    else:
        user = User.objects.get(rezo_rez_uid=id)

    i = Interface.objects.filter(domain__name=name, machine__user=user)
    if i:
        i = i[0]
        for histo in machine['historique']:
            date_hist = histo.split(',')[0]
            every_else = "".join(histo.split(',')[1:])
            user = every_else.split(":")[0].replace(" ", "")
            user_query = User.objects.filter(pseudo=user)
            comment = "".join(every_else.split(":")[1:])
            with transaction.atomic(), reversion.create_revision():
                i.save()
                if user_query:
                    reversion.set_user(user_query.first())
                reversion.set_comment(comment)
                format_date = parser.parse(date_hist, dayfirst=True)
                reversion.set_date_created(make_aware(format_date, get_current_timezone(), is_dst=False))






#!/bin/bash
# Monte toutes les partitions, et recompte les quotas

# Fonction de capture adaptée pour permettre un suivi du type de signal reçu
trap2() {
    # Don't need func anymore
    func="$1"; shift

    # Raccourci de "for sig in $@"
    for sig; do
        trap "$func $sig" "$sig"
    done
}

sigint(){
    SIG=$1
    echo -e '\n\n'
    echo "${SIG} reçu, on quitte.";
    exit 0;
}

trap2 'sigint' SIGINT SIGTERM

confirm(){
    while true; do
        read -p "Exécuter [1;31m$*[0m ? [o/n]" yn;

        case $yn in
            [YyOo]* )
                "$@";
                break;;
            [Nn]* )
                echo "Ok, donc je ne le fais pas.";
                break;;
            *)
                echo "Répondre Oui ou non";;
        esac
    done
}

cat << EOF
[1;32mCe programme va recompter les quotas et remonter les disques. Les deux premières commandes
sont facultatives si le boot s'est bien passé.[0m

[1;31mL'appel à quotacheck remonte les disques en lecture seule pendant le comptage, évitez donc
d'utiliser ce programme en production, et préférez-le pour une maintenance au boot du serveur
avant de repartager les homes en NFS.[0m

Si vous avez besoin de recompter les quotas en live, faites l'enchaînement suivant (dans bash,
en tant que root)
# for i in {a..z}; do quotaoff -u /home-adh/\${i} && quotacheck -uvm /home-adh/\${i} && quotaon -u /home-adh/\${i}; done
# for i in logs mail; do quotaoff -u /home-adh/\${i} && quotacheck -uvm /home-adh/\${i} && quotaon -u /home-adh/\${i}; done

Ajoutez c aux options de quotacheck si vous voulez recréer les fichiers de quotas intégralement.
Cela imposera de remettre les quotas aux utilisateurs ensuite.

EOF

confirm /usr/scripts/gestion/iscsi/update.sh
confirm mount -a

echo "Désactivation des quotas"
quotaoff -aug

echo -n "Recréer les fichiers de quota intégralement ? (si non, quotacheck repartira des précédents, ce qui est suffisant. Si vous avez un doute, mettez N) [y/N]"
read answer
if [ ! "${answer}!" = "y!" ]; then
    CBIT=''
else
    CBIT='c'
    echo "Pensez à faire les setquota qui vont bien après la régénération."
fi

echo -n "Remonter les partitions en lecture seule ? (dans le doute, non si on est en prod) [y/N]"
read answer
if [ ! "${answer}!" = "y!" ]; then
    MBIT='m'
else
    MBIT=''
fi

confirm quotacheck -vu${CBIT}${MBIT} -a
quotaon -aug
service quotarpc restart

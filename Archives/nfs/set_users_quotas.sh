#!/bin/bash
# Permet de réinitialiser les quotas utilisateurs
#
# Les configs spécifiques pour les utilisateurs ayant demandé du rab sont ici aussi.
# Faudrait bcfg2-iser éventuellement, mais perso j'ai la FLEMME.
#
# PEB

SIZE_SOFT=10000000
SIZE_HARD=12000000
INODES_SOFT=0
INODES_HARD=0

# Jours
DELAY=31
DELAYSEC=$(( ${DELAY}*86400 ))

function do_quotas {
    USER=$1
    SSOFT=$2
    SHARD=$3
    ISOFT=$4
    IHARD=$5
    WHICH=$6
    echo "J'installe les quotas pour ${USER}…"

    setquota ${USER} ${SSOFT} ${SHARD} ${ISOFT} ${IHARD} ${WHICH}
}

function apply_quotas {
    cd /home/mail
    ls -Alh|tail -n +2|awk '{if($3 != $3+0 && $3 == $9) {print $3}}'|while read user; do
        do_quotas $user ${SIZE_SOFT} ${SIZE_HARD} ${INODES_SOFT} ${INODES_HARD} -a
    done;

    # Config pour les clubs/users spéciaux
    do_quotas "root" 0 0 0 0 -a
    do_quotas "club-salsa" 20000000 25000000 0 0 /home/c

    echo "J'ai fini"
    cd -
}

# Fonction de capture adaptée pour permettre un suivi du type de signal reçu
trap2() {
    # Don't need func anymore
    func="$1"; shift

    # Raccourci de "for sig in $@"
    for sig; do
        trap "$func $sig" "$sig"
    done
}

sigint(){
    SIG=$1
    echo -e '\n\n'
    echo "${SIG} reçu, on quitte.";
    exit 0;
}

trap2 'sigint' SIGINT SIGTERM

confirm(){
    while true; do
        read -p "Exécuter [1;31m$*[0m ? [o/n]" yn;

        case $yn in
            [YyOo]* )
                "$@";
                break;;
            [Nn]* )
                echo "Ok, donc je ne le fais pas.";
                break;;
            *)
                echo "Répondre Oui ou non";;
        esac
    done
}

cat << EOF
Ce programme va réinitialiser tous les quotas utilisateurs à une valeur standardisée.
Il est un peu verbeux et dit à quel utilisateur il en est.

EOF

echo "apply_quotas est une fonction qui affecte les quotas à chaque utilisateur."
confirm apply_quotas
echo "La commande suivante permet de pousser les délais pour le dépassement soft à ${DELAY} jours"
confirm setquota -t ${DELAYSEC} ${DELAYSEC} -a

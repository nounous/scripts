#!/usr/scripts/python.sh
#-*- coding: utf-8 -*-

"""
    Plugin Nagios récupérant l'état de certaines ressources de l'association
"""

from __future__ import print_function, unicode_literals

import os, sys
from argparse import ArgumentParser
from collections import OrderedDict

from lc_ldap import shortcuts, ldap as base_ldap
from gestion.config import NETs, rid_primaires

import nagiosplugin

# Définition des arguments que mange le script
parser = ArgumentParser(description=__doc__)
parser.add_argument('-B', '--buildings', action='store_true', help="Statistiques sur les bâtiments")
parser.add_argument('-N', '--networks', action='store_true', help="Statistiques sur les réseaux IP du Crans")

if __name__ == "__main__":
    args = parser.parse_args()

# Connexion à la base LDAP
ldap = shortcuts.lc_ldap_readonly()

# Liste des bâtiments à regarder
BATS = [
    "A",    # Bâtiment A
    "B",    # Bâtiment B
    "C",    # Bâtiment C
    "G",    # Bâtiment G
    "H",    # Bâtiment H
    "I",    # Bâtiment I
    "J",    # Bâtiment J
    "M",    # Bâtiment M
    "P",    # Pavillon des Jardins
]

# Plages de RIDs utilisées pour compter les attributions d'IPs
RIDS = OrderedDict([
    (name, rid_primaires[name]) for name in rid_primaires if name in NETs and
        name != 'multicast'
])

def _count_segment(first, last):
    """
        Compte le nombre de RIDs entre first et last (inclus) dans la base LDAP.
    """
    return len(
            ldap.search_ext_s('ou=data,dc=crans,dc=org',
                scope=base_ldap.SCOPE_SUBTREE,
                filterstr='(&(rid>=%d)(rid<=%d))' % (first, last),
                attrsonly=0, attrlist=[b'rid']
            )
        )

def _count_rid(plages):
    """
        Compte le nombre de RIDs d'une liste de plages, dans la base LDAP
    """
    return sum(_count_segment(first, last) for (first, last) in plages)

def _total_rid(plages):
    """
        Total théorique du nombre de RIDs, dans une liste de plages, sans les
        éventuels .0 et .255 (si IPv4 dispo)
    """
    tot = 0
    for (first, last) in plages:
        tot += last - first + 1
        # En vrai, il faut retirer les rids d'IPs en .255 ou .0,
        # si ipv4 publique (bit 15 pas à 1) ou privée dispos (bit 14 pas à 0).
        if first >> 14 != 0b10:
            for xxx in [0, 255]:
                # On calcule l'indice de la première et dernière IP en .xxx
                # dans l'intervalle [first, last], à coup de division entière
                jlast = (last-xxx)/256  # arrondi inf
                jfirst = -((-first+xxx)/256) #arrondi sup
                # Et donc on retire le nombre d'ip finissant en .xxx dans
                # l'intervale
                tot -= jlast - jfirst + 1

    return tot

class Association(nagiosplugin.Resource):
    """
        Ressource représentant les ressources de l'association
    """
    def __init__(self, buildings, networks):
        self.buildings = buildings
        self.networks = networks

    def get_buildings(self):
        """
            Compte le nombre d'adhérent par bâtiment
        """
        bats_stats = OrderedDict()
        for letter in BATS:
            bats_stats[letter] = len(ldap.search(u"chbre=%s*" % letter))

        return bats_stats

    def get_networks(self):
        """
            Calcule les proportions d'IP attribuées/restantes.
            Basé sur le plugin Munin stats-ip.
        """
        nets_stats = OrderedDict()

        for subnet, plages in RIDS.items():
            nets_stats[subnet] = (_count_rid(plages)*100)/_total_rid(plages)

        return nets_stats

    def probe(self):
        probes = OrderedDict()

        if self.buildings:
            for bat, nb_adh in self.get_buildings().items():
                yield nagiosplugin.Metric("Batiment %s" % bat,
                        nb_adh,
                        uom=None,
                        min=0,
                        max=None,
                        context="nb_adh"
                    )

        if self.networks:
            for subnet, value in self.get_networks().items():
                yield nagiosplugin.Metric("Reseau %s" % subnet,
                        value,
                        uom='%',
                        min=0,
                        max=100,
                        context="subnets"
                    )

@nagiosplugin.guarded
def main():
    check = nagiosplugin.Check(
        Association(args.buildings, args.networks),
        nagiosplugin.ScalarContext('nb_adh', '1000', '2000'),
        nagiosplugin.ScalarContext('subnets', '90', '95'),
    )

    check.main()

if __name__ == '__main__':
    main()

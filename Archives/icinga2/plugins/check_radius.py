#!/usr/scripts/python.sh
# -*- coding: utf-8 -*-

"""
    Vérification de la disponibilité d'un serveur RADIUS pour l'authentification
"""

from __future__ import unicode_literals

import sys
import hashlib
from argparse import ArgumentParser

import pyrad.packet
from pyrad.client import Client, Timeout
from pyrad.dictionary import Dictionary

import nagiosplugin
from nagiosplugin.state import Ok, Warn, Critical, Unknown

from gestion import secrets_new

class Radius(nagiosplugin.Resource):
    """
        Essaye une authentification RADIUS en filaire pour vérifier que
        FreeRADIUS continue à tourner correctement
    """
    def __init__(self, hostname, nas_id, user_name, calling_station_id, nas_port_type, nas_port, bind_to):
        self.hostname = hostname
        self.nas_id = nas_id
        self.user_name = user_name
        self.calling_station_id = calling_station_id
        self.nas_port_type = nas_port_type
        self.nas_port = nas_port
        self.bind_to = (bind_to, 0)
        self.__server = None

    def initialize_client(self):
        self.__server = Client(
            server=self.hostname,
            secret=secrets_new.get('radius_key').encode('utf-8'),
            dict=Dictionary("/usr/scripts/icinga2/utils/dictionary.pyrad"),
        )
        self.__server.bind(self.bind_to)

    def create_request(self):
        return self.__server.CreateAuthPacket(
            code=pyrad.packet.AccessRequest,
            NAS_Identifier=self.nas_id,
            User_Name=self.user_name,
            Calling_Station_Id=self.calling_station_id,
            NAS_Port=self.nas_port,
            NAS_Port_Type=self.nas_port_type,
        )

    def use_chap(self, request, passwd):
        request.id = request.CreateID()
        request.authenticator = request.CreateAuthenticator()

        bytes_passwd = passwd.strip().encode('utf-8')

        hashed_pass = hashlib.md5(b'%s%s%s' % (chr(request.id), bytes_passwd, request.authenticator)).digest()
        chap_passwd = chr(request.id) + hashed_pass

        request[b'CHAP-Challenge'] = request.authenticator
        request[b'CHAP-Password'] = chap_passwd

    def send(self, request):
        try:
            return self.__server.SendPacket(request).code
        except Timeout:
            return 255

    def probe(self):
        self.initialize_client()
        request = self.create_request()
        self.use_chap(request, self.user_name)
        response = self.send(request)
        return nagiosplugin.Metric('radius', response, context='radius')

class RadiusContext(nagiosplugin.Context):
    """
        Contexte pour l'analyse des réponses aux requêtes RADIUS
    """
    radius_ok_response = {2}
    radius_warning_response = {1, 4, 11}
    radius_critical_response = {3}
    radius_retcode = {
        1   :   'Access-Request',
        2   :   'Access-Accept',
        3   :   'Access-Reject',
        4   :   'Accounting-Request',
        5   :   'Accounting-Response',
        11  :   'Access-Challenge',
        12  :   'Status-Server',
        13  :   'Status-Client',
        255 :   'Not Used (Reserved)',
    }

    def describe(self, metric):
        if metric.value in self.radius_retcode.keys():
            return ("Reponse du serveur : %s" % self.radius_retcode[metric.value])
        else:
            return ("Reponse du serveur non reconnue : %s" % metric.value)

    def evaluate(self, metric, resource):
        value = metric.value
        if value in self.radius_ok_response:
            return nagiosplugin.Result(Ok, metric=metric)
        elif value in self.radius_warning_response:
            return nagiosplugin.Result(Warn, metric=metric)
        elif value in self.radius_critical_response:
            return nagiosplugin.Result(Critical, metric=metric)
        else:
            return nagiosplugin.Result(Unknown, metric=metric)

parser = ArgumentParser(description=__doc__)
parser.add_argument('-H', '--hostname', type=unicode, required=True, help="Adresse IP ou FQDN du serveur RADIUS")
parser.add_argument('-i', '--nas-identifier', type=unicode, required=True, help="Nom du NAS initiant la connexion (NAS-Identifier)")
parser.add_argument('-u', '--user-name', type=unicode, required=True, help="Nom d'utilisateur pour l'authentification (User-Name)")
parser.add_argument('-s', '--calling-station-id', type=unicode, required=True, help="Adresse MAC de l'utilisateur final (Calling-Station-Id)")
parser.add_argument('-t', '--nas-port-type', type=unicode, required=True, help="Type de connexion (Ethernet/Wireless/...) (NAS-Port-Type)")
parser.add_argument('-p', '--nas-port', type=int, required=True, help="Port pour lequel l'authentification sera demandée (NAS-Port)")
parser.add_argument('-b', '--bind-to', type=unicode, default="127.0.1.0", help="Adresse depuis laquelle sera envoyée la requête")

args = parser.parse_args()

@nagiosplugin.guarded
def main():
    check = nagiosplugin.Check(
        Radius(
            args.hostname,
            args.nas_identifier,
            args.user_name,
            args.calling_station_id,
            args.nas_port_type,
            args.nas_port,
            args.bind_to,
        ),
        RadiusContext('radius'),
    )
    check.main()

if __name__ == '__main__':
    main()

#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8; mode: python -*-
"""Permet de construire un CSV contenant l'ensemble des contributions
financières des adhérents sur une année civile."""

COMPTE_MEMBRES = "410 Membres"
COMPTE_COTISATIONS = "756 Cotisation"
# Impressions, accès internet
COMPTE_PRESTATIONS = "7061 Contribution accès internet"
COMPTE_SOLDE = "7062 Crédit solde"

FIELD_SEP = ','

CHAMPS = [
    'date',
    'description',
    'amount',
    'account1',
    'account2'
]

import argparse
import re
import sys

from lc_ldap import shortcuts, objets

def collect_adh_cai(annee):
    """Récupère les données pour l'année fournie"""

    out = []

    conn = shortcuts.lc_ldap_readonly()

    __filtre = u"(&(fid=*)(recuPaiement>=%(annee)s0101000000+0100)(recuPaiement<=%(annee)s1231235959+0100)(!(controle=FALSE)))" % {
        'annee': annee,
    }

    factures = conn.search(
        __filtre,
        sizelimit=0,
    )

    for facture in factures:
        date = facture['recuPaiement'][0].value.strftime("%d/%m/%Y")
        _proprio = facture.proprio()
        description = "%s %s" % (
            _proprio['prenom'][0] if isinstance(_proprio, objets.adherent) else "Club",
            _proprio['nom'][0] if isinstance(_proprio, (objets.adherent, objets.club)) else "Le Crans",
        )
        for article in facture['article']:
            if not u"CAI" in article["code"] and not "ADH" in article["code"] or float(article['pu']) == 0.0:
                continue
            category = COMPTE_COTISATIONS if article['code'] == u'ADH' else COMPTE_PRESTATIONS
            memo = 'Adhésion' if article['code'] == u'ADH' else 'Contribution accès internet'
            tn = str(article['pu'])

            out.append({
                'date': date,
                'description': "%s de %s" % (memo, description,),
                'amount': tn,
                'account1': COMPTE_MEMBRES,
                'account2': category,
            })

    with open("/usr/scripts/var/compta_adh_cai.csv", 'w') as csv_file:
        csv_file.write(FIELD_SEP.join(CHAMPS) + "\n")
        for entry in out:
            csv_file.write(
                "%s\n" % (
                    FIELD_SEP.join(
                        [
                            entry[champ]
                            for champ in CHAMPS
                        ],
                    ),
                ),
            )

def collect_solde(annee):
    """Récupère les updates de solde (entrée d'argent réelle"""

    conn = shortcuts.lc_ldap_readonly()

    histo_solde_re = re.compile(
        r'(?P<date>[0-9]{2}/[0-9]{2}/%s).*credit (?P<montant>[0-9]+.[0.*-9]{1,2}) Euros (?P<raison>\[.*)' % (annee,),
        re.IGNORECASE,
    )

    # Récupère les adhérents dans un tableau.
    adherents = conn.search(u'(&(|(aid=*)(cid=*))(uid=*))', sizelimit=0)

    with open('/usr/scripts/var/compta_solde.csv', 'w') as solde_file:
        solde_file.write(FIELD_SEP.join(CHAMPS) + '\n')
        for adherent in adherents:
            for histo_line in adherent['historique']:
                z = histo_solde_re.match(unicode(histo_line))
                if z is not None:
                    if "rembo" in z.groupdict()['raison'].lower() or u'raté' in z.groupdict()['raison'].lower() or 'rate' in z.groupdict()['raison'].lower():
                        continue
                    date = z.groupdict()['date']

                    montant = "%.2f" % (
                        float(z.groupdict()['montant']),
                    )

                    entry = {
                        'date': str(date),
                        'description': str(
                            "Crédit solde de %s %s" % (
                                adherent['prenom'][0] if isinstance(adherent, objets.adherent) else "Club",
                                adherent['nom'][0],
                            ),
                        ),
                        'amount': str(montant),
                        'account1': COMPTE_MEMBRES,
                        'account2': COMPTE_SOLDE,
                    }

                    solde_file.write(
                        FIELD_SEP.join(
                            [
                                entry[champ]
                                for champ in CHAMPS
                            ]
                        ) + '\n'
                    )


if __name__ == '__main__':
    PARSER = argparse.ArgumentParser(description="Production d'un CSV à partir des factures.", add_help=False)
    PARSER.add_argument('-c', '--cotis', action='store_true', help='Rapport des cotis/cai')
    PARSER.add_argument('-h', '--help', action='store_true', help='Affiche cette aide et quitte.')
    PARSER.add_argument('-s', '--solde', action='store_true', help='Uniquement le rapport des soldes')
    PARSER.add_argument('-t', '--tout', action='store_true', help='Tout faire')
    PARSER.add_argument('annee', type=str, nargs=1, help="L'année pour laquelle le CSV est à produire.")

    ARGS = PARSER.parse_args()

    if ARGS.help:
        PARSER.print_help()
        sys.exit(0)

    if ARGS.tout:
        ARGS.solde = True
        ARGS.cotis = True

    if ARGS.solde:
        collect_solde(ARGS.annee[0])

    if ARGS.cotis:
        collect_adh_cai(ARGS.annee[0])
